<!doctype html>
<html lang="<?php echo e(app()->getLocale()); ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" id="site_name" data-site_name="<?php echo e(url('/')); ?>" />
        <title><?php echo config('define.title_name'); ?></title>
        <!-- Themes -->
		<link rel="shortcut icon" type="image/png" href="/favicon.png"/>
		<link rel="shortcut icon" type="image/png" href="images/icons/notif.png"/>

		<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/blue-theme.css')); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/pink-theme.css')); ?>">
		<!-- Fonts -->
		<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/font.css')); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/font-awesome.min.css')); ?>">
		<!-- Jquery UI -->
		<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/jquery-ui.css')); ?>">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="<?php echo e(asset('css/bootstrap.min.css')); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/datatable.min.css')); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/new_layout.css?version=2')); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo e(asset('enjoyhint/enjoyhint.css?version=0.1')); ?>">
		<!--[if !IE]><!-->
		<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/unsupported_new.css?version=1')); ?>">
		<!--<![endif]-->
	    <!--[if IE]>
		<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/unsupported_old.css?version=1')); ?>">
		<![endif]-->
		<link rel="icon" href="" type="image/x-icon">
		<!-- full calendar css -->
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.css">
    </head>
    <!-- GET THE COLOR THEME OF THE USER -->
    <?php 
    	$color = DB::table('users as u')
    	->join('theme_color as t', 't.theme_color_id', '=', 'u.theme_color_id')
    	->leftJoin("profile_image as pi" , function($q) {
            $q->on('u.id' ,  '=' , 'pi.user_id')
                ->where('pi.status' , null)
                ->where('pi.is_profile', 1);
            })
    	->where("u.id" , Auth::user()->id)->get()->first();	    	
		
		$color->image = $color->image == null ? url('/').'/public/images/icons/default-user.png' : url('/').'/public/profile/'.Auth::user()->id.'/thumbnail_profile/'.$color->image;

    	############ FOR BUSINESSS ################

    	# GET THE STORAGE USED 
    	$storage_used =  DB::table('storage as s')
			            ->select('*')
			          //	 ->where('status' , '=' , 'A')
			            ->where('user_id' , Auth::user()->parent_business_owner_id  )
			            ->sum('storage');
		
	    # GET THE SUBSCRIPTION
	    $subscription = DB::table('user_subscription')
	            ->select('*')
	            ->where('user_id' , '=' , Auth::user()->parent_business_owner_id )
	          //  ->where('subscription_status' , '=' , 'A')
	            ->sum('amount'); 


	    ####### END BUSINESS STORAGE AND SUBSCRIPTION ############
	            	            
		# GET NOTIFICATION
		$notification = DB::table('task_notification as tc')
	            ->select('*' , 'u2.firstname as notif_first_name' , 'u2.lastname as notif_last_name')
	            ->join('users as u ' , 'id' , '=' , 'tc.user_id')
	            ->join('tasks as t' , 't.task_id' , '=' , 'tc.task_id')
	            ->join('users as u2' , 'u2.id' , '=' , 'tc.notif_by')
	            ->where('user_id' , Auth::user()->id  )
	            ->where('seen' , 0)
	            ->get(); 

    ?>

    <body class="<?php echo e($color->color); ?>-theme body flex col" data-log_id="<?php echo e(Auth::user()->id); ?>" data-path="<?php echo e(app_path()); ?>" data-fullname="<?php echo e(Auth::user()->firstname); ?> <?php echo e(Auth::user()->lastname); ?>">
    	<div class="unsupported-browser">
    		<img src="<?php echo e(asset('images/final/unsupported.png')); ?>">
    		<h3>Unsupported Browser</h3>
    		<p>You're using a browser we don't support yet. We only support the recent versions of major browsers like Chrome, Firefox, Safari and Edge.</p>
    	</div>
    	<input type="hidden" id="timezone">
    	<!-- Main Header -->
    	<div class="main-header">
	    	<nav class="flex middle space-bet <?php echo e($current_user->color); ?>-bg-primary">
	    		<div class="left">
					<a href="javascript:void(0);" class="mobile-menu" data-toggle="modal" data-target="#sideNav">
						<img src="<?php echo e(asset('images/final/m-menu.png')); ?>">
					</a>
					<a href="<?php echo e(url('task')); ?>" class="nav-logo">
						<img src="<?php echo e(asset('images/final/task-logo.png')); ?>">
					</a>
	    		</div>
				<p class="project-title">Task Banana</p>
				<div class="right">
					<!-- Create New Account -->
					<?php if(Auth::user()->created_by != null): ?>
					<a href="<?php echo e(url('sign_up')); ?>" class="nav-menu create-account">Create My Own Account</a>
					<!-- end -->
					<?php endif; ?>
					<!-- Archive Menu -->
					<a href="<?php echo e(url('archive')); ?>" class="nav-menu nav-archive"><img src="<?php echo e(asset('images/final/archive.png')); ?>"></a>
					<!-- End Archive Menu -->

					<!-- Calendar Menu -->
					<a href="javascript:void(0);" class="nav-menu nav-calendar" id="view_calendar" onclick="view_calendar();"><img src="<?php echo e(asset('images/final/Calendar.png')); ?>"></a>
					<!-- End Calendar Menu -->

					<!-- Accept Members Menu -->
					<?php if(Auth::user()->parent_business_owner_id  == Auth::user()->id ): ?>
					<div class="btn-group accept-menu">
						<a href="javascript:void(0);" onclick="accept_member()" class="nav-menu nav-accept dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<img class="img-desktop" src="images/final/accept-members.png"><img class="img-mobile" src="images/final/m-accept-members.png"><span class="badge accept accept-numbers"><?php echo e($new_members); ?></span></a>
						<div class="dropdown-menu dropdown-menu-right">
							<ul class="scrollbar" id="accept_member_lists">
							</ul>
						</div>
					</div>
					<!-- <a class="green btn-inset" id="add_new_member" href="javascript:void(0);" onclick="accept_member()">
						<span>Accept New Members</span>
					</a> -->
					<?php endif; ?>
					<!-- End Accept Members Menu -->

					<!-- Notification Menu -->
					<div class="btn-group notif-menu">
						<a href="javascript:void(0);" class="nav-menu dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-desktop" src="<?php echo e(asset('images/final/notif.png')); ?>"><img class="img-mobile" src="<?php echo e(asset('images/final/m-notif.png')); ?>"><span class="badge notif" id="count_notif_div"><?php echo e($notification->count()); ?></span></a>
						<div class="dropdown-menu dropdown-menu-right">
							<ul class="scrollbar" id="scrollbar_ul">
								<?php $__currentLoopData = $notification; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<?php if($data->action_type == 'T'): ?> 
									<li class="unread">
										<a href="<?php echo e(url('task')); ?>?t=<?php echo e($data->task_id); ?>" class="" data-task_id="<?php echo e($data->task_id); ?>" data-task_notification_id="<?php echo e($data->task_notification_id); ?>">
											<div class="notif-item flex">
												<img src="images/sample/user-pic-1.png">
												<div class="notif-info">
													<p class="friend-name"><?php echo e($data->notif_first_name); ?> <?php echo e($data->notif_last_name); ?></p>
													<p class="info">You Have a New Task</p>
												</div>
											</div>
										</a>
									</li>
									<?php else: ?>
									<li class="unread">
										<a href="<?php echo e(url('task')); ?>?c=<?php echo e($data->task_notification_id); ?>&t=<?php echo e($data->task_id); ?>" class="" data-task_id="<?php echo e($data->task_id); ?>" data-task_notification_id="<?php echo e($data->task_notification_id); ?>">
											<div class="notif-item flex">
												<img src="images/sample/user-pic-1.png">
												<div class="notif-info">
													<p class="friend-name"><?php echo e($data->notif_first_name); ?> <?php echo e($data->notif_last_name); ?></p>
													<p class="info">Commented on <strong><?php echo e($data->title); ?></strong> 	<br> (<span title="<?php echo e($data->comment); ?>"><?php echo e($data->comment); ?></span>) </p>
												</div>
											</div>
										</a>
									</li>
									<?php endif; ?>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</ul>
							<?php if($notification->count() > 0): ?>
							<ul id="ul_mark_all">
								<li class="mark-all">
									<a href="<?php echo e(url('task')); ?>?t=<?php echo e($data->task_id); ?>" onclick ="mark_all_as_read(<?php echo e(Auth::user()->id); ?>)">
										<div class="notif-item">
											<p>Mark all as read</p>
										</div>
									</a>
								</li>
							</ul>
							<?php else: ?>
							<ul id="ul_no_notif">
								<li class="no-comments">
									<a href="<?php echo e(url('task')); ?>">
										<div class="notif-item flex">
											<p>No available notification</p>
										</div>
									</a>
								</li>
							</ul>
							<?php endif; ?>
						</div>
					</div>
					<!-- End Notification Menu -->

					<!-- Settings Menu -->
					<!-- <a href="javascript:void(0);" class="nav-menu"><img src="<?php echo e(asset('images/icons/settings.png')); ?>"></a> -->
					<!-- End Settings Menu -->

					<!-- Profile Menu -->
					<div class="btn-group profile-menu">
						<a href="<?php echo e(url('profile')); ?>" class="nav-menu dropdown-toggle" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false"><img src="<?php echo e($color->image); ?>" class="profile-image">
						</a>
						<div class="dropdown-menu dropdown-menu-right profile pull-center">
							<div class="top">
								<h3 class="full-name"><?php echo e($User->firstname); ?> <?php echo e($User->lastname); ?> <span class="user-name">(<?php echo e($User->firstname); ?>)</span></h3>
								<p class="email"><?php echo e($User->email); ?></p>
								<br class="all">
								<div class="usage flex col">
									<p class="usage-label">Usages</p>
									<div class="progress-bar">
										<div class="progress">
										    <?php if($subscription == 0 ): ?>
										    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" style="width:0%">
										    </div>
										    <?php else: ?> 
										    <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo ($storage_used / 1000 / 1000) ?>" aria-valuemin="0" aria-valuemax="<?php echo $subscription ?>" style="width:<?php echo (($storage_used / 1000 / 1000)  / $subscription) * 100 ?>%">
										    </div>
										    <?php endif; ?>
										 </div>
									</div>
									<p class="usage-mb"><span><?php echo number_format( ( $storage_used / 1000 / 1000 ) ,2) ?></span> MB of <span><?php echo e($subscription); ?></span> MB used</p>
									<a href="<?php echo e(url('/payment')); ?>" class="upgrade-link">Upgrade Space</a>
								</div>
								<br>
						
							</div>
							<!-- <div class="dropdown-divider"></div> -->
							<div class="bottom flex col middle">
								<a href="<?php echo e(url('profile')); ?>">Profile</a>
								<div class="change-theme">
									<a href="javascript:void(0);" class="flex center middle">Change Theme Color <i class="change-color pink"></i></a>
									<?php $color = $Profile->theme->color; ?>
									<div class="color-pick hide">
										<form class="flex">
											<span>
												<input type="radio" id="color_pink" class="color-option pink" name="optradio" value="1" <?php echo $color == 'pink' ? 'checked' : '' ?>>
												<label for="color_pink"></label>
											</span>
											<span>
												<input type="radio" id="color_blue" class="color-option blue" name="optradio" value="2" <?php echo $color == 'blue' ? 'checked' : '' ?>>
												<label for="color_blue"></label>
											</span>
						                </form>
									</div>
								</div>
								<!-- <a href="javascript:void(0);" class="flex">Email Notification 
									<label class="switch">
										<input type="checkbox" class="switch-input" checked>
										<span class="switch-label" data-on="On" data-off="Off"></span>
										<span class="switch-handle"></span>
									</label>
								</a> -->
								<!-- <a href="javascript:void(0);">Help</a> -->
								<!-- <a href="javascript:void(0);" id="logout">Logout</a> -->
								<a href="<?php echo e(route('logout')); ?>" id="logout" 
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                    <?php echo e(csrf_field()); ?>

                                </form>

							</div>
						</div>
					</div>
					<!-- End Profile Menu -->
				</div>
			</nav>
		<?php echo $__env->yieldContent('content'); ?>
		<div class="bottom-nav middle space-even">
			<a href="javascript:void(mobile_view_member());" class="view-members"><img class="unactive-img" src="<?php echo e(asset('images/final/member-icon.png')); ?>"><img class="p-active-img" src="<?php echo e(asset('images/final/member-icon2.png')); ?>"><img class="b-active-img" src="<?php echo e(asset('images/final/member-icon3.png')); ?>"></a>
			<a href="javascript:void(0);" target="#project_list_panel" class="right-section"><img class="unactive-img" src="<?php echo e(asset('images/final/project-icon.png')); ?>"><img class="p-active-img" src="<?php echo e(asset('images/final/project-icon2.png')); ?>"><img class="b-active-img" src="<?php echo e(asset('images/final/project-icon3.png')); ?>"></a>
			<a href="<?php echo e(url('task')); ?>" class="active"><img class="unactive-img" src="<?php echo e(asset('images/final/dashboard-icon.png')); ?>"><img class="p-active-img" src="<?php echo e(asset('images/final/dashboard-icon2.png')); ?>"><img class="b-active-img" src="<?php echo e(asset('images/final/dashboard-icon3.png')); ?>"></a>
			<a href="javascript:void(0);" target="#my_todo_panel" class="right-section"><img class="unactive-img" src="<?php echo e(asset('images/final/task-list.png')); ?>"><img class="p-active-img" src="<?php echo e(asset('images/final/task-list2.png')); ?>"><img class="b-active-img" src="<?php echo e(asset('images/final/task-list3.png')); ?>"></a>
			<a href="javascript:void(0);" target="#reminders_panel" class="right-section"><img class="unactive-img" src="<?php echo e(asset('images/final/reminder-icon.png')); ?>"><img class="p-active-img" src="<?php echo e(asset('images/final/reminder-icon2.png')); ?>"><img class="b-active-img" src="<?php echo e(asset('images/final/reminder-icon3.png')); ?>"></a>
		</div>

		<footer>
			<p class="footer">&copy; <?php echo date("Y"); ?> Copyright. Powered by Trackerteer | All Rights Reserved.</p>
		</footer>		  	
        <!-- Latest compiled and minified -->
		<script type="text/javascript" src="<?php echo e(asset('js/jquery-2.2.4.min.js?version=2')); ?>"></script>
		<script type="text/javascript" src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
		<script type="text/javascript" src="<?php echo e(asset('js/datatable.min.js')); ?>"></script>
		<script type="text/javascript" src="<?php echo e(asset('js/jquery-ui.js')); ?>"></script>
		<script type="text/javascript" src="<?php echo e(asset('js/lottie.js')); ?>"></script>
		<script type="text/javascript" src="<?php echo e(asset('js/task.js')); ?>"></script>
		<script type="text/javascript" src="<?php echo e(asset('js/todo.js')); ?>"></script>
		<script type="text/javascript" src="<?php echo e(asset('js/payment_details.js')); ?>"></script>
		<script type="text/javascript" src="<?php echo e(asset('js/profile.js')); ?>"></script>
		<script type="text/javascript" src="<?php echo e(asset('js/js-notify.min.js')); ?>"></script>
		<script type="text/javascript" src="<?php echo e(asset('js/archive.js')); ?>"></script>
		<script type="text/javascript" src="<?php echo e(asset('enjoyhint/enjoyhint.js')); ?>"></script>
		<!-- <script type="text/javascript" src="<?php echo e(asset('js/tutorial.js')); ?>"></script> -->
		<!-- full calendar js -->
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone-with-data.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.4/jstz.min.js"></script>
		<script type="text/javascript">
			var tz = jstz.determine(); // Determines the time zone of the browser client
    		var timezone = tz.name(); //
    		$('#timezone').val(timezone);
    		$(document).ready(function(){
    			$forms = $('.form-control');
    			// function to remove autocomplete
    			for (i = 0; i < $forms.length; i++)
				{
				    field_type = $forms[i].type.toLowerCase();
				    switch (field_type)
				    {
					    case "password":
					        $forms[i].value = "";
					        break;
				  
				    }
				}
    		})
		</script>
    </body>
</html>
