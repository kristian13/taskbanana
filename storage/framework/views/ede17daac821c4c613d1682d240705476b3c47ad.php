<?php 
    # GET THE STORAGE USED 
    $storage_used =  DB::table('storage as s')
                    ->select('*')
                    ->join('users as u ' , 'id' , '=' , 'user_id')
                    ->where('parent_business_owner_id' , Auth::user()->parent_business_owner_id  )
                    ->sum('storage');
    # GET THE SUBSCRIPTION
    $subscription = DB::table('user_subscription')
            ->select('*')
            ->where('user_id' , '=' , Auth::user()->id )
            ->sum('amount'); 
?>

<?php $__env->startSection('content'); ?>
    </div>

    <?php
        $color = DB::table('users as u')
    	->join('theme_color as t', 't.theme_color_id', '=', 'u.theme_color_id')
    	->leftJoin("profile_image as pi" , function($q) {
            $q->on('u.id' ,  '=' , 'pi.user_id')
                ->where('pi.status' , null)
                ->where('pi.is_profile', 1);
            })
    	->where("u.id" , Auth::user()->id)->get()->first();	    	
		
		$color->image = $color->image == null ? url('/').'/public/images/icons/default-user.png' : url('/').'/public/profile/'.Auth::user()->id.'/thumbnail_profile/'.$color->image;

    ?>
    
    <!-- End Main Header -->
    <div class="main-container profile-page flex col">
        <div class="profile-head">
            <div class="max-container flex">
                <div class="top">
                    <a href="<?php echo e(url('/task')); ?>"><img src="<?php echo e(URL::asset('/public/images/final/profile-left-arrow.png')); ?>"></a>
                    <p class="page">Profile</p>
                </div>
                <div class="user-info flex middle">
                    <div class="user-image">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#profile_photo">
                            <img src="<?php echo e($color->image); ?>" alt="Avatar" class="avatar">
                            <span>Update</span>
                        </a>
                    </div>
                    <div class="user-details flex col center">
                        <p class="full-name"><?php echo e($User->firstname); ?> <?php echo e($User->lastname); ?></p>
                        <p class="email-add"><?php echo e($User->email); ?></p>
                    </div>
                </div>
                <div class="usage-info flex col">
                    <p class="usage-label text-center" >Usages</p>
                    <div class="progress-bar">
                        <div class="percentage" style="width: <?php echo ($storage_used / 1000 / 1000 );  ?>%;"></div>
                    </div>
                    <p class="usage-mb"><span><?php echo number_format( ( $storage_used / 1000 / 1000 ), 2) ?></span> MB of <span><?php echo e($subscription); ?></span> MB used</p>
                    <a href="<?php echo e(url('/payment')); ?>" class="upgrade">Upgrade Space</a>
                </div>
            </div>
        </div>
        <div class="filter-section profile-page">
            <div class="filter-body flex space-bet">
                <div class="filter-buttons max-container">
                    <!-- <button class="active">Member</button> -->
                    <!--  <a href="javascript:void(0);"><p>My Activities</p></a> -->
                          <a href="javascript:void(0);" class="active"><p>Profile</p></a>
                    <!--  <a href="javascript:void(0);"><p>Projects Created</p></a> -->
                    <a href="<?php echo e(url('/task')); ?>"><p>Back to Home</p></a>
                </div>
            </div>
        </div>
        <div class="max-container profile-page">
            <div class="personal-info">
                <p class="info-label">Personal Information</p>
                 <div id="message_profile">
                </div>
                <form method="post" action="<?php echo e(url('/profile/update')); ?>" id="personal_update" class="needs-validation" novalidate>
                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                    <input type="hidden" name="update" value="personal_update">
                    <div class="form-row">
                        <div class="form-group col-md-6 col-xs-12">
                            <label>First name</label>
                            <input type="text" class="form-control" name="firstname" placeholder="First name" value="<?php echo e($Profile->firstname); ?>" required>
                            <span class="invalid-feedback">Please input your first name.</span>
                        </div>
                        <div class="form-group col-md-6 col-xs-12">
                            <label>Last name</label>
                            <input type="text" class="form-control" name="lastname" placeholder="Last name" value="<?php echo e($Profile->lastname); ?>" required>
                            <span class="invalid-feedback">Please input yout last name.</span>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 col-xs-12">
                            <label>Address</label>
                            <input type="text" class="form-control" name="address" placeholder="Address" value="<?php echo e($Profile->address); ?>" required>
                            <span class="invalid-feedback">Please provide a valid address.</span>
                        </div>
                        <div class="form-group col-md-6 col-xs-12">
                            <label>City</label>
                            <input type="text" class="form-control" name="city" placeholder="City" value="<?php echo e($Profile->city); ?>" required>
                            <span class="invalid-feedback">Please provide a valid city.</span>
                        </div>
                        <div class="form-group col-md-6 col-xs-12">
                            <label>State</label>
                            <input type="text" class="form-control" name="state" placeholder="State" value="<?php echo e($Profile->state); ?>" required>
                            <span class="invalid-feedback">Please provide a valid state.</span>
                        </div>
                        <div class="form-group col-md-6 col-xs-12">
                            <label>Zipcode</label>
                            <input type="text" class="form-control" name="zipcode" placeholder="Zipcode" value="<?php echo e($Profile->zipcode); ?>" required>
                            <span class="invalid-feedback">Please provide a valid zip.</span>
                        </div>
                        <div class="form-group col-md-6 col-xs-12">
                            <label>Business Name</label>
                            <input type="text" class="form-control" name="business_name" placeholder="Business Name" value="<?php echo e($Profile->business_name); ?>" required>
                            <span class="invalid-feedback">Please provide a valid business name.</span>
                        </div>
                        <div class="form-group col-md-6 col-xs-12">
                            <label>Business Nature</label>
                            <select class="custom-select form-control" name="business_nature" required>
                                <option value="">Select Business Nature</option>
                                <?php $__currentLoopData = $Business_Nature_Parent; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <optgroup label="<?php echo e($parent->business_nature_parent_name); ?>">
                                        <?php $__currentLoopData = $parent->nature_child; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $select = $Profile->business_nature_child_id == $child->business_nature_child_id ? 'selected' : ''; ?>
                                            <option value="<?php echo e($child->business_nature_child_id); ?>" <?php echo e($select); ?>><?php echo e($child->business_nature_child_name); ?> </option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </optgroup>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                            <span class="invalid-feedback">Example invalid custom select feedback</span>
                        </div>
                    </div>
                    <br clear="all">
                    <button class="btn btn-save btn-inset" type="submit">Save <i class="fa fa-refresh fa-spin fa-1x fa-fw hide cinfo"></i></button>
                </form>
                <hr>
                <!-- Change Email -->
                <!--    <div class="dropdown change-email">
                        <a href="javascript:void(0);" class="nav-menu dropdown-toggle" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false"><p>Change Email Address</p>
                        </a>
                        <div class="dropdown-menu dropdown-menu-left">
                            <a href="javascript:void(0);" class="close">&#9587;</a>
                            <div class="dropdown-header">
                                Change Email Address
                            </div>
                            <div class="dropdown-body">
                                <form method="post" action="<?php echo e(url('profile/update')); ?>" id="email_update">
                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    <input type="hidden" name="update" value="email_update">
                                    <div class="form-group">
                                        <label>Current Email Address</label>
                                        <span><?php echo e($Profile->email); ?></span>
                                    </div>
                                    <div class="form-group">
                                        <label>New Email Address</label>
                                        <input type="email" class="form-control" name="email" value="" required>
                                    </div>
                                    <button type="submit" class="btn btn-save" disabled>Save <i class="fa fa-refresh fa-spin fa-1x fa-fw hide cemail"></i></button>
                                </form>
                            </div>
                        </div>
                    </div> -->
                <!-- End Change Email -->

                <!-- Change Password -->
                <div class="dropdown change-password">
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#profile_photo"><p>Change Profile Picture</p>
                    </a>
                </div>
                <!-- End Change Password -->

                <!-- Change Password -->
                <div class="dropdown change-password">
                    <a href="javascript:void(0);" class="nav-menu dropdown-toggle" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false"><p>Change Password</p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-left">
                        <a href="javascript:void(0);" class="close">&#9587;</a>
                        <div class="dropdown-header">
                            Change Password
                        </div>
                        <div id="change_pass_error"></div>
                        <div class="dropdown-body">
                            <form autocomplete="off">
                                <div class="form-group">
                                    <label>Old Password</label>
                                    <input type="password" class="form-control" name="old_password" value="" required autocomplete="nope" >
                                </div>
                                <div class="form-group">
                                    <label>New Password</label>
                                    <input type="password" class="form-control" name="new_password" value="" required autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label>Retype Password</label>
                                    <input type="password" class="form-control" name="retype_password" value="" required autocomplete="off">
                                </div>
                                <button type="submit" class="btn btn-save change_password">Save <i class="fa fa-refresh fa-spin fa-1x fa-fw hide cpword"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Change Password -->

                <!-- Change Language -->
                <div class="dropdown change-language">
                    <a href="javascript:void(0);" class="nav-menu dropdown-toggle" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false"><p>Change Language</p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-left">
                        <a href="javascript:void(0);" class="close">&#9587;</a>
                        <div class="dropdown-header">
                            Change Language 
                        </div>
                        <div class="dropdown-body">
                            <ul>
                                <li <?php if (Auth::user()->locale == 'en'): ?>
                                    class="active"
                                <?php endif ?> ><a href="<?php echo e(url('locale/en')); ?>" ><p><!-- <i class="fa fa-language"></i> &nbsp; -->English</p></a></li>
                                <li <?php if (Auth::user()->locale == 'ch'): ?>
                                    class="active" 
                                <?php endif ?> ><a href="<?php echo e(url('locale/ch')); ?>" ><p><!-- <i class="fa fa-language"></i> &nbsp; -->Chinese</p></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Change Language -->

                <!-- Change Timezone -->
                <div class="dropdown change-language">
                    <a href="javascript:void(0);" class="nav-menu dropdown-toggle" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false"><p>Change Timezone</p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-left">
                        <a href="javascript:void(0);" class="close">&#9587;</a>
                        <div class="dropdown-header">
                            Change Timezone
                        </div>
                        <div class="dropdown-body">
                            <div class="form-group">
                                <select class="form-control" id="profile_timezone">
                                <?php $__currentLoopData = $timezones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $timezone=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <?php if($timezone == Auth::user()->timezone ): ?>
                                   <option value="<?php echo e($timezone); ?>" selected="true"><?php echo e($value); ?></option>
                                  <?php else: ?>
                                    <option value="<?php echo e($timezone); ?>"><?php echo e($value); ?></option>
                                  <?php endif; ?>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <br>
                                <button type="submit" class="btn btn-save change_timezone" >Save <i class="fa fa-refresh fa-spin fa-1x fa-fw hide ctz"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Change Language -->

                <!-- Change Theme -->
                <div class="dropdown change-theme">
                    <a href="javascript:void(0);" class="nav-menu dropdown-toggle" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false"><p>Change Theme</p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-left">
                        <a href="javascript:void(0);" class="close">&#9587;</a>
                        <div class="dropdown-header">
                            Change Theme
                        </div>
                        <?php $color = $Profile->theme->color; ?>
                        <div class="dropdown-body">
                            <form class="flex space-even">
                                <span>
                                    <input type="radio" id="color_pink2" class="color-option pink" name="optradio" value="1" <?php echo $color == 'pink' ? 'checked' : '' ?>>
                                    <label for="color_pink2"></label>
                                </span>
                                <span>
                                    <input type="radio" id="color_blue2" class="color-option blue" name="optradio" value="2" <?php echo $color == 'blue' ? 'checked' : '' ?>>
                                    <label for="color_blue2"></label>
                                </span>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Change Theme -->

                <!-- Change Email Notification -->
               <!--  <div class="dropdown change-emailnotif">
                    <a href="javascript:void(0);" class="nav-menu dropdown-toggle" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false"><p>Change Email Notification</p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-left">
                        <a href="javascript:void(0);" class="close">&#9587;</a>
                        <div class="dropdown-header">
                            Change Email Notification
                        </div>
                        <div class="dropdown-body">
                            <p class="flex middle">Email Notification
                                <label class="switch">
                                    <input type="checkbox" class="switch-input" checked>
                                    <span class="switch-label" data-on="On" data-off="Off"></span>
                                    <span class="switch-handle"></span>
                                </label>
                            </p>
                        </div>
                    </div>
                </div> -->
                <!-- End Change Email Notification -->
            </div>
        </div>

        <div class="row hide">

            <div class="col-lg-3" im="image_list">
            </div>

            <div class="col-lg-6">
            
                <h4>Personal Information</h4>
                <div id="message">
                    
                </div>
                <!-- <form method="post" enctype="multipart/form-data" action="<?php echo e(url('/profile/update')); ?>">
                    <input type="hidden" name="update" id="upload_profilee" value="upload_profile">
                    <input type="file" name="images" id="image-upload" multiple>
                    <input type="submit" value="Upload Image" id="upload_profile" name="submit">
                </form> -->
                <br>
                <form method="post" action="<?php echo e(url('/profile/update')); ?>" id="personal_updatee">
                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                    <input type="hidden" name="update" value="personal_update">
                    <div class="col-lg-6">
                        <label>Full Name</label>
                        <input type="text" name="firstname" id="firstname" value="<?php echo e($Profile->firstname); ?>"></div>
                    <div class="col-lg-6">
                        <label>Last Name</label>
                        <input type="text" name="lastname" value="<?php echo e($Profile->lastname); ?>">
                    </div>
                    <div class="col-lg-6">
                        <label>Address</label>
                        <input type="text" name="address" value="<?php echo e($Profile->address); ?>">
                    </div>
                    <div class="col-lg-6">
                        <label>City</label>
                        <input type="text" name="city" value="<?php echo e($Profile->city); ?>">
                    </div>
                    <div class="col-lg-6">
                        <label>State</label>
                        <input type="text" name="state" value="<?php echo e($Profile->state); ?>">
                    </div>
                    <div class="col-lg-6">
                        <label>Zipcode</label>
                        <input type="text" name="zipcode" value="<?php echo e($Profile->zipcode); ?>">
                    </div>
                    <label>Business Name</label>
                    <input type="text" name="business_name" value="<?php echo e($Profile->business_name); ?>">
                    
                    <div class="form-group">
                        <label class="control-label">Business Nature</label>
                        <select name="business_nature" class="form-control " style="color: #5e5e5e;" required="">
                        <option value="" selected="">Select Business Nature</option>
                        <?php $__currentLoopData = $Business_Nature_Parent; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <optgroup label="<?php echo e($parent->business_nature_parent_name); ?>">
                                <?php $__currentLoopData = $parent->nature_child; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php $select = $Profile->business_nature_child_id == $child->business_nature_child_id ? 'selected' : ''; ?>
                                    <option value="<?php echo e($child->business_nature_child_id); ?>" <?php echo e($select); ?>><?php echo e($child->business_nature_child_name); ?> </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </optgroup>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                    <button type="submit" class="search-assignee blue-green">submit</button>
                </form>
                <br>
                <form method="post" action="<?php echo e(url('profile/update')); ?>" id="email_update">
                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                    <input type="hidden" name="update" value="email_update">
                    <label>Change Email</label>
                    <input type="text" name="email" value="<?php echo e($Profile->email); ?>"><br>
                    <button type="submit" class="search-assignee blue-green">submit</button>
                </form>
                <br>
               <!-- <form method="post" action="<?php echo e(url('profile/update')); ?>" id="lang_update">
                    <input type="hidden" name="update" value="lang_update">
                    <label>Change Language</label>
                    <input type="text" name="lang"><br>
                    <button type="submit" class="search-assignee blue-green">submit</button>
                </form> -->
                <br>

              
            </div>  
            <div class="col-lg-3"></div>
        </div>
    </div>

    <!-- Modal for Change Profile -->
    <div class="modal fade taskbanana" id="profile_photo" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&#9587;</span>
                        </button>
                        <h5 class="modal-title" id="">Profile Picture</h5>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="upload-box">
                                <div class="upload-btn">
                                    <input type="hidden" name="is_new" class="is_new"/>
                                    <input type="file" name="images" id="upload_input" multiple/>
                                </div>
                                <img src="#" id="upload_img" class="hide">
                            </div>
                        </div>
                        <br clear="all">
                        <div class="form-group">
                            <label>Suggested Photos</label>
                            <div class="photo-list clearfix">
                                <?php $__currentLoopData = $Profile->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="photo-item" data-id="<?php echo e($image->profile_image_id); ?>">
                                    <img src="public/profile/<?php echo e($image->user_id); ?>/<?php echo e($image->image); ?>">
                                    <a href="#" fi-func="delete" fi-name="avatar.png" class="img-delete flex middle center">
                                        <span>&#9587;</span>
                                    </a>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                              
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-save " id="upload_profile">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Modal for Change Profile 
    http://localhost/trackatask/public/profile/1/thumbnail_profile/06122019_5de9f351406fe_1572843381161.JPEG
    -->

    <?php echo $__env->make('modals.task-modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('theme.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>