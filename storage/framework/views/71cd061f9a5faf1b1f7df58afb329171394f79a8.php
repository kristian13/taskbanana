<?php $__env->startSection('content'); ?>
		<!-- <div class="project-list flex">
			<a href="<?php echo e(url('/task')); ?>" class="active"><p>All</p></a>
			<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<a href="<?php echo e(url('/task?isSearch=true&project-id=')); ?><?php echo e($value->category_id); ?>"><p><?php echo e($value->category_name); ?> </p></a>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			<a href="javascript:void(0);" class="add-project"><p>Add Project <span>&#9587;</span></p></a>
		</div> -->
		<div class="filter-section">
			<div class="filter-header flex space-bet <?php echo e($current_user->color); ?>-bg-secondary">
				<div class="left flex middle">
					<div class="btn-group card-menu project-btn">
						<button class="dropdown-toggle flex" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<?php if(!$categories->isEmpty()): ?>
							<p data-toggle="tooltip" data-placement="bottom" title="Rocky Superdog (Mobile Game) (30%)"><?php echo e($categories[0]->category_name); ?> <span></span></p><i class="fa fa-caret-down" aria-hidden="true"></i>
							<?php else: ?>
							<p data-toggle="tooltip" data-placement="bottom" >NO PROJECT ADDED<span></span></p>
							<?php endif; ?>
						</button>
						<div class="dropdown-menu dropdown-menu-right scrollbar">
							<ul>
								<?php if(!$categories->isEmpty()): ?>
									<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<li>
										<a href="<?php echo e(url('/task?p=')); ?><?php echo e($value->category_id); ?>"><p><?php echo e($value->category_name); ?> <span></span></p></a>
									</li>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>
							</ul>
						</div>
					</div>

					<div class="search-bar flex middle">
						<img src="<?php echo e(asset('images/final/search-icon.png')); ?>">
						<form action="" method="GET">
							<!-- <input type="text" name="" placeholder="Search Task Title" onkeyup="kmsearchtitle(this, '.list-card>.list-info>.title')"> -->
							<input type="text" class="search-input-old" name="s" placeholder="Search Task Title">
							<!-- <div class="btn-group card-menu filter-dropdown">
								<button class="dropdown-toggle flex" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									All <i class="fa fa-caret-down" aria-hidden="true"></i>
								</button>
								<input type="hidden" name="" class="search-option" value="">
								<div class="dropdown-menu dropdown-menu-right scrollbar">
									<ul>
										<li data-value="">
											<a href="javascript:void(0);">All</a>
										</li>
										<li data-value=".list-card>.list-info>h3.title">
											<a href="javascript:void(0);">Task Title</a>
										</li>
										<li data-value=".list-card>.list-info>.card-members>img">
											<a href="javascript:void(0);">Assignee</a>
										</li>
									</ul>
								</div>		
							</div> -->
							<input class="btn btn-sm search-btn <?php echo e($current_user->color); ?>-bg-primary" type="submit" value="Search">
							<a href="<?php echo e(url('task')); ?>" class="btn reset-btn">Reset</a>
						</form>
					</div>
					<!-- <a class="filter-btn" href="" data-toggle="modal" data-target="#filter_modal">
						<img src="<?php echo e(asset('images/final/filter2.png')); ?>">
						<span>Filter by</span>
					</a> -->
				</div>
				<div class="right flex">
					<a id="view_member" href="javascript:void(0);" onclick="view_member()">
						<img src="<?php echo e(asset('images/final/view-members.png')); ?>">
						<span><?php echo e(__('translation.view_member')); ?></span>
					</a>
					<a id="add_new_member" href="javascript:void(0);" onclick="add_new_member()">
						<img src="<?php echo e(asset('images/final/add-member.png')); ?>">
						<span><?php echo e(__('translation.add_member')); ?></span>
					</a>
					<!-- <a id="view_calendar" href="javascript:void(0);" onclick="view_calendar();">
						<img src="<?php echo e(asset('images/final/Calendar.png')); ?>">
						<span><?php echo e(__('translation.view_calendar')); ?></span>
					</a> -->
					<a id="add_task" href="javascript:void(0);">
						<img src="<?php echo e(asset('images/final/add-task.png')); ?>">
						<span><?php echo e(__('translation.add_task')); ?></span>
					</a>
					<a id="create_project" href="javascript:void(0);">
						<img src="<?php echo e(asset('images/final/add-task.png')); ?>">
						<span>Create a Project</span>
					</a>
				</div>
			</div>
			<!-- <div class="filter-body">
				<form  action="<?php echo e(url('/task')); ?>" method="GET" role="search">
					<input type="hidden" name="isSearch" value="true">
					<div class="filter-buttons">
						<a href="javascript:void(0);" class="active"><p>Business</p></a>
					</div>
					<div class="filter-by">
						<div class="filter-member">
							<div class="member-list" id="business">
								<div class="checkbox checkbox-gray checkbox-xs">
				                    <input id="1" class="checkAll styled" type="checkbox" checked>
				                    <label for="1">All Members</label>
				                </div>
								<?php $__currentLoopData = $get_my_business_team; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<div class="checkbox checkbox-gray checkbox-xs">
					                    <input id="<?php echo e($team->id); ?>" class=" styled" name = assignee[] type="checkbox" value="<?php echo e($team->id); ?>">
					                    <label for="<?php echo e($team->id); ?>"><?php echo e($team->firstname); ?> <?php echo e($team->lastname); ?></label>
					                </div>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</div>
							<button type="submit" class="search-assignee blue-green">Filter</button>
						</div>
					</div>
				</form>
			</div> -->
		</div>
	</div>
	<!-- End Main Header -->
	<div class="main-container flex">
		<?php if($errors->any()): ?>
        	<div class="alert alert-danger" role="alert"><?php echo e(implode('', $errors->all(':message'))); ?></div>
		<?php endif; ?>
		<div class="flex space-bet">
			<!-- Task Section -->
			<div class="task-container scrollbar">
				<div class="list-wrapper" id="list-cards">
					<div class="list-item pending-box" id="new_task" >
						<div class="list-container pink <?php echo e($current_user->color); ?>-bg-secondary">
							<div class="list-header flex middle space-bet"><h2><?php echo e(__('translation.new_task')); ?></h2><span><?php if( $get_new_task !== null ): ?> <?php echo e($get_new_task->total()); ?> <?php endif; ?></span></div>
							<div class="list-body scrollbar">
								<?php if(@isset($get_new_task)): ?>
									<?php $__currentLoopData = $get_new_task->items(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<div class="list-card clearfix <?php echo e($value->task_id); ?>" id="div<?php echo e($value->task_id); ?>" data-t="<?php echo e($value->task_unique); ?>">
											<div class="list-tools flex space-bet">
												<span class="project-name"><?php echo e($value->category_name); ?></span>
												<div class="flex">
													<!-- <?php if($value->task_creator == Auth::user()->id): ?>
													<span class="edit-task" onclick="edit_task(<?php echo e($value->task_id); ?>);" data-creator="" data-task_name="" data-task_id="" data-toggle="tooltip" data-placement="bottom" title="Edit Task"><img src="<?php echo e(asset('images/icons/card-edit.png')); ?>"></span>
													<span class="remove-task" data-task_id="<?php echo e($value->task_id); ?>" data-toggle="tooltip" data-placement="bottom" title="Delete Task"><img src="<?php echo e(asset('images/icons/card-delete.png')); ?>"></span>	
													<?php endif; ?> -->
													<?php if(Auth::user()->id != $value->task_creator): ?>
													<span class="view_threads" data-toggle="tooltip" data-placement="bottom" title="<?php echo e(__('translation.show_threads')); ?>" data-card="working" data-task_id="<?php echo e($value->task_id); ?>" data-task_creator="<?php echo e($value->task_creator); ?>" data-task_status="N" onclick="view_threads( <?php echo e($value->task_id); ?> , <?php echo e($value->task_creator); ?> , 'N' , false ,'<?php echo e(__('translation.deadline')); ?>' , '<?php echo e(__('translation.task_complete')); ?>'  , '<?php echo e(__('translation.reply')); ?>' );"><img src="<?php echo e(asset('images/icons/show-thread-icon.png')); ?>"></span>
													<?php endif; ?>
													<?php if($value->task_creator == Auth::user()->id): ?>
													<div class="btn-group card-menu">
														<span class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															<img src="<?php echo e(asset('images/icons/card-ellipsis.png')); ?>">
														</span>
														<div class="dropdown-menu dropdown-menu-right">
															<ul>
																<li>
																	<a href="javascript:void(0);" class="dropdown-item edit-task" data-creator="<?php echo e($value->task_creator); ?>" data-task_name="" data-task_id="<?php echo e($value->task_id); ?>"><p>Edit</p></a>
																</li>
																<li>
																	<a href="javascript:void(0);" class="dropdown-item remove-task" data-task_id="<?php echo e($value->task_unique); ?>"><p>Delete</p></a>
																</li>
															</ul>
														</div>
													</div>
													<?php endif; ?>
												</div>
											</div>
											<div class="list-info">
												<p class="date-created"><?php echo e(Carbon\Carbon::parse($value->task_created)->format('D, M j Y')); ?></p>
												<h3 id="title_task" class="title" aria-data="<?php echo e($value->title); ?>"><?php echo e($value->title); ?>

												</h3>
												<p class="created-by">By  <span><?php echo e($value->firstname); ?> <?php echo e($value->lastname); ?></span></p>
												<div class="deadline">
													<img src="<?php echo e(asset('images/icons/card-date.png')); ?>">
													<span class="badge-text"> <?php if($value->deadline): ?> Deadline: <span id="deadline_task"><?php echo e(Carbon\Carbon::parse($value->deadline)->format('D, M j Y')); ?> </span> <?php else: ?> No Deadline <?php endif; ?></span>
												</div>
												<div class="description rmore" id="desc_task">
													<?php echo $value->description; ?>

												</div>
												<br clear="all">
												<div class="card-members">
													<p id="assignee-to">Assigned to</p>
													<?php $__currentLoopData = $value->assignee; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
														<?php if($r->comment != null): ?>
															<?php if($r->image != null): ?>
																<img id="assign_img" data-status="seen" class="a <?php echo e($r->id); ?>" data-generate='<?php echo e($r->id); ?>' data-name="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>" aria-data="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>" src="public/profile/<?php echo e($r->id); ?>/thumbnail_profile/<?php echo e($r->image); ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?> (Seen)">
															<?php else: ?>
																<img id="assign_img" data-status="seen" class="a <?php echo e($r->id); ?>" data-generate='<?php echo e($r->id); ?>' data-name="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>" aria-data="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>"  src="images/icons/default-user.png"  data-toggle="tooltip" data-placement="bottom" title="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?> (Seen)">
															<?php endif; ?>
														<?php else: ?>
															<?php if($r->comment != null): ?>
																<img id="assign_img" data-status="unseen" class="a <?php echo e($r->id); ?>" data-generate='<?php echo e($r->id); ?>' data-name="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>" aria-data="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>"  src="public/profile/<?php echo e($r->id); ?>/thumbnail_profile/<?php echo e($r->image); ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?> (Unseen)">
															<?php else: ?>
																<img id="assign_img" data-status="unseen" class="a <?php echo e($r->id); ?>" data-generate='<?php echo e($r->id); ?>' data-name="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>" aria-data="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>"  src="images/icons/default-user.png" data-toggle="tooltip" data-placement="bottom" title="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?> (Unseen)">
															<?php endif; ?>
														<?php endif; ?>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
													<button class="add-member-btn" data-t="<?php echo e($value->task_unique); ?>" data-task_creator="<?php echo e($value->task_creator); ?>"  data-task-g="<?php echo e($value->task_id); ?>"  data-toggle="tooltip" data-placement="bottom" title="Add Member"><div class="add-icon">&#9587;</div></button>
												</div>
												<?php if($value->attachment->count() > 0 ): ?>
												<label class="attachment-title show-label"><span class="attachment-label">Show Attachment</span> <img src="<?php echo e(asset('images/icons/card-clip.png')); ?>"><span class="count-items"><?php echo e($value->attachment->count()); ?></span></label>
												<div class="attachment-list" style="display: none;">
													<div class="file-attachment">
														<div class="file-list clearfix">
															<?php $__currentLoopData = $value->attachment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<?php if($file->file_type == 'doc' || $file->file_type == 'docx' || $file->file_type == 'DOC' || $file->file_type == 'DOCX'): ?>
																	<div class="file-item js-open-viewer col-sm-6">
																		<div class="file-wrapper flex middle space-bet">
																			<div class="file-icon">
																				<img src="images/icons/file-blue.png">
																				<span><?php echo e($file->file_type); ?></span>
																			</div>
																			<p class="file-name"><?php echo e($file->image); ?></p>
																		</div>
																	</div>
																<?php elseif($file->file_type == 'xls' || $file->file_type == 'xlsx' || $file->file_type == 'XLS' || $file->file_type == 'XLSX'): ?>
																	<div class="file-item js-open-viewer col-sm-6">
																		<div class="file-wrapper flex middle space-bet">
																			<div class="file-icon">
																				<img src="images/icons/file-darkgreen.png">
																				<span><?php echo e($file->file_type); ?></span>
																			</div>
																			<p class="file-name"><?php echo e($file->image); ?></p>
																		</div>
																	</div>
																<?php elseif($file->file_type == 'ppt' || $file->file_type == 'pptx' || $file->file_type == 'PPT' || $file->file_type == 'PPTX'): ?>
																	<div class="file-item js-open-viewer  col-sm-6">
																		<div class="file-wrapper flex middle space-bet">
																			<div class="file-icon">
																				<img src="images/icons/file-orange.png">
																				<span><?php echo e($file->file_type); ?></span>
																			</div>
																			<p class="file-name"><?php echo e($file->image); ?></p>
																		</div>
																	</div>
																<?php elseif($file->file_type == 'pdf' || $file->file_type == 'PDF'): ?>
																	<div class="file-item js-open-viewer  col-sm-6">
																		<div class="file-wrapper flex middle space-bet">
																			<div class="file-icon">
																				<img src="images/icons/file-red.png">
																				<span><?php echo e($file->file_type); ?></span>
																			</div>
																			<p class="file-name"><?php echo e($file->image); ?></p>
																		</div>
																	</div>
																<?php elseif($file->file_type == 'zip' || $file->file_type == 'rar' || $file->file_type == 'tar' || $file->file_type == 'gzip' || $file->file_type == 'gz' || $file->file_type == '7z' || $file->file_type == 'ZIP' || $file->file_type == 'RAR' || $file->file_type == 'TAR' || $file->file_type == 'GZIP' || $file->file_type == 'GZ' || $file->file_type == '7Z'): ?>
																	<div class="file-item js-open-viewer  col-sm-6">
																		<div class="file-wrapper flex middle space-bet">
																			<div class="file-icon">
																				<img src="images/icons/file-lightgreen.png">
																				<span><?php echo e($file->file_type); ?></span>
																			</div>
																			<p class="file-name"><?php echo e($file->image); ?></p>
																		</div>
																	</div>
																<?php elseif($file->file_type == 'jpg' || $file->file_type == 'jpeg' || $file->file_type == 'png' || $file->file_type == 'gif' || $file->file_type == 'JPG' || $file->file_type == 'JPEG' || $file->file_type == 'PNG' || $file->file_type == 'GIF'): ?>
																<?php else: ?>
																	<div class="file-item js-open-viewer  col-sm-6">
																		<div class="file-wrapper flex middle space-bet">
																			<div class="file-icon">
																				<img src="images/icons/file-gray.png">
																				<span><?php echo e($file->file_type); ?></span>
																			</div>
																			<p class="file-name"><?php echo e($file->image); ?></p>
																		</div>
																	</div>
																<?php endif; ?>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														</div>
														<div class="image-list clearfix">
															<?php $__currentLoopData = $value->attachment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<?php if($file->file_type == 'jpg' || $file->file_type == 'jpeg' || $file->file_type == 'png' || $file->file_type == 'gif' || $file->file_type == 'JPG' || $file->file_type == 'JPEG' || $file->file_type == 'PNG' || $file->file_type == 'GIF'): ?>
																<div class="image-item js-open-viewer">
																<img src="<?php echo e(URL::to('/')); ?>/storage/tasks_attachment/<?php echo e($value->task_id); ?>/thumbnail/<?php echo e($file->image); ?>">
																</div>
																<?php endif; ?>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														</div>
														<div class="file-info hide">
															<?php $__currentLoopData = $value->attachment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<?php if($file->file_type == 'doc' || $file->file_type == 'docx' || $file->file_type == 'DOC' || $file->file_type == 'DOCX'): ?>
																	<div class="hidden-item">
																		<div class="file-type"><?php echo e($file->file_type); ?></div>
																		<div class="file-name"><?php echo e($file->image); ?></div>
																		<div class="file-src"><?php echo e(URL::to('/')); ?><?php echo e($file->full_path); ?></div>
																	</div>
																<?php elseif($file->file_type == 'xls' || $file->file_type == 'xlsx' || $file->file_type == 'XLS' || $file->file_type == 'XLSX'  || $file->file_type == 'csv'): ?>
																	<div class="hidden-item">
																		<div class="file-type"><?php echo e($file->file_type); ?></div>
																		<div class="file-name"><?php echo e($file->image); ?></div>
																		<div class="file-src"><?php echo e(URL::to('/')); ?><?php echo e($file->full_path); ?></div>
																	</div>
																<?php elseif($file->file_type == 'ppt' || $file->file_type == 'pptx' || $file->file_type == 'PPT' || $file->file_type == 'PPTX'): ?>
																	<div class="hidden-item">
																		<div class="file-type"><?php echo e($file->file_type); ?></div>
																		<div class="file-name"><?php echo e($file->image); ?></div>
																		<div class="file-src"><?php echo e(URL::to('/')); ?><?php echo e($file->full_path); ?></div>
																	</div>
																<?php elseif($file->file_type == 'pdf' || $file->file_type == 'PDF'): ?>
																	<div class="hidden-item">
																		<div class="file-type"><?php echo e($file->file_type); ?></div>
																		<div class="file-name"><?php echo e($file->image); ?></div>
																		<div class="file-src"><?php echo e(URL::to('/')); ?><?php echo e($file->full_path); ?></div>
																	</div>
																<?php elseif($file->file_type == 'zip' || $file->file_type == 'rar' || $file->file_type == 'tar' || $file->file_type == 'gzip' || $file->file_type == 'gz' || $file->file_type == '7z' || $file->file_type == 'ZIP' || $file->file_type == 'RAR' || $file->file_type == 'TAR' || $file->file_type == 'GZIP' || $file->file_type == 'GZ' || $file->file_type == '7Z'): ?>
																	<div class="hidden-item">
																		<div class="file-type"><?php echo e($file->file_type); ?></div>
																		<div class="file-name"><?php echo e($file->image); ?></div>
																		<div class="file-src"><?php echo e(URL::to('/')); ?><?php echo e($file->full_path); ?></div>
																	</div>
																<?php elseif($file->file_type == 'jpg' || $file->file_type == 'jpeg' || $file->file_type == 'png' || $file->file_type == 'gif' || $file->file_type == 'JPG' || $file->file_type == 'JPEG' || $file->file_type == 'PNG' || $file->file_type == 'GIF'): ?>
																<?php else: ?>
																	<div class="hidden-item">
																		<div class="file-type"><?php echo e($file->file_type); ?></div>
																		<div class="file-name"><?php echo e($file->image); ?></div>
																		<div class="file-src"><?php echo e(URL::to('/')); ?><?php echo e($file->full_path); ?></div>
																	</div>
																<?php endif; ?>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
															<?php $__currentLoopData = $value->attachment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<?php if($file->file_type == 'jpg' || $file->file_type == 'jpeg' || $file->file_type == 'png' || $file->file_type == 'gif' || $file->file_type == 'JPG' || $file->file_type == 'JPEG' || $file->file_type == 'PNG' || $file->file_type == 'GIF'): ?>
																<div class="hidden-item">
																	<div class="file-type"><?php echo e($file->file_type); ?></div>
																	<div class="file-name"><?php echo e($file->image); ?></div>
																	<div class="file-src"><?php echo e(URL::to('/')); ?><?php echo e($file->full_path); ?></div>
																</div>
																<?php endif; ?>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														</div>
									            	</div>
												</div>
												<?php endif; ?>
												<div class="recent-comments clearfix hide">
													<p class="comments-label">Recent Comment</p>
													<div class="top flex middle">
														<img src="<?php echo e(asset('images/sample/user-pic-3.png')); ?>">
														<div class="info">
															<p class="user-name"></p>
															<p class="comment-time"></p>
														</div>
													</div>
													<p class="comment rmore-comment"></p>
												</div>
												<div class="form-group hide">
													<form action="" method="POST" enctype="multipart/form-data">
														<input type="hidden" name="task_id" value="">
														<input type="hidden" name="status" value="">
														<input type="hidden" name="created_by" value="">
														<div class="textbox form-group">
															<textarea class="form-control required" style="height: 25%" name="comment" placeholder="Comment Here..." required></textarea>
															<span class="invalid-feedback">Please input comment here.</span>
															<div class="file-attachment">
									            				<div class="flex middle space-bet">
									            					<button type="button" class="btn btn-xs btn-info btn-submit pull-left"><?php echo e(__('translation.reply')); ?></button>
											            			<label class="add-file-button card-add-file"><?php echo e(__('translation.add_files')); ?></label>
																	<input type="file" name="images[]" class="file-custom-input" multiple style="display: none;">
									                            </div>
																<div class="file-list clearfix"></div>
																<div class="image-list clearfix"></div>
									            			</div>
															<div class="show-thread-btn text-right">
																<a href="javascript:void(0);" onclick="" class="view_threads" data-card="working" data-task_id="" data-task_creator="" data-task_status="I"><p class="link-inset">Show Threads</p></a>
															</div>
														</div>
													</form>
												</div>

												<!-- DONT SHOW IF THE USER CREATED THE TASK -->
												<?php if(Auth::user()->id != $value->task_creator): ?>
												<div class="form-group">
													<div class="textbox form-group">
														<div class="show-thread-btn text-right">  
															<a href="javascript:void(0);" class="view_threads" data-task_id="<?php echo e($value->task_id); ?>" data-task_creator="<?php echo e($value->task_creator); ?>" data-task_status="N" data-archive="false" data-lang_deadline="<?php echo e(__('translation.deadline')); ?>" data-lang_task_complete="<?php echo e(__('translation.task_complete')); ?>" data-lang_reply="<?php echo e(__('translation.reply')); ?>"><p class="link-inset"><?php echo e(__('translation.show_threads')); ?></p></a>
														</div>
													</div>
												</div>
												<?php endif; ?>
											</div>
										</div>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									<!-- <a href="javascript:void(0);" class="btn btn-xs btn-primary view_more_new_task " onclick="view_more_new_task(`<?php echo e($get_new_task['last_task_id']); ?>`)">View more</a> -->
								<?php endif; ?>
							</div>
							<?php if( $get_new_task !== null && $get_new_task->total() > 5): ?>
								<a href="javascript:void(0);" class="view_more_task" data-task="task_new" last_page="<?php echo e($get_new_task->lastPage()); ?>" current_page="<?php echo e($get_new_task->currentPage()); ?>" data-action='loadmore_new_task' >View more</a>
							<?php endif; ?>
						</div>
					</div>
					<div class="list-item progress-box" id="waiting_for_answer">  
						<div class="list-container pink <?php echo e($current_user->color); ?>-bg-secondary">
							<div class="list-header flex middle space-bet"><h2><?php echo e(__('translation.waiting_for_answer')); ?></h2><span><?php if(@isset($get_waiting_for_answer['total_task'])): ?> <?php endif; ?></span></div>
							<div class="list-body scrollbar waiting_div" >
								<?php if(@isset($get_waiting_for_answer)): ?>
									<?php $__currentLoopData = $get_waiting_for_answer->items(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<div class="list-card clearfix" id="div<?php echo e($value->task_id); ?>" data-t="<?php echo e($value->task_unique); ?>">
											<div class="list-tools flex space-bet">
												<span class="project-name"><?php echo e($value->category_name); ?></span>
												<div class="flex">
													<!-- <span class="comment-task" data-task_name="" data-task_id="" data-toggle="tooltip" data-placement="bottom" title="New Comments"><img src="images/icons/comment.png"><span class="count unread"><?php echo e($value->unread); ?></span></span> -->

													<span class="view_threads show_threads_remove_notif" data-lang-deadline="<?php echo e(__('translation.deadline')); ?>" data-lang-task_complete="<?php echo e(__('translation.task_complete')); ?>" data-lang-reply="<?php echo e(__('translation.reply')); ?>" data-card="working" data-task_id="<?php echo e($value->task_id); ?>" data-task_creator="<?php echo e($value->task_creator); ?>" data-task_status="I" data-toggle="tooltip" data-placement="bottom" title="<?php echo e(__('translation.show_threads')); ?>" onclick="view_threads( <?php echo e($value->task_id); ?> , <?php echo e($value->task_creator); ?> , 'I' , false ,'<?php echo e(__('translation.deadline')); ?>' , '<?php echo e(__('translation.task_complete')); ?>'  , '<?php echo e(__('translation.reply')); ?>' );"><img src="<?php echo e(asset('images/icons/show-thread-icon.png')); ?>"></span>
													
													<!-- <span class="edit-task" onclick="edit_task('<?php echo e($value->task_unique); ?>');" data-creator="" data-task_name="" data-task_id="" data-toggle="tooltip" data-placement="bottom" title="Edit Task"><img src="<?php echo e(asset('images/icons/card-edit.png')); ?>"></span> -->
													<!-- <?php if($value->task_creator == Auth::user()->id): ?>
													<span class="remove-task" data-task_id="<?php echo e($value->task_unique); ?>" data-toggle="tooltip" data-placement="bottom" title="Delete Task"><img src="<?php echo e(asset('images/icons/card-delete.png')); ?>"></span>
													<?php endif; ?> -->
													<?php if($value->task_creator == Auth::user()->id): ?>
													<div class="btn-group card-menu">
														<span class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															<img src="<?php echo e(asset('images/icons/card-ellipsis.png')); ?>">
														</span>
														<div class="dropdown-menu dropdown-menu-right">
															<ul>
																<li>
																	<a href="javascript:void(0);" class="dropdown-item edit-task" data-creator="<?php echo e($value->task_creator); ?>" data-task_name="" data-task_id="<?php echo e($value->task_id); ?>"><p>Edit</p></a>
																</li>
																<li>
																	<a href="javascript:void(0);" class="dropdown-item remove-task" data-task_id="<?php echo e($value->task_unique); ?>"><p>Delete</p></a>
																</li>
															</ul>
														</div>
													</div>
													<?php endif; ?>
												</div>
											</div>
											
											<div class="list-info">
												<p class="date-created"><?php echo e(Carbon\Carbon::parse($value->task_created)->format('D, M j Y')); ?></p>
												<h3 class="title" aria-data="<?php echo e($value->title); ?>"><?php echo e($value->title); ?>

												</h3>
												<p class="created-by">By  <span><?php echo e($value->firstname); ?> <?php echo e($value->lastname); ?></span></p>
												<div class="deadline">
													<img src="<?php echo e(asset('images/icons/card-date.png')); ?>">
													<span class="badge-text">Deadline: <?php if($value->deadline): ?> <?php echo e(Carbon\Carbon::parse($value->deadline)->format('D, M j Y')); ?> <?php else: ?> No Deadline <?php endif; ?></span>
												</div>
												<div class="description rmore">
													<?php echo $value->description; ?>

												</div>
												<br clear="all">
												<div class="card-members">
													<p>Assigned to</p>
													<?php $__currentLoopData = $value->assignee; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
														<?php if($r->comment != null): ?>
															<?php if($r->image != null): ?>
																<img data-status="seen" class="a <?php echo e($r->id); ?>" data-generate='<?php echo e($r->id); ?>' data-name="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>" aria-data="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>" src="public/profile/<?php echo e($r->id); ?>/thumbnail_profile/<?php echo e($r->image); ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?> (Seen)">
															<?php else: ?>
																<img data-status="seen" class="a <?php echo e($r->id); ?>" data-generate='<?php echo e($r->id); ?>' data-name="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>" aria-data="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>"  src="images/icons/default-user.png" data-toggle="tooltip" data-placement="bottom" title="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?> (Seen)">
															<?php endif; ?>
														<?php else: ?>
															<?php if($r->comment != null): ?>
																<img data-status="unseen" class="a <?php echo e($r->id); ?>" data-generate='<?php echo e($r->id); ?>' data-name="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>" aria-data="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>"  src="public/profile/<?php echo e($r->id); ?>/thumbnail_profile/<?php echo e($r->image); ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?> (Unseen)">
															<?php else: ?>
																<img data-status="unseen" class="a <?php echo e($r->id); ?>" data-generate='<?php echo e($r->id); ?>' data-name="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>" aria-data="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>"  src="images/icons/default-user.png" data-toggle="tooltip" data-placement="bottom" title="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?> (Unseen)">
															<?php endif; ?>
														<?php endif; ?>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
													<!-- <img data-status="member-seen" data-name="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>" src="<?php echo e(asset('images/sample/user-pic-2.png')); ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?> (Unseen)">
													<img data-status="member-unseen" data-name="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>" src="<?php echo e(asset('images/sample/user-pic-2.png')); ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?> (Unseen)"> -->
													<button class="add-member-btn" data-toggle="tooltip" data-placement="bottom" title="Add Member"><div class="add-icon">&#9587;</div></button>
												</div>
												<?php if($value->attachment->count() > 0 ): ?>
												<label class="attachment-title show-label"><span class="attachment-label">Show Attachment</span> <img src="<?php echo e(asset('images/icons/card-clip.png')); ?>"><span class="count-items"><?php echo e($value->attachment->count()); ?></span></label>
												<div class="attachment-list" style="display: none;">
													<div class="file-attachment">
														<div class="file-list clearfix">
															<?php $__currentLoopData = $value->attachment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<?php if($file->file_type == 'doc' || $file->file_type == 'docx' || $file->file_type == 'DOC' || $file->file_type == 'DOCX'): ?>
																	<div class="file-item js-open-viewer col-sm-6">
																		<div class="file-wrapper flex middle space-bet">
																			<div class="file-icon">
																				<img src="images/icons/file-blue.png">
																				<span><?php echo e($file->file_type); ?></span>
																			</div>
																			<p class="file-name"><?php echo e($file->image); ?></p>
																		</div>
																	</div>
																<?php elseif($file->file_type == 'xls' || $file->file_type == 'xlsx' || $file->file_type == 'XLS' || $file->file_type == 'XLSX'): ?>
																	<div class="file-item js-open-viewer col-sm-6">
																		<div class="file-wrapper flex middle space-bet">
																			<div class="file-icon">
																				<img src="images/icons/file-darkgreen.png">
																				<span><?php echo e($file->file_type); ?></span>
																			</div>
																			<p class="file-name"><?php echo e($file->image); ?></p>
																		</div>
																	</div>
																<?php elseif($file->file_type == 'ppt' || $file->file_type == 'pptx' || $file->file_type == 'PPT' || $file->file_type == 'PPTX'): ?>
																	<div class="file-item js-open-viewer col-sm-6">
																		<div class="file-wrapper flex middle space-bet">
																			<div class="file-icon">
																				<img src="images/icons/file-orange.png">
																				<span><?php echo e($file->file_type); ?></span>
																			</div>
																			<p class="file-name"><?php echo e($file->image); ?></p>
																		</div>
																	</div>
																<?php elseif($file->file_type == 'pdf' || $file->file_type == 'PDF'): ?>
																	<div class="file-item js-open-viewer col-sm-6">
																		<div class="file-wrapper flex middle space-bet">
																			<div class="file-icon">
																				<img src="images/icons/file-red.png">
																				<span><?php echo e($file->file_type); ?></span>
																			</div>
																			<p class="file-name"><?php echo e($file->image); ?></p>
																		</div>
																	</div>
																<?php elseif($file->file_type == 'zip' || $file->file_type == 'rar' || $file->file_type == 'tar' || $file->file_type == 'gzip' || $file->file_type == 'gz' || $file->file_type == '7z' || $file->file_type == 'ZIP' || $file->file_type == 'RAR' || $file->file_type == 'TAR' || $file->file_type == 'GZIP' || $file->file_type == 'GZ' || $file->file_type == '7Z'): ?>
																	<div class="file-item js-open-viewer col-sm-6">
																		<div class="file-wrapper flex middle space-bet">
																			<div class="file-icon">
																				<img src="images/icons/file-lightgreen.png">
																				<span><?php echo e($file->file_type); ?></span>
																			</div>
																			<p class="file-name"><?php echo e($file->image); ?></p>
																		</div>
																	</div>
																<?php elseif($file->file_type == 'jpg' || $file->file_type == 'jpeg' || $file->file_type == 'png' || $file->file_type == 'gif' || $file->file_type == 'JPG' || $file->file_type == 'JPEG' || $file->file_type == 'PNG' || $file->file_type == 'GIF'): ?>
																<?php else: ?>
																	<div class="file-item js-open-viewer col-sm-6">
																		<div class="file-wrapper flex middle space-bet">
																			<div class="file-icon">
																				<img src="images/icons/file-gray.png">
																				<span><?php echo e($file->file_type); ?></span>
																			</div>
																			<p class="file-name"><?php echo e($file->image); ?></p>
																		</div>
																	</div>
																<?php endif; ?>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														</div>
														<div class="image-list clearfix">
															<?php $__currentLoopData = $value->attachment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<?php if($file->file_type == 'jpg' || $file->file_type == 'jpeg' || $file->file_type == 'png' || $file->file_type == 'gif' || $file->file_type == 'JPG' || $file->file_type == 'JPEG' || $file->file_type == 'PNG' || $file->file_type == 'GIF'): ?>
																<div class="image-item js-open-viewer">
																	<img src="<?php echo e(URL::to('/')); ?>/storage/tasks_attachment/<?php echo e($value->task_id); ?>/thumbnail/<?php echo e($file->image); ?>">
																</div>
																<?php endif; ?>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														</div>
														<div class="file-info hide">
															<?php $__currentLoopData = $value->attachment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<?php if($file->file_type == 'doc' || $file->file_type == 'docx' || $file->file_type == 'DOC' || $file->file_type == 'DOCX'): ?>
																	<div class="hidden-item">
																		<div class="file-type"><?php echo e($file->file_type); ?></div>
																		<div class="file-name"><?php echo e($file->image); ?></div>
																		<div class="file-src"><?php echo e(URL::to('/')); ?><?php echo e($file->full_path); ?></div>
																	</div>
																<?php elseif($file->file_type == 'xls' || $file->file_type == 'xlsx' || $file->file_type == 'XLS' || $file->file_type == 'XLSX'): ?>
																	<div class="hidden-item">
																		<div class="file-type"><?php echo e($file->file_type); ?></div>
																		<div class="file-name"><?php echo e($file->image); ?></div>
																		<div class="file-src"><?php echo e(URL::to('/')); ?><?php echo e($file->full_path); ?></div>
																	</div>
																<?php elseif($file->file_type == 'ppt' || $file->file_type == 'pptx' || $file->file_type == 'PPT' || $file->file_type == 'PPTX'): ?>
																	<div class="hidden-item">
																		<div class="file-type"><?php echo e($file->file_type); ?></div>
																		<div class="file-name"><?php echo e($file->image); ?></div>
																		<div class="file-src"><?php echo e(URL::to('/')); ?><?php echo e($file->full_path); ?></div>
																	</div>
																<?php elseif($file->file_type == 'pdf' || $file->file_type == 'PDF'): ?>
																	<div class="hidden-item">
																		<div class="file-type"><?php echo e($file->file_type); ?></div>
																		<div class="file-name"><?php echo e($file->image); ?></div>
																		<div class="file-src"><?php echo e(URL::to('/')); ?><?php echo e($file->full_path); ?></div>
																	</div>
																<?php elseif($file->file_type == 'zip' || $file->file_type == 'rar' || $file->file_type == 'tar' || $file->file_type == 'gzip' || $file->file_type == 'gz' || $file->file_type == '7z' || $file->file_type == 'ZIP' || $file->file_type == 'RAR' || $file->file_type == 'TAR' || $file->file_type == 'GZIP' || $file->file_type == 'GZ' || $file->file_type == '7Z'): ?>
																	<div class="hidden-item">
																		<div class="file-type"><?php echo e($file->file_type); ?></div>
																		<div class="file-name"><?php echo e($file->image); ?></div>
																		<div class="file-src"><?php echo e(URL::to('/')); ?><?php echo e($file->full_path); ?></div>
																	</div>
																<?php elseif($file->file_type == 'jpg' || $file->file_type == 'jpeg' || $file->file_type == 'png' || $file->file_type == 'gif' || $file->file_type == 'JPG' || $file->file_type == 'JPEG' || $file->file_type == 'PNG' || $file->file_type == 'GIF'): ?>
																<?php else: ?>
																	<div class="hidden-item">
																		<div class="file-type"><?php echo e($file->file_type); ?></div>
																		<div class="file-name"><?php echo e($file->image); ?></div>
																		<div class="file-src"><?php echo e(URL::to('/')); ?><?php echo e($file->full_path); ?></div>
																	</div>
																<?php endif; ?>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
															<?php $__currentLoopData = $value->attachment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<?php if($file->file_type == 'jpg' || $file->file_type == 'jpeg' || $file->file_type == 'png' || $file->file_type == 'gif' || $file->file_type == 'JPG' || $file->file_type == 'JPEG' || $file->file_type == 'PNG' || $file->file_type == 'GIF'): ?>
																<div class="hidden-item">
																	<div class="file-type"><?php echo e($file->file_type); ?></div>
																	<div class="file-name"><?php echo e($file->image); ?></div>
																	<div class="file-src"><?php echo e(URL::to('/')); ?><?php echo e($file->full_path); ?></div>
																</div>
																<?php endif; ?>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														</div>
									            	</div>
												</div>
												<?php endif; ?>
												<div class="flex middle center clearfix">
													<button class="add-comment <?php echo e($current_user->color); ?>-bg-primary"><?php echo e(__('translation.add_comment')); ?></button>
												</div>
												<div class="form-group" style="display: none;">
													<form action="" method="POST" class="card-reply-form" enctype="multipart/form-data">
														<input type="hidden" name="task_id" value="">
														<input type="hidden" name="status" value="">
														<input type="hidden" name="created_by" value="">
														<div class="textbox form-group">
															<textarea maxlength="400" class="form-control card-text-area-<?php echo e($value->task_id); ?> required card-text-area" style="height: 25%" name="reply" placeholder="Comment Here..." required></textarea>
															<p class="pull-right count-<?php echo e($value->task_id); ?> "> <small id="count"> 0 </small> / <small>400</small> </p>
															<br>
															<span class="invalid-feedback">Please input comment here.</span>
															<div class="file-attachment">
									            				<div class="flex middle space-bet">
									            					<button type="button" class="btn btn-xs quick_reply quick_reply-<?php echo e($value->task_id); ?>   btn-submit pull-left" data-task_id="<?php echo e($value->task_id); ?>" data-created_by="<?php echo e($value->task_creator); ?>" data-task_status="C"><?php echo e(__('translation.reply')); ?></button>
											            			<label class="add-file-button card-add-file"><?php echo e(__('translation.add_files')); ?></label>
											            			<input type="file" name="images[]" class="file-custom-input" multiple style="display: none;">
									                            </div>
																<div class="file-list clearfix"></div>
																<div class="image-list clearfix"></div>
									            			</div>
														</div>
													</form>
												</div>
												<div class="recent-comments clearfix">
													<p class="comments-label">Recent Comment </p>
													<div class="top flex middle">
														<?php if($value->last_comment->image == null): ?>
														<img src="<?php echo e(asset('images/sample/user-pic-3.png')); ?>">
														<?php else: ?>
														<img src="public/profile/<?php echo e($value->last_comment->id); ?>/thumbnail_profile/<?php echo e($value->last_comment->image); ?>">
														<?php endif; ?>
														<div class="info">
															<p class="user-name"><?php echo e($value->last_comment->firstname); ?> <?php echo e($value->last_comment->lastname); ?></p>
															<p class="comment-time"><?php echo e(Carbon\Carbon::parse($value->last_comment->date_added)->format('D, M j Y H:i:s A')); ?></p>
														</div>
													</div>
													<p class="comment rmore-comment"><?php echo $value->last_comment->comment; ?></p>
												</div>
												<br clear="all">
												<div class="show-thread-btn text-right">
													<a href="javascript:void(0);" class="view_threads show_threads_remove_notif" data-lang_deadline="<?php echo e(__('translation.deadline')); ?>" data-lang_task_complete="<?php echo e(__('translation.task_complete')); ?>" data-lang_reply="<?php echo e(__('translation.reply')); ?>" data-task_id="<?php echo e($value->task_id); ?>" data-task_creator="<?php echo e($value->task_creator); ?>" data-task_status="I">
														<p class="link-inset"><?php echo e(__('translation.show_threads')); ?></p>
													</a>
												</div>
											</div>
										</div>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>
							</div>
							<?php if( $get_waiting_for_answer !== null && $get_waiting_for_answer->total() > 5): ?>
								<a href="javascript:void(0);" class="view_more_task" data-task="task_wfa" last_page="<?php echo e($get_waiting_for_answer->lastPage()); ?>" current_page="<?php echo e($get_waiting_for_answer->currentPage()); ?>" data-action='loadmore_waiting_for_answer' >View more</a>
							<?php endif; ?>
						</div>
					</div>
					<div class="list-item check-box" id="waiting_for_me">
						<div class="list-container pink <?php echo e($current_user->color); ?>-bg-secondary">
							<div class="list-header flex middle space-bet"><h2><?php echo e(__('translation.waiting_for_me')); ?></h2><span> <?php if(@isset($get_waiting_for_me['total_task'])): ?> <?php endif; ?></span></div>
							<div class="list-body scrollbar waiting_for_me_div">
								<?php if(@isset($get_waiting_for_me)): ?>
									<?php $__currentLoopData = $get_waiting_for_me->items(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<div class="list-card clearfix" id="div<?php echo e($value->task_id); ?>" data-t="<?php echo e($value->task_unique); ?>">
											<div class="list-tools flex space-bet">
												<span class="project-name"><?php echo e($value->category_name); ?></span>
												<div class="flex">
													<!-- <span class="comment-task" data-task_id="" data-toggle="tooltip" data-placement="bottom" title="New Comments"><img src="images/icons/comment.png"><span class="count unread"></span></span> -->
													<span class="view_threads show_threads_remove_notif" data-toggle="tooltip" data-placement="bottom" title="<?php echo e(__('translation.show_threads')); ?>" data-card="working" data-task_id="<?php echo e($value->task_id); ?>" data-task_creator="<?php echo e($value->task_creator); ?>" data-task_status="C" onclick="view_threads( <?php echo e($value->task_id); ?> , <?php echo e($value->task_creator); ?> , 'C' , false ,'<?php echo e(__('translation.deadline')); ?>' , '<?php echo e(__('translation.task_complete')); ?>'  , '<?php echo e(__('translation.reply')); ?>' );"><img src="<?php echo e(asset('images/icons/show-thread-icon.png')); ?>"></span>
													<!-- <?php if($value->task_creator == Auth::user()->id): ?>
													<span class="edit-task" onclick="edit_task(<?php echo e($value->task_id); ?>);" data-creator="" data-task_id="" data-toggle="tooltip" data-placement="bottom" title="Edit Task"><img src="<?php echo e(asset('images/icons/card-edit.png')); ?>"></span>
													<span class="remove-task" data-task_id="<?php echo e($value->task_id); ?>" data-toggle="tooltip" data-placement="bottom" title="Delete Task"><img src="<?php echo e(asset('images/icons/card-delete.png')); ?>"></span>	
													<?php endif; ?> -->
													<?php if($value->task_creator == Auth::user()->id): ?>
													<div class="btn-group card-menu">
														<span class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															<img src="<?php echo e(asset('images/icons/card-ellipsis.png')); ?>">
														</span>
														<div class="dropdown-menu dropdown-menu-right">
															<ul>
																<li>
																	<a href="javascript:void(0);" class="dropdown-item edit-task" data-creator="<?php echo e($value->task_creator); ?>" data-task_name="" data-task_id="<?php echo e($value->task_id); ?>"><p>Edit</p></a>
																</li>
																<li>
																	<a href="javascript:void(0);" class="dropdown-item remove-task" data-task_id="<?php echo e($value->task_unique); ?>"><p>Delete</p></a>
																</li>
															</ul>
														</div>
													</div>
													<?php endif; ?>
												</div>
											</div>
											<div class="list-info">
												<p class="date-created"><?php echo e(Carbon\Carbon::parse($value->task_created)->format('D, M j Y')); ?></p>
												<h3 class="title" aria-data="<?php echo e($value->title); ?>"><?php echo e($value->title); ?>

												</h3>
												<p class="created-by">By  <span><?php echo e($value->firstname); ?> <?php echo e($value->lastname); ?></span></p>
												<div class="deadline">
													<img src="<?php echo e(asset('images/icons/card-date.png')); ?>">
													<span class="badge-text">Deadline: <?php if($value->deadline): ?> <?php echo e(Carbon\Carbon::parse($value->deadline)->format('D, M j Y')); ?> <?php else: ?> No Deadline <?php endif; ?></span>
												</div>
												<div class="description rmore">
													<?php echo $value->description; ?>

												</div>
												<br clear="all">
												<div class="card-members">
													<p>Assigned to</p>
													<?php $__currentLoopData = $value->assignee; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
														<?php if($r->comment != null): ?>
															<?php if($r->image != null): ?>
																<img data-status="seen" class="a <?php echo e($r->id); ?>" data-generate='<?php echo e($r->id); ?>' data-name="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>" aria-data="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>" src="public/profile/<?php echo e($r->id); ?>/thumbnail_profile/<?php echo e($r->image); ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?> (Seen)">
															<?php else: ?>
																<img data-status="seen" class="a <?php echo e($r->id); ?>" data-generate='<?php echo e($r->id); ?>' data-name="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>" aria-data="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>"  src="images/icons/default-user.png" data-toggle="tooltip" data-placement="bottom" title="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?> (Seen)">
															<?php endif; ?>
														<?php else: ?>
															<?php if($r->comment != null): ?>
																<img data-status="unseen" class="a <?php echo e($r->id); ?>" data-generate='<?php echo e($r->id); ?>' data-name="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>" aria-data="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>"  src="public/profile/<?php echo e($r->id); ?>/thumbnail_profile/<?php echo e($r->image); ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?> (Unseen)">
															<?php else: ?>
																<img data-status="unseen" class="a <?php echo e($r->id); ?>" data-generate='<?php echo e($r->id); ?>' data-name="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>" aria-data="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?>"  src="images/icons/default-user.png" data-toggle="tooltip" data-placement="bottom" title="<?php echo e($r->firstname); ?> <?php echo e($r->lastname); ?> (Unseen)">
															<?php endif; ?>
														<?php endif; ?>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
													<button class="add-member-btn" data-toggle="tooltip" data-placement="bottom" title="Add Member"><div class="add-icon">&#9587;</div></button>
												</div>
												<?php if($value->attachment->count() > 0 ): ?>
												<label class="attachment-title show-label"><span class="attachment-label">Show Attachment</span> <img src="<?php echo e(asset('images/icons/card-clip.png')); ?>"><span class="count-items"><?php echo e($value->attachment->count()); ?></span></label>
												<div class="attachment-list" style="display: none;">
													<div class="file-attachment">
														<div class="file-list clearfix">
															<?php $__currentLoopData = $value->attachment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<?php if($file->file_type == 'doc' || $file->file_type == 'docx' || $file->file_type == 'DOC' || $file->file_type == 'DOCX'): ?>
																	<div class="file-item js-open-viewer col-sm-6">
																		<div class="file-wrapper flex middle space-bet">
																			<div class="file-icon">
																				<img src="images/icons/file-blue.png">
																				<span><?php echo e($file->file_type); ?></span>
																			</div>
																			<p class="file-name"><?php echo e($file->image); ?></p>
																		</div>
																	</div>
																<?php elseif($file->file_type == 'xls' || $file->file_type == 'xlsx' || $file->file_type == 'XLS' || $file->file_type == 'XLSX'): ?>
																	<div class="file-item js-open-viewer col-sm-6">
																		<div class="file-wrapper flex middle space-bet">
																			<div class="file-icon">
																				<img src="images/icons/file-darkgreen.png">
																				<span><?php echo e($file->file_type); ?></span>
																			</div>
																			<p class="file-name"><?php echo e($file->image); ?></p>
																		</div>
																	</div>
																<?php elseif($file->file_type == 'ppt' || $file->file_type == 'pptx' || $file->file_type == 'PPT' || $file->file_type == 'PPTX'): ?>
																	<div class="file-item js-open-viewer col-sm-6">
																		<div class="file-wrapper flex middle space-bet">
																			<div class="file-icon">
																				<img src="images/icons/file-orange.png">
																				<span><?php echo e($file->file_type); ?></span>
																			</div>
																			<p class="file-name"><?php echo e($file->image); ?></p>
																		</div>
																	</div>
																<?php elseif($file->file_type == 'pdf' || $file->file_type == 'PDF'): ?>
																	<div class="file-item js-open-viewer col-sm-6">
																		<div class="file-wrapper flex middle space-bet">
																			<div class="file-icon">
																				<img src="images/icons/file-red.png">
																				<span><?php echo e($file->file_type); ?></span>
																			</div>
																			<p class="file-name"><?php echo e($file->image); ?></p>
																		</div>
																	</div>
																<?php elseif($file->file_type == 'zip' || $file->file_type == 'rar' || $file->file_type == 'tar' || $file->file_type == 'gzip' || $file->file_type == 'gz' || $file->file_type == '7z' || $file->file_type == 'ZIP' || $file->file_type == 'RAR' || $file->file_type == 'TAR' || $file->file_type == 'GZIP' || $file->file_type == 'GZ' || $file->file_type == '7Z'): ?>
																	<div class="file-item js-open-viewer col-sm-6">
																		<div class="file-wrapper flex middle space-bet">
																			<div class="file-icon">
																				<img src="images/icons/file-lightgreen.png">
																				<span><?php echo e($file->file_type); ?></span>
																			</div>
																			<p class="file-name"><?php echo e($file->image); ?></p>
																		</div>
																	</div>
																<?php elseif($file->file_type == 'jpg' || $file->file_type == 'jpeg' || $file->file_type == 'png' || $file->file_type == 'gif' || $file->file_type == 'JPG' || $file->file_type == 'JPEG' || $file->file_type == 'PNG' || $file->file_type == 'GIF'): ?>
																<?php else: ?>
																	<div class="file-item js-open-viewer col-sm-6">
																		<div class="file-wrapper flex middle space-bet">
																			<div class="file-icon">
																				<img src="images/icons/file-gray.png">
																				<span><?php echo e($file->file_type); ?></span>
																			</div>
																			<p class="file-name"><?php echo e($file->image); ?></p>
																		</div>
																	</div>
																<?php endif; ?>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														</div>
														<div class="image-list clearfix">
															<?php $__currentLoopData = $value->attachment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<?php if($file->file_type == 'jpg' || $file->file_type == 'jpeg' || $file->file_type == 'png' || $file->file_type == 'gif' || $file->file_type == 'JPG' || $file->file_type == 'JPEG' || $file->file_type == 'PNG' || $file->file_type == 'GIF'): ?>
																<div class="image-item js-open-viewer">
																	<img src="<?php echo e(URL::to('/')); ?>/storage/tasks_attachment/<?php echo e($value->task_id); ?>/thumbnail/<?php echo e($file->image); ?>">
																</div>
																<?php endif; ?>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														</div>
														<div class="file-info hide">
															<?php $__currentLoopData = $value->attachment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<?php if($file->file_type == 'doc' || $file->file_type == 'docx' || $file->file_type == 'DOC' || $file->file_type == 'DOCX'): ?>
																	<div class="hidden-item">
																		<div class="file-type"><?php echo e($file->file_type); ?></div>
																		<div class="file-name"><?php echo e($file->image); ?></div>
																		<div class="file-src"><?php echo e(URL::to('/')); ?><?php echo e($file->full_path); ?></div>
																	</div>
																<?php elseif($file->file_type == 'xls' || $file->file_type == 'xlsx' || $file->file_type == 'XLS' || $file->file_type == 'XLSX'): ?>
																	<div class="hidden-item">
																		<div class="file-type"><?php echo e($file->file_type); ?></div>
																		<div class="file-name"><?php echo e($file->image); ?></div>
																		<div class="file-src"><?php echo e(URL::to('/')); ?><?php echo e($file->full_path); ?></div>
																	</div>
																<?php elseif($file->file_type == 'ppt' || $file->file_type == 'pptx' || $file->file_type == 'PPT' || $file->file_type == 'PPTX'): ?>
																	<div class="hidden-item">
																		<div class="file-type"><?php echo e($file->file_type); ?></div>
																		<div class="file-name"><?php echo e($file->image); ?></div>
																		<div class="file-src"><?php echo e(URL::to('/')); ?><?php echo e($file->full_path); ?></div>
																	</div>
																<?php elseif($file->file_type == 'pdf' || $file->file_type == 'PDF'): ?>
																	<div class="hidden-item">
																		<div class="file-type"><?php echo e($file->file_type); ?></div>
																		<div class="file-name"><?php echo e($file->image); ?></div>
																		<div class="file-src"><?php echo e(URL::to('/')); ?><?php echo e($file->full_path); ?></div>
																	</div>
																<?php elseif($file->file_type == 'zip' || $file->file_type == 'rar' || $file->file_type == 'tar' || $file->file_type == 'gzip' || $file->file_type == 'gz' || $file->file_type == '7z' || $file->file_type == 'ZIP' || $file->file_type == 'RAR' || $file->file_type == 'TAR' || $file->file_type == 'GZIP' || $file->file_type == 'GZ' || $file->file_type == '7Z'): ?>
																	<div class="hidden-item">
																		<div class="file-type"><?php echo e($file->file_type); ?></div>
																		<div class="file-name"><?php echo e($file->image); ?></div>
																		<div class="file-src"><?php echo e(URL::to('/')); ?><?php echo e($file->full_path); ?></div>
																	</div>
																<?php elseif($file->file_type == 'jpg' || $file->file_type == 'jpeg' || $file->file_type == 'png' || $file->file_type == 'gif' || $file->file_type == 'JPG' || $file->file_type == 'JPEG' || $file->file_type == 'PNG' || $file->file_type == 'GIF'): ?>
																<?php else: ?>
																	<div class="hidden-item">
																		<div class="file-type"><?php echo e($file->file_type); ?></div>
																		<div class="file-name"><?php echo e($file->image); ?></div>
																		<div class="file-src"><?php echo e(URL::to('/')); ?><?php echo e($file->full_path); ?></div>
																	</div>
																<?php endif; ?>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
															<?php $__currentLoopData = $value->attachment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<?php if($file->file_type == 'jpg' || $file->file_type == 'jpeg' || $file->file_type == 'png' || $file->file_type == 'gif' || $file->file_type == 'JPG' || $file->file_type == 'JPEG' || $file->file_type == 'PNG' || $file->file_type == 'GIF'): ?>
																<div class="hidden-item">
																	<div class="file-type"><?php echo e($file->file_type); ?></div>
																	<div class="file-name"><?php echo e($file->image); ?></div>
																	<div class="file-src"><?php echo e(URL::to('/')); ?><?php echo e($file->full_path); ?></div>
																</div>
																<?php endif; ?>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														</div>
									            	</div>
												</div>
												<?php endif; ?>
												<div class="flex middle center clearfix">
													<button class="add-comment <?php echo e($current_user->color); ?>-bg-primary "><?php echo e(__('translation.add_comment')); ?></button>
												</div>
												<div class="form-group" style="display: none;">
													<form action="" method="POST" class="card-reply-form" enctype="multipart/form-data">
														<input type="hidden" name="task_id" value="">
														<input type="hidden" name="status" value="">
														<input type="hidden" name="created_by" value="">
														<div class="textbox form-group">
															<textarea maxlength="400" class="form-control card-text-area-<?php echo e($value->task_id); ?> required card-text-area" style="height: 25%" name="reply" placeholder="Comment Here..." required></textarea>
															<p class="pull-right count-<?php echo e($value->task_id); ?> "> <small id="count"> 0 </small> / <small>400</small> </p>
															<br>
															<span class="invalid-feedback">Please input comment here.</span>
															<div class="file-attachment">
									            				<div class="flex middle space-bet">
									            					<button type="button" class="btn btn-xs quick_reply quick_reply-<?php echo e($value->task_id); ?>   btn-submit pull-left" data-task_id="<?php echo e($value->task_id); ?>" data-created_by="<?php echo e($value->task_creator); ?>" data-task_status="C"><?php echo e(__('translation.reply')); ?></button>
											            			<label class="add-file-button card-add-file"><?php echo e(__('translation.add_files')); ?></label>
											            			<input type="file" name="images[]" class="file-custom-input" multiple style="display: none;">
									                            </div>
																<div class="file-list clearfix"></div>
																<div class="image-list clearfix"></div>
									            			</div>
														</div>
													</form>
												</div>
												<div class="recent-comments clearfix">
													<p class="comments-label">Recent Comment</p>
													<div class="top flex middle">
														<?php if($value->last_comment->image == null): ?>
														<img src="<?php echo e(asset('images/sample/user-pic-3.png')); ?>">
														<?php else: ?>
														<img src="public/profile/<?php echo e($value->last_comment->id); ?>/thumbnail_profile/<?php echo e($value->last_comment->image); ?>">
														<?php endif; ?>
														<div class="info">
															<p class="user-name"><?php echo e($value->last_comment->firstname); ?> <?php echo e($value->last_comment->lastname); ?></p>
															<p class="comment-time"><?php echo e(Carbon\Carbon::parse($value->last_comment->date_added)->format('D, M j Y H:i:s A')); ?>  </p>
														</div>
													</div>
													<p class="comment rmore-comment"><?php echo $value->last_comment->comment; ?></p>
												</div>
												<br clear="all">
												<div class="show-thread-btn text-right">
													<a href="javascript:void(0);" class="view_threads  <?php echo e($value->task_id); ?>_click show_threads_remove_notif" data-task_id="<?php echo e($value->task_id); ?>" data-task_creator="<?php echo e($value->task_creator); ?>" data-task_status="C" data-archive="false" data-lang_deadline="<?php echo e(__('translation.deadline')); ?>" data-lang_task_complete="<?php echo e(__('translation.task_complete')); ?>" data-lang_reply="<?php echo e(__('translation.reply')); ?>"><p class="link-inset"><?php echo e(__('translation.show_threads')); ?></p></a>
												</div>
											</div>
										</div>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>
							</div>
							<?php if( $get_waiting_for_me !== null && $get_waiting_for_me->total() > 5): ?>
								<a href="javascript:void(0);" class="view_more_task" data-task="task_wfm" last_page="<?php echo e($get_waiting_for_me->lastPage()); ?>" current_page="<?php echo e($get_waiting_for_me->currentPage()); ?>" data-action='loadmore_waiting_for_me' >View more</a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<!-- End Task Section -->
			<?php echo $__env->make('theme.todo', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			<!-- End To Do List Section -->

			<!-- <div class="float-action search-active">
				<ul>
					<li>
						<button id="add_task"><img src="<?php echo e(asset('images/final/action-add-task.png')); ?>"></button>
					</li>
					<li>
						<button data-toggle="modal" data-target="#filter_modal"><img src="<?php echo e(asset('images/final/action-filter.png')); ?>"></button>
					</li>
					<li>
						<button><img src="<?php echo e(asset('images/final/action-search.png')); ?>"></button>
					</li>
				</ul>
				<button id="action_btn"><img src="<?php echo e(asset('images/final/action-pen.png')); ?>"></button>
			</div> -->

			<div class="float-action2" id="container-floating">
				<div class="sub-btn" id="add_task"><img src="<?php echo e(asset('images/final/action-add-task.png')); ?>" /></div>
				<div class="sub-btn" data-toggle="modal" data-target="#filter_modal"><img src="<?php echo e(asset('images/final/action-filter.png')); ?>" /></div>
				<div class="sub-btn"><img src="<?php echo e(asset('images/final/action-search.png')); ?>" /><input type="text" name="" placeholder="Search Task Title" onkeyup="kmsearchtitle(this, '.list-card>.list-info>.title')"></div>

				<div class="action-btn" id="floating-button">
					<p class="close-action">&#9587;</p>
					<img class="edit-action" src="https://ssl.gstatic.com/bt/C3341AA7A1A076756462EE2E5CD71C11/1x/bt_compose2_1x.png">
				</div>
			</div>
			<div class="float-add-member" onclick="add_new_member()">
				<div class="action-btn" id="floating-addmember">
					<p class="close-action">&#9587;</p>
				</div>
			</div>				
			<div class="mobile-members-div flex col">
				<ul class="nav nav-tabs flex space-bet">
					<li class="nav-item active" onclick="mobile_view_member()">
						<a class="nav-link" id="members_tab" data-toggle="tab" href="#panel_members" role="tab" aria-controls="home" aria-selected="true">Members</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="pending_tab" data-toggle="tab" href="#panel_pending" role="tab" aria-controls="contact" aria-selected="false">Pending</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane fade active in" id="panel_members" aria-labelledby="members_tab">
						<div class="search-bar text-center">
	            			<input type="text" name="" placeholder="Search Accounts" class="search-engine text-center" onkeyup="kmsearch(this, '#mob_business_list>li>.user-info')">
	            		</div>
	            		<div id="mob_business_list">
	            			
	            		</div>
					</div>
					<div class="tab-pane fade" id="panel_pending" aria-labelledby="pending_tab">
						<ul class="scrollbar">
							<li>
								<a href="javascript:void(0);">
									<div class="accept-item flex">
										<img src="images/icons/default-user.png">
										<div class="accept-info">
											<!-- <p class="friend-name">Diana Jameson</p> -->
											<p class="info">Diana Jameson wants to add Pie David to Trackatask Website Task sample sample</p>
											<p class="time-date">4 hours ago</p>
											<div class="button-div">
												<button>Cancel</button>
												<button class="accept-btn">Accept</button>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="javascript:void(0);">
									<div class="accept-item flex">
										<img  src="images/sample/user-pic-3.png">
										<div class="accept-info">
											<!-- <p class="friend-name">Jake Parker</p> -->
											<p class="info">Diana Jameson wants to add Pie David to Trackatask Website Task sample sample</p>
											<p class="time-date">4 hours ago</p>
											<div class="button-div">
												<button>Cancel</button>
												<button class="accept-btn">Accept</button>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="javascript:void(0);">
									<div class="accept-item flex">
										<img src="images/sample/user-pic-3.png">
										<div class="accept-info">
											<p class="info">Diana Jameson wants to add Pie David to Trackatask Website Task sample sample</p>
											<p class="time-date">Yesterday at 12:51am</p>
											<div class="button-div">
												<button>Cancel</button>
												<button class="accept-btn">Accept</button>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="javascript:void(0);">
									<div class="accept-item flex">
										<img src="images/sample/user-pic-4.png">
										<div class="accept-info">
											<p class="info">Diana Jameson wants to add Pie David to Trackatask Website Task sample sample</p>
											<p class="time-date">4 hours ago</p>
											<div class="button-div">
												<button>Cancel</button>
												<button class="accept-btn">Accept</button>
											</div>
										</div>
									</div>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- <div class="mobile-div">
			mobile div
		</div> -->
	</div>
	<?php echo $__env->make('modals.task-modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('theme.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>