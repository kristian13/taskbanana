<?php
	$color = DB::table('users as u')
	->join('theme_color as t', 't.theme_color_id', '=', 'u.theme_color_id')
	->leftJoin("profile_image as pi" , function($q) {
        $q->on('u.id' ,  '=' , 'pi.user_id')
            ->where('pi.status' , null)
            ->where('pi.is_profile', 1);
        })
	->where("u.id" , Auth::user()->id)->get()->first();	    	
	
	$color->image = $color->image == null ? url('/').'/public/images/icons/default-user.png' : url('/').'/public/profile/'.Auth::user()->id.'/thumbnail_profile/'.$color->image;

?>
<!-- Modal Alert-->
<div class="modal fade taskbanana" id="alert_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        	<form>
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&#9587;</span>
	                </button>
	            </div>
	            <div class="modal-body">
            		<div class="form-group">
						<div id="pink_success" class="hide"></div>
						<div id="blue_success" class="hide"></div>
						<div id="warning_animation" class="hide"></div>
						<div id="error_animation" class="hide"></div>
						<div id="question_animation" class="hide"></div>
            			<label></label>
            			<p></p>
            		</div>
				</div>
	            <div class="modal-footer">
	            </div>
        	</form>
        </div>
    </div>
</div>
<!-- End Modal Alert -->

<!-- modal for Side Nav -->
<div class="modal fade" id="sideNav" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content flex col">
			<div class="menu-info flex middle space-bet">
				<div class="user-image">
                    <a href="javascript:void(0);">
                        <img src="<?php echo e($color->image); ?>" class="profile-image">
                    </a>
                </div>
                <div class="user-info">
                	<h3 class="full-name"><?php echo e($User->firstname); ?> <?php echo e($User->lastname); ?> <span class="user-name">(<?php echo e($User->firstname); ?>)</span></h3>
					<p class="email"><?php echo e($User->email); ?></p>
	                <div class="progress-div">
						<div class="progress">
						    <div class="progress-bar" role="progressbar" aria-valuenow="1.489683" aria-valuemin="0" aria-valuemax="25.00" style="width:52%">
						    </div>
						 </div>
					</div>
					<p class="usage-info"><span class="mb-used">1.49</span> MB of <span class="mb-total">25</span> MB</p>
                </div>
                <img class="menu-settings" src="<?php echo e(asset('images/final/m-menu-settings.png')); ?>">
            </div>
            <div class="list-menu">
            	<ul>
            		<a href="javascript:void(0);">
	            		<li class="flex middle"><span class="menu-icon flex center middle"><img src="<?php echo e(asset('images/final/m-menu-upgrade.png')); ?>"></span><span>Upgrade Storage</span></li>
	            	</a>
	            	<a href="javascript:void(0);">
	            		<li class="flex middle"><span class="menu-icon flex center middle"><img src="<?php echo e(asset('images/final/m-menu-calendar.png')); ?>"></span><span>View Calendar</span></li>
	            	</a>
	            	<a href="<?php echo e(url('archive')); ?>">
	            		<li class="flex middle"><span class="menu-icon flex center middle"><img src="<?php echo e(asset('images/final/m-menu-archive.png')); ?>"></span><span>View Archive List</span></li>
	            	</a>
	            	<a href="javascript:void(0);" class="change-theme">
	            		<li class="flex middle"><span class="menu-icon flex center middle"><img src="<?php echo e(asset('images/final/m-menu-theme.png')); ?>"></span><span>Change Theme Color</span></li>
	            	</a>
	            	<div class="theme-color" style="display: none;">
	            		<form class="flex col">
	            			<a href="<?php echo e(url('archive')); ?>">
			            		<li class="flex middle"><input type="radio" id="color_pink" class="color-option pink" name="optradio" value="1"><label for="color_pink">Pink</label></span></li>
			            	</a>
			            	<a href="javascript:void(0);">
			            		<li class="flex middle"><span class="menu-icon flex center middle"><input type="radio" id="color_blue" class="color-option blue" name="optradio" value="2"><label for="color_blue">Blue</label></span></li>
			            	</a>
		            	</form>
	            	</div>
	            	<a href="javascript:void(0);">
	            		<li class="flex middle"><span class="menu-icon flex center middle"><img src="<?php echo e(asset('images/final/m-menu-email.png')); ?>"></span><span>Email Notification</span></li>
	            	</a>
	            	<a href="javascript:void(0);">
	            		<li class="flex middle"><span class="menu-icon flex center middle"><img src="<?php echo e(asset('images/final/m-menu-about.png')); ?>"></span><span>About Us</span></li>
	            	</a>
	            	<a href="javascript:void(0);">
	            		<li class="flex middle"><span class="menu-icon flex center middle"><img src="<?php echo e(asset('images/final/m-menu-help.png')); ?>"></span><span>Help</span></li>
	            	</a>
	            	<a href="<?php echo e(route('logout')); ?>" id="logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
	            		<li><span class="menu-logout">Logout</span></li>
	            	</a>
            	</ul>
            </div>
		</div>
	</div>
</div>
<!-- end modal for Side Nav -->

<!-- Modal for Filter -->
<div class="modal fade taskbanana" id="filter_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        	<form>
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&#9587;</span>
	                </button>
	                <h5 class="modal-title" id="">Filter By</h5>
	            </div>
	            <div class="modal-body">
	            	<div class="form-group">
						<label class="control-label">Member</label>
						<div class="btn-group open">
	            			<input type="text" class="form-control" id="search_member" placeholder="Search Assignee Name">
	            			<span class="invalid-feedback">Please input assignee/member name.</span>
							<span class="btn btn-secondary dropdown-toggle hide" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"></span>
							<div class="dropdown-menu dropdown-menu-right member-results open">
								<!-- <ul>
									<li class="choose-name" data-generate="" data-user-id="2">
										<img src="http://localhost/trackatask_new/images/sample/user-pic-4.png">
										<span>Joan Magpayo</span>
									</li>
								</ul> -->
							</div>
						</div>
            			<div class="member-list clearfix hide"></div>
            			<div id="filter-lists"></div>
            			</div>
					</div>
	            <div class="modal-footer">
	                <button type="button" class="btn no-bg-btn btn-cancel" data-dismiss="modal">Cancel</button>
	            	<button type="button" class="btn no-bg-btn btn-save filter_member">Filter <i class="fa fa-refresh fa-spin fa-1x fa-fw hide arinfo"></i></button>
	            </div>
        	</form>
        </div>
    </div>
</div>
<!-- end Modal for Filter -->

<!-- modal for calendar -->
<div class="modal fade taskbanana" id="calendar_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 750px;">
        <div class="modal-content">
        	<form>
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&#9587;</span>
	                </button>
	                <h5 class="modal-title" id="">Calendar</h5>
	            </div>
	            <div class="modal-body">
            		<div id="calendar_div"></div>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-cancel" data-dismiss="modal">Close</button>
	            </div>
        	</form>
        </div>
    </div>
</div>
<!-- end modal calendar -->

<!-- Modal Set Reminder-->
<div class="modal fade taskbanana" id="reminder_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        	<form>
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&#9587;</span>
	                </button>
	                <h5 class="modal-title" id="">Repeated Work</h5>
	            </div>
	            <div class="modal-body">
            		<div class="form-group">
            			<label>Reminder Name*</label>
            			<input type="text" name="reminder_name" class="form-control" placeholder="Enter Reminder Name" autocomplete="false" required>
            		</div>
            		<div class="form-group">
            			<label>Description*</label>
            			<input type="text" name="description" class="form-control" placeholder="Enter Description" autocomplete="false" required>
            		</div>
            		<div class="form-group">
            			<label>Start Remind At*</label>
            			<input type="time" name="start_time" class="form-control" value="08:00" required>
            		</div>
            		<div class="form-group clearfix">
            			<div class="col-sm-6">
	            			<label>Remind Me Every*</label>
	            			<select class="form-control" id="select_reminder_form">
	            				<option value="daily">Every day</option>
	            				<option value="weekly">Every week</option>
	            				<option value="monthly">Every month</option>
	            				<option value="custom">Custom</option>
	            			</select>
            			</div>
            			<div class="col-sm-6 days_of_week">
            				<label for="">Repeat Every (Week)*</label>
	            			<input type="number" name="recurring" class="form-control" id="" value="1" step="1" min="1">
            			</div>
            		</div>
            		<div class="form-group days_of_week">
            			<label for="">Choose Days of the Week*</label>
            			<div class="reminder-days">
	            			<div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="checkbox-modal-monday" class="styled" type="checkbox" name="days_of_the_week[]" value="monday">
			                    <label for="checkbox-modal-monday">Monday</label>
			                </div>

		                    <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="checkbox-modal-tuesday" class="styled" type="checkbox" name="days_of_the_week[]" value="tuesday">
			                    <label for="checkbox-modal-tuesday">Tuesday</label>
			                </div>

		                    <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="checkbox-modal-wednesday" class="styled" type="checkbox" name="days_of_the_week[]" value="wednesday">
			                    <label for="checkbox-modal-wednesday">Wednesday</label>
		                    </div>

		                    <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="checkbox-modal-thursday" class="styled" type="checkbox" name="days_of_the_week[]" value="thursday">
			                    <label for="checkbox-modal-thursday">Thursday</label>
		                    </div>

		                    <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="checkbox-modal-friday" class="styled" type="checkbox" name="days_of_the_week[]" value="friday">
			                    <label for="checkbox-modal-friday">Friday</label>
		                    </div>

		                    <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="checkbox-modal-saturday" class="styled" type="checkbox" name="days_of_the_week[]" value="saturday">
			                    <label for="checkbox-modal-saturday">Saturday</label>
		                    </div>

		                    <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="checkbox-modal-sunday" class="styled" type="checkbox" name="days_of_the_week[]" value="sunday">
			                    <label for="checkbox-modal-sunday">Sunday</label>
		                    </div>
            			</div>
					</div>
					<!-- <div class="form-group assignee-checkbox">
            			<label for="">Copy to</label>
            			<div class="panel panel-default">
							<div class="panel-heading"></div>
							<div class="panel-body" id="business_panel_reminder" style="display: none;"></div>
							<div class="panel-body" id="personal_panel_reminder" style=""></div>
						</div>
					</div> -->
					<div class="form-group p-relative assignee-div">
            			<label>Search Assignee*</label>
            			<div class="">
	            			<input type="text" class="form-control" id="search_member_reminder" placeholder="Search Assignee Name">
							<span class="btn btn-secondary dropdown-toggle hide" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></span>
							<div class="dropdown-menu dropdown-menu-right member-results open wew" style="position:relative;width:100%;">
								<ul></ul>
							</div>
						</div>
            			<div class="assignee-list clearfix hide"></div>
            			<div class="clearfix hide" id="data-lists"></div>
            		</div>
				</div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
	                <button type="submit" class="btn btn-save add_reminder">Add Repeated Work <i class="fa fa-refresh fa-spin fa-1x fa-fw hide arinfo"></i></button>
	            </div>
        	</form>
        </div>
    </div>
</div>
<!-- End Modal Set Reminder -->

<!-- Modal Edit Reminder-->
<div class="modal fade taskbanana" id="edit_reminder_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        	<form>
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&#9587;</span>
	                </button>
	                <h5 class="modal-title" id="">Edit Repeated Work</h5>
	            </div>
	            <div class="modal-body">
					<input type="hidden" name="reminder_id" value="">
            		<div class="form-group">
            			<label>Reminder Name*</label>
            			<input type="text" name="reminder_name" class="form-control" placeholder="Enter Reminder Name" autocomplete="false" required>
            		</div>
            		<div class="form-group">
            			<label>Description*</label>
            			<input type="text" name="description" class="form-control" placeholder="Enter Description" autocomplete="false" required>
            		</div>
            		<div class="form-group">
            			<label>Start Remind At*</label>
            			<input type="time" name="start_time" class="form-control" value="08:00" required>
            		</div>
            		<div class="form-group clearfix">
            			<div class="col-sm-6">
	            			<label>Remind Me Every*</label>
	            			<select class="form-control" id="select_reminder_form">
	            				<option value="daily">Every day</option>
	            				<option value="weekly">Every week</option>
	            				<option value="monthly">Every month</option>
	            				<option value="custom">Custom</option>
	            			</select>
            			</div>
            			<div class="col-sm-6 days_of_week">
            				<label for="">Repeat Every (Week)</label>
	            			<input type="number" name="recurring" class="form-control" id="" value="1" step="1" min="1">
            			</div>
            		</div>
            		<div class="form-group days_of_week">
            			<label for="">Choose Days of the Week*</label>
            			<div class="reminder-days">
	            			<div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="e-checkbox-modal-monday" class="styled" type="checkbox" name="days_of_the_week[]" value="monday">
			                    <label for="e-checkbox-modal-monday">Monday</label>
			                </div>

		                    <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="e-checkbox-modal-tuesday" class="styled" type="checkbox" name="days_of_the_week[]" value="tuesday">
			                    <label for="e-checkbox-modal-tuesday">Tuesday</label>
			                </div>

		                    <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="e-checkbox-modal-wednesday" class="styled" type="checkbox" name="days_of_the_week[]" value="wednesday">
			                    <label for="e-checkbox-modal-wednesday">Wednesday</label>
		                    </div>

		                    <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="e-checkbox-modal-thursday" class="styled" type="checkbox" name="days_of_the_week[]" value="thursday">
			                    <label for="e-checkbox-modal-thursday">Thursday</label>
		                    </div>

		                    <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="e-checkbox-modal-friday" class="styled" type="checkbox" name="days_of_the_week[]" value="friday">
			                    <label for="e-checkbox-modal-friday">Friday</label>
		                    </div>

		                    <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="e-checkbox-modal-saturday" class="styled" type="checkbox" name="days_of_the_week[]" value="saturday">
			                    <label for="e-checkbox-modal-saturday">Saturday</label>
		                    </div>

		                    <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="e-checkbox-modal-sunday" class="styled" type="checkbox" name="days_of_the_week[]" value="sunday">
			                    <label for="e-checkbox-modal-sunday">Sunday</label>
		                    </div>
            			</div>
					</div>
					<!-- <div class="form-group assignee-checkbox">
            			<label for="">Copy to</label>
            			<div class="panel panel-default">
							<div class="panel-heading"></div>
							<div class="panel-body" id="business_panel_reminder" style="display: none;"></div>
							<div class="panel-body" id="personal_panel_reminder" style=""></div>
						</div>
					</div> -->
					<div class="form-group p-relative assignee-div">
            			<label>Search Assignee*</label>
            			<div class="">
	            			<input type="text" class="form-control" id="search_member_reminder_edit" placeholder="Search Assignee Name">
							<span class="btn btn-secondary dropdown-toggle hide" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></span>
							<div class="dropdown-menu dropdown-menu-right member-results open" style="position:relative;width:100%;">
								<ul></ul>
							</div>
						</div>
            			<div class="assignee-list clearfix hide"></div>
            			<div class="clearfix hide" id="data-lists"></div>
            		</div>
				</div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
	                <button type="submit" class="btn btn-save edit_this_reminder">Edit Repeated Work <i class="fa fa-refresh fa-spin fa-1x fa-fw hide rinfo"></i></button>
	            </div>
        	</form>
        </div>
    </div>
</div>
<!-- End Modal Set Reminder -->

<!-- Modal Add Task -->
<div class="modal fade taskbanana" id="add_task_modal" tabindex="-1" role="dialog" aria-hidden="true" data-create_new='false'>
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        	<form id="add-task" method="POST" enctype="multipart/form-data" class="universal" data-url="<?php echo e(url('/add_task')); ?>" >
        		<?php echo e(csrf_field()); ?> <!-- dont remove -->
        		<input type="hidden" name="type_of_post" value="add_task" class="type_of_post">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&#9587;</span>
	                </button>
	                <h5 class="modal-title" id="">Add New Task</h5>
	            </div>
	            <div class="modal-body">
	            	<div class="form-group" id="at-project-title">
            			<label>Project Title</label>
            			<div class="dropdown-add flex">
            				<select class="form-control" name="category_id" id="category_add_task">
	            			</select>
	            			<input type="text" name="temp_project_title" class="form-control hide" placeholder="New Project Title">
	            			<input type="hidden" name="new_project_title">
	            			<!-- <button class="blue flex middle center btn-inset add-option" type="button">&#9547;</button> -->
	            			<button class="btn-inset cancel-add hide" type="button">Cancel</button>
            			</div>
            		</div>
            		<div class="form-group" id="at-task-name">
            			<label>Task Name/Subject *</label>
            			<input type="text" class="form-control" name="title" placeholder="Enter Task Name/Subject" autocomplete="false">
            			<span class="invalid-feedback">Please input task name/subject.</span>
            		</div>
            		<div class="form-group" id="at-description">
            			<label>Description *</label>
            			<div class="texteditor-wrap">
				            <div class="texteditor-toolbar">
				                <div class="btn-group btn-group-sm right-buttons">
				                    <button type="button" class="btn pd-bold texteditor-btn" title="Bold">B</button>
				                    <button type="button" class="btn pd-italic texteditor-btn" title="Italic">I</button>
				                    <button type="button" class="btn pd-underline texteditor-btn" title="Underline">U</button>
				                    <button type="button" class="btn pd-strikethrough texteditor-btn" title="Strike Through">S</button>
				                </div>
				                <div class="btn-group btn-group-sm pull-right">
				                    <button type="button" class="btn pd-undo texteditor-btn" title="Undo"><i class="fa fa-undo"></i></button>
				                    <button type="button" class="btn pd-redo texteditor-btn" title="Redo"><i class="fa fa-repeat"></i></button>
				                </div>
				            </div>
				            <div contenteditable="true" class="texteditor-content">
								<p><br></p>
				            </div>
				            <textarea name="description" class="hide"></textarea>
				        </div>
				        <span class="invalid-feedback">Please input task description.</span>
            		</div>
            		<div class="form-group" id="at-deadline">
            			<label>Set Deadline</label>
            			<input type="date" name="deadline" class="form-control">
            		</div>
            		<div class="form-group p-relative assignee-div" id="at-search-assignee">
            			<label>Search Assignee*</label>
            			<div class="btn-group open">
	            			<input type="text" class="form-control" id="search_member" placeholder="Search Assignee Name" autocomplete="off">
							<span class="btn btn-secondary dropdown-toggle hide" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"></span>
							<div class="dropdown-menu dropdown-menu-right member-results open">
							</div>
						</div>
            			<div class="assignee-list clearfix hide"></div>
            			<div class="clearfix hide" id="data-lists"></div>
            		</div>
            		<div class="form-group assignee-checkbox hide">
            			<label>Search To Select an Assignee *</label>
            			<div class="panel panel-default">
            				<!-- Render data at AJAX -->
            				<div class="panel-heading">
            					<input type="text" id="search_member" class="form-control">
            				</div>
            				<!-- Render data at AJAX -->
            				<div class="panel-body" id="business_panel">
            					<div class="checkbox checkbox-gray checkbox-xs">
				                    <input id="assinee_business" class="checkAll styled" type="checkbox" checked>
				                    <label for="assinee_business">All Members</label>
				                </div>
				                <?php if(!empty($get_my_business_team)): ?>
					               	<?php $__currentLoopData = $get_my_business_team; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					               		<!-- <div class="checkbox checkbox-gray checkbox-xs">
						                    <input id="assinee_<?php echo e($value->id); ?>" class="styled" name="task_users[]" value="<?php echo e($value->id); ?>" type="checkbox">
						                    <label for="assinee_<?php echo e($value->id); ?>"><?php echo e($value->firstname); ?> <?php echo e($value->lastname); ?></label>
						                </div> -->
					               	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				               	<?php endif; ?>
            				</div>
            				<!-- Render data at AJAX -->
            			<!-- 	<div class="panel-body" id="personal_panel" style="display: none;">
            					<div class="checkbox checkbox-gray checkbox-xs">
				                    <input id="assinee_personal" class="checkAll styled" type="checkbox">
				                    <label for="assinee_personal">All Members</label>
				                </div>
				                
            				</div> -->
            			</div>
            		</div>
            		<div class="form-group">
            			<!-- <label>Add File/Attachment</label> -->
            			<!-- <div style="text-align: center;">File Input here...</div> -->
            			<br clear="all">
            			<div class="file-attachment">
            				<div class="flex middle">
		            			<label for="add_file" class="add-file-button" id="at-add-file-btn">Add Files</label>
								<input type="file" id="add_file" name="images[]" class="file-custom-input" multiple style="display: none;">
								<div class="image-optimization flex middle" style="display: none">
									<span class="optimization-label">Image Optimization</span>
									<div class="radio radio-gray radio-inline">
		                                <input type="radio" name="image-opt" id="low" class="image-opt" value="1" checked="">
		                                <label for="low">
		                                    Low
		                                </label>
		                            </div>
		                            <div class="radio radio-gray radio-inline">
		                                <input type="radio" name="image-opt" id="normal" class="image-opt" value="2">
		                                <label for="normal">
		                                    Normal
		                                </label>
		            				</div>
								</div>
                            </div>
                            <br clear="all">
							<div class="file-list clearfix">
							</div>
							<div class="image-list clearfix">
							</div>
            			</div>
	                </div>
				</div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
	                <a href="javascript:void(0);" type="submit" id="submit_addtask" class="btn btn-save btn-submit" value="Add Task">Add Task</a>
	            </div>
        	</form>
        </div>
    </div>
</div>
<!-- End Modal Add Task -->

<!-- Modal Add project -->
<div class="modal fade taskbanana " id="create_project_task_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
        		<?php echo e(csrf_field()); ?> <!-- dont remove -->
        		<input type="hidden" name="type_of_post" value="add_task" class="type_of_post">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&#9587;</span>
	                </button>
	                <h5 class="modal-title" id="">Create New Project</h5>
	            </div>
	            <div class="modal-body">
	            	<div class="form-group" id="at-new-project">
	            		<label class="control-label">New Project Title</label>
	            		<input type="text" name="project_title" class="form-control new_project_name" placeholder="New Project Title" />	
	            		<span class="invalid-feedback">Please input project title.</span>
	            	</div>
				</div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-cancel btn-cancel-new-project " data-dismiss="modal">Cancel</button>
	                <button type="button" class="btn btn-save btn-add-project" value="Add Task" id="at-add-project-btn">Add Project</button>
	            </div>
        </div>
    </div>
</div>
<!-- End Modal Add project -->

<!-- Modal Edit Task -->
<div class="modal fade taskbanana" id="edit_task_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        	<form id="add-task" method="POST" enctype="multipart/form-data" class="universal" action="<?php echo e(url('/edit_task')); ?>">
        		<?php echo e(csrf_field()); ?> <!-- dont remove -->
				<input type="hidden" name="type_of_post" value="edit_task" class="type_of_post">
				<input type="hidden" name="x" value="" class="edit_x">
				<input type="hidden" value="" class="edit_created_by">
				
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&#9587;</span>
	                </button>
	                <h5 class="modal-title" id="">Edit Task</h5>
	            </div>
	            <div class="modal-body">
            		<div class="form-group">
            			<label>Task Name/Subject</label>
						<input type="text" class="edit_subject form-control" name="title" placeholder="Enter Task Name/Subject" autocomplete="false" required>
						<span class="invalid-feedback">Please input task name/subject.</span>
            		</div>
            		<div class="form-group">
            			<label>Description</label>
            			<div class="texteditor-wrap">
				            <div class="texteditor-toolbar">
				                <div class="btn-group btn-group-sm right-buttons">
				                    <button type="button" class="btn pd-bold texteditor-btn" title="Bold">B</button>
				                    <button type="button" class="btn pd-italic texteditor-btn" title="Italic">I</button>
				                    <button type="button" class="btn pd-underline texteditor-btn" title="Underline">U</button>
				                    <button type="button" class="btn pd-strikethrough texteditor-btn" title="Strike Through">S</button>
				                </div>
				                <div class="btn-group btn-group-sm pull-right">
				                    <button type="button" class="btn pd-undo texteditor-btn" title="Undo"><i class="fa fa-undo"></i></button>
				                    <button type="button" class="btn pd-redo texteditor-btn" title="Redo"><i class="fa fa-repeat"></i></button>
				                </div>
				            </div>
				            <div contenteditable="true" class="texteditor-content edit_description_texteditor">
								<p><br></p>
				            </div>
				            <textarea name="description" class="hide edit_description"></textarea>
						</div>
						<span class="invalid-feedback">Please input description.</span>

            		</div>
            		<div class="form-group">
            			<label>Set Deadline</label>
            			<input type="date" name="deadline" class="edit_deadline form-control">
            		</div>
            		<div class="form-group">
            			<!-- <label>Assignee</label> -->
            			<!-- <div class="panel panel-default">
            				<div class="panel-heading"> -->
            					<!-- <div class="radio radio-gray radio-inline">
	                                <input type="radio" name="radio1" id="radio1" value="option1" checked="">
	                                <label for="radio1">
	                                    Business Team
	                                </label>
	                            </div> -->
	                            <?php if($get_my_personal_team): ?>
	                            <!-- <div class="radio radio-gray radio-inline">
	                                <input type="radio" name="radio1" id="radio2" value="option2">
	                                <label for="radio2">
	                                    Personal
	                                </label>
	                            </div> -->
	                            <?php endif; ?>
            				<!-- </div>
            				<div class="panel-body" id="business_panel_edit">
							</div>
							<div class="panel-body" id="personal_panel_edit" style="display: none;">
							</div> -->
            				<!-- <div class="panel-body"> -->
            					<!-- <div class="checkbox checkbox-gray checkbox-xs">
				                    <input id="assinee_1" class="styled" type="checkbox" checked>
				                    <label for="assinee_1">All Members</label>
				                </div> -->
				               	<?php $__currentLoopData = $get_my_business_team; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				               		<!-- <div class="checkbox checkbox-gray checkbox-xs">
					                    <input id="assinee_<?php echo e($value->id); ?>" class="styled" name="task_users[]" value="<?php echo e($value->id); ?>" type="checkbox">
					                    <label for="assinee_<?php echo e($value->id); ?>"><?php echo e($value->firstname); ?> <?php echo e($value->lastname); ?></label>
					                </div> -->
				               	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            				<!-- </div> -->
							<!-- </div> -->
							
							<div class="form-group p-relative assignee-div">
								<label>Search Assignee*</label>
								<div class="btn-group" style="display: unset;">
									<input type="text" class="form-control" id="search_member_edit" placeholder="Search Assignee Name">
									<span class="btn btn-secondary dropdown-toggle hide" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></span>
									<div class="dropdown-menu dropdown-menu-right member-results open" style="position: relative;width: 100%;">
									</div>
								</div>
								<div class="assignee-list clearfix hide"></div>
								<div class="clearfix hide" id="data-lists"></div>
							</div>
            			</div>
            		<div class="form-group">
            			<br clear="all">
            			<div class="file-attachment">
            				<div class="flex middle">
		            			<label for="edit_file" class="add-file-button">Add Files</label>
								<input type="file" id="edit_file" name="images[]" class="edit-file-custom-input" multiple style="display: none;">
								<div class="image-optimization flex middle" style="display: none;">
									<span class="optimization-label">Image Optimization</span>
									<div class="radio radio-gray radio-inline">
		                                <input type="radio" name="image-opt" id="low" class="image-opt" value="1" checked="">
		                                <label for="low">
		                                    Low
		                                </label>
	               					</div>
		                            <div class="radio radio-gray radio-inline">
		                                <input type="radio" name="image-opt" id="normal" class="image-opt" value="2">
		                                <label for="normal">
		                                    Normal
		                                </label>
	            					</div>
								</div>
                            </div>
                            <br clear="all">
							<div class="file-list clearfix">
							</div>
							<div class="image-list clearfix">
							</div>
            			</div>
	                </div>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
	                <button type="submit" id="submit_edittask" class="btn btn-save btn-submit" value="Add Task">Update Task</button>
	            </div>
        	</form>
        </div>
    </div>
</div>
<!-- End Modal Edit Task -->

<!-- Modal View Threads -->
<div class="modal fade taskbanana" id="view_thread_modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&#9587;</span>
                </button>
                <h5 class="modal-title"><p class="modal-project"></p></h5>
				<br>
                <div class="modal-tools">
            		<!-- <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="Edit Task"><img class="edit-icon" src="<?php echo e(asset('images/icons/view-modal-edit.png')); ?>"></a>
            		<a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="Delete Task"><img class="delete-icon" src="<?php echo e(asset('images/icons/view-modal-delete.png')); ?>"></a> -->
            		<div class="btn-group card-menu">
						<span class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<img src="<?php echo e(url('/')); ?>/images/icons/card-ellipsis.png">
						</span>
						<div class="dropdown-menu dropdown-menu-right">
							<ul>
								<li>
									<a href="javascript:void(0);" class="dropdown-item edit-task" data-creator="" data-task_name="" data-task_id=""><p>Edit</p></a>
								</li>
								<li>
									<a href="javascript:void(0);" class="dropdown-item remove-task" ><p>Delete</p></a>
								</li>
								<li>
									<a href="javascript:void(0);" class="dropdown-item co_author" data-task_id=""><p>Add Co-Author</p></a>
								</li>
								<li>
									<a href="javascript:void(0);" class="dropdown-item leave_task" data-task_id=""><p>Leave Task</p></a>
								</li>
							</ul>
						</div>
					</div>
            	</div>
            </div>
            <div class="modal-body">
            	<div class="form-group">
            		<div class="deadline">
						<img src="<?php echo e(asset('images/icons/card-date.png')); ?>">
						<span class="badge-text" >Deadline: <span id="deadline"></span></span>
					</div>
            		<h3 id="task_title" class="title"></h3>
            		<p class="created-by">(Created by: <span id="created_by_thread_span"></span>)</p>
            		<p class="date-created">(Date created: <span id="">24-02-2019</span>)</p>
					<div class="description rmore-modal" id="description">
						
					</div>
            	</div>
            	<div class="form-group">
            		<div class="assignee" id="assignee">
            			<!-- ASSIGNEE HERE.. -->
            		</div>
            	</div>
            	<div class="form-group" id="task-file-attachment">
            		
            	</div>
        		<div class="form-group">
        			<form method="POST" id="reply_form" enctype="multipart/form-data">
        				<div class="texteditor-wrap">
            				<textarea class="hide" name="reply" id="reply_text_area"></textarea>
				            <div class="texteditor-toolbar">
				                <div class="btn-group btn-group-sm right-buttons">
				                    <button type="button" class="btn pd-bold texteditor-btn" title="Bold">B</button>
				                    <button type="button" class="btn pd-italic texteditor-btn" title="Italic">I</button>
				                    <button type="button" class="btn pd-underline texteditor-btn" title="Underline">U</button>
				                    <button type="button" class="btn pd-strikethrough texteditor-btn" title="Strike Through">S</button>
				                </div>
				                <div class="btn-group btn-group-sm pull-right">
				                    <button type="button" class="btn pd-undo texteditor-btn" title="Undo"><i class="fa fa-undo"></i></button>
				                    <button type="button" class="btn pd-redo texteditor-btn" title="Redo"><i class="fa fa-repeat"></i></button>
				                </div>
				            </div>
				            <div contenteditable="true" class="texteditor-content">
								<p><br></p>
				            </div>
				        </div>
			            <span class="invalid-feedback">Please input text.</span>

        				<!-- <textarea class="form-control" name="reply" id="reply_text_area"></textarea>
        				<br>
        				<input type="file"  name="images[]" class="form-control file_reply">
        				<input type="file"  name="images[]" class="form-control file_reply"> -->
        			
	        			<!-- <span class="invalid-feedback">Please input task description.</span> -->
				        <div class="file-attachment">
	        				<div class="flex middle space-bet">
			        			<!-- APPEND REPLY BUTTON -->
			        			<div id="reply_div"><!-- dont remove id kate haha --></div>
		            			<label for="show_add_file" class="add-file-button"><img src="images/icons/add-file.png"> Add Files</label>
								<input type="file" id="show_add_file" name="images[]" class="file-custom-input" multiple style="display: none;">
	                        </div>
					        <!-- <button type="button" class="btn btn-xs" id="btn_request_completion"><?php echo e(__('translation.request_for_completion')); ?></button> -->
					        <div id="task_complete_div"></div>
	                        <br clear="all">
							<div class="file-list clearfix"></div>
							<div class="image-list clearfix"></div>
	        			</div>

		        		<!-- div for all replies with image -->
		        		<div class="comment-section clearfix scrollbar" id="thread_comments"></div>
		        	</form>
        		</div>
        		<div class="comment-section clearfix scrollbar">
					<div class="comment_loadmore_container text-center">
						<button class="comment_loadmore <?php echo e($current_user->color); ?>-bg-primary" current_page="" last_page="">Load More</button>
					</div>
				</div>
            </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-cancel" data-dismiss="modal">Close</button>
                <span id="task_complete_div"></span>
            </div> -->
        </div>
    </div>
</div>
<!-- End Modal View Threads -->

<!-- Modal Members -->
<div class="modal fade taskbanana" id="member_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        	<form method="POST" enctype="multipart/form-data">
        		<!-- <input type="hidden" name="type_of_post" value="add_task" class="type_of_post"> -->
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&#9587;</span>
	                </button>
	                <h5 class="modal-title">Members</h5>
	            </div>
	            <div class="modal-body">
            		<ul class="nav nav-pills flex">
            			<!-- <li class="nav-item active" id="business_nav">
            				<a class="nav-link" id="business_tab" data-toggle="pill" href="#pills_business" role="tab" aria-controls="pills_business" aria-selected="true">Business</a>
            			</li> -->
            			<!-- <li class="nav-item" id="personal_nav">
            				<a class="nav-link" id="personal_tab" data-toggle="pill" href="#pills_personal" role="tab" aria-controls="pills_personal" aria-selected="false" onclick="my_personal_team_click()">Personal</a>
            			</li> -->
            			<!-- <li class="nav-item">
            				<a class="nav-link" id="members_tab" data-toggle="pill" href="#pills_members" role="tab" aria-controls="pills_members" aria-selected="false">Members</a>
            			</li> -->
            		</ul>
            		<br clear="all">
            		<div class="tab-content member-list scrollbar">
            			<!-- Business Section -->
            			<div class="tab-pane fade active in" id="pills_business" role="tabpanel" aria-labelledby="business_tab">
		            		<div class="search-bar text-center">
		            			<input type="text" name="" placeholder="Search Accounts" class="search-engine text-center" onkeyup="kmsearch(this, '#business_list>li>.user-info')">
		            		</div>
		            		<br clear="all">
            				<ul id="business_list" class="search-list">
            					
            				</ul>
            			</div>
            			<!-- End Business Section -->
            			<!-- Personal Section -->
            			<div class="tab-pane fade" id="pills_personal" role="tabpanel" aria-labelledby="personal_tab">
            				<div class="search-bar text-center">
		            			<input type="text" name="" placeholder="Search Accounts" class="search-engine text-center" onkeyup="kmsearch(this, '#personal_list>li>.user-info')">
		            		</div>
		            		<br clear="all">
            				<ul id="personal_list" class="search-list">
            					
            				</ul>
            			</div>
            			<!-- End Personal Section -->
            			<!-- Member Section -->
            			<div class="tab-pane fade" id="pills_members" role="tabpanel" aria-labelledby="members_tab">
            				<div class="search-bar text-center">
		            			<input type="text" name="" placeholder="Search Accounts" class="search-engine text-center" onkeyup="kmsearch(this, '#members_list>li>.user-info')">
		            		</div>
		            		<br clear="all">
            				<ul id="members_list" class="search-list">
            					<li class="flex middle" >
            						<img class="user-image" src="<?php echo e(asset('images/sample/user-pic-1.png')); ?>">
            						<div class="user-info flex space-bet" aria-data="Joan Magpayo">
            							<p>Joan Magpayo</p>
            							<div class="tools flex">
            								<a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="Delete"><img src="<?php echo e(asset('images/icons/minus-icon.png')); ?>"></a>
            							</div>
            						</div>
            					</li>
            					<li class="flex middle">
            						<img class="user-image" src="<?php echo e(asset('images/sample/user-pic-2.png')); ?>">
            						<div class="user-info flex space-bet" aria-data="Jayson Kevin Montemayor">
            							<p>Jayson Kevin Montemayor</p>
            							<div class="tools flex">
            								<a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="Delete"><img src="<?php echo e(asset('images/icons/minus-icon.png')); ?>"></a>
            							</div>
            						</div>
            					</li>
            					<li class="flex middle">
            						<img class="user-image" src="<?php echo e(asset('images/sample/user-pic-3.png')); ?>">
            						<div class="user-info flex space-bet" aria-data="Mia Taborlupa">
            							<p>Mia Taborlupa</p>
            							<div class="tools flex">
            								<a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="Add"><img src="<?php echo e(asset('images/icons/plus-icon.png')); ?>"></a>
            							</div>
            						</div>
            					</li>
            					<li class="flex middle">
            						<img class="user-image" src="<?php echo e(asset('images/sample/user-pic-4.png')); ?>">
            						<div class="user-info flex space-bet" aria-data="Charmaine Peralta">
            							<p><span>Charmaine Peralta</span></p>
            							<div class="tools flex">
            								<a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="Add"><img src="<?php echo e(asset('images/icons/plus-icon.png')); ?>"></a>
            							</div>
            						</div>
            					</li>
            				</ul>
            			</div>
            			<!-- End Member Section -->
            		</div>
	            </div>
        	</form>
        </div>
    </div>
</div>
<!-- End Modal Members -->

<!-- Add New Member -->
<div class="modal fade taskbanana" id="add_new_member_modal"  tabindex="-1" role="dialog" aria-hidden="true" >
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&#9587;</span>
				</button>
				<h5 class="modal-title" id="">Add New Member</h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<form action="<?php echo e(url('/add_new_member')); ?>" method="POST">
						<?php echo e(csrf_field()); ?> <!-- dont remove -->
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div class="form-group" id="at-email">
								<label class = "control-label">Email</label>
								<input type="email" name="email" class="form-control">
								<span class="invalid-feedback">Please input email.</span>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div class="form-group" id="at-fname">
								<label class = "control-label">First Name</label>
								<input type="text" name="firstname" class="form-control">
								<span class="invalid-feedback">Please input first name.</span>
							</div>
						</div>
					<div class="col-lg-12 col-md-12 col-xs-12">
						<div class="form-group" id="at-lname">
							<label class = "control-label">Last Name</label>
							<input type="text" name="lastname" class="form-control">
							<span class="invalid-feedback">Please input last name.</span>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
	            <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
	            <button type="submit" class="btn btn-save btn-submit" value="Add New Member" id="at-add-member-btn">Submit</button>
	        </div>
	        </form>
		</div>
	</div>
</div>
<!-- End Add New Member -->

<!-- modal question for business or personal use -->
<div class="modal fade taskbanana" id="business_or_personal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&#9587;</button> 
				<h4 class="modal-title">Message</h4>                                                             
			</div> 
			<div class="modal-body text-center">
				<label class="radio-inline "><input type="radio" onclick="add_new_member()" name="optradio">Business Related ?</label>
				<label class="radio-inline"><input type="radio" onclick="add_new_member_personal()" name="optradio">Own Account ?</label>
			</div>   
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>                               
			</div>
		</div>                                                                       
	</div>                                          
</div>
<!-- modal question for business or personal use -->

<!-- File Viewer-->
<div id="fileViewer" class="carousel file-viewer slide hide" data-ride="carousel" data-interval="false">
	<div class="file-viewer-header js-close-viewer">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="z-index: 1">
			<span aria-hidden="true">&#9587;</span>
		</button>
	</div>
	<div class="file-viewer-underlay js-close-viewer"></div>

	<!-- Indicators -->
	<ol class="carousel-indicators">
		<!-- <li data-target="#fileViewer" data-slide-to="0" class="active"></li>
		<li data-target="#fileViewer" data-slide-to="1"></li>
		<li data-target="#fileViewer" data-slide-to="2"></li> -->
	</ol>

	<!-- Wrapper for slides -->
	<div class="carousel-inner">
		<!-- <div class="item frame js-close-viewer active">
			<div class="frame-preview-wrapper scroll-bar">
				<div class="frame-preview">
					<img src="images/sample/1.png" alt="Los Angeles">
				</div>
			</div>
			<div class="file-viewer-overlay flex center middle">
				<div class="file-details js-stop active" item="0" card="0">
					<p>
						<a href="" target="_blank">
							<i class="fa fa-external-link" aria-hidden="true"></i>Open in New Tab
						</a>
						<a href="" download="">
							<i class="fa fa-download" aria-hidden="true"></i>Download
						</a>
						<a href="javascript:void(0);" class="rotate-image">
							<i class="fa fa-repeat" aria-hidden="true"></i>Rotate
						</a>
					</p>
				</div>
			</div>
		</div> -->
	
	</div>

	<!-- Left and right controls -->
	<a class="left carousel-control" href="#fileViewer" data-slide="prev">
		<span class="arrow"></span>
	</a>
	<a class="right carousel-control" href="#fileViewer" data-slide="next">
		<span class="arrow"></span>
	</a>
</div>
<!-- End File Viewer -->

<!-- error message for storage -->
<div class="modal taskbanana fade" id="storage_error_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&#9587;</span>
        </button>
      </div>
      <div class="modal-body">
        <h3 class="text-center">You already used all of your MB storage !</h3>

      </div>
      <div class="modal-footer">
      	<a href="<?php echo e(url('payment')); ?>" class="text-center btn btn-success "> Buy additional Storage </a>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- end modal -->

<!-- modal for list of to be accepted members -->
<div class="modal taskbanana fade" id="accept_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
      	<h4 class="text-center">Pending Members</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          	<span aria-hidden="true">&#9587;</span>
        </button>
      </div>
      <div class="modal-body">
       	<table class="table table-bordered">
       		<thead>
       			<th>Name</th>
       			<th>Action</th>
       		</thead>
       		<tbody id="accept_tbody">
       		</tbody>
       	</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- end modal -->

<!-- add new member on existing task -->
<div class="modal taskbanana fade" id="add_new_member_modal_existing" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&#9587;</span>
				</button>
				<h5 class="modal-title" id="">Add New Member</h5>
			</div>
			<div class="modal-body">
				<table class="table table-bordered">
					<thead>
						<th>Name</th>
						<th>Action</th>
					</thead>
					<tbody id="add_new_member_table">
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-cancel" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- end add new member -->