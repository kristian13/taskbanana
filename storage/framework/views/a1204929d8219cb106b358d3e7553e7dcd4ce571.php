<?php $__env->startSection('content'); ?>

	<div class="filter-section archive-page">
		<div class="filter-header flex space-bet">
			<div class="max-container">
				<a class="back-button flex middle" href="<?php echo e(url('task')); ?>"><img src="<?php echo e(URL::asset('/public/images/final/profile-left-arrow.png')); ?>"> Back to Task</a>
			</div>
		</div>
	</div>
</div>
<!-- End Main Header -->

<!-- Main Container -->
<div class="main-container archive-page max-container flex center">
	<div class="inner-container flex col">
		<ul class="nav nav-pills flex space-bet" id="pills-tab" role="tablist">
			<li class="nav-item active">
				<a class="nav-link" id="pills-all-tab" data-toggle="pill" href="#pills-all" role="tab" aria-controls="pills-all" aria-selected="true"><p>All</p></a>
			</li>
			<li class="nav-item">
				<a class="nav-link" id="pills-finished-tab" data-toggle="pill" href="#pills-finished" role="tab" aria-controls="pills-finished" aria-selected="false"><p>Finished</p></a>
			</li>
			<li class="nav-item">
				<a class="nav-link" id="pills-dismissed-tab" data-toggle="pill" href="#pills-dismissed" role="tab" aria-controls="pills-dismissed" aria-selected="false"><p>Dismissed</p></a>
			</li>
		</ul>
		<div class="tab-content flex" id="pills-tabContent">
			<div class="tab-pane fade flex col active in" id="pills-all" role="tabpanel" aria-labelledby="pills-all-tab">
				<div class="search">
					<form method="GET">
						<input type="hidden" name="all" value="1">
						<div class="col-lg-6 col-md-6 col-xs-12">
							<div class="form-group">
								<select class="form-control select-type "  >
									<option value="1" <?php echo e(request()->task_title_search  ? 'selected':''); ?> >Task Title</option>
									<option value="2" <?php echo e(request()->project  ? 'selected':''); ?> >Project Title</option>
								</select>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-12">
							<div class="form-group text-box-field" id="text-box-field">
								<input type="text" name="task_title_search" class="form-control task_title_search_archive" placeholder="Search Task Title">
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-12">
							<div class="form-group pull-left">
								<input type="submit" value="Search" class="btn <?php echo e($current_user->color); ?>-bg-primary">
								<?php if(request()->project  ||  request()->task_title_search): ?>
								<a href="<?php echo e(url('archive')); ?>" class="btn btn-primary">Reset Search</a>
								<?php endif; ?>
							</div>
						</div>
					</form>
				</div>
				<div class="archive-list flex col scrollbar">
					<?php $__currentLoopData = $archive; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					
					<div class="archive-item flex space-bet archive-number_<?php echo e($row->task_id); ?>">
						<div class="left-div">
							<div class="top flex space-bet">
								<div class="title"><?php echo e($row->title); ?></div>
								<div class="date-created">Created <?php echo e($row->created_at); ?></div>
							</div>
							<div class="created-by">By <span><?php echo e($row->author->firstname); ?></span></div>
							<div class="description">
								<?php echo $row->description; ?>

							</div>
						</div>
						<div class="right-div flex col center">
							<button class="view-all" data-card="working" data-task_id="<?php echo e($row->task_id); ?>" data-task_creator="<?php echo e($row->created_by); ?>" data-task_status="I"  onclick="view_threads( <?php echo e($row->task_id); ?> , <?php echo e($row->created_by); ?> , '<?php echo e($row->task_status); ?>' , true , '<?php echo e(__('translation.deadline')); ?>' , '<?php echo e(__('translation.task_complete')); ?>'  , '<?php echo e(__('translation.reply')); ?>'  );">View</button>
							<button class="unarchive" onclick="unarchive( <?php echo e($row->task_id); ?> )">Unarchive</button>
						</div>
					</div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
				<?php if( $archive !== null && $archive->total() > 5): ?>
				<div class="text-center" style="margin-top: 2px;">
					<a href="javascript:void(0);" class="btn btn-sm search-btn load_more_archive" data-last_page="<?php echo e($archive->lastPage()); ?>" data-current_page="<?php echo e($archive->currentPage()); ?>" >View More</a>
				</div>
				<?php endif; ?>
			</div>
			<div class="tab-pane fade flex col" id="pills-finished" role="tabpanel" aria-labelledby="pills-finished-tab">
				<div class="search">
					<form method="GET">
						<input type="hidden" name="finished" value="1">
						<div class="col-lg-6 col-md-6 col-xs-12">
							<div class="form-group">
								<select class="form-control select-type "  >
									<option value="1" <?php echo e(request()->task_title_search  ? 'selected':''); ?> >Task Title</option>
									<option value="2" <?php echo e(request()->project  ? 'selected':''); ?> >Project Title</option>
								</select>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-12">
							<div class="form-group text-box-field" id="text-box-field">
								<input type="text" name="task_title_search" class="form-control task_title_search_archive" placeholder="Search Task Title">
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-12">
							<div class="form-group pull-left">
								<input type="submit" value="Search" class="btn <?php echo e($current_user->color); ?>-bg-primary">
								<?php if(request()->project  ||  request()->task_title_search): ?>
								<a href="<?php echo e(url('archive')); ?>" class="btn btn-primary">Reset Search</a>
								<?php endif; ?>
							</div>
						</div>
					</form>
				</div>
				<div class="archive-list archive-list-finished-div flex col">
					<div class="archive-item flex space-bet">
						<div class="left-div">
							<div class="top flex space-bet">
								<div class="title">Test Task 7</div>
								<div class="date-created">Created 2018-10-03</div>
							</div>
							<div class="created-by">By <span>Conna Walker</span></div>
							<div class="description">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
							</div>
						</div>
						<div class="right-div flex col center">
							<button class="view-all">View</button>
							<button class="unarchive">Unarchive</button>
						</div>
					</div>
				</div>
				<div class="text-center" id="view_more_finished_div" style="margin-top: 2px;">
					<a href="javascript:void(0);" class="btn btn-sm search-btn load_more_archive_finished">View More</a>
				</div>
			</div>
			<div class="tab-pane fade flex col" id="pills-dismissed" role="tabpanel" aria-labelledby="pills-dismissed-tab">
				<div class="search">
					<form method="GET">
						<input type="hidden" name="dismissed" value="1">
						<div class="col-lg-6 col-md-6 col-xs-12">
							<div class="form-group">
								<select class="form-control select-type "  >
									<option value="1" <?php echo e(request()->task_title_search  ? 'selected':''); ?> >Task Title</option>
									<option value="2" <?php echo e(request()->project  ? 'selected':''); ?> >Project Title</option>
								</select>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-12">
							<div class="form-group text-box-field" id="text-box-field">
								<input type="text" name="task_title_search" class="form-control task_title_search_archive" placeholder="Search Task Title">
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-12">
							<div class="form-group pull-left">
								<input type="submit" value="Search" class="btn <?php echo e($current_user->color); ?>-bg-primary">
								<?php if(request()->project  ||  request()->task_title_search): ?>
								<a href="<?php echo e(url('archive')); ?>" class="btn btn-primary">Reset Search</a>
								<?php endif; ?>
							</div>
						</div>
					</form>
				</div>
				<div class="archive-list archive-list-dismissed-div flex col">
					<div class="archive-item flex space-bet">
						<div class="left-div">
							<div class="top flex space-bet">
								<div class="title">Test Task 7</div>
								<div class="date-created">Created 2018-10-03</div>
							</div>
							<div class="created-by">By <span>Conna Walker</span></div>
							<div class="description">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
							</div>
						</div>
						<div class="right-div flex col center">
							<button class="view-all">View</button>
							<button class="unarchive">Unarchive</button>
						</div>
					</div>
				</div>
				<div class="text-center" id="view_more_dismissed_div" style="margin-top: 2px;">
					<a href="javascript:void(0);" class="btn btn-sm search-btn load_more_archive_dismissed">View More</a>
				</div>
			</div>
		</div>
	</div>
	<!-- <table class="table table-hover hide">
		<thead>
			<tr>
				<th>Task Name</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $__currentLoopData = $archive; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<tr>
				<td><?php echo e($row->title); ?></td>
				<td>
					<a href="javascript:void(0);" class="btn btn-xs btn-info view_threads  "  data-card="working" data-task_id="<?php echo e($row->task_id); ?>" data-task_creator="<?php echo e($row->created_by); ?>" data-task_status="I"  onclick="view_threads( <?php echo e($row->task_id); ?> , <?php echo e($row->created_by); ?> , '<?php echo e($row->task_status); ?>' , true , '<?php echo e(__('translation.deadline')); ?>' , '<?php echo e(__('translation.task_complete')); ?>'  , '<?php echo e(__('translation.reply')); ?>'  );"><?php echo e(__('translation.show_threads')); ?></a>
				</td>
			</tr>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</tbody>
	</table> -->
	<br clear="all">
	<!-- <?php echo $archive->links(); ?> -->
</div>

<!-- End Main Container -->
<?php echo $__env->make('modals.task-modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
<?php $__env->stopSection(); ?>


<?php echo $__env->make('theme.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>