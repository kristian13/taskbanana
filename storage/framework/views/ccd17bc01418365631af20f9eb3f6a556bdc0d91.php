<!-- To Do List Section -->
<div class="to-do">
	<div class="to-do-wrapper flex col">
		<div class="top flex">
			<button class="panel-buttons active" data-target="#my_todo_panel">My To Do's</button>
			<button class="panel-buttons" data-target="#project_list_panel">Project List</button>
			<button class="panel-buttons hide" data-target="#reminders_panel">Reminders</button>
		</div>
		<div class="bottom flex">
			<div class="my-todo panel-body show" id="my_todo_panel">
				<?php 
					$countCompleted = $User->todo_list()
	                                	->Where('is_active',1)
	                                	->Where('todo_status','A')
	                                	->where('status',1)->get(); //task completed

	                $countAll = $User->todo_list()
	                                	->Where('is_active',1)
	                                	->Where('todo_status','A')->get();
	                                	
					if(count($countAll) > 0){
						$currentPercent = (count($countCompleted) / count($countAll));
					}else {
						$currentPercent = 0;
					}

					$currentPercent = round(($currentPercent * 100)); 	
				?>

				<!-- <div class="checklist-progress">
					<?php  $completed_task = $User->todo_list()
											->Where('is_active',1)
											->Where('todo_status',"A")
											->Where('status',1) //task completed
											->orderBy('created_at', 'desc')
											->get()->count(); ?>

					<p class="checklist-label">Progress</p>
					<div class="checklist-progress-bar">
						<div class="current-percentage" style="width: <?php echo e($currentPercent); ?>%;"></div>
					</div>
					<p class="percentage-count"><?php echo e($currentPercent); ?>%</p>
					<a href="javascript:void(0);" class="hide-completed pull-right current"><img src="images/final/todo-hide.png">Hide completed items (<span class="completed-items"><?php echo e($completed_task); ?></span>)</a>
					<a href="javascript:void(0);" class="show-completed hide pull-right"><img src="images/final/todo-show.png">Show checked items <span class="checked-items">(1)</span></a>
					<br clear="all">
					<p class="checklist-completed-text hide quiet js-completed-message">Everything in this checklist is complete!</p>
				</div>
 -->

				
				<button class="create-btn flex middle  my_todo_btn">
					<div class="add-icon">&#9587;</div><p>Create New</p>
				</button>
				<form class="hide my_todo">
					<div class="flex">
						<div class="line-input left-icon">
							<label>Create New</label>
							<span id="is_new" data-status=""></span>
							<input type="text" id="to_do_name">
						</div>
						<div class="line-input right-icon">
							<label>Input Date</label>
							<span class="date-native">
									<input type="date" class="native-date datepicker hasDatepicker" id="todo_date"  onchange="inputDateFormater(this)" required>
									<label data-date="" data-date-format="MM/DD/YYYY" for="todo_date"></label>
							</span>
						</div>
					</div>
					<button type="submit" class="" id="to_do_add_btn">Add to List</button>
					<button type="button" class="cancel-btn">Cancel</button>
				</form>
				<div class="todo-list flex">
					<div class="checklist-body scrollbar">
						<div class="add-section clearfix">
							<!-- <form class="flex">
								<div class="line-input left-icon">
									<label>Create New</label>
									<input type="text" id="to_do_name">
								</div>
								<div class="line-input right-icon">
									<label>Input Deadline</label>
									<input type="text" onfocus="(this.type='date')" onblur="if(this.value==''){this.type='text'}">
								</div>
							</form>
							<br clear="all">
							<form class="form-inline flex">
								<input type="text" class="form-control" id="to_do_name" placeholder="Add to do">
								<input type="date" class="form-control datepicker hasDatepicker" id="todo_date" data-date-format="dd/mm/yyyy" placeholder="Date" required>
								<button type="submit" class="blue btn-inset" id="to_do_add_btn">Add</button>
							</form> -->
						</div>
						<div class="checklist-list" id="todo_div">
							<?php  
								$todo_list = $User->todo_list()
									->Where('is_active',1)
									->Where('todo_status',"A")
									->orderBy('created_at', 'desc')
									->paginate(5);

								
							?> 
							<?php if(!$todo_list->isEmpty()): ?>
								<?php $__currentLoopData = $todo_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $todo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<div class="checkbox checkbox-gray2 checkbox-sm todo_div_<?php echo e($todo->todo_id); ?> <?php echo e($todo->status > 0 ? 'task_finished' : ''); ?>">

					                    <input id="task_<?php echo e($todo->todo_id); ?>" 
					                    		class="styled todo_click <?php echo e($todo->status > 0 ? 'task_finished' : ''); ?>" 
					                    		type="checkbox" 
					                    		data-todo_id="<?php echo e($todo->todo_id); ?>" 
					                    		value="<?php echo e($todo->todo_id); ?>" 
					                    		name="todo[]" 
					                    		<?php echo e($todo->status > 0 ? 'checked' : ''); ?>>

					                    <label for="task_<?php echo e($todo->todo_id); ?>"></label>
					                    <p class="todo_text_<?php echo e($todo->todo_id); ?>"><?php echo e($todo->todo_name); ?></p>
					                    <input type="text" value="<?php echo e($todo->todo_name); ?>" class="input_<?php echo e($todo->todo_id); ?> hide" style="position: inherit;">
					                    <span class="todo_date_<?php echo e($todo->todo_id); ?>" data-date="<?php echo e($todo->date_from); ?>"><?php echo e(\Carbon\Carbon::parse($todo->date_from)->format('M d, Y')); ?></span>
					                    
					                    <!-- TOOLS -->
					                    <div class="todo-tools">
					                    	<div class="edit_todo_<?php echo e($todo->todo_id); ?>">
						                    	<a href="javascript:void(0);">
						                    		<img class="pink edit_todo" data-id="<?php echo e($todo->todo_id); ?>" src="<?php echo e(asset('images/final/my-todo-edit.png')); ?>">
						                    	</a>
						                    	<a href="javascript:void(0);">
						                    		<img class="pink delete_todo" data-id="<?php echo e($todo->todo_id); ?>" src="<?php echo e(asset('images/final/my-todo-delete.png')); ?>">
						                    	</a>
						                    </div>
						                    <div class="save_todo_<?php echo e($todo->todo_id); ?> hide">
						                    	<a href="javascript:void(0);">
				                    				<img class="pink save_todo" data-action="save" data-id="<?php echo e($todo->todo_id); ?>" src="<?php echo e(asset('images/final/my-todo-save.png')); ?>">
						                    	</a>
						                    	<a href="javascript:void(0);">
					                    			<img class="pink cancel_todo" data-action="cancel" data-id="<?php echo e($todo->todo_id); ?>" src="<?php echo e(asset('images/final/my-todo-delete.png')); ?>">
						                    	</a>
						                    </div>
					                    </div>
								    </div>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									<div class="text-center" style="margin-top: 2px;">
										<a href="javascript:void(0);" class="btn btn-sm search-btn load_more_todo <?php echo e($current_user->color); ?>-bg-primary" data-last_page="<?php echo e($todo_list->lastPage()); ?>" data-current_page="<?php echo e($todo_list->currentPage()); ?>" >View More</a>
									</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>

			<div class="projects panel-body hide" id="project_list_panel">
				<button class="create-btn flex middle ">
					<div class="add-icon">&#9587;</div><p>Create New Project</p>
				</button>
				<form class="hide">
					<div class="flex">
						<div class="line-input left-icon">
							<label>Create New Project</label>
							<input type="text" id="project_name" autocomplete="off">
						</div>
					</div>
					<a href="javascript:void(0);" onclick="addproject()">Add Project</a>
					<button type="button" class="cancel-btn">Cancel</button>
				</form>
				<div class="todo-list">
					<div class="project-body scrollbar">
						<div class="project-list">
							<?php if(!$categories->isEmpty()): ?>
							<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="project-item flex center middle">
								<p class="project-title flex middle"><img class="project-icon" src="<?php echo e(asset('images/final/project-icon.png')); ?>"> <?php echo e($value->category_name); ?> </p>
								<p class="project-percent"></p>
								<!-- TOOLS -->
			                    <div class="todo-tools">
			                    	<div class="edit_todo_">
				                    	<a href="javascript:void(0);">
				                    		<img class="pink" data-id="" src="<?php echo e(asset('images/final/my-todo-edit.png')); ?>">
				                    	</a>
				                    	<a href="javascript:void(0);">
				                    		<img class="pink" data-id="" src="<?php echo e(asset('images/final/my-todo-delete.png')); ?>">
				                    	</a>
				                    </div>
			                    </div>
							</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>

			<div class="reminders panel-body hide" id="reminders_panel">
				<button class="create-btn flex middle" data-toggle="modal" data-target="#reminder_modal">
					<div class="add-icon">&#9587;</div><p>Create Reminder</p>
				</button>
				<div class="todo-list">
					<div class="reminder-body scrollbar">
						<?php $__currentLoopData = $User->get_reminders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							
							<div class="reminder-item flex col">
								<div class="reminder-list">
									<div class="flex up">
										<p class="reminder-title flex middle"><?php echo e($r->reminder->reminder_name); ?></p>
										<!-- TOOLS -->
										<div class="todo-tools">
											<div class="edit_todo_">
												<a href="javascript:void(0);" class="toggle-btn">
													<img class="pink" data-reminder_id="" src="images/final/my-todo-arrow.png">
												</a>
												<a href="javascript:void(0);">
													<img class="pink" data-reminder_id="<?php echo e($r->reminder->id); ?>" data-toggle="modal" data-target="#edit_reminder_modal" src="images/final/my-todo-edit.png">
												</a>
												<a href="javascript:void(0);">
													<img class="pink" data-reminder_id="<?php echo e($r->reminder->id); ?>" src="images/final/my-todo-delete.png">
												</a>
											</div>
										</div>
									</div>
									<p class="reminder-description">
									<?php echo e($r->reminder->description); ?>

										<!-- Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	 -->
									</p>
								</div>
							</div>
							
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

