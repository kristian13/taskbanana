<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('users', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->string('firstname')->nullable();
        //     $table->string('lastname')->nullable();
        //     $table->string('email')->unique();
        //     $table->string('address')->nullable();
        //     $table->string('city')->nullable();
        //     $table->string('state')->nullable();
        //     $table->string('zipcode')->nullable();
        //     $table->tinyInteger('country_id')->unsigned()->default('0');
        //     $table->tinyInteger('account_type_id')->unsigned()->index();
        //     $table->tinyInteger('business_nature_child_id')->unsigned()->index();
        //     $table->string('business_name');
        //     $table->string('timezone')->nullable();
        //     $table->tinyInteger('profile_image_id')->unsigned()->index()->default('0');
        //     $table->tinyInteger('subscription_status')->unsigned()->default('1');
        //     $table->tinyInteger('parent_business_owner_id')->unsigned()->default('0');
        //     $table->string('password');
        //     $table->rememberToken();
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       // Schema::dropIfExists('users');
    }
}
