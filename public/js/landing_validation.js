$(document).on('click', '[data-target="#sign_up_modal"], [data-target="#login_modal"]', function() {
	$('#sign_up_modal, #login_modal').find('.has-error').removeClass('has-error');
});

// METHOD RESET ALL DATA WHEN CLICK CANCEL BTN
$(document).on('click', '.btn-cancel', function(){
  $(':input').val('');
  $("#validation-errors-signup").empty();
});

$(document).on('click', '.donthaveaccnt_signup', function(){
  $('#login_modal').modal('toggle');

  setTimeout(function(){
    $('#sign_up_modal').modal('toggle');
  }, 400);

});


// METHOD REMEMBER PASSWORD
let rmCheck = document.getElementById("rememberMe"),
    emailInput = document.getElementById("email");

if (localStorage.checkbox && localStorage.checkbox != "") {
  rmCheck.setAttribute("checked", "checked");
  emailInput.value = localStorage.username;
} else {
  rmCheck.removeAttribute("checked");
  emailInput.value = "";
}

function lsRememberMe() {
  if (rmCheck.checked && emailInput.value != "") {
    localStorage.username = emailInput.value;
    localStorage.checkbox = rmCheck.value;
  } else {
    localStorage.username = "";
    localStorage.checkbox = "";
  }
}

// METHOD VISIBLE PASSWORD
function showPassword() {
  var pass = document.getElementById('password');
  var signup_pass = document.getElementById('password_signup');
  var signup_cpass = document.getElementById('password_confirmation');

  var eye_closed = $('.eye_closed_icon');
  var eye_open = $('.eye_open_icon');

  if (pass.type === "password" || signup_pass.type === "password_signup" || signup_cpass.type === "password_confirmation") {
      pass.type = "text";
      signup_pass.type = "text";
      signup_cpass.type = "text";
      eye_open.removeClass('hidden');
      eye_closed.addClass('hidden');
  } else {
      pass.type = "password";
      signup_pass.type = "password";
      signup_cpass.type = "password";
      eye_open.addClass('hidden');
      eye_closed.removeClass('hidden');
  }
}

// METHOD VALIDATION FOR SIGNUP
$(document).on('click', '#sign_up_modal .btn-save', function(e) {
	var modal = $('#sign_up_modal'),
	count_error = 0,
	fname = modal.find('input[name="firstname"]'),
	lname = modal.find('input[name="lastname"]'),
	email = modal.find('input[name="email"]'),
  password = modal.find('input[name="password"]'),
  password_confirmation = modal.find('input[name="password_confirmation"]'),
	timezone = modal.find('select[name="timezone"]'),
	business_nature = modal.find('select[name="business_nature_child_id"]'),
	business_name = modal.find('input[name="business_name"]');

	$('#sign_up_modal').find('.has-error').removeClass('has-error');

	if ($.trim(fname.val()) === '') {
		fname.closest('.form-group').addClass('has-error');
		count_error++;
	}

	if ($.trim(lname.val()) === '') {
		lname.closest('.form-group').addClass('has-error');
		count_error++;
	}

	if (!validateEmail(email.val())) {
		email.closest('.form-group').addClass('has-error');
		count_error++;
	}

	if ($.trim(password.val()) === '') {
		password.closest('.form-group').addClass('has-error');

  }
  
  if ($.trim(password_confirmation.val()) === '') {
		password_confirmation.closest('.form-group').addClass('has-error');

	}

	if (!timezone.val()) {
		timezone.closest('.form-group').addClass('has-error');
	}

	if (!business_nature.val()) {
		business_nature.closest('.form-group').addClass('has-error');
	}

	if ($.trim(business_name.val()) === '') {
		business_name.closest('.form-group').addClass('has-error');
	}

	if (count_error > 0) {
		return false;
  }
  
  e.preventDefault();
  $.ajax({
    url:"register",
    dataType: "json",
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    data:{
      firstname: $("#firstName").val(),  
      lastname: $("#lastName").val(), 
      email: $("#email_signup").val(), 
      password: $("#password_signup").val(),
      password_confirmation: $("#password_confirmation").val(),
      timezone: $("#timezone").val(),
      business_nature_child_id: $("#business_nature").val(),
      business_name: $("#business_name").val(),
    },
    method: "POST",
    success: function(r) {
      var site_name = $('#site_name').data('site_name');
      $("#validation-errors-signup").empty();

      if(r.status) {
        window.location = site_name+'/email-verification/send_email';

      } else{
        $.each(r.message, function(key, value){
          $('#validation-errors-signup').append('<p>'+value+'</p>');
        });


        // $('#validation-errors').html(e.message[0]);
      }
    },
    error:function(e){
      console.log(e.responseText);
    }
  });

});


// METHOD VALIDATIONS FOR LOGIN
$(document).on('click', '#login_modal .login-btn', function(e) { 
	var count_error = 0,
	email = $('#login_modal').find('input[name="email"]').val(),
  	password = $('#login_modal').find('input[name="password"]');
  	validation_error = $('#validation-errors').html('');

  	$('#login_modal').find('.has-error').removeClass('has-error');

	if (!validateEmail(email)) {
		$('#login_modal').find('input[name="email"]').closest('.form-group').addClass('has-error');
		validation_error;
		count_error++;
	}

	if($.trim(password.val()) === '') {
		password.closest('.form-group').addClass('has-error');
		validation_error;
		count_error++;
	}

	if (count_error > 0) {
		return false;
	}

  	e.preventDefault();

	$.ajax({
		url:"login",
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		method: "POST",
		data:{ email: $("#email").val(), password: $("#password").val() },
		dataType: "json",
		success: function(r) {
			var site_name = $('#site_name').data('site_name');

			if(r.status) {
				window.location = site_name+'/task';
			} else{
				$('#validation-errors').html(r.message);
				
				if(r.message === 'Please check your verification link on your email.'){
					$('.resendLink').removeClass('hide');
				} else {
					$('.resendLink').addClass('hide');
				}
			}
		},
		error:function(e){
			console.log(e.responseText);
		}
	});
  
});

$(document).on('click','.resendVerification',function(e){
	e.preventDefault();

	$.ajax({
		url:"resendVerificationLink",
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		method: "POST",
		data:{ email: $("#email").val(), password: $("#password").val() },
		dataType: "json",
		beforeSend: function() {
			$('.resendLink').find('a').addClass('hide');
			$('.resendLink').find('span').removeClass('hide');
		},	
		success: function(r) {
			$('#validation-errors').html(r.message);	
			if(r.message === 'Check your email for verification link.'){
				$('.resendLink').find('span').addClass('hide');
				$('.resendLink').find('a').removeClass('hide');	
			}
		},
		error:function(e){
			console.log(e.responseText);
		}
	});
})


function validateEmail(email) {
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

$(function() {
  $(document).on('click' , '.subscription_landing_btn' , function () {
    $('#subscription').val($(this).data('subscription_id'));
    $('.amount-').html($(this).data('amount'));
    $('#amount-').val($(this).data('amount'));
    $('.pricing_container').css('opacity', '0');
    setTimeout(function() {
      $('.pricing_container').show().css('opacity', '1');
    }, 200);
  });

    $('form.require-validation').bind('submit', function(e) {
      var $form         = $(e.target).closest('form'),
          inputSelector = ['input[type=email]', 'input[type=password]',
                           'input[type=text]', 'input[type=file]',
                           'textarea'].join(', '),
          $inputs       = $form.find('.required').find(inputSelector),
          $errorMessage = $form.find('div.error'),
          valid         = true;

      $errorMessage.addClass('hide');
      $('.has-error').removeClass('has-error');
      $inputs.each(function(i, el) {
        var $input = $(el);
        if ($input.val() === '') {
          $input.parent().addClass('has-error');
          $errorMessage.removeClass('hide');
          e.preventDefault(); // cancel on first error
        }
      });
    });
  });

  $(function() {
    var $form = $("#payment-form");

    $form.on('submit', function(e) {
      if (!$form.data('cc-on-file')) {
          e.preventDefault();
          Stripe.setPublishableKey($form.data('stripe-publishable-key'));
          Stripe.createToken({
            number: $('.card-number').val(),
            cvc: $('.card-cvc').val(),
            exp_month: $('.card-expiry-month').val(),
            exp_year: $('.card-expiry-year').val()
          }, stripeResponseHandler);
      }
    });

    function stripeResponseHandler(status, response) {
      if (response.error) {
        $('.error')
          .removeClass('hide')
          .find('.alert')
          .text(response.error.message);
      } else {
        // token contains id, last4, and card type
        var token = response['id'];
        // insert the token into the form so it gets submitted to the server
        $form.find('input[type=text]').empty();
        $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
        $form.get(0).submit();
      }
    }
  })
