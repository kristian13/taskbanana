$(function() {
    //check if modal is visible 
    $('#view_thread_modal').on('hidden.bs.modal', function () {
        $('.list-card').removeClass('modal_active')
    });
	// Read	MORE
	function readmore(se,op,always){
		var cr = document.createElement;[].forEach.call(document.querySelectorAll(se),function(el){
			if(el.clientHeight > op) { 
				el.style.overflow = 'hidden'
				var n = cr.call(document,'a'),sis = cr.call(document,'span')
				sis.innerText = '...'
				n.href = '#'
				n.open = true;
				n.onclick = function(e){
					e.preventDefault()
					if(n.open) {
						n.open = false
						n.innerText = 'read more'
						n.className= 'morelink'
						el.style.height = op+'px'
						sis.style.display = 'block'
					} else {
						n.open = true
						n.innerText = 'show less'
						el.style.height = 'auto'
						sis.style.display = 'none'
					} 

					(always && !e.dispatch) && always(this, !n.open) 
				}
				el.parentElement.insertBefore(n,el.nextElementSibling)
				var d = new Event('click');d.dispatch = true;
				n.dispatchEvent(d);
			}
		})
	}

    readmore('.rmore', 102, function(el, oorc){
        var card = el.closest('.list-card'); 
        oorc && card.closest('.list-body').scrollTo(0, parseInt(card.offsetTop) /* - 120*/) 
    })

    readmore('.rmore-modal', 60);

    // Toggle Filter
    $(document).ready(function(){
        // Filter 
        $(".filter-btn").click(function(){
            $(".filter-body").slideToggle();
        });

        // Modal Attachment
        $('#view_thread_modal .attachment-title').click(function() {
            $("#view_thread_modal .attachment-list").slideToggle();
            if ($(this).hasClass('hide-label')) {
                $(this).removeClass('hide-label').addClass('show-label').text('Show Attachment');
            } else {
                $(this).removeClass('show-label').addClass('hide-label').text('Hide Attachment');
            }
        });
    });

    // $(document).on('click', '.test-notif', function() {
	// 	$.notify({
	// 		// options
	// 		icon: 'glyphicon glyphicon-warning-sign',
	// 		title: '',
	// 		message: '4th Task Sample Design Layout',
	// 		url: 'https://github.com/mouse0270/bootstrap-notify',
	// 		target: '_blank'
	// 	},{
	// 		// settings
	// 		element: 'body',
	// 		position: null,
	// 		type: "remind",
	// 		allow_dismiss: true,
	// 		newest_on_top: false,
	// 		showProgressbar: false,
	// 		placement: {
	// 			from: "top",
	// 			align: "right"
	// 		},
	// 		offset: 20,
	// 		spacing: 10,
	// 		z_index: 1031,
	// 		delay: 0,
	// 		timer: 0,
	// 		url_target: '_blank',
	// 		mouse_over: null,
	// 		animate: {
	// 			enter: 'animated fadeInRight',
	// 			exit: 'animated fadeOutUp'
	// 		},
	// 		onShow: null,
	// 		onShown: function(){$('.drop-notif').dropdown()},
	// 		onClose: null,
	// 		onClosed: null,
	// 		icon_type: 'class',
	// 		template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} flex" role="alert">' +
	// 			// '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
	// 			// '<span data-notify="icon"></span> ' +
	// 			'<img src="images/icons/clock.png">'+
	// 			// '<span data-notify="title">{1}</span> ' +
	// 			'<div class="flex col">'+
	// 				'<span data-notify="message">{2}</span>' +
	// 				'<span data-notify="date">01/20/2019</span>' +
	// 			'</div>'+
	// 			'<div class="notify-tools flex middle space-even">'+
	// 				'<a class="complete" href="#" data-notify="dismiss"><p>Complete</p></a>'+
	// 				'<div class="dropup">'+ 
	// 					'<a href="#" class="dropdown -toggle remind" id="drop1" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> Remind me </a>'+
	// 					 '<ul class="dropdown-menu" aria-labelledby="drop1"> '+
	// 						 '<li><a href="#">Never</a></li>'+
	// 						 '<li><a href="#">Remind after 5mins</a></li>'+
	// 						 '<li><a href="#">Remind after 10mins</a></li>'+
	// 						 '<li><a href="#">Remind after 15mins</a></li>'+
	// 						 // '<li role="separator" class="divider"></li>'+
	// 						 // '<li><a href="#">Separated link</a></li>'+
	// 					 '</ul>'+
	// 				 '</div>'+
	// 			'</div>'+
	// 			// '<div class="progress" data-notify="progressbar">' +
	// 			// 	'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
	// 			// '</div>' +
	// 			// '<a href="{3}" target="{4}" data-notify="url"></a>' +
	// 		'</div>' 
	// 	});
    // });

    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();

    $(document).on('click', function(e) {
        $('[data-toggle="popover"]').each(function() {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide').data('bs.popover').inState.click = false // fix for BS 3.3.6
            }
        });
    });
});

// Add New Member
function add_new_member(parent_business_owner_id , created_by) {
    
    if (created_by != undefined  ) {
        var modal = $('#business_or_personal').modal("show");
        modal.find('#type_add_member').val("false");
    } else {
        $('#business_or_personal').modal("hide");
        var modal = $('#add_new_member_modal').modal("show");
        modal.find('#type_add_member').val("false");
    }
}

// function add_new_member for personal
function add_new_member_personal() {
    $('#business_or_personal').modal("hide");
    var modal = $('#add_new_member_modal').modal("show");
    modal.find('#type_add_member').val("true");
}

// function show threads
function view_threads (task_id , created_by ,  task_status , archive = false)  {
    $('#created_by_thread_span').html('');
    $('#task_status').html('');
    $('#deadline').html('');
    $('#task_title').html('');
    $('#description').html('');
    $('#reply_div').html('');
    $('#task_complete_div').html('');
    $('#reply_text_area').val('');
    $('#div'+task_id).addClass("modal_active");
    var log_id = $('.body').data('log_id');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    // if archive remove text box form
    if (archive == true) {
        $('#reply_form').remove();
        $('#reply_div').remove();
    }
    $.ajax({
        url: "view_threads",
        data:{task_id : task_id , _token:$('meta[name="csrf-token"]').attr('content') },
        method: "POST" , 
        success : function (response) {
            var json = jQuery.parseJSON(response);
            var fullname = json.firstname+' '+json.lastname;
            $('#created_by_thread_span').html(fullname);
            $('.reply_btn').attr('data-task_id' , json.task_id);
            // $('#reply_div').append("<a href='javascript:void(0);' class='btn btn-info reply_btn'  onclick='reply_function("+json.task_id+"  , "+json.created_by+"   ,\"" + json.task_status + "\")'>Reply</a>");
            $('#reply_div').append("<a href='javascript:void(0);' data-created_by="+json.task_creator+" data-task_id="+json.task_id+" data-task_status ="+task_status+" class='btn btn-info reply_btn'>Reply</a>");
            if (log_id == json.task_creator ) {
        		$('#task_complete_div').append(' <button type="button" data-task_id='+json.task_id+' class="btn btn-success task_complete">Task Complete</button>');
            }
            var modal = $('#view_thread_modal').modal("show");
            modal.find('#task_status').append(json.task_status)
            modal.find('#deadline').append(json.deadline)
            modal.find('#task_title').append(json.title)
            modal.find('#description').append(json.description)
            // for assignee
            var assignee = "";
            $.each(json.assignee , function (key , value ) {
        		assignee +='<img data-status="" data-name="'+value.firstname+'" src="images/sample/user-pic-1.png" data-toggle="tooltip" data-placement="bottom" title=" '+value.firstname+'">';
            }); 

            
            // for file attached
            var file_tmp = "",
                $file = "",
                $image = "";

            $.each(json.attachment, function(key, value) {
                console.log(value.image);
                console.log(value.file_type);
                if (value.file_type === "jpg" || value.file_type === "png" || value.file_type === "jpeg") {
                    $image += '<div class="image-item">'+
                                    '<a href="javascript:void(0);"><img src="task_attachment/'+value.image+'"></a>'+
                                '</div>';
                } else {
                    $file += '<div class="file-item col-sm-6">'+
                                '<a href="javascript:void(0);" class="file-wrapper flex middle space-bet">'+
                                    '<div class="file-icon">';
                        switch(value.image.match(/\.([\w]+)$/)[1]) {
                            case 'doc':
                            case 'docx':
                                $file += '<img src="images/icons/file-blue.png">';
                            break;

                            case 'xls':
                            case 'xlsx':
                                $file += '<img src="images/icons/file-darkgreen.png">';
                            break;

                            case 'ppt':
                            case 'pptx':
                            break;

                            case 'pdf':
                                $file += '<img src="images/icons/file-red.png">';
                            break;

                            case 'zip':
                            case 'rar':
                            case 'tar':
                            case 'gzip':
                            case 'gz':
                            case '7z':
                                $file += '<img src="images/icons/file-lightgreen.png">';
                            break;

                            case 'htm':
                            case 'html':
                                $file += '<img src="images/icons/file-gray.png">';
                            break;

                            case 'txt':
                            case 'csv':
                            case 'ini':
                            case 'csv':
                            case 'java':
                            case 'php':
                            case 'js':
                            case 'css':
                                $file += '<img src="images/icons/file-white.png">';
                            break;

                            case 'avi':
                            case 'mpg':
                            case 'mkv':
                            case 'mov':
                            case 'mp4':
                            case '3gp':
                            case 'webm':
                            case 'wmv':
                            case 'm4a':
                                $file += '<img src="images/icons/file-orange.png">';
                            break;

                            case 'mp3':
                            case 'wav':
                                $file += '<img src="images/icons/file-orange.png">';
                            break;

                            default:
                                $file += '<img src="images/icons/file-white.png">';
                        }
                        $file += '<span>'+value.file_type+'</span>'+
                            '</div>'+
                            '<p class="file-name">'+value.image+'</p>'+
                        '</a>'+
                    '</div>';
                }
                // file_tmp +='<div class="image-item">'+
                //                 '<a href="javascript:void(0);"><img src="task_attachment/'+value.image+'"></a>'+
                //             '</div>';
            });

            file_tmp += '<label class="attachment-title hide-label">Hide Attachment</label>'+
                            '<div class="attachment-list" id="attachment-list">'+
                                '<!-- attachment lists -->'+
                                '<div class="file-list clearfix">'+$file+
                                '</div>'+
                                '<div class="image-list clearfix">'+$image+
                                '</div>'+
                            '</div>';

            // for comments and images
            var comments = "";
            $.each( json.comment , function (key , value ) {
                // console.log(value.images);
                // console.log(value.images.length);
                comments += '<div class="comment-item">'+
                                '<div class="top flex middle">'+
                                    '<img class="comment-user-img" src="images/sample/user-pic-4.png">'+
                                    '<div>'+
                                        '<p class="comment-user-name">'+value.data.firstname+' '+value.data.lastname+'</p>'+
                                        '<p class="comment-time">'+value.data.date_added+'</p>'+
                                    '</div>'+
                                '</div>'+
                                '<p class="comment-text">'+value.data.comment+'</p>';
                                if (!value.images.length) {
                                    console.log('no files');
                                } else {
                                    var $image = "";
                                    var $file = "";
                                    $.each(value.images, function(k , v) {
                                        if (v.file_type === "jpg" || v.file_type === "png" || v.file_type === "jpeg") {
                                            $image += '<div class="image-item">'+
                                                            '<a href="javascript:void(0);"><img src="task_reply_attachment/'+v.image+'"></a>'+
                                                        '</div>';
                                        } else {
                                            $file += '<div class="file-item col-sm-6">'+
                                                        '<a href="javascript:void(0);" class="file-wrapper flex middle space-bet">'+
                                                            '<div class="file-icon">';
                                                switch(v.image.match(/\.([\w]+)$/)[1]) {
                                                    case 'doc':
                                                    case 'docx':
                                                        $file += '<img src="images/icons/file-blue.png">';
                                                    break;

                                                    case 'xls':
                                                    case 'xlsx':
                                                        $file += '<img src="images/icons/file-darkgreen.png">';
                                                    break;

                                                    case 'ppt':
                                                    case 'pptx':
                                                    break;

                                                    case 'pdf':
                                                        $file += '<img src="images/icons/file-red.png">';
                                                    break;

                                                    case 'zip':
                                                    case 'rar':
                                                    case 'tar':
                                                    case 'gzip':
                                                    case 'gz':
                                                    case '7z':
                                                        $file += '<img src="images/icons/file-lightgreen.png">';
                                                    break;

                                                    case 'htm':
                                                    case 'html':
                                                        $file += '<img src="images/icons/file-gray.png">';
                                                    break;

                                                    case 'txt':
                                                    case 'csv':
                                                    case 'ini':
                                                    case 'csv':
                                                    case 'java':
                                                    case 'php':
                                                    case 'js':
                                                    case 'css':
                                                        $file += '<img src="images/icons/file-white.png">';
                                                    break;

                                                    case 'avi':
                                                    case 'mpg':
                                                    case 'mkv':
                                                    case 'mov':
                                                    case 'mp4':
                                                    case '3gp':
                                                    case 'webm':
                                                    case 'wmv':
                                                    case 'm4a':
                                                        $file += '<img src="images/icons/file-orange.png">';
                                                    break;

                                                    case 'mp3':
                                                    case 'wav':
                                                        $file += '<img src="images/icons/file-orange.png">';
                                                    break;

                                                    default:
                                                        $file += '<img src="images/icons/file-white.png">';
                                                }
                                                $file += '<span>'+v.file_type+'</span>'+
                                                    '</div>'+
                                                    '<p class="file-name">'+v.image+'</p>'+
                                                '</a>'+
                                            '</div>';
                                        }
                                    });
                    comments += '<label class="attachment-title show-label">Show Attachment</label>'+
                                '<div class="attachment-list" style="display: none;">'+
                                    // File Attachment
                                    '<div class="file-list clearfix">'+$file+
                                    '</div>'+
                                    // Image Attachment
                                    '<div class="image-list clearfix">'+$image+
                                    '</div>'+
                                '</div>';
                                }
                comments += '</div>';
            });
            modal.find('#assignee').html(assignee)
            modal.find('#thread_comments').html(comments)
            modal.find('#task-file-attachment').html(file_tmp)

            // tooltip
            $('[data-toggle="tooltip"]').tooltip();

            // Modal Attachment
            $('#view_thread_modal .attachment-title').click(function() {
                $(this).next(".attachment-list").slideToggle();
                if ($(this).hasClass('hide-label')) {
                    $(this).removeClass('hide-label').addClass('show-label').text('Show Attachment');
                } else {
                    $(this).removeClass('show-label').addClass('hide-label').text('Hide Attachment');
                }
            });
}

//reply_btn
$(document).on('click' , '.reply_btn' , function(e) {
    e.preventDefault();
    var task_id =  $(this).data('task_id');
    var created_by =  $(this).data('created_by');
    var task_status =  $(this).data('task_status');
    var log_id = $('.body').data('log_id');
    var full_name = $('.body').data('fullname');
    var date = new Date();
    var formatted = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() 

    if (task_status == 'N') {
        var clone = $( "#div"+task_id ).clone().prependTo( ".waiting_div:first" );
        var view_threads = clone.find('.view_threads').attr('data-task_status','I').attr("onclick" , "view_threads( "+task_id+" , "+created_by+" , 'I' );");
		clone.find('.last-comment-comment').html($('#reply_text_area').val())
		clone.find('.last-comment-date').html(formatted)
		clone.find('.last-comment-name').html((  full_name  ));
        $('.modal_active:first').remove();
    } else if (task_status == 'I') {
        var clone = $( "#div"+task_id );
		clone.find('.last-comment-name').html((  full_name  ));
		clone.find('.last-comment-comment').html($('#reply_text_area').val())
		clone.find('.last-comment-date').html(formatted)
        /*var view_threads = clone.find('.view_threads').attr('data-task_status','I').attr("onclick" , "view_threads( "+task_id+" , "+created_by+" , 'I' );");*/
    }else{
        var clone = $( "#div"+task_id ).clone().prependTo( ".waiting_div" );
        var view_threads = clone.find('.view_threads').attr('data-task_status','I').attr("onclick" , "view_threads( "+task_id+" , "+created_by+" , 'I' );");
		clone.find('.last-comment-comment').html($('#reply_text_area').val())
		clone.find('.last-comment-date').html(formatted)
		clone.find('.last-comment-name').html((  full_name  ));
        $('.modal_active:last').remove();
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	var file_data = $('.file_reply').prop('files');  
	var form_data = new FormData(); 
	form_data.append('reply', $('#reply_text_area').val());	
	form_data.append('task_id', task_id);	
	form_data.append('task_status',task_status);	
	$.each(jQuery('input[type=file]'), function(i, value) 
	{
	    form_data.append('image[]', value.files[0]);
	});

     $.ajax({
        url : "reply_thread" , 
    	data : form_data , 
        method : "POST" , 
        cache:false,
        contentType: false,
        processData: false,
    	success : function (response) {
    		console.log(response)
            $('#view_thread_modal').find('[fi-func="delete"]').trigger('click');
        }
    });

});
// function reply_function (task_id , created_by ,  task_status) {
// 	// validation first
// 	if ($('#reply_text_area').val() == "") {
// 		// add css here for designers
// 		alert("please reply!");
// 		return false;
// 	} 

// 	$.ajaxSetup({
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         }
//     });
// 	var formData =$('#reply_form').serializeArray();
// 	console.log(formData);
//     $.ajax({
//     	url : "reply_thread" , 
//     	data : formData , 
//     	method : "POST" , 
//     	cache:false,
//         contentType: false,
//         processData: false,
//     	success : function (response) {
//     		
//     	}
//     });

// 	var clone = $( "#div"+task_id ).clone().appendTo( ".waiting_div" );
// 	var view_threads = clone.find('.view_threads').attr('data-task_status','I').attr("onclick" , "view_threads( "+task_id+" , "+created_by+" , 'I' );");

// }

// Add Task Submit Error Validation
$(document).on('click', '#submit_addtask', function(e) {
    //e.preventDefault();

    var $form = $(this).closest('form'),
        account_type = $form.find('input[name="account_type"]:checked').val(),
        $count_error = 0,
        $title = $form.find('[name="title"]'),
        $description = $form.find('[name="description"]'),
        $select = $form.find('select'),
        $new_input = $form.find('[name="temp_project_title"]'),
        $checked = $('.assignee-checkbox [type="checkbox"]:checked').length,
        $dropdown_add = $form.find('.dropdown-add');

    // title validation
    if ($.trim($title.val()) === '') {
        $title.closest('.form-group').addClass('has-error');
    } else {
        $title.closest('.form-group').removeClass('has-error');
    }

    // description validation
    if (($.trim($description.val()) === '') || ($.trim($description.val()) === '<p><br></p>')) {
        $description.closest('.form-group').addClass('has-error');
    } else {
        $description.closest('.form-group').removeClass('has-error');
    }

    // project title validation => ($select.val() === '1') && ($.trim($new_input.val()) !== '')
    if ($.trim($new_input.val()) !== '') {
        $dropdown_add.addClass('invalid');
        $dropdown_add.closest('.form-group').addClass('has-error');
        $dropdown_add.closest('.form-group').find('.invalid-feedback').addClass('show');
        $dropdown_add.closest('.form-group').find('.invalid-input').addClass('hide');
    } else {
        $dropdown_add.find('.add-to-dropdown').addClass('add-option').removeClass('add-to-dropdown').html('&#9547;');
        $dropdown_add.find('[name="temp_project_title"], .cancel-add').addClass('hide');
        $dropdown_add.removeClass('invalid');
        $dropdown_add.closest('.form-group').removeClass('has-error');
        $dropdown_add.closest('.form-group').find('.invalid-feedback').removeClass('show');
        $dropdown_add.closest('.form-group').find('.invalid-input').removeClass('hide');
    }

    // assignee validation
    if (!$checked) {
        $form.find('.assignee-checkbox').addClass('has-error');
    } else {
        $form.find('.assignee-checkbox').removeClass('has-error');
    }

    // count remaining errors
    $form.find('.has-error').each(function() {
        $count_error++;
    });

    if ($count_error > 0) {
        return false;
    }

    //$.ajax({
    //  url
    //})

});

// Reset Add Task Modal
$(document).on('click', '#add_task', function() {
    $('#category_add_task').html(''); // set select to empty
    $add_task = $('#add_task_modal');
    $add_task.find('.form-group').removeClass('has-error');
    $add_task.find('.dropdown-add').removeClass('invalid');
    $add_task.find('.add-to-dropdown').addClass('add-option').removeClass('add-to-dropdown').html('&#9547;');
    $add_task.find('[name="temp_project_title"], .cancel-add, .task-percent').addClass('hide');
    $add_task.find('[name="title"], [name="description"], [name="deadline"], [name="temp_project_title"]').val('');
    $add_task.find('.texteditor-content').html('<p><br></p>');
    $add_task.find('.task-list').empty();
    $add_task.find('option').attr('selected', false);
    $add_task.find('option[value="1"]').attr('selected', '');
    $add_task.find('select').val('1').trigger('change');
    $add_task.find('.assignee-checkbox [type="checkbox"]').prop('checked', false);
    $add_task.find('[fi-func="delete"]').trigger('click');

    // append all project titles
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}}); // ajax setup 1 liner
    $.ajax({
        url : "get_categories" ,
        data:{token:$('meta[name="csrf-token"]').attr('content')},
        method : "POST", 
        success  : function (response) {
            var tmp = "";
            var json = jQuery.parseJSON(response);

            $.each(json , function (k , v) {
                    tmp += '<option value="'+v.category_id+'">'+v.category_name+'</option>';
});
            $('#category_add_task').append(tmp);

        }  
    });
});

// Add Option Button for Project Title
$(document).on('click', '.dropdown-add .add-option', function() {
    var $dropdown_add = $(this).closest('.dropdown-add');
    $(this).addClass('add-to-dropdown').removeClass('add-option').text('Add');
    $(this).closest('.form-group').removeClass('has-error');
    $dropdown_add.find('[name="temp_project_title"], .cancel-add').removeClass('hide');
});

// Select Project Title Validation
$(document).on('change', '.dropdown-add select', function() {
    var me = $(this);
    var $dropdown_add = $(this).closest('.dropdown-add');
    $dropdown_add.find('.add-to-dropdown').addClass('add-option').removeClass('add-to-dropdown').html('&#9547;');
    $dropdown_add.find('[name="temp_project_title"], .cancel-add').addClass('hide');
    $dropdown_add.find('[name="temp_project_title"]').val('');
    $dropdown_add.removeClass('invalid');
    $dropdown_add.closest('.form-group').find('.invalid-feedback').removeClass('show');
    $dropdown_add.closest('form').find('.invalid-label, .max-label, .invalid-max-label').addClass('hide');
    $dropdown_add.closest('form').find('.task-percent .note').removeClass('hide').css('visibility', 'visible');
    $(this).closest('.form-group').removeClass('has-error');

    var $modal = $(this).closest('#add_task_modal');
    var $task_list = '<div class="x new-task" id="slide_div">'+
                        '<h6>New Task <span id="span" class="percent-count">0%</span></h6>'+
                        '<div data-percentage="0" data-task_name="" class="slider_div_class ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">'+
                            '<div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min" style="width: 0%;"></div>'+
                            '<span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span>'+
                        '</div>'+
                        '<input type="hidden" name="new_task_input_percentage" class="new_task_percentage">'+
                    '</div>';

    // get all task under project title
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var existing_task = "";
    $.ajax({
        url: "get_task_under_categories",
        data:{token:$('meta[name="csrf-token"]').attr('content') , category_id : $(this).val()  },
        method: "POST" , 
        success : function (response) {
            var json = jQuery.parseJSON(response);
            $.each(json , function ( k ,v ) {
                existing_task += '<div class="x" id="slide_div" >'+
                                    '<h6>'+v.title+'<span id="span" class="percent-count">'+v.task_percentage+'%</span></h6>'+
                                    '<div data-percentage="'+v.task_percentage+'" data-task_name="Test 17" class="slider_div_class_existing ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">'+
                                        '<div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min"></div>'+
                                        '<span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: '+v.task_percentage+'%;"></span>'+
                                    '</div>'+
                                    '<input type="hidden" name = "existing_task_percentage['+v.task_id+']" class = "existing_task_percentage" value='+v.task_percentage+' >'+
                    '</div>';
            }); 
            me.closest('form').find('.task-list').append(existing_task);
            // Slider
            $('.slider_div_class_existing').each(function() {
                var me = $(this);
                $(this).slider({
                    range: "min",
                    animate: true,
                    value: $(this).data('percentage'),
                    min: 0,
                    max: 100 ,
                    step: 1,
                    slide: function(event, ui) {
                        //compute total percentage 
                        if (ui.value != 0) {
                            $(this).removeClass('has-error');
                        } else {
                            $(this).addClass('has-error');
                        }
                        var sum = ui.value;
                        $('.x').each(function(i){
                            if(this !== me.closest('#slide_div')[0])
                                sum += parseFloat($(this).find('span').text());
                        });

                        if(sum > 100) { 
                            return false;
                        } else {
                            me.closest('#slide_div').find('#span').html(ui.value +'%');
                            var task_id = me.closest('#slide_div').data('task_id');
                            if (task_id == undefined) {
                                $('.existing_task_percentage').val(ui.value);
                            } else {
                                $('#'+task_id).val(ui.value); 
                            }  
                        }

                        $('#sum_percentage').val(sum);
                        $(".percent-count").filter(function() { return $(this).text() === "0%"; }).closest('.x').find('.slider_div_class').addClass('has-error');

                        var percent0_length = $(".percent-count").filter(function() { return $(this).text() === "0%"; }).length;
                        if ((percent0_length === 0) && (sum >= 100)) {
                            $modal.find('.invalid-label, .invalid-max-label, .task-percent .note').addClass('hide');
                            $modal.find('.max-label').removeClass('hide');
                        } else if ((percent0_length != 0) && (sum >= 100)) {
                            $modal.find('.invalid-max-label').removeClass('hide');
                            $modal.find('.max-label, .invalid-label, .task-percent .note').addClass('hide');
                        } else if ((percent0_length === 0) && (sum < 100)) {
                            $modal.find('.task-percent .note').removeClass('hide').css('visibility', 'hidden');
                            $modal.find('.max-label, .invalid-label, .invalid-max-label').addClass('hide');
                        } else {
                            $modal.find('.invalid-label').removeClass('hide');
                            $modal.find('.max-label, .invalid-max-label, .task-percent .note').addClass('hide');
                        }
                    }, 
                    change: function(event, ui) {
                      
                    }
                });
            }); 
        }
    }); 
    if ($(this).val() != '1') {
        $(this).closest('form').find('.task-percent').removeClass('hide');
        $(this).closest('form').find('.task-list').html($task_list);
    } else {
        $(this).closest('form').find('.task-percent').addClass('hide');
        $(this).closest('form').find('.task-list').empty();
    }

    // Slider
    $('.slider_div_class').each(function() {
        var me = $(this);
        $(this).slider({
            range: "min",
            animate: true,
            value: $(this).data('percentage'),
            min: 0,
            max: 100 ,
            step: 1,
            slide: function(event, ui) {
                //compute total percentage 
                if (ui.value != 0) {
                    $(this).removeClass('has-error');
                } else {
                    $(this).addClass('has-error');
                }

                var sum = ui.value;
                $('.x').each(function(i){
                    if(this !== me.closest('#slide_div')[0])
                        sum += parseFloat($(this).find('span').text());
                });

                if(sum > 100) { 
                    return false;
                } else {
                    me.closest('#slide_div').find('#span').html(ui.value +'%');
                    var task_id = me.closest('#slide_div').data('task_id');
                    if (task_id == undefined) {
                        $('.new_task_percentage').val(ui.value);
                    } else {
                        $('#'+task_id).val(ui.value); 
                    }  
                }

                $('#sum_percentage').val(sum);
                $(".percent-count").filter(function() { return $(this).text() === "0%"; }).closest('.x').find('.slider_div_class').addClass('has-error');

                var percent0_length = $(".percent-count").filter(function() { return $(this).text() === "0%"; }).length;
                if ((percent0_length === 0) && (sum >= 100)) {
                    $modal.find('.invalid-label, .invalid-max-label, .task-percent .note').addClass('hide');
                    $modal.find('.max-label').removeClass('hide');
                } else if ((percent0_length != 0) && (sum >= 100)) {
                    $modal.find('.invalid-max-label').removeClass('hide');
                    $modal.find('.max-label, .invalid-label, .task-percent .note').addClass('hide');
                } else if ((percent0_length === 0) && (sum < 100)) {
                    $modal.find('.task-percent .note').removeClass('hide').css('visibility', 'hidden');
                    $modal.find('.max-label, .invalid-label, .invalid-max-label').addClass('hide');
                } else {
                    $modal.find('.invalid-label').removeClass('hide');
                    $modal.find('.max-label, .invalid-max-label, .task-percent .note').addClass('hide');
                }
            }, 
            change: function(event, ui) {
              
            }
        });
    }); 
});

// Add Project Title to dropdown
$(document).on('click', '.dropdown-add .add-to-dropdown', function() {
    var $dropdown_add = $(this).closest('.dropdown-add'),
        $modal = $(this).closest('#add_task_modal'),
        $form = $(this).closest('form'),
        $hidden_title = $dropdown_add.find('[name="new_project_title"]'),
        $new_project_title = $dropdown_add.find('[name="temp_project_title"]').val(),
        $max = 0;
    if ($.trim($new_project_title) === '') {
        // if new_project_title has no value
        $dropdown_add.addClass('invalid');
        $dropdown_add.closest('.form-group').find('.invalid-input').removeClass('hide');
        $dropdown_add.closest('.form-group').find('.invalid-feedback').removeClass('show');
    } else {
        // if new_project_title has value
        $hidden_title.val($new_project_title);
        // remove invalid or has-error class
        $dropdown_add.removeClass('invalid');
        $dropdown_add.closest('.form-group').find('.invalid-feedback').removeClass('show');
        $dropdown_add.closest('.form-group').removeClass('has-error');
        // get highest value in the selection
        $dropdown_add.find('option').each(function() {
            var $value = parseInt($(this).val());
            $max = ($value > $max) ? $value : $max;
        });
        // add the input value to the selection
        var $option = '<option value="'+($max + 1)+'" selected>'+$new_project_title+'</option>'
        $dropdown_add.find('option').removeAttr('selected');
        $dropdown_add.find('select').prepend($option);
        $dropdown_add.find('.add-to-dropdown').addClass('add-option').removeClass('add-to-dropdown').html('&#9547;');
        $dropdown_add.find('[name="temp_project_title"], .cancel-add').addClass('hide');
        $dropdown_add.find('[name="temp_project_title"]').val('');
        // reset task percentage
        var $new_task = '<div class="x new-task" id="slide_div">'+
                        '<h6>New Task <span id="span" class="percent-count">0%</span></h6>'+
                        '<div data-percentage="0" data-task_name="" class="slider_div_class ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">'+
                            '<div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min" style="width: 0%;"></div>'+
                            '<span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span>'+
                        '</div>'+
                        '<input type="hidden" name="new_task_input_percentage" class="new_task_percentage">'+
                    '</div>';
        $form.find('.task-percent').removeClass('hide');
        $form.find('.task-percent .task-list').html($new_task);
        $form.find('.invalid-label, .max-label, .invalid-max-label').addClass('hide');
        $form.find('.task-percent .note').removeClass('hide').css('visibility', 'visible');
        // Slider
        $('.slider_div_class').each(function() {
            var me = $(this);
            $(this).slider({
                range: "min",
                animate: true,
                value: $(this).data('percentage'),
                min: 0,
                max: 100 ,
                step: 1,
                slide: function(event, ui) {
                    // me.closest('#slide_div').find('#span').html(ui.value +'%')
                    //compute total percentage 
                   var sum = ui.value;
                    $(  $('.x') ).each(function(i){
                        if(this !== me.closest('#slide_div')[0])
                            sum += parseFloat($(this).text());
                    });
                    if(sum > 100) { 
                        return false;
                    } else {
                        me.closest('#slide_div').find('#span').html(ui.value +'%');
                        var task_id = me.closest('#slide_div').data('task_id');
                        if (task_id == undefined) {
                            $('.new_task_percentage').val(ui.value);
                        } else {
                            $('#'+task_id).val(ui.value); 
                        }  
                    }

                    var percent0_length = $(".percent-count").filter(function() { return $(this).text() === "0%"; }).length;
                    if ((percent0_length === 0) && (sum >= 100)) {
                        $modal.find('.invalid-label, .invalid-max-label, .task-percent .note').addClass('hide');
                        $modal.find('.max-label').removeClass('hide');
                    } else if ((percent0_length != 0) && (sum >= 100)) {
                        $modal.find('.invalid-max-label').removeClass('hide');
                        $modal.find('.max-label, .invalid-label, .task-percent .note').addClass('hide');
                    } else if ((percent0_length === 0) && (sum < 100)) {
                        $modal.find('.task-percent .note').removeClass('hide').css('visibility', 'hidden');
                        $modal.find('.max-label, .invalid-label, .invalid-max-label').addClass('hide');
                    } else {
                        $modal.find('.invalid-label').removeClass('hide');
                        $modal.find('.max-label, .invalid-max-label, .task-percent .note').addClass('hide');
                    }
                }, 
                change: function(event, ui) {
                }
            });
        }); 
    }
});

// Cancel Project Title
$(document).on('click', '.dropdown-add .cancel-add', function() {
    var $dropdown_add = $(this).closest('.dropdown-add');
    $dropdown_add.find('.add-to-dropdown').addClass('add-option').removeClass('add-to-dropdown').html('&#9547;');
    $dropdown_add.find('[name="temp_project_title"], .cancel-add').addClass('hide');
    $dropdown_add.find('[name="temp_project_title"]').val('');
    $dropdown_add.removeClass('invalid');
    $(this).closest('.form-group').removeClass('has-error');
});

// Change Account Type
$(document).on('change', '.assignee-checkbox [name="account_type"]', function() {
    $('.assignee-checkbox [type="checkbox"]').prop('checked', false);
});

// Check All Members
$(document).on('click', '.checkAll', function() {
    $(this).parent().parent().find('input:checkbox').prop('checked', this.checked)
});

// Check if All Assignees are selected
$(document).on('click', '.checkAssignee', function() {
    var $assignee = $(this).parent().parent().find('.checkAssignee'),
        $count_check = 0;

    $assignee.each(function() {
        if ($(this).is(':checked')) {
            $count_check++;
        }
    });

    if($assignee.length === $count_check) {
        $(this).parent().parent().find('.checkAll').prop('checked', true);
    } else {
        $(this).parent().parent().find('.checkAll').prop('checked', false);
    }
});

// Dropdown Stop Propagation
$(document).on('click', '.profile-menu .dropdown-menu.profile, .profile-page .change-email .dropdown-menu, .profile-page .change-password .dropdown-menu, .profile-page .change-language .dropdown-menu, .profile-page .change-theme .dropdown-menu, .profile-page .change-emailnotif .dropdown-menu', function (e) {
  e.stopPropagation();
});

// Dropdown Close Button
$(document).on('click', '.dropdown .close', function() {
    var $dropdown = $(this).closest('.dropdown');
    $dropdown.removeClass('open dropup');
    $dropdown.find('.dropdown-toggle').attr('aria-expanded', 'true');
});

// Dropdown Auto Reposition
$(document).on("shown.bs.dropdown", ".dropdown", function () {
    // calculate the required sizes, spaces
    var $ul = $(this).children(".dropdown-menu");
    var $button = $(this).children(".dropdown-toggle");
    var ulOffset = $ul.offset();
    // how much space would be left on the top if the dropdown opened that direction
    var spaceUp = (ulOffset.top - $button.height() - $ul.height()) - $(window).scrollTop();
    // how much space is left at the bottom
    var spaceDown = $(window).scrollTop() + $(window).height() - (ulOffset.top + $ul.height());
    // switch to dropup only if there is no space at the bottom AND there is space at the top, or there isn't either but it would be still better fit
    if (spaceDown < 0 && (spaceUp >= 0 || spaceUp > spaceDown))
      $(this).addClass("dropup");
}).on("hidden.bs.dropdown", ".dropdown", function() {
    // always reset after close
    $(this).removeClass("dropup");
});



// Change Theme Color 
$(document).on('click', '.change-theme > a', function() {
    $(this).parent().find('.color-pick').toggleClass('hide');
});

// Form Validation
(function() {
    'use strict';
    window.addEventListener('load', function() {
        var forms = document.getElementsByClassName('needs-validation');
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

// Text Editor
$('.texteditor-wrap').each(function(){
    var qs = function(el,taco) {return el.querySelector(taco)}
    var bold = qs(this,'.pd-bold');
    var italic = qs(this,'.pd-italic');
    var underline = qs(this,'.pd-underline');
    var strikethrough = qs(this,'.pd-strikethrough');
    var undo = qs(this,'.pd-undo');
    var redo = qs(this,'.pd-redo');
    var texteditor = qs(this,'.texteditor-content');
    var textarea = qs(this,'textarea');

    bold.addEventListener('click', function(e){
        e.preventDefault();
        texteditor.focus();
        document.execCommand('bold');
    })
    italic.addEventListener('click', function(e){
        e.preventDefault();
        texteditor.focus();
        document.execCommand('italic');
    })
    underline.addEventListener('click', function(e){
        e.preventDefault();
        texteditor.focus();
        document.execCommand('underline');
    })
    strikethrough.addEventListener('click', function(e){
        e.preventDefault();
        texteditor.focus();
        document.execCommand('strikeThrough');
    })
    undo.addEventListener('click', function(e){
        e.preventDefault();
        document.execCommand('undo');
    })
    redo.addEventListener('click', function(e){
        e.preventDefault();
        document.execCommand('redo');
    })

    document.execCommand("defaultParagraphSeparator", false, "p");

    texteditor.addEventListener('input', function() {
        if(texteditor.innerHTML === "") texteditor.innerHTML = '<p><br></p>'; 
        textarea.value = texteditor.innerHTML;
    });

    $(this).on('click', '.texteditor-toolbar .right-buttons button', function() {
        $(this).toggleClass('active');
    });

    $(this).on('click', '.texteditor-content', function(e) { 
        $(this).closest('.texteditor-wrap').find('.texteditor-toolbar button').removeClass('active');
        var bubble = e.target ;
        if(this === e.target)
        {   
            if(window.getSelection)
            {
                bubble = window.getSelection().anchorNode.parentNode;
            } else if(window.selection)// IE 8 below
            {   
                bubble = window.selection().anchorNode.parentNode;
            }
        }

        while(bubble.tagName !== 'P' && this !== bubble) {
            switch (bubble.tagName) {
                case 'B':
                    bold.classList.add('active');
                    break;
                case 'U':
                    underline.classList.add('active');
                    break;
                case 'STRIKE':
                    strikethrough.classList.add('active');
                    break;
                case 'I':
                    italic.classList.add('active');
                    break;
                default:
                    break;
            }
            bubble = bubble.parentNode || bubble.parentElement
        }
    });
});

$(function() {
    // File Input
    $('.file-custom-input').on('change',function(e){
        var $this = $(this), $root = $this.closest('.file-attachment');
        var files = e.target.files;e.target.pfiles = e.target.pfiles ? e.target.pfiles.concat([].slice.call(files)) : [].slice.call(files);
        if($this[0].hasAttribute('name')){
            var input = '<input type="hidden" filelist name="'+$this.attr('name')+'">'
            $this.parent().append(input)
            $this.removeAttr('name')
        }

        var $filelist = $root.find('[filelist]'), oldlist = !!$filelist.val() ? JSON.parse($filelist.val()) : []

        $.each(files, function(i, file){ 
            if(file.name in oldlist) return;

            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){  
                var pieces =  file.name.split(/[\.]+/);
                var extension = pieces[pieces.length - 1];
                var template = '';
                var images = '';
                switch(file.name.match(/\.([\w]+)$/)[1]) {
                    case 'doc':
                    case 'docx':
                        // template += '<div>doc; docx</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-blue.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'xls':
                    case 'xlsx':
                        // template += '<div>xls; xlxs</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-darkgreen.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'ppt':
                    case 'pptx':
                        // template += '<div>ppt; pptx</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-orange.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'pdf':
                        // template += '<div>pdf</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-red.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'zip':
                    case 'rar':
                    case 'tar':
                    case 'gzip':
                    case 'gz':
                    case '7z':
                        // template += '<div>zip; rar; tar; gzip; gz; 7z</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-lightgreen.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'htm':
                    case 'html':
                        // template += '<div>htm; html</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'txt':
                    case 'csv':
                    case 'ini':
                    case 'java':
                    case 'php':
                    case 'js':
                    case 'css':
                        // template += '<div>txt; csv; ini; java; php; js; css</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'avi':
                    case 'mpg':
                    case 'mkv':
                    case 'mov':
                    case 'mp4':
                    case '3gp':
                    case 'webm':
                    case 'wmv':
                    case 'm4a':
                        // template += '<div>avi; mpg; mkv; mov; mp4; 3gp; webm; wmv; m4a</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;
                    
                    case 'mp3':
                    case 'wav':
                        // template += '<div>mp3; wav</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'jpg':
                    case 'jpeg':
                    case 'png':
                    case 'gif':
                        // images += '<img src="">';
                        images += '<div class="image-item">'+
                                    '<img src="'+e.target.result+'">'+
                                    '<a href="#" fi-func="delete" fi-name="'+file.name+'" class="img-delete flex middle center"><span>&#9587;</span></a>'+
                                '</div>';
                    break;

                    default:
                        // template += '<div>default file</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-gray.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                }

                images && $root.find('.image-list').append(images);
                template && $root.find('.file-list').append(template);
                var getfile = { name: file.name, value: e.target.result }
                oldlist.push(getfile)
                $filelist.val(JSON.stringify(oldlist))

            };
        });


    }).closest('.file-attachment').on('click', '[fi-func="delete"]',function(e){
        e.preventDefault();
        var $delete_btn = $(this), 
			f_input		= $(e.delegateTarget).find('input[type="file"]')[0], 
			d_name 		= $delete_btn.attr('fi-name').trim(),
			$filelist  	= $(e.delegateTarget).find('[filelist]')

        var files = JSON.parse($filelist.val()).filter(function(file){
            return file.name !== d_name
        });

        $filelist.val(JSON.stringify(files))

        $delete_btn.closest('.image-item,.file-item').remove();

    });

    // EDIT File Input
    $('.edit-file-custom-input').on('change',function(e){
        console.log('qweq');
        var $this = $(this), $root = $this.closest('.file-attachment');
        var files = e.target.files;e.target.pfiles = e.target.pfiles ? e.target.pfiles.concat([].slice.call(files)) : [].slice.call(files);
        if($this[0].hasAttribute('name')){
            var input = '<input type="hidden" edit_filelist name="'+$this.attr('name')+'">';
            $this.parent().append(input);
            $this.removeAttr('name');
        }

        var $filelist = $root.find('[edit_filelist]'), oldlist = !!$filelist.val() ? JSON.parse($filelist.val()) : []

        $.each(files, function(i, file){ 
            if(file.name in oldlist) return;

            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){  
                var pieces =  file.name.split(/[\.]+/);
                var extension = pieces[pieces.length - 1];
                var template = '';
                var images = '';
                switch(file.name.match(/\.([\w]+)$/)[1]) {
                    case 'doc':
                    case 'docx':
                        // template += '<div>doc; docx</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-blue.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'xls':
                    case 'xlsx':
                        // template += '<div>xls; xlxs</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-darkgreen.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'ppt':
                    case 'pptx':
                        // template += '<div>ppt; pptx</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-orange.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'pdf':
                        // template += '<div>pdf</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-red.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'zip':
                    case 'rar':
                    case 'tar':
                    case 'gzip':
                    case 'gz':
                    case '7z':
                        // template += '<div>zip; rar; tar; gzip; gz; 7z</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-lightgreen.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'htm':
                    case 'html':
                        // template += '<div>htm; html</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'txt':
                    case 'csv':
                    case 'ini':
                    case 'java':
                    case 'php':
                    case 'js':
                    case 'css':
                        // template += '<div>txt; csv; ini; java; php; js; css</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'avi':
                    case 'mpg':
                    case 'mkv':
                    case 'mov':
                    case 'mp4':
                    case '3gp':
                    case 'webm':
                    case 'wmv':
                    case 'm4a':
                        // template += '<div>avi; mpg; mkv; mov; mp4; 3gp; webm; wmv; m4a</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;
                    
                    case 'mp3':
                    case 'wav':
                        // template += '<div>mp3; wav</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'jpg':
                    case 'jpeg':
                    case 'png':
                    case 'gif':
                        // images += '<img src="">';
                        images += '<div class="image-item">'+
                                    '<img src="'+e.target.result+'">'+
                                    '<a href="#" fi-func="delete" fi-name="'+file.name+'" class="img-delete flex middle center"><span>&#9587;</span></a>'+
                                '</div>';
                    break;

                    default:
                        // template += '<div>default file</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-gray.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                }

                images && $root.find('.image-list').append(images);
                template && $root.find('.file-list').append(template);
                var getfile = { name: file.name, value: e.target.result }
                oldlist.push(getfile)
                $filelist.val(JSON.stringify(oldlist))

            };
        });


    }).closest('.file-attachment').on('click', '[fi-func="delete"]',function(e){
        e.preventDefault();
        var $delete_btn = $(this), 
			f_input		= $(e.delegateTarget).find('input[type="file"]')[0], 
			d_name 		= $delete_btn.attr('fi-name').trim(),
			$filelist  	= $(e.delegateTarget).find('[edit_filelist]');
        var files = JSON.parse($filelist.val()).filter(function(file){
            return file.name !== d_name
        });

        $filelist.val(JSON.stringify(files))

        $delete_btn.closest('.image-item,.file-item').remove();

    });

});

// Edit Task Modal 
function edit_task(task_id) {
    var task_id = task_id;
    var data = { action:'show_edit', task_id:task_id };

    $.ajax({
        url : "task_actions", 
        data : data, 
        dataType: 'json',
        method : "POST",
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
        success: function(r) {
			//console.log(r);
            $('.edit_task_id').val(task_id);
			$('.edit_created_by').val(r.task.created_by);
            $('.edit_subject').val(r.task.title); 
            $('.edit_description').val(r.task.description);
            $('.edit_description_texteditor').html(r.task.description)
            $('.edit_deadline').val(r.task.deadline); 

            if (typeof r.attachment !== undefined && r.attachment.length > 0){
    			var files =	append_files(r);
                //console.log(files);
                $('#edit_task_modal').find('.image-list').append(files[0]);
                $('#edit_task_modal').find('.file-list').append(files[1]);
            }

        },
        complete:function(c){
            var r = c.responseJSON;
            $('#edit_task_modal').modal("show",c);
        }
    })

}

function append_files(files) {

    var images     = '';
    var template  = '';

    $.each(files.attachment,function(k,v){
        var pieces =  v.image.split(/[\.]+/);
        var extension = pieces[pieces.length - 1];
        switch(v.image.match(/\.([\w]+)$/)[1]) {
        case 'doc':
        case 'docx':
            // template += '<div>doc; docx</div>';
            template += '<div class="file-item col-sm-6">'+
                            '<div class="file-wrapper flex middle space-bet">'+
                                '<div class="file-icon"><img src="images/icons/file-blue.png"><span>'+extension+'</span></div>'+
                                '<p class="file-name">'+v.image+'</p>'+
                                '<button type="button" fi-func="delete" fi-name="'+v.image+'">Delete</button>'+
                            '</div>'+
                        '</div>';
        break;

        case 'xls':
        case 'xlsx':
            // template += '<div>xls; xlxs</div>';
            template += '<div class="file-item col-sm-6">'+
                            '<div class="file-wrapper flex middle space-bet">'+
                                '<div class="file-icon"><img src="images/icons/file-darkgreen.png"><span>'+extension+'</span></div>'+
                                '<p class="file-name">'+v.image+'</p>'+
                                '<button type="button" fi-func="delete" fi-name="'+v.image+'">Delete</button>'+
                            '</div>'+
                        '</div>';
        break;

        case 'ppt':
        case 'pptx':
            // template += '<div>ppt; pptx</div>';
            template += '<div class="file-item col-sm-6">'+
                            '<div class="file-wrapper flex middle space-bet">'+
                                '<div class="file-icon"><img src="images/icons/file-orange.png"><span>'+extension+'</span></div>'+
                                '<p class="file-name">'+v.image+'</p>'+
                                '<button type="button" fi-func="delete" fi-name="'+v.image+'">Delete</button>'+
                            '</div>'+
                        '</div>';
        break;

        case 'pdf':
            // template += '<div>pdf</div>';
            template += '<div class="file-item col-sm-6">'+
                            '<div class="file-wrapper flex middle space-bet">'+
                                '<div class="file-icon"><img src="images/icons/file-red.png"><span>'+extension+'</span></div>'+
                                '<p class="file-name">'+v.image+'</p>'+
                                '<button type="button" fi-func="delete" fi-name="'+v.image+'">Delete</button>'+
                            '</div>'+
                        '</div>';
        break;

        case 'zip':
        case 'rar':
        case 'tar':
        case 'gzip':
        case 'gz':
        case '7z':
            // template += '<div>zip; rar; tar; gzip; gz; 7z</div>';
            template += '<div class="file-item col-sm-6">'+
                            '<div class="file-wrapper flex middle space-bet">'+
                                '<div class="file-icon"><img src="images/icons/file-lightgreen.png"><span>'+extension+'</span></div>'+
                                '<p class="file-name">'+v.image+'</p>'+
                                '<button type="button" fi-func="delete" fi-name="'+v.image+'">Delete</button>'+
                            '</div>'+
                        '</div>';
        break;

        case 'htm':
        case 'html':
            // template += '<div>htm; html</div>';
            template += '<div class="file-item col-sm-6">'+
                            '<div class="file-wrapper flex middle space-bet">'+
                                '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                '<p class="file-name">'+v.image+'</p>'+
                                '<button type="button" fi-func="delete" fi-name="'+v.image+'">Delete</button>'+
                            '</div>'+
                        '</div>';
        break;

        case 'txt':
        case 'csv':
        case 'ini':
        case 'java':
        case 'php':
        case 'js':
        case 'css':
            // template += '<div>txt; csv; ini; java; php; js; css</div>';
            template += '<div class="file-item col-sm-6">'+
                            '<div class="file-wrapper flex middle space-bet">'+
                                '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                '<p class="file-name">'+v.image+'</p>'+
                                '<button type="button" fi-func="delete" fi-name="'+v.image+'">Delete</button>'+
                            '</div>'+
                        '</div>';
        break;

        case 'avi':
        case 'mpg':
        case 'mkv':
        case 'mov':
        case 'mp4':
        case '3gp':
        case 'webm':
        case 'wmv':
        case 'm4a':
            // template += '<div>avi; mpg; mkv; mov; mp4; 3gp; webm; wmv; m4a</div>';
            template += '<div class="file-item col-sm-6">'+
                            '<div class="file-wrapper flex middle space-bet">'+
                                '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                '<p class="file-name">'+v.image+'</p>'+
                                '<button type="button" fi-func="delete" fi-name="'+v.image+'">Delete</button>'+
                            '</div>'+
                        '</div>';
        break;
        
        case 'mp3':
        case 'wav':
            // template += '<div>mp3; wav</div>';
            template += '<div class="file-item col-sm-6">'+
                            '<div class="file-wrapper flex middle space-bet">'+
                                '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                '<p class="file-name">'+v.image+'</p>'+
                                '<button type="button" fi-func="delete" fi-name="'+v.image+'">Delete</button>'+
                            '</div>'+
                        '</div>';
        break;

        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'gif':
            // images += '<img src="">';
            images += '<div class="image-item">'+
                        '<img src="public/task_attachment/'+v.image+'">'+
                        '<a href="#" fi-name="'+v.image+'" data-tasks_attachment_id="'+v.tasks_attachment_id+'"  data-task_id="'+files.task.task_id+'" data-task_id="'+files.task.task_id+'" class="img-delete img_del_attachment flex middle center"><span>&#9587;</span></a>'+
                    '</div>';
        break;

        default:
            // template += '<div>default file</div>';
            template += '<div class="file-item col-sm-6">'+
                            '<div class="file-wrapper flex middle space-bet">'+
                                '<div class="file-icon"><img src="images/icons/file-gray.png"><span>'+extension+'</span></div>'+
                                '<p class="file-name">'+v.image+'</p>'+
                                '<button type="button" fi-func="delete" fi-name="'+v.image+'">Delete</button>'+
                            '</div>'+
                        '</div>';
        }
    });

return [images,template];
}


$(document).on('click','#submit_edittask',function(e){
    e.preventDefault();
	var form 		= $(this).closest('form');
	var task_id		= form.find('.edit_task_id').val();
    var title       = form.find('.edit_subject').val();
    var description = form.find('.edit_description').val();
    var deadline    = form.find('.edit_deadline').val();
	var assignee    = $.map(form.find('input[name="task_users[]"]:checked'),function(v,k){  return v.value });
    var unsigned    = $.map(form.find('input[name="task_users[]"]'),function(v,k){ 
		if( $(v).is(":not(:checked)") ) { return v.value }
    });
    
	
	var form_data = new FormData();
		
	$.each(jQuery('#edit_file').prop("files"), function(i, value) {
		form_data.append('images[]', value);
	});

	form_data.append('task_id',task_id);
	form_data.append('title',title);
	form_data.append('description',description);
	form_data.append('deadline',deadline);
	form_data.append('assignee', assignee);
	form_data.append('unsigned', unsigned);


	// var data = {
	// 	task_id:task_id,
	// 	title:title,
	// 	description:description,
	// 	deadline:deadline,
	// 	assignee:assignee,
	// 	unsigned:unsigned,
	// 	file:form_data,
	// }

    $.ajax({
        url:'task/edit_task',
        type:'post',
		data: form_data, 
		cache:false,
		contentType: false,
		processData: false,
		//dataType:'JSON',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
        success:function(r){
            console.log(r);
			$('#edit_task_modal').modal('hide');
		},
		error:function(e){
			//console.log(e);
        }
    });

});

$(document).on('click', '.img_del_attachment', function(e){
    var me = $(this).closest('.image-item');
	var task_id 		  = $(this).attr('data-tasks_attachment_id'); 
    var task_attachment_id = $(this).attr('data-tasks_attachment_id'); 

    var data = { 
        action: 'remove_task_attachment',
        task_id:task_id,
        task_attachment_id:task_attachment_id 
    }
    
    var c = confirm('Remove this image');
    if(c){
        $.ajax({
            url: "task_actions",
            data:data,
            dataType: 'json',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function(r){
                console.log(r);
                if(r.result){
                    me.remove();
                }
            }
        });
    }
});


//Reminder
$(document).on('click','.add_reminder',function(e){
	e.preventDefault();
	var form = $(this).closest('form');
	var reminder_name = form.find('input[name="reminder_name"]').val();
	var start_time    = form.find('input[name="start_time"]').val();
	var repeat_every  = form.find('#select_reminder_form').val();
	var recurring     = form.find('input[name="recurring"]').val();
	var days_of_the_week_true = $.map(form.find('input[name="days_of_the_week[]"]:checked'),function(v,k){  return v.value });
	var days_of_the_week_false = $.map(form.find('input[name="days_of_the_week[]"]'),function(v,k){ 
		if( $(v).is(":not(:checked)") ) { return v.value }
	});
	
	var data = {
		reminder_name: reminder_name, 
		start_time: start_time, 
		repeat_every: repeat_every, 
		recurring: recurring, 
		days_of_the_week_true: days_of_the_week_true, 
		days_of_the_week_false: days_of_the_week_false
	}

	$.ajax({
		url:'add_reminder',
		method: "post",
		dataType: "JSON",
		data: data,
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		success :function(r){
			console.log(r);
		}
	});
});


// View Calendar
function view_calendar () {
    var initialLocaleCode = 'en';
    var modal = $('#calendar_modal').modal("show");
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: "view_calendar",
        data:{token:$('meta[name="csrf-token"]').attr('content') , timezone : $('#timezone').val()  },
        method: "posT" , 
        success : function (response) {
            var json = jQuery.parseJSON(response);
            var fcSources = {
                full : json
            };
            $('#calendar_div').fullCalendar({
                header: {
                    left: '',
                    center: 'prev title next',
                    right: ''
                },
                views: {
                    list: {
                        type: 'agenda',
                        buttonText: 'day'
                    },
                },
                locale: initialLocaleCode,
                eventSources:  [fcSources.full] ,
                
                dayClick: function(calEvent, jsEvent, view) {
                    
                }
            });
        }
    });
}

// Search
function kmsearch(el, lists_selector) {
    var search = el.value,
        lists = document.querySelectorAll(lists_selector),
        component = new RegExp('\('+search+'\)','i')
    for(var i = 0, l = lists.length; i < l; (function(list) { 
          var data = list.getAttribute('aria-data'),
              isMatch = component.test(data)
          list.parentElement.style.display = isMatch ? 'flex' : 'none'
          list.children[0].innerHTML = isMatch ? data.replace(component,'<span class="highlight">$1</span>') : data 
          i++
    })(lists[i], i));
}

// change the status to task complete 
$(document).on('click' , '.task_complete' , function () {
    var task_id = $(this).data('task_id');
    alert(task_id)
});

// for choose if business or personal
$(document).on('change' ,'.radio_member' , function () {
    if ($(this).val() == 'business') { // for business 
        $('#business_panel').show();
        $('#personal_panel').hide();
    } else { // for personal

        $('#business_panel').hide();
        $('#personal_panel').show();
    }
});

// function for view member
function view_member () {
    var modal = $('#member_modal').modal("show");
    $('#business_list').html('');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url : "view_member" , 
        data:{true : 1 ,  _token:$('meta[name="csrf-token"]').attr('content') },
        method : "POST" ,
        success : function ( response )  {
            var json = jQuery.parseJSON(response);
            var tmp = "";
            $.each(json , function ( k , v ) {
                if  (v.profile_image) {
                    var profile_image  = 'profile/'+v.profile_image.image;
                } else {
                    var profile_image  = "images/icons/default-user.png";
                }
                tmp +=  '<li class="flex middle" >'+
                            '<img class="user-image" src="'+profile_image+'">'+
                            '<div class="user-info flex space-bet" aria-data="'+v.firstname+' '+v.lastname+'">'+
                                '<p>'+v.firstname+' '+v.lastname+'</p>'+
                                '<div class="tools flex">'+
                                    '<a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="Delete"><img src="images/icons/minus-icon.png"></a>'+
                                '</div>'+
                            '</div>'+
                        '</li>';
            });
            modal.find('#business_list').append(tmp);
        }
    });
}

function my_personal_team_click () {
    var modal = $('#member_modal');
    $('#personal_list').html('');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url : "view_member_personal" , 
        data:{true : 1 ,  _token:$('meta[name="csrf-token"]').attr('content') },
        method : "POST" ,
        success : function ( response )  {
            var json = jQuery.parseJSON(response);
            console.log(json);
            var tmp = "";
            $.each(json , function ( k , v ) {
                if  (v.profile_image) {
                    var profile_image  = 'profile/'+v.profile_image.image;
                } else {
                    var profile_image  = "images/icons/default-user.png";
                }
                tmp +=  '<li class="flex middle" >'+
                            '<img class="user-image" src="'+profile_image+'">'+
                            '<div class="user-info flex space-bet" aria-data="'+v.firstname+' '+v.lastname+'">'+
                                '<p>'+v.firstname+' '+v.lastname+'</p>'+
                                '<div class="tools flex">'+
                                    '<a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="Delete"><img src="images/icons/minus-icon.png"></a>'+
                                '</div>'+
                            '</div>'+
                        '</li>';
            })
            modal.find('#personal_list').append(tmp);
        }
    });
}

// for button FILTER BY
function filter_btn () { }

// Show modal
$(document).on('show.bs.modal','#add_task_modal', function () {
    var me = $(this);
    var data = '';
    get_teams(me,data);
});

$(document).on('show.bs.modal','#edit_task_modal', function (e) {
    var me = $(this);
    var data = e.relatedTarget.responseJSON;
    get_teams(me,data);

});

// Hide modal
// ---------------------------------------------------
$(document).on('hide.bs.modal','#add_task_modal', function () {
    var me = $(this);
    me.find('.panel-heading').html('');
    me.find('#business_panel').html('');
    me.find('#personal_panel').html('');
}); 

$(document).on('hide.bs.modal','#edit_task_modal', function () { 
    var me = $(this);
    $(this).find('.file-list,.image-list').empty();
    $('.edit_description_texteditor').html('');
    me.find('#personal_panel').hide();
    me.find('.panel-heading').html('');
    me.find('#business_panel').html('');
    me.find('#personal_panel').html('');
    me.find('#business_panel').show();
    me.find('#personal_panel').hide();
}); 

function get_teams(me,data) {
    var me = me; // set instance variable;
    var data = data;

    $.ajax({
        url:'task/get_teams',
        type : 'POST',
        async: false,
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        dataType: "json",
        success: function(r){
            var assignee_title = "";
            var b = false, p = false;
			
            var business_team  = '<div class="checkbox checkbox-gray checkbox-xs">'+
    								'<input id="assignee_business" class="checkAssignee checkAll styled" type="checkbox">'+
    								'<label for="assignee_business">All Members</label>'+
                                 '</div>';
            
            var personal_team  = '<div class="checkbox checkbox-gray checkbox-xs">'+
    								'<input id="assignee_personal" class="checkAssignee checkAll styled" type="checkbox">'+
    								'<label for="assignee_personal">All Members</label>'+
                                 '</div>';

            if(data == '') { // Add Task
                if(r.get_my_business_team.length > 0){
                    b=true;
                    assignee_title += 
                        '<div class="radio radio-gray radio-inline">'+
                            '<input type="radio" name="account_type" id="business" class=" radio_member b_radio" value="business">'+
                            '<label for="radio1">'+
                                'Business Team'+
                            '</label>'+
                        '</div>';
                
                       $.each(r.get_my_business_team,function(k,v){
                           business_team +=
                            '<div class="checkbox checkbox-gray checkbox-xs">'+
							'<input id="assinee_'+v.id+'" class="styled" name="task_users[]" value="'+v.id+'" type="checkbox">'+
                            '<label for="assinee_'+v.id+'">'+v.firstname+' '+v.lastname+'</label>'+
                        '</div>';
                       });
                }
    
                if(r.get_my_personal_team.length > 0){
                    p=true;
                    assignee_title += 
                    '<div class="radio radio-gray radio-inline">'+
                        '<input type="radio" name="account_type" id="personal" class="radio_member p_radio" value="personal">'+
                        '<label for="radio2">'+
                            'Personal'+
                        '</label>'+
                    '</div>';

                    $.each(r.get_my_personal_team,function(k,v){
                        personal_team +=
                        '<div class="checkbox checkbox-gray checkbox-xs">'+
							'<input id="assinee_'+v.id+'" class="styled" name="task_users[]" value="'+v.id+'" type="checkbox">'+
                            '<label for="assinee_'+v.id+'">'+v.firstname+' '+v.lastname+'</label>'+
                        '</div>';
                    });
                }
			
				me.find('#business_panel').html(business_team);
				me.find('#personal_panel').html(personal_team);
				
				if(b == true){
					$('.b_radio').attr('checked', true);
            }

			}

            else { //Edit Task
                if(data.task.type == 'B'){ // BUSINESS
                    assignee_title += 
                    '<div class="radio radio-gray radio-inline">'+
                        '<input type="radio" name="account_type" id="business" class=" radio_member b_radio" value="business" checked>'+
                        '<label for="radio1">'+
                            'Business Team'+
                        '</label>'+
                    '</div>';

                    //load all available business members
                    if(r.get_my_business_team.length > 0){
                        $.each(r.get_my_business_team,function(k,v){
							let display = '';
							if($('.edit_created_by').val() != v.id){ display = 'hide'; }

                            business_team +=
                            '<div class="checkbox checkbox-gray checkbox-xs">'+
								'<input id="assinee_'+v.id+'" class="styled '+display+'" name="task_users[]" value="'+v.id+'" type="checkbox">'+
                                '<label for="assinee_'+v.id+'">'+v.firstname+' '+v.lastname+'</label>'+
                            '</div>';
                        });
                    }
                }
                else { // PERSONAL
                    assignee_title += 
                    '<div class="radio radio-gray radio-inline">'+
                        '<input type="radio" name="account_type" id="personal" class="radio_member p_radio" value="personal" checked>'+
                        '<label for="radio2">'+
                            'Personal'+
                        '</label>'+
                    '</div>';

                    //load all available personal members
                    $.each(r.get_my_personal_team,function(k,v){
						let display = '';
						if($('.edit_created_by').val() != v.id){ display = 'hide'; }
                        personal_team +=
                        '<div class="checkbox checkbox-gray checkbox-xs">'+
							'<input id="assinee_'+v.id+'" class="styled '+display+'" name="task_users[]" value="'+v.id+'" type="checkbox">'+
                            '<label for="assinee_'+v.id+'">'+v.firstname+' '+v.lastname+'</label>'+
                        '</div>';
                    });
                }

                //same business_team variable
                if(data.task.type == 'B'){
                    me.find('#business_panel').html(business_team);
                    me.find('#business_panel').show();
                    me.find('#personal_panel').hide();
                }
                if(data.task.type == 'P'){
                    me.find('#personal_panel').html(personal_team);
                    me.find('#business_panel').hide();
                    me.find('#personal_panel').show();
                    //console.log('wew');
                }

                //mark assignee that are assign  
                $.each(data.assignee,function(k,v){
                    $('#assinee_'+v[0].id).prop('checked',true);
                });

				
            }
            

			me.find('.panel-heading').html(assignee_title);
				

        },
        error: function(e){
            //console.log(e.responseText);
        }
    }); 
}
