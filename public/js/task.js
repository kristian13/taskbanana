$(function() {
    //check if modal is visible 
    $('#view_thread_modal').on('hidden.bs.modal', function () {
        $('.list-card').removeClass('modal_active')
    });

    readmore('.rmore', 75, function(el, oorc){
        //92
        var card = el.closest('.list-card'); 
        if(oorc) card.closest('.list-body').scrollTo(0, parseInt(card.offsetTop) /* - 120*/) 
    })

    readmore('.rmore-modal', 60);
    readmore('.rmore-comment', 52);

    // Toggle Filter
    $(document).ready(function(){
        // Filter 
        // $(".filter-btn").click(function(){
        //     $(".filter-body").slideToggle();
        // });

        // Card Attachment
        $('.attachment-title').click(function() {
            $(this).next('.attachment-list').slideToggle();
            if ($(this).hasClass('hide-label')) {
                $(this).removeClass('hide-label').addClass('show-label');
                $(this).find('.attachment-label').text('Show Attachment');
            } else {
                $(this).removeClass('show-label').addClass('hide-label');
                $(this).find('.attachment-label').text('Hide Attachment');
            }
        });
    });

    // Toggle Add Comment
    $(document).on('click', '.add-comment', function() {
        $(this).addClass('hide');
        $(this).parent().next('.form-group').css('display', 'block');
        // $(this).parent().next('.form-group').slideToggle();
    });

    // $(document).on('click', '.test-notif', function() {
    //  $.notify({
    //      // options
    //      icon: 'glyphicon glyphicon-warning-sign',
    //      title: '',
    //      message: '4th Task Sample Design Layout',
    //      url: 'https://github.com/mouse0270/bootstrap-notify',
    //      target: '_blank'
    //  },{
    //      // settings
    //      element: 'body',
    //      position: null,
    //      type: "remind",
    //      allow_dismiss: true,
    //      newest_on_top: false,
    //      showProgressbar: false,
    //      placement: {
    //          from: "top",
    //          align: "right"
    //      },
    //      offset: 20,
    //      spacing: 10,
    //      z_index: 1031,
    //      delay: 0,
    //      timer: 0,
    //      url_target: '_blank',
    //      mouse_over: null,
    //      animate: {
    //          enter: 'animated fadeInRight',
    //          exit: 'animated fadeOutUp'
    //      },
    //      onShow: null,
    //      onShown: function(){$('.drop-notif').dropdown()},
    //      onClose: null,
    //      onClosed: null,
    //      icon_type: 'class',
    //      template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} flex" role="alert">' +
    //          // '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
    //          // '<span data-notify="icon"></span> ' +
    //          '<img src="images/icons/clock.png">'+
    //          // '<span data-notify="title">{1}</span> ' +
    //          '<div class="flex col">'+
    //              '<span data-notify="message">{2}</span>' +
    //              '<span data-notify="date">01/20/2019</span>' +
    //          '</div>'+
    //          '<div class="notify-tools flex middle space-even">'+
    //              '<a class="complete" href="#" data-notify="dismiss"><p>Complete</p></a>'+
    //              '<div class="dropup">'+ 
    //                  '<a href="#" class="dropdown -toggle remind" id="drop1" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> Remind me </a>'+
    //                   '<ul class="dropdown-menu" aria-labelledby="drop1"> '+
    //                       '<li><a href="#">Never</a></li>'+
    //                       '<li><a href="#">Remind after 5mins</a></li>'+
    //                       '<li><a href="#">Remind after 10mins</a></li>'+
    //                       '<li><a href="#">Remind after 15mins</a></li>'+
    //                       // '<li role="separator" class="divider"></li>'+
    //                       // '<li><a href="#">Separated link</a></li>'+
    //                   '</ul>'+
    //               '</div>'+
    //          '</div>'+
    //          // '<div class="progress" data-notify="progressbar">' +
    //          //  '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
    //          // '</div>' +
    //          // '<a href="{3}" target="{4}" data-notify="url"></a>' +
    //      '</div>' 
    //  });
    // });

    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();

    $(document).on('click', function(e) {
        $('[data-toggle="popover"]').each(function() {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide').data('bs.popover').inState.click = false // fix for BS 3.3.6
            }
        });
    });
});

// Read More
function readmore(se,op,always){
    $(se).each(function(i, el) {
        if($(el).height() > op) { 
            $(el).css('overflow', 'hidden')
            var $n = $('<a/>'), $sis = $('<span/>')
            $sis.text('...')
            $n.prop('href', '#')
            $n.prop('open', true);
            $n.on('click', function(e){
                e.preventDefault()
                if($n.prop('open')) {
                    $n.prop('open', false)
                    $n.text('read more')
                    $n.addClass('morelink')
                    $(el).css('height', op+'px')
                    $sis.css('display', 'block')
                } else {
                    $n.prop('open', true)
                    $n.text('show less')
                    $(el).css('height', 'auto')
                    $sis.css('display', 'none')
                }
                if(always && !e.dispatch) always(this, !$n.prop('open'))
            })
            $(el).parent().append($n[0], $(el).next()[0])
            $n.trigger('click', {dispatch: true});
        }
    })
}

// Add New Member
function add_new_member() {
    var modal = $('#add_new_member_modal').modal("show");
    var $add_modal = $('#add_new_member_modal');
    $add_modal.find('.form-group').removeClass('has-error');
    $add_modal.find('.form-group input').val('');
}

/*// function add_new_member for personal
function add_new_member_personal() {
    $('#business_or_personal').modal("hide");
    var modal = $('#add_new_member_modal').modal("show");
    modal.find('#type_add_member').val("true");
}*/


// this function callout on '.reply_btn' on ajax response data;
// function show threads
function view_threads (task_id, created_by, task_status, archive = false, lang_deadline = false, lang_task_complete = false, lang_reply = false)  {

    $('#created_by_thread_span').html('');
    $('#task_status').html('');
    $('#assignee').empty();
    $('#deadline').html('');
    $('#task_title').html('');
    $('#description').html('');
    $('#reply_div').html('');
    $('#task_complete_div').html('');
    $('#reply_text_area').val('');
    $('#div'+task_id).addClass("modal_active");
    $('#view_thread_modal').find('#reply_form').removeClass('has-error');
    var log_id = $('.body').data('log_id');
    
    // if archive remove text box form
    if (archive == true) {
        $('#reply_form').remove();
        $('#reply_div').remove();
        $('#task_status').hide();
        $('.modal-project').hide();
    }

    $.ajax({
        url: "view_threads",
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        data:{ task_id : task_id , _token:$('meta[name="csrf-token"]').attr('content') },
        method: "POST" , 
        dataType: 'JSON',
        success : function (response) {
            console.log(response);
            var task_card = $('#div'+task_id); // find the task card 
            task_card.find('.unread').text(0); // set all to 0
            var json = response;
            var fullname = json.firstname+' '+json.lastname;
            if (response.storage_checked == true) { // if no storage available
                $('.texteditor-wrap').hide();
                $('.add-file-button').hide();
                $('#reply_div').hide();
                $('#task_complete_div').hide();
            } else {
                $('.texteditor-wrap').show();
                $('#reply_div').show();
                $('#task_complete_div').show();
                $('.add-file-button').show();
            }
           
            $('#created_by_thread_span').html(fullname);
            $('.reply_btn').attr('data-task_id' , json.task_id);
            // $('#reply_div').append("<a href='javascript:void(0);' class='btn btn-info reply_btn'  onclick='reply_function("+json.task_id+"  , "+json.created_by+"   ,\"" + json.task_status + "\")'>Reply</a>");
            // $('#reply_div').append("<a href='javascript:void(0);' data-created_by="+json.task_creator+" data-task_id="+json.task_id+" data-task_status ="+task_status+" class='btn btn-info reply_btn'>Reply</a>");
            $('#reply_div').append('<button type="button" data-created_by="'+json.task_creator+'" data-task_id="'+json.task_id+'" data-task_status ="'+task_status+'" class="btn btn-xs reply_btn" id="btn_return">'+json.button.lang_reply+'</button>');
            if (log_id == json.task_creator ) {
                //$('#task_complete_div').append(' <button type="button" data-task_id='+json.task_id+' class="btn btn-success task_complete">'+lang_task_complete+'</button>');
                $('#task_complete_div').append('<button type="button" data-task_id='+json.task_id+' class="btn btn-xs task_complete" id="btn_done">'+json.button.lang_task_complete+'</button>');
            }
            
            var modal = $('#view_thread_modal').modal("show");
            modal.find('#task_status').append(json.task_status)
            modal.find('#deadline').append(json.deadline)
            modal.find('#task_title').append(json.title)
            modal.find('#description').append(json.description)
            
            // for assignee
            $('.modal_active').find('.card-members').clone().appendTo('#assignee');
            var assignee = "";
            $.each(json.assignee , function (key , value ) {
            //  assignee +='<img data-status="unseen" data-name="'+value.firstname+'" src="images/sample/user-pic-1.png" data-toggle="tooltip" data-placement="bottom" title=" '+value.firstname+' (Unseen)">';

            }); 
           
            // for file attached
            var file_tmp = "",
                $file = "",
                $image = "",
                $image_hidden = "",
                $file_hidden = "";

            $.each(json.attachment , function (key , value ) {
                if (value.file_type === "jpg" || value.file_type === "png" || value.file_type === "jpeg") {
                    $image += '<div class="image-item js-open-viewer">'+
                                    '<a href="javascript:void(0);"><img src="task_thumbnail/'+value.image+'"></a>'+
                            '</div>';
                    $image_hidden += '<div class="hidden-item"><p class="file-type">'+value.file_type+'</p>'+
                                    '<p class="file-name">'+value.image+'</p>'+
                                    '<p class="file-src">'+$('#site_name').data('site_name')+value.full_path+'</p>'+
                                    '<img src="'+$('#site_name').data('site_name')+value.full_path+'"></div>';
                } else {
                    $file_hidden += '<div class="hidden-item"><p class="file-type">'+value.file_type+'</p>'+
                                    '<p class="file-name">'+value.image+'</p>'+
                                    '<p class="file-src">'+$('#site_name').data('site_name')+value.full_path+'</p>'+
                                    '</div>';
                    $file += '<div class="file-item js-open-viewer col-sm-6">'+
                                '<a href="javascript:void(0);" class="file-wrapper flex middle space-bet">'+
                                    '<div class="file-icon">';
                        switch(value.file_type) {
                            case 'doc':
                            case 'docx':
                                $file += '<img src="images/icons/file-blue.png">';
                            break;

                            case 'xls':
                            case 'xlsx':
                                $file += '<img src="images/icons/file-darkgreen.png">';
                            break;

                            case 'ppt':
                            case 'pptx':
                            break;

                            case 'pdf':
                                $file += '<img src="images/icons/file-red.png">';
                            break;

                            case 'zip':
                            case 'rar':
                            case 'tar':
                            case 'gzip':
                            case 'gz':
                            case '7z':
                                $file += '<img src="images/icons/file-lightgreen.png">';
                            break;

                            case 'htm':
                            case 'html':
                                $file += '<img src="images/icons/file-gray.png">';
                            break;

                            case 'txt':
                            case 'csv':
                            case 'ini':
                            case 'csv':
                            case 'java':
                            case 'php':
                            case 'js':
                            case 'css':
                                $file += '<img src="images/icons/file-white.png">';
                            break;

                            case 'avi':
                            case 'mpg':
                            case 'mkv':
                            case 'mov':
                            case 'mp4':
                            case '3gp':
                            case 'webm':
                            case 'wmv':
                            case 'm4a':
                                $file += '<img src="images/icons/file-orange.png">';
                            break;

                            case 'mp3':
                            case 'wav':
                                $file += '<img src="images/icons/file-orange.png">';
                            break;

                            default:
                                $file += '<img src="images/icons/file-white.png">';
                        }
                        $file += '<span>'+value.file_type+'</span>'+
                            '</div>'+
                            '<p class="file-name">'+value.image+'</p>'+
                        '</a>'+
                    '</div>';
                }
                // file_tmp +='<div class="image-item">'+
                //                 '<a href="javascript:void(0);"><img src="task_attachment/'+value.image+'"></a>'+
                //             '</div>';
            });
            
            file_tmp += '<label class="attachment-title hide-label">Hide Attachment</label>'+
                            '<div class="attachment-list" id="attachment-list">'+
                                '<!-- attachment lists -->'+
                                '<div class="file-list clearfix">'+$file+
                                '</div>'+
                                '<div class="image-list clearfix">'+$image+
                                '</div>'+
                                '<div class="file-info hide">'+$file_hidden+''+$image_hidden+'</div>'+
                            '</div>';

            // for comments and images
            var comments = "";
            $.each( json.comment , function (key , value ) {
                // console.log(value.images);
                // console.log(value.images.length);
                comments += '<div class="comment-item">'+
                                '<div class="top flex middle">'+
                                    '<img class="comment-user-img" src="images/sample/user-pic-4.png">'+
                                    '<div>'+
                                        '<p class="comment-user-name">'+value.data.firstname+' '+value.data.lastname+'</p>'+
                                        '<p class="comment-time">'+value.data.date_added+'</p>'+
                                    '</div>'+
                                '</div>'+
                                '<p class="comment-text">'+value.data.comment+'</p>';
                                if (!value.images.length) {
                                    // console.log('no files');
                                } else {
                                    var $image = "",
                                        $file = "",
                                        $image_hidden = "",
                                        $file_hidden = "";

                                    $.each(value.images , function (k , v) {
                                                        if (v.file_type === "jpg" || v.file_type === "png" || v.file_type === "jpeg") {
                                                            $image += '<div class="image-item js-open-viewer">'+
                                                                            '<a href="javascript:void(0);"><img src="'+$('#site_name').data('site_name')+'/storage/tasks_reply_attachment/'+task_id+'/thumbnail/'+v.image+'"></a>'+
                                                                        '</div>';
                                                            $image_hidden += '<div class="hidden-item"><p class="file-type">'+v.file_type+'</p>'+
                                                                            '<p class="file-name">'+v.image+'</p>'+
                                                                            '<p class="file-src">'+$('#site_name').data('site_name')+v.full_path+'</p>'+
                                                                            '<img src="'+$('#site_name').data('site_name')+v.full_path+'"></div>';
                                                        } else {
                                                            $file_hidden += '<div class="hidden-item"><p class="file-type">'+v.file_type+'</p>'+
                                                                            '<p class="file-name">'+v.image+'</p>'+
                                                                            '<p class="file-src">'+$('#site_name').data('site_name')+v.image+'</p>'+
                                                                            '</div>';
                                                            $file += '<div class="file-item js-open-viewer col-sm-6">'+
                                                                        '<a href="javascript:void(0);" class="file-wrapper flex middle space-bet">'+
                                                                            '<div class="file-icon">';
                                                                switch(v.file_type) {
                                                                    case 'doc':
                                                                    case 'docx':
                                                                        $file += '<img src="images/icons/file-blue.png">';
                                                                    break;

                                                                    case 'xls':
                                                                    case 'xlsx':
                                                                        $file += '<img src="images/icons/file-darkgreen.png">';
                                                                    break;

                                                                    case 'ppt':
                                                                    case 'pptx':
                                                                    break;

                                                                    case 'pdf':
                                                                        $file += '<img src="images/icons/file-red.png">';
                                                                    break;

                                                                    case 'zip':
                                                                    case 'rar':
                                                                    case 'tar':
                                                                    case 'gzip':
                                                                    case 'gz':
                                                                    case '7z':
                                                                        $file += '<img src="images/icons/file-lightgreen.png">';
                                                                    break;

                                                                    case 'htm':
                                                                    case 'html':
                                                                        $file += '<img src="images/icons/file-gray.png">';
                                                                    break;

                                                                    case 'txt':
                                                                    case 'csv':
                                                                    case 'ini':
                                                                    case 'csv':
                                                                    case 'java':
                                                                    case 'php':
                                                                    case 'js':
                                                                    case 'css':
                                                                        $file += '<img src="images/icons/file-white.png">';
                                                                    break;

                                                                    case 'avi':
                                                                    case 'mpg':
                                                                    case 'mkv':
                                                                    case 'mov':
                                                                    case 'mp4':
                                                                    case '3gp':
                                                                    case 'webm':
                                                                    case 'wmv':
                                                                    case 'm4a':
                                                                        $file += '<img src="images/icons/file-orange.png">';
                                                                    break;

                                                                    case 'mp3':
                                                                    case 'wav':
                                                                        $file += '<img src="images/icons/file-orange.png">';
                                                                    break;

                                                                    default:
                                                                        $file += '<img src="images/icons/file-white.png">';
                                                                }
                                                                $file += '<span>'+v.file_type+'</span>'+
                                                                    '</div>'+
                                                                    '<p class="file-name">'+v.image+'</p>'+
                                                                '</a>'+
                                                            '</div>';
                                                        }
                                    });
                                    comments += '<label class="attachment-title show-label">Show Attachment</label>'+
                                                '<div class="attachment-list" style="display: none;">'+
                                                    // File Attachment
                                                    '<div class="file-list clearfix">'+$file+
                                                    '</div>'+
                                                    // Image Attachment
                                                    '<div class="image-list clearfix">'+$image+
                                                    '</div>'+
                                                    '<div class="file-info hide">'+$file_hidden+''+$image_hidden+'</div>'+
                                                '</div>';
                                }
                comments +=  '</div>';
            });

           
            
            // modal.find('#assignee').html(assignee)
            modal.find('#thread_comments').html(comments)
            modal.find('#task-file-attachment').html(file_tmp)
            
            // tooltip
            $('[data-toggle="tooltip"]').tooltip();
            
            // Modal Attachment
            $('#view_thread_modal .attachment-title').click(function() {
                $(this).next(".attachment-list").slideToggle();
                if ($(this).hasClass('hide-label')) {
                    $(this).removeClass('hide-label').addClass('show-label').text('Show Attachment');
                } else {
                    $(this).removeClass('show-label').addClass('hide-label').text('Hide Attachment');
                }
            });
        }
    });
}

// Show Threads
$(document).on('click','.view_threads',function() {

    var task_id = $(this).attr('data-task_id');
    var task_creator = $(this).attr('data-task_creator');
    var task_status = $(this).attr('data-task_status');
    var archive = $(this).attr('data-archive');
    var lang_deadline = $(this).attr('data-lang_deadline');
    var lang_reply = $(this).attr('data-lang_reply');
    var lang_task_complete = $(this).attr('data-lang_task_complete');


    $('#created_by_thread_span').html('');
    $('#task_status').html('');
    $('#assignee').empty();
    $('#deadline').html('');
    $('#task_title').html('');
    $('#description').html('');
    $('#reply_div').html('');
    $('#task_complete_div').html('');
    $('#reply_text_area').val('');
    $('#div'+task_id).addClass("modal_active");
    $('#view_thread_modal').find('#reply_form').removeClass('has-error');
    var log_id = $('.body').data('log_id');
    // $.ajaxSetup({
    //     headers: {
    //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //     }
    // });
    
    // if archive remove text box form
    if (archive == true) {
        $('#reply_form').remove();
        $('#reply_div').remove();
    }

     $.ajax({
        url: "view_threads",
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        data:{task_id : task_id },
        method: "POST",
        dataType : 'JSON', 
        success : function (response) {
            var task_card = $('#div'+task_id); // find the task card 
            task_card.find('.unread').text(0); // set all to 0
            var json = response;
            var fullname = json.firstname+' '+json.lastname;
            //console.log(json.storage_checked )
            if (response.storage_checked == true) { // if no storage available
                $('.texteditor-wrap').hide();
                $('.add-file-button').hide();
                $('#reply_div').hide();
                $('#task_complete_div').hide();
            } else {
                $('.texteditor-wrap').show();
                $('#reply_div').show();
                $('#task_complete_div').show();
                $('.add-file-button').show();
            }

            $('#created_by_thread_span').html(fullname);
            $('.reply_btn').attr('data-task_id' , json.task_id);
            // $('#reply_div').append("<a href='javascript:void(0);' class='btn btn-info reply_btn'  onclick='reply_function("+json.task_id+"  , "+json.created_by+"   ,\"" + json.task_status + "\")'>Reply</a>");
            // $('#reply_div').append("<a href='javascript:void(0);' data-created_by="+json.task_creator+" data-task_id="+json.task_id+" data-task_status ="+task_status+" class='btn btn-info reply_btn'>Reply</a>");
            $('#reply_div').append('<button type="button" data-created_by="'+json.task_creator+'" data-task_id="'+json.task_id+'" data-task_status ="'+task_status+'" class="btn btn-xs reply_btn" id="btn_return">'+lang_reply+'</button>');
            if (log_id == json.task_creator ) {
                //$('#task_complete_div').append(' <button type="button" data-task_id='+json.task_id+' class="btn btn-success task_complete">'+lang_task_complete+'</button>');
                $('#task_complete_div').append('<button type="button" data-task_id='+json.task_id+' class="btn btn-xs task_complete" id="btn_done">'+lang_task_complete+'</button>');
            }
            var modal = $('#view_thread_modal').modal("show");
            modal.find('#task_status').append(json.task_status);
            modal.find('#deadline').append(json.deadline);
            modal.find('#task_title').append(json.title);
            modal.find('#description').append(json.description);
            modal.find('.comment_loadmore').attr('current_page',json.pagination.current_page);
            modal.find('.comment_loadmore').attr('last_page',json.pagination.last_page);
            $('#view_thread_modal').find('.comment_loadmore').attr('data-task_id',task_id);
            
            // loadmore comment
            if(json.pagination.current_page == json.pagination.last_page || json.pagination.last_page == 0){
                $('#view_thread_modal').find('.comment_loadmore').hide();
            } else {
                $('#view_thread_modal').find('.comment_loadmore').show();
            }
            
            
            // for assignee
            $('.modal_active').find('.card-members').clone().appendTo('#assignee');
            var assignee = "";
            $.each(json.assignee , function (key , value ) {
            //  assignee +='<img data-status="unseen" data-name="'+value.firstname+'" src="images/sample/user-pic-1.png" data-toggle="tooltip" data-placement="bottom" title=" '+value.firstname+' (Unseen)">';

            }); 

            // for file attached
            var file_tmp = "",
                $file = "",
                $image = "",
                $image_hidden = "",
                $file_hidden = "";

            $.each(json.attachment , function (key , value ) {

                if (value.file_type === "jpg" || value.file_type === "png" || value.file_type === "jpeg") {
                    $image += '<div class="image-item js-open-viewer">'+
                                    '<a href="javascript:void(0);"><img src="'+$('#site_name').data('site_name')+'/storage/tasks_attachment/'+task_id+'/thumbnail/'+value.image+'"></a>'+
                            '</div>';
                    $image_hidden += '<div class="hidden-item"><p class="file-type">'+value.file_type+'</p>'+
                                    '<p class="file-name">'+value.image+'</p>'+
                                    '<p class="file-src">'+$('#site_name').data('site_name')+value.full_path+'</p>'+
                                    '<img src="'+$('#site_name').data('site_name')+value.full_path+'"></div>';
                } else {
                    $file_hidden += '<div class="hidden-item"><p class="file-type">'+value.file_type+'</p>'+
                                    '<p class="file-name">'+value.image+'</p>'+
                                    '<p class="file-src">'+$('#site_name').data('site_name')+value.full_path+'</p>'+
                                    '</div>';
                    $file += '<div class="file-item js-open-viewer col-sm-6">'+
                                '<a href="javascript:void(0);" class="file-wrapper flex middle space-bet">'+
                                    '<div class="file-icon">';
                        switch(value.file_type) {
                            case 'doc':
                            case 'docx':
                                $file += '<img src="images/icons/file-blue.png">';
                            break;

                            case 'xls':
                            case 'xlsx':
                                $file += '<img src="images/icons/file-darkgreen.png">';
                            break;

                            case 'ppt':
                            case 'pptx':
                            break;

                            case 'pdf':
                                $file += '<img src="images/icons/file-red.png">';
                            break;

                            case 'zip':
                            case 'rar':
                            case 'tar':
                            case 'gzip':
                            case 'gz':
                            case '7z':
                                $file += '<img src="images/icons/file-lightgreen.png">';
                            break;

                            case 'htm':
                            case 'html':
                                $file += '<img src="images/icons/file-gray.png">';
                            break;

                            case 'txt':
                            case 'csv':
                            case 'ini':
                            case 'csv':
                            case 'java':
                            case 'php':
                            case 'js':
                            case 'css':
                                $file += '<img src="images/icons/file-white.png">';
                            break;

                            case 'avi':
                            case 'mpg':
                            case 'mkv':
                            case 'mov':
                            case 'mp4':
                            case '3gp':
                            case 'webm':
                            case 'wmv':
                            case 'm4a':
                                $file += '<img src="images/icons/file-orange.png">';
                            break;

                            case 'mp3':
                            case 'wav':
                                $file += '<img src="images/icons/file-orange.png">';
                            break;

                            default:
                                $file += '<img src="images/icons/file-white.png">';
                        }
                        $file += '<span>'+value.file_type+'</span>'+
                            '</div>'+
                            '<p class="file-name">'+$('#site_name').data('site_name')+value.full_path+'</p>'+
                        '</a>'+
                    '</div>';
                }
            });

            file_tmp += '<label class="attachment-title hide-label">Hide Attachment</label>'+
                            '<div class="attachment-list" id="attachment-list">'+
                                '<!-- attachment lists -->'+
                                '<div class="file-list clearfix">'+$file+
                                '</div>'+
                                '<div class="image-list clearfix">'+$image+
                                '</div>'+
                                '<div class="file-info hide">'+$file_hidden+''+$image_hidden+'</div>'+
                            '</div>';

            // for comments and images
            var comments = "";
            $.each( json.comment , function (key , value ) {
                // console.log(value);
                // console.log(value.images.length);
                comments += '<div class="comment-item">'+
                                '<div class="top flex middle">'+
                                    '<img class="comment-user-img" src="images/sample/user-pic-4.png">'+
                                    '<div>'+
                                        '<p class="comment-user-name">'+value.data.firstname+' '+value.data.lastname+'</p>'+
                                        '<p class="comment-time">'+value.data.date_added+'</p>'+
                                    '</div>'+
                                '</div>'+
                                '<p class="comment-text">'+value.data.comment+'</p>';
                                if (!value.images.length) {
                                    // console.log('no files');
                                } else {
                                    var $image = "",
                                        $file = "",
                                        $image_hidden = "",
                                        $file_hidden = "";

                    $.each(value.images , function (k , v) {
                            if (v.file_type === "jpg" || v.file_type === "png" || v.file_type === "jpeg") {
                                $image += '<div class="image-item js-open-viewer">'+
                                                '<a href="javascript:void(0);"><img src="'+$('#site_name').data('site_name')+'/storage/tasks_reply_attachment/'+task_id+'/thumbnail/'+v.image+'"></a>'+
                                            '</div>';
                                $image_hidden += '<div class="hidden-item"><p class="file-type">'+v.file_type+'</p>'+
                                                '<p class="file-name">'+v.image+'</p>'+
                                                '<p class="file-src">'+$('#site_name').data('site_name')+v.full_path+'</p>'+
                                                '<img src="'+$('#site_name').data('site_name')+v.full_path+'"></div>';
                            } else {
                                $file_hidden += '<div class="hidden-item"><p class="file-type">'+v.file_type+'</p>'+
                                                '<p class="file-name">'+v.image+'</p>'+
                                                '<p class="file-src">'+$('#site_name').data('site_name')+v.full_path+'</p>'+
                                                '</div>';
                                $file += '<div class="file-item js-open-viewer col-sm-6">'+
                                            '<a href="javascript:void(0);" class="file-wrapper flex middle space-bet">'+
                                                '<div class="file-icon">';
                                    switch(v.image) {
                                        case 'doc':
                                        case 'docx':
                                            $file += '<img src="images/icons/file-blue.png">';
                                        break;

                                        case 'xls':
                                        case 'xlsx':
                                            $file += '<img src="images/icons/file-darkgreen.png">';
                                        break;

                                        case 'ppt':
                                        case 'pptx':
                                        break;

                                        case 'pdf':
                                            $file += '<img src="images/icons/file-red.png">';
                                        break;

                                        case 'zip':
                                        case 'rar':
                                        case 'tar':
                                        case 'gzip':
                                        case 'gz':
                                        case '7z':
                                            $file += '<img src="images/icons/file-lightgreen.png">';
                                        break;

                                        case 'htm':
                                        case 'html':
                                            $file += '<img src="images/icons/file-gray.png">';
                                        break;

                                        case 'txt':
                                        case 'csv':
                                        case 'ini':
                                        case 'csv':
                                        case 'java':
                                        case 'php':
                                        case 'js':
                                        case 'css':
                                            $file += '<img src="images/icons/file-white.png">';
                                        break;

                                        case 'avi':
                                        case 'mpg':
                                        case 'mkv':
                                        case 'mov':
                                        case 'mp4':
                                        case '3gp':
                                        case 'webm':
                                        case 'wmv':
                                        case 'm4a':
                                            $file += '<img src="images/icons/file-orange.png">';
                                        break;

                                        case 'mp3':
                                        case 'wav':
                                            $file += '<img src="images/icons/file-orange.png">';
                                        break;

                                        default:
                                            $file += '<img src="images/icons/file-white.png">';
                                    }
                                    $file += '<span>'+v.file_type+'</span>'+
                                        '</div>'+
                                        '<p class="file-name">'+$('#site_name').data('site_name')+v.full_path+'</p>'+
                                    '</a>'+
                                '</div>';
                            }
                    });
                    comments += '<label class="attachment-title show-label">Show Attachment</label>'+
                                '<div class="attachment-list" style="display: none;">'+
                                    // File Attachment
                                    '<div class="file-list clearfix">'+$file+
                                    '</div>'+
                                    // Image Attachment
                                    '<div class="image-list clearfix">'+$image+
                                    '</div>'+
                                    '<div class="file-info hide">'+$file_hidden+''+$image_hidden+'</div>'+
                                '</div>';
                                }
                comments +=  '</div>';
            });
            // modal.find('#assignee').html(assignee)
            modal.find('#thread_comments').html(comments)
            modal.find('#task-file-attachment').html(file_tmp)

            // tooltip
            $('[data-toggle="tooltip"]').tooltip();

            // Modal Attachment
            $('#view_thread_modal .attachment-title').click(function() {
                $(this).next(".attachment-list").slideToggle();
                if ($(this).hasClass('hide-label')) {
                    $(this).removeClass('hide-label').addClass('show-label').text('Show Attachment');
                } else {
                    $(this).removeClass('show-label').addClass('hide-label').text('Hide Attachment');
                }
            });
        }
    });
});

function decodeHTMLEntities(text) {
  return $("<textarea/>")
    .text(text)
    .html();
}

function decodeEntities(encodedString) {
    var translate_re = /&(nbsp|amp|quot|lt|gt);/g;
    var translate = {
        "nbsp":" ",
        "amp" : "&",
        "quot": "\"",
        "lt"  : "<",
        "gt"  : ">"
    };
    return encodedString.replace(translate_re, function(match, entity) {
        return translate[entity];
    }).replace(/&#(\d+);/gi, function(match, numStr) {
        var num = parseInt(numStr, 10);
        return String.fromCharCode(num);
    });
}

$(document).on('click','.comment_loadmore',function() {
    var me = $(this);
    var task_id = $(this).data('task_id');
    var page = parseInt($(this).attr('current_page'));    
        page = (page + 1);

    $.ajax({
        url   : "loadmore_comment",
        data  : { task_id : task_id, page : page },
        header: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        method: "get" ,
        beforeSend: function() {
            me.text('Loading...');
        }, 
        success : function (r) {
            if(r.current_page == r.last_page){ me.hide();}

            $.each(r.data,function(k,v){
                var comments = '<div class="comment-item">'+
                                        '<div class="top flex middle">'+
                                            '<img class="comment-user-img" src="'+v.image+'">'+
                                            '<div>'+
                                                '<p class="comment-user-name">'+v.firstname+' '+v.lastname+'</p>'+
                                                '<p class="comment-time">'+v.date_added+'</p>'+
                                            '</div>'+
                                        '</div>'+
                                        '<p class="comment-text">'+v.comment+'</p>'+
                                '</div>';
            
                $('#view_thread_modal').find('#thread_comments').append(comments);
            })
           
            $('#view_thread_modal').find('.comment_loadmore').attr('current_page',r.current_page);             
        },
        complete: function(){
            me.text('Load More');
        }
    });
});

//for threads reply
$(document).on('click' , '.reply_btn' , function(e) {
    e.preventDefault();
    var task_id_raw =$(this).data('task_id'); 
    var message =  $(this).closest('#view_thread_modal').find('.texteditor-content').html();
    var $modal_textarea =  $(this).closest('form').find('.texteditor-wrap');
    var $card_formgroup = $(this).closest('.form-group');
    var $card_textarea = $(this).closest('form').find('.card-text-area');
    $modal_textarea.parent().removeClass('has-error');
    $card_formgroup.removeClass('has-error');


    if ($(this).attr("id") === "btn_return") {
        if ($.trim($modal_textarea.find('textarea').val()) === '') {
            $modal_textarea.parent().addClass('has-error');
            return false;
        }
    } else {
        if ($.trim($card_textarea.val()) === '') {
            //console.log($(this).closest('.form-group').addClass('has-error'));
            return false;
        }
    }

    $('#div'+task_id_raw).addClass('modal_active');
        var task_id =  $(this).data('task_id'),

        created_by =  $(this).data('created_by'),
        task_status =  $(this).data('task_status'),
        log_id = $('.body').data('log_id'),
        full_name = $('.body').data('fullname'),
        date = new Date(),
        formatted = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds(),
        $counting = 0,
        $form = $(this).closest('form'),
        $description = $form.find('[name="reply"], [name="comment"]');

    if (task_status == 'N') {
        var clone = $( "#div"+task_id ).clone().prependTo( ".waiting_div:first" );
        var view_threads = clone.find('.view_threads').attr('data-task_status','I').attr("onclick" , "view_threads( "+task_id+" , "+created_by+" , 'I' );");
        clone.find('.rmore-comment').html(decodeHTMLEntities($('#reply_text_area').val()))
        clone.find('.rmore-comment').html(decodeHTMLEntities($form.find('textarea').val()))
        clone.find('.comment-time').html(formatted)
        clone.find('.user-name').html((  full_name  ));
       /* clone.find('textarea').val('')*/
        $('.modal_active:first').remove();
    } else if (task_status == 'I') {
        var clone = $( "#div"+task_id );
        $('#div'+task_id).find('.rmore-comment').nextAll().remove();
        clone.find('.rmore-comment').html(message)
        clone.find('.comment-time').html(formatted)
        clone.find('.user-name').html((  full_name  ));
       /* clone.find('textarea').val('')*/
        /*var view_threads = clone.find('.view_threads').attr('data-task_status','I').attr("onclick" , "view_threads( "+task_id+" , "+created_by+" , 'I' );");*/
    }else{
        var clone = $( "#div"+task_id ).clone().prependTo( ".waiting_div" );
        var view_threads = clone.find('.view_threads').attr('data-task_status','I').attr("onclick" , "view_threads( "+task_id+" , "+created_by+" , 'I' );");
        $('#div'+task_id).find('.rmore-comment').nextAll().remove();
        clone.find('.rmore-comment').html(message)
        clone.find('.comment-time').html(formatted)
        clone.find('.user-name').html((  full_name  ));
        $('.modal_active:last').remove();
       /* clone.find('textarea').val('')*/
    }


    // var file_data = $('.file_reply').prop('files');  
    // var form_data = new FormData(); 
    // form_data.append('reply', $('#reply_text_area').val());  
    // form_data.append('task_id', task_id);    
    // form_data.append('task_status',task_status);

    var form_data = new FormData(); 
    form_data.append('task_id', task_id);
    form_data.append('task_status', task_status);
    form_data.append('reply', message);
    $.each($('#show_add_file').prop("files"), function(i, value) {
        form_data.append('files[]', value);
    });
   
    $.ajax({
        url : "reply_thread" , 
        data : form_data , 
        method : "POST" , 
        context: this,
        cache:false,
        processData: false , 
        contentType: false,
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
        success : function (response) {
            //console.log(response);
            clone.find('textarea').val('')
            // if($(this).closest('#reply_form').length) 
            window.view_threads(task_id, created_by, task_status, false, false, false, true);

            //reset after submit
            setToDefaultReply()
        }
    });

});

function setToDefaultReply(){
    $('#show_add_file').val('');
    $('#view_thread_modal').find('.image-list').empty();
}

//for task card reply
$(document).on('click' , '.quick_reply' , function(e) {
    e.preventDefault();
    var task_id_raw =$(this).data('task_id'); 
    var message =  $(this).closest('form').find('textarea').val();
    var $modal_textarea =  $(this).closest('form').find('.texteditor-wrap');
    var $card_formgroup = $(this).closest('.form-group');
    var $card_textarea = $(this).closest('form').find('.card-text-area');
    $modal_textarea.parent().removeClass('has-error');
    $card_formgroup.removeClass('has-error');

    if ($(this).attr("id") === "btn_return") {
        if ($.trim($modal_textarea.find('textarea').val()) === '') {
            $modal_textarea.parent().addClass('has-error');
            return false;
        }
    } else {
        if ($.trim($card_textarea.val()) === '') {
            return false;
        }
    }

    $('#div'+task_id_raw).addClass('modal_active');
        var task_id =  $(this).data('task_id'),

        created_by =  $(this).data('created_by'),
        task_status =  $(this).data('task_status'),
        log_id = $('.body').data('log_id'),
        full_name = $('.body').data('fullname'),
        date = new Date(),
        formatted = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds(),
        $counting = 0,
        $form = $(this).closest('form'),
        $description = $form.find('[name="reply"], [name="comment"]');

    if (task_status == 'N') {
        var clone = $( "#div"+task_id ).clone().prependTo( ".waiting_div:first" );
        var view_threads = clone.find('.view_threads').attr('data-task_status','I').attr("onclick" , "view_threads( "+task_id+" , "+created_by+" , 'I' );");
        clone.find('.rmore-comment').html(decodeHTMLEntities($('#reply_text_area').val()))
        clone.find('.rmore-comment').html(decodeHTMLEntities($form.find('textarea').val()))
        clone.find('.comment-time').html(formatted)
        clone.find('.user-name').html((  full_name  ));
        $('.modal_active:first').remove();
    } else if (task_status == 'I') {
        var clone = $( "#div"+task_id );
        $('#div'+task_id).find('.rmore-comment').nextAll().remove();
        clone.find('.rmore-comment').html(decodeHTMLEntities($form.find('textarea').val()))
        clone.find('.comment-time').html(formatted)
        clone.find('.user-name').html((  full_name  ));
    }else{
        var clone = $( "#div"+task_id ).clone().prependTo( ".waiting_div" );
        var view_threads = clone.find('.view_threads').attr('data-task_status','I').attr("onclick" , "view_threads( "+task_id+" , "+created_by+" , 'I' );");
        $('#div'+task_id).find('.rmore-comment').nextAll().remove();
        clone.find('.rmore-comment').html(decodeHTMLEntities($form.find('textarea').val()))
        clone.find('.comment-time').html(formatted)
        clone.find('.user-name').html((  full_name  ));
        $('.modal_active:last').remove();
    }

    
    $files =  $form.find('.file-custom-input');
    var form_data = new FormData(); 
    form_data.append('task_id', task_id);
    form_data.append('task_status', task_status);
    form_data.append('reply', message);
    $.each($files.prop("files"), function(i, value) {
        form_data.append('files[]', value);
    });
   

    $.ajax({
        url : "reply_thread" , 
        data : form_data , 
        method : "POST" , 
        context: this,
        cache:false,
        processData: false , 
        contentType: false,
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
        success : function (response) {
            //console.log(response);
            clone.find('textarea').val('')
            if($(this).closest('#reply_form').length) 
            window.view_threads(task_id, created_by, task_status, false, false, false, true);
        }
    });

});

// function reply_function (task_id , created_by ,  task_status) {
//  // validation first
//  if ($('#reply_text_area').val() == "") {
//      // add css here for designers
//      alert("please reply!");
//      return false;
//  } 

//  $.ajaxSetup({
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         }
//     });
//  var formData =$('#reply_form').serializeArray();
//  console.log(formData);
//     $.ajax({
//      url : "reply_thread" , 
//      data : formData , 
//      method : "POST" , 
//      cache:false,
//         contentType: false,
//         processData: false,
//      success : function (response) {
//          
//      }
//     });

//  var clone = $( "#div"+task_id ).clone().appendTo( ".waiting_div" );
//  var view_threads = clone.find('.view_threads').attr('data-task_status','I').attr("onclick" , "view_threads( "+task_id+" , "+created_by+" , 'I' );");

// }

// Remove Add Files
$(document).on('click', '.img-delete', function(){
    $(this).parent().remove();
});

// Add Task Submit Error Validation
$(document).on('click', '#submit_addtask', function(e) {
    e.preventDefault();

    var $form = $(this).closest('form'),
        account_type = $form.find('input[name="account_type"]:checked').val(),
        $count_error = 0,
        $title = $form.find('[name="title"]'),
        $description = $form.find('[name="description"]'),
        $select = $form.find('select'),
        $new_input = $form.find('[name="temp_project_title"]'),
        $checked = $('.assignee-checkbox [type="checkbox"]:checked').length,
        $dropdown_add = $form.find('.dropdown-add');
    
    // title validation
    if ($.trim($title.val()) === '') {
        $title.closest('.form-group').addClass('has-error');
    } else {
        $title.closest('.form-group').removeClass('has-error');
    }

    // description validation
    if (($.trim($description.val()) === '') || ($.trim($description.val()) === '<p><br></p>')) {
        $description.closest('.form-group').addClass('has-error');
    } else {
        $description.closest('.form-group').removeClass('has-error');
    }

    // project title validation => ($select.val() === '1') && ($.trim($new_input.val()) !== '')
    if ($.trim($new_input.val()) !== '') {
        $dropdown_add.addClass('invalid');
        $dropdown_add.closest('.form-group').addClass('has-error');
        $dropdown_add.closest('.form-group').find('.invalid-feedback').addClass('show');
        $dropdown_add.closest('.form-group').find('.invalid-input').addClass('hide');
    } else {
        $dropdown_add.find('.add-to-dropdown').addClass('add-option').removeClass('add-to-dropdown').html('&#9547;');
        $dropdown_add.find('[name="temp_project_title"], .cancel-add').addClass('hide');
        $dropdown_add.removeClass('invalid');
        $dropdown_add.closest('.form-group').removeClass('has-error');
        $dropdown_add.closest('.form-group').find('.invalid-feedback').removeClass('show');
        $dropdown_add.closest('.form-group').find('.invalid-input').removeClass('hide');
    }
    // count remaining errors
    $form.find('.has-error').each(function() {
        $count_error++;
    });

    if ($count_error > 0) {
        return false;
    } else {

        // var data   = new FormData($form[0]);
        var form        = $(this).closest('form');
        var category_id = form.find('#category_add_task option:selected').val();
        var title       = form.find('input[name=title]').val();
        var description = form.find('textarea[name=description]').val();
        var deadline    = form.find('input[name=deadline]').val();
        var assignee    = JSON.stringify($.map(form.find('.assignee-item a'), function(v,k) { return $(v).attr('data-user-id'); }));

        var form_data = new FormData();
        
        $.each(jQuery('#add_file').prop("files"), function(i, value) {
            form_data.append('images[]', value);
        });

        form_data.append('category_id',category_id);
        form_data.append('title',title);
        form_data.append('description',description);
        form_data.append('deadline',deadline);
        form_data.append('assignee', assignee);

        $.ajax({
            url : 'add_task' ,
            method : "POST" ,
            data :  form_data ,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            dataType : 'JSON',
            cache:false,
            processData: false , 
            contentType: false,
            dataType: "JSON" ,
            beforeSend : function () {
            } ,
            success : function ( response ) {
                console.log(response);
                var site_name = $('#site_name').data('site_name');
                var modal = $('#add_task_modal'),
                project_title = modal.find('[name="category_id"] [selected]').text(),
                task_name = modal.find('[name="title"]').val(),
                description = modal.find('.texteditor-content').html(),
                deadline_val = modal.find('[name="deadline"]').val(),
                deadline = deadline_val === "" ? new Date().toDateString("yyyy-MM-dd") : new Date(modal.find('[name="deadline"]').val()).toDateString("yyyy-MM-dd"),
                date_created = new Date().toDateString("yyyy-MM-dd"),
                assignee = modal.find('.assignee-item'),
                file = modal.find('.file-attachment .file-list'),
                image = modal.find('.file-attachment .image-list'),
                account_id = $('body').attr('data-log_id'),
                account_name = $('body').attr('data-fullname');

                var tmp = "", task_id = "", files = "", url = "", content = "";
                $.each(response.result , function (k, v) {
                    task_id = v.task_id;
                    url += site_name+v.full_path;
                    tmp += "<img src='"+site_name+v.full_path+"'>";
                    content += v.image+","+site_name+v.full_path+";";
                });

                var file_names = content.split(';'),
                file_item = "",
                hidden_file = "",
                image_item = "",
                hidden_image = "",
                hidden = "";

                for (i = 0; i < file_names.length; i++) {
                    var data = file_names[i].split(',');
                    var file = data[0];
                    var url = data[1];
                    var ext = file.split('.').reverse()[0];
                    if (ext=== "jpg" || ext === "jpeg" || ext === "png" || ext === "gif" || ext === "JPG" || ext === "JPEG" || ext === "PNG" || ext === "GIF") {
                        // console.log(file_names[i]);
                        image_item += '<div class="image-item js-open-viewer">'+
                                            '<img src="'+url+'">'+
                                        '</div>';
                        hidden_image += '<div class="hidden-item">'+
                                            '<div class="file-type">'+ext+'</div>'+
                                            '<div class="file-name">'+file+'</div>'+
                                            '<div class="file-src">'+url+'</div>'+
                                        '</div>';
                    } else if (ext === "doc" || ext === "DOC") {
                        file_item += '<div class="file-item js-open-viewer col-sm-6">'+
                                            '<div class="file-wrapper flex middle space-bet">'+
                                                '<div class="file-icon">'+
                                                    '<img src="images/icons/file-blue.png">'+
                                                    '<span>'+ext+'</span>'+
                                                '</div>'+
                                                '<p class="file-name">'+file+'</p>'+
                                            '</div>'+
                                        '</div>';
                        hidden_file += '<div class="hidden-item">'+
                                            '<div class="file-type">'+ext+'</div>'+
                                            '<div class="file-name">'+file+'</div>'+
                                            '<div class="file-src">'+url+'</div>'+
                                        '</div>';
                    } else if (ext === "xls" || ext === "xlsx" || ext === "XLS" || ext === "XLXS") {
                        file_item += '<div class="file-item js-open-viewer col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon">'+
                                                '<img src="images/icons/file-darkgreen.png">'+
                                                '<span>'+ext+'</span>'+
                                            '</div>'+
                                            '<p class="file-name">'+file+'</p>'+
                                        '</div>'+
                                    '</div>';
                        hidden_file += '<div class="hidden-item">'+
                                            '<div class="file-type">'+url+'</div>'+
                                            '<div class="file-name">'+file+'</div>'+
                                            '<div class="file-src">'+url+'</div>'+
                                        '</div>';
                    } else if (ext === "ppt" || ext === "pptx" || ext === "PPT" || ext === "PPTX") {
                        file_item += '<div class="file-item js-open-viewer col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon">'+
                                                '<img src="images/icons/file-orange.png">'+
                                                '<span>'+ext+'</span>'+
                                            '</div>'+
                                            '<p class="file-name">'+file+'</p>'+
                                        '</div>'+
                                    '</div>';
                        hidden_file += '<div class="hidden-item">'+
                                            '<div class="file-type">'+url+'</div>'+
                                            '<div class="file-name">'+file+'</div>'+
                                            '<div class="file-src">'+url+'</div>'+
                                        '</div>';
                    } else if (ext === "pdf" || ext === "PDF") {
                        file_item += '<div class="file-item js-open-viewer col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon">'+
                                                '<img src="images/icons/file-red.png">'+
                                                '<span>'+ext+'</span>'+
                                            '</div>'+
                                            '<p class="file-name">'+file+'</p>'+
                                        '</div>'+
                                    '</div>';
                        hidden_file += '<div class="hidden-item">'+
                                            '<div class="file-type">'+url+'</div>'+
                                            '<div class="file-name">'+file+'</div>'+
                                            '<div class="file-src">'+url+'</div>'+
                                        '</div>';
                    } else if (ext === "zip" || ext === "rar" || ext === "tar" || ext === "gzip" || ext === "gz" || ext === "7z" || ext === "ZIP" || ext === "RAR" || ext === "TAR" || ext === "GZIP" || ext === "GZ" || ext === "7Z") {
                        file_item += '<div class="file-item js-open-viewer col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon">'+
                                                '<img src="images/icons/file-lightgreen.png">'+
                                                '<span>'+ext+'</span>'+
                                            '</div>'+
                                            '<p class="file-name">'+file+'</p>'+
                                        '</div>'+
                                    '</div>';
                        hidden_file += '<div class="hidden-item">'+
                                            '<div class="file-type">'+url+'</div>'+
                                            '<div class="file-name">'+file+'</div>'+
                                            '<div class="file-src">'+url+'</div>'+
                                        '</div>';
                    }
                }
                hidden += hidden_file;
                hidden += hidden_image;

                var assignee_list = "";
                assignee.each(function() {
                    var img = $(this).find('img').attr('src');
                    var assignee_name = $(this).find('span').text();
                    var assignee_id = $(this).find('.assignee_choose').attr('data-user-id');
                    assignee_list += '<img data-status="unseen" class="a '+assignee_id+'" data-generate="'+assignee_id+'" data-name="'+assignee_name+'" aria-data="'+assignee_name+'" src="'+img+'" data-toggle="tooltip" data-placement="bottom" title="'+assignee_name+' (Unseen)">';
                });

                // Deadline Text Remove when deadline null
                if(response.data.deadline === null){
                    var deadline_txt = '';
                    var deadline = 'No Deadline';
                } else{
                    var deadline_txt = 'Deadline:';
                }

                var list_card = '<div class="list-card clearfix '+task_id+'" id="div'+task_id+'" data-t="$2y$10$wq3SA8gn5CyoY57nX6LfXOM5uLYu/kZrDOq.W5/mWhkRhjW1uhZZK">'+
                                '<div class="list-tools flex space-bet">'+
                                    '<span class="project-name">'+project_title+'</span>'+
                                    '<div class="flex">'+
                                        '<div class="btn-group card-menu">'+
                                            '<span class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                                                '<img src="'+$('#site_name').data('site_name')+'/images/icons/card-ellipsis.png">'+
                                            '</span>'+
                                            '<div class="dropdown-menu dropdown-menu-right">'+
                                                '<ul>'+
                                                    '<li>'+
                                                        '<a href="javascript:void(0);" class="dropdown-item edit-task" data-creator="'+account_id+'" data-task_name="" data-task_id="'+task_id+'">'+
                                                            '<p>Edit</p>'+
                                                        '</a>'+
                                                    '</li>'+
                                                    '<li>'+
                                                        '<a href="javascript:void(0);" class="dropdown-item remove-task" data-task_id="$2y$10$wq3SA8gn5CyoY57nX6LfXOM5uLYu/kZrDOq.W5/mWhkRhjW1uhZZK">'+
                                                            '<p>Delete</p>'+
                                                        '</a>'+
                                                    '</li>'+
                                                '</ul>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="list-info">'+
                                    '<p class="date-created">'+date_created+'</p>'+
                                    '<h3 class="title" aria-data="Test">'+task_name+'</h3>'+
                                    '<p class="created-by">By <span>'+account_name+'</span></p>'+
                                    '<div class="deadline">'+
                                        '<img src="'+$('#site_name').data('site_name')+'/images/icons/card-date.png">'+
                                        '<span class="badge-text">'+deadline_txt+'  '+deadline+'</span>'+
                                    '</div>'+
                                    '<div class="description rmore">'+
                                        description+
                                    '</div>'+
                                    '<br clear="all">'+
                                    '<div class="card-members">'+
                                        '<p id="assignee-to">Assigned to</p>'+ assignee_list+
                                        '<button class="add-member-btn" data-t="$2y$10$wq3SA8gn5CyoY57nX6LfXOM5uLYu/kZrDOq.W5/mWhkRhjW1uhZZK" data-task_creator="'+account_id+'" data-task-g="'+task_id+'" data-toggle="tooltip" data-placement="bottom" title="Add Member">'+
                                            '<div class="add-icon">╳</div>'+
                                        '</button>'+
                                    '</div>'+
                                    '<label class="attachment-title show-label"><span class="attachment-label">Show Attachment</span> <img src="'+$('#site_name').data('site_name')+'/images/icons/card-clip.png"><span class="count-items">'+(parseInt(file_names.length) - 1)+'</span></label>'+
                                    '<div class="attachment-list" style="display: none;">'+
                                        '<div class="file-attachment">'+
                                            '<div class="file-list clearfix">'+file_item+
                                            '</div>'+
                                            '<div class="image-list clearfix">'+image_item+
                                            '</div>'+
                                            '<div class="file-info hide">'+hidden+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';

                $('#new_task .list-body').prepend(list_card);
                $('#add_task_modal').modal('hide');
            },
            complete : function () {
                $('#add_task_modal').find('#search_member').val('');
                $('#add_task_modal').find('.assignee-list').addClass('hide').html('');
            }
        }); 
    }

});



// Reset Add Task Modal
$(document).on('click', '#add_task', function() {
    // storage check 
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax({
        url:'check_storage',
        method : "POST" , 
        success : function (response) {
            console.log(response)
            if (response == '1') { // if full
                $('#storage_error_modal').modal("show");
            } else {
                $('#add_task_modal').modal($('body').is('.tutorial-mode') ? {backdrop:'static',keyboard:false,show:true} : 'show');
                //$('#category_add_task').html(''); // set select to empty
                $add_task = $('#add_task_modal');
                $add_task.find('#category_add_task').html(''); // set select to empty
                $add_task.find('.form-group').removeClass('has-error');
                $add_task.find('.dropdown-add').removeClass('invalid');
                $add_task.find('.add-to-dropdown').addClass('add-option').removeClass('add-to-dropdown').html('&#9547;');
                $add_task.find('[name="temp_project_title"], .cancel-add, .task-percent').addClass('hide');
                $add_task.find('[name="title"], [name="description"], [name="deadline"], [name="temp_project_title"]').val('');
                $add_task.find('.texteditor-content').html('<p><br></p>');
                $add_task.find('.task-list').empty();
                $add_task.find('option').attr('selected', false);
                $add_task.find('option[value="1"]').attr('selected', '');
                $add_task.find('select').val('1').trigger('change');
                $add_task.find('.assignee-checkbox [type="checkbox"]').prop('checked', false);
                $add_task.find('[fi-func="delete"]').trigger('click');

                // append all project titles
                $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}}); // ajax setup 1 liner
                $.ajax({
                    url : "get_categories" ,
                    data:{token:$('meta[name="csrf-token"]').attr('content')},
                    method : "POST", 
                    success  : function (response) {
                        var tmp = "";
                        // var json = jQuery.parseJSON(response);

                        $.each(response, function (k, v) {
                            if (v.category_id === 1) {
                                tmp += '<option value="'+v.category_unique+'" selected>'+v.category_name+'</option>';
                                $add_task.find(' .task-percent').addClass('hide');
                            }else{
                                 tmp += '<option value="'+v.category_unique+'">'+v.category_name+'</option>';
                            } 
                        });
                            
                        $('#category_add_task').append(tmp);
                        if ($add_task.attr('data-create_new') !== 'false') {
                            $('#category_add_task').val($add_task.attr('data-create_new'))
                        }else {
                            $('#category_add_task').val(1)
                            $('#category_add_task').append(tmp);
                        }
                    }  
                });
            }
        }
});
});

//CREATE Project old
/*$(document).on('click', '#create_project', function() {
    // storage check 
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax({
        url:'check_storage',
        method : "POST" , 
        success : function (response) {

            if (response == '1') { // if full
                $('#storage_error_modal').modal("show");
            } else {
                $('#create_project_task_modal').modal("show");
               
                $add_task = $('#create_project_task_modal');
                $add_task.find('#category_add_task').html(''); // set select to empty
                $add_task.find('.form-group').removeClass('has-error');
                $add_task.find('.dropdown-add').removeClass('invalid');
                $add_task.find('.add-to-dropdown').addClass('add-option').removeClass('add-to-dropdown').html('&#9547;');
                $add_task.find('[name="temp_project_title"], .cancel-add, .task-percent').addClass('hide');
                $add_task.find('[name="title"], [name="description"], [name="deadline"], [name="temp_project_title"]').val('');
                $add_task.find('.texteditor-content').html('<p><br></p>');
                $add_task.find('.task-list').empty();
                $add_task.find('option').attr('selected', false);
                $add_task.find('option[value="1"]').attr('selected', '');
                $add_task.find('select').val('1').trigger('change');
                $add_task.find('.assignee-checkbox [type="checkbox"]').prop('checked', false);
                $add_task.find('[fi-func="delete"]').trigger('click');

                // append all project titles
                $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}}); // ajax setup 1 liner
                $.ajax({
                    url : "get_categories" ,
                    data:{token:$('meta[name="csrf-token"]').attr('content')},
                    method : "POST", 
                    success  : function (response) {
                        var tmp = "";
                        var json = jQuery.parseJSON(response);
                        tmp += '<option value="" selected>Please Add Project Name</option>';
                        $add_task.find(' .task-percent').addClass('hide');
                        $.each(json , function (k , v) {
                            if (v.category_id === 1) {
                                tmp += '<option value="'+v.category_unique+'" >'+v.category_name+'</option>';
                                //$add_task.find(' .task-percent').addClass('hide');
                            } else {
                                tmp += '<option value="'+v.category_unique+'">'+v.category_name+'</option>';
                }
                        });
                        $('#category_add_task').append(tmp);
                    }  
                });
            }
        }
});
});*/

//CREATE PROJECT NEW
$(document).on('click'  ,'#create_project' , function () {
    $('#create_project_task_modal').show();
    $('#create_project_task_modal').modal("show");
});

// create a project click button
$(document).on('click' ,'.btn-add-project' , function (e) {
    var $input = $(this).closest('.modal-content').find('[name="project_title"]');
    $input.parent().removeClass('has-error');
   
    if ($.trim($input.val()) === "") {
        $input.parent().addClass('has-error');
        return false;
    } else {
        var project = $('#create_project_task_modal [name="project_title"]').val();
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url : 'add_project' , 
            data : {project_title : project} ,
            method : "POST" ,
            success : function (response) {
                console.log(response);
                $('#add_task_modal').attr("data-create_new" , response);
              
                $('#create_project_task_modal').removeClass("in");
                $('.modal-backdrop').remove();
                $('#create_project_task_modal').hide();
                e.stopPropagation(); 
                $('#add_task').trigger("click");
            } 
        });
    }
    e.preventDefault();
});


// create a project keypress
$(document).on('keyup' ,'.new_project_name' , function (e) {
     if(e.which === 13){ // 13 === enter on keyboard
            var $input = $(this).closest('.modal-content').find('[name="project_title"]');
            $input.parent().removeClass('has-error');

            if ($.trim($input.val()) === "") {
                $input.parent().addClass('has-error');
                return false;
            } else {
                var project = $('#create_project_task_modal [name="project_title"]').val();
                $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
                $.ajax({
                    url : 'add_project' , 
                    data : {project_title : project} ,
                    method : "POST" ,
                    success : function (response) {
                        console.log(response);
                        $('#add_task_modal').attr("data-create_new" , response);
                      
                        $('#create_project_task_modal').removeClass("in");
                        $('.modal-backdrop').remove();
                        $('#create_project_task_modal').hide();
                        e.stopPropagation(); 
                        $('#add_task').trigger("click");
                    } 
                }) 
            }

             e.preventDefault();
     }
   
  
});

// Add Option Button for Project Title
$(document).on('click', '.dropdown-add .add-option', function() {
    var $dropdown_add = $(this).closest('.dropdown-add');
    $(this).addClass('add-to-dropdown').removeClass('add-option').text('Add');
    $(this).closest('.form-group').removeClass('has-error');
    $dropdown_add.find('[name="temp_project_title"], .cancel-add').removeClass('hide');
});

// Select Project Title Validation
$(document).on('change', '.dropdown-add select', function() {
    var me = $(this);
    var $dropdown_add = $(this).closest('.dropdown-add');
    $dropdown_add.find('.add-to-dropdown').addClass('add-option').removeClass('add-to-dropdown').html('&#9547;');
    $dropdown_add.find('[name="temp_project_title"], .cancel-add').addClass('hide');
    $dropdown_add.find('[name="temp_project_title"]').val('');
    $dropdown_add.removeClass('invalid');
    $dropdown_add.closest('.form-group').find('.invalid-feedback').removeClass('show');
    $dropdown_add.closest('form').find('.invalid-label, .max-label, .invalid-max-label').addClass('hide');
    $dropdown_add.closest('form').find('.task-percent .note').removeClass('hide').css('visibility', 'visible');
    $(this).closest('.form-group').removeClass('has-error');

    var $modal = $(this).closest('#add_task_modal');
    var $task_list = '<div class="x new-task" id="slide_div">'+
                        '<h6>New Task <span id="span" class="percent-count">0%</span></h6>'+
                        '<div data-percentage="0" data-task_name="" class="slider_div_class ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">'+
                            '<div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min" style="width: 0%;"></div>'+
                            '<span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span>'+
                        '</div>'+
                        '<input type="hidden" name="new_task_input_percentage" class="new_task_percentage">'+
                    '</div>';

    // get all task under project title
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var existing_task = "";
    $.ajax({
        url: "get_task_under_categories",
        data:{token:$('meta[name="csrf-token"]').attr('content') , category_id : $(this).val()  },
        method: "POST" , 
        success : function (response) {
            // var json = jQuery.parseJSON(response);
            $.each(response, function (k, v) {
                existing_task += '<div class="x" id="slide_div" >'+
                                    '<h6>'+v.title+'<span id="span" class="percent-count">'+v.task_percentage+'%</span></h6>'+
                                    '<div data-percentage="'+v.task_percentage+'" data-task_name="Test 17" class="slider_div_class_existing ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">'+
                            '<div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min"></div>'+
                                        '<span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: '+v.task_percentage+'%;"></span>'+
                        '</div>'+
                                    '<input type="hidden" name = "existing_task_percentage['+v.task_id+']" class = "existing_task_percentage" value='+v.task_percentage+' >'+
                    '</div>';
            }); 
            me.closest('form').find('.task-list').append(existing_task);
            // Slider
            $('.slider_div_class_existing').each(function() {
                var me = $(this);
                $(this).slider({
                    range: "min",
                    animate: true,
                    value: $(this).data('percentage'),
                    min: 0,
                    max: 100 ,
                    step: 1,
                    slide: function(event, ui) {
                        //compute total percentage 
                        if (ui.value != 0) {
                            $(this).removeClass('has-error');
                        } else {
                            $(this).addClass('has-error');
                        }
                        var sum = ui.value;
                        $('.x').each(function(i){
                            if(this !== me.closest('#slide_div')[0])
                                sum += parseFloat($(this).find('span').text());
                        });

                        if(sum > 100) { 
                            return false;
                        } else {
                            me.closest('#slide_div').find('#span').html(ui.value +'%');
                            var task_id = me.closest('#slide_div').data('task_id');
                            if (task_id == undefined) {
                                $('.existing_task_percentage').val(ui.value);
                            } else {
                                $('#'+task_id).val(ui.value); 
                            }  
                        }

                        $('#sum_percentage').val(sum);
                        $(".percent-count").filter(function() { return $(this).text() === "0%"; }).closest('.x').find('.slider_div_class').addClass('has-error');

                        var percent0_length = $(".percent-count").filter(function() { return $(this).text() === "0%"; }).length;
                        if ((percent0_length === 0) && (sum >= 100)) {
                            $modal.find('.invalid-label, .invalid-max-label, .task-percent .note').addClass('hide');
                            $modal.find('.max-label').removeClass('hide');
                        } else if ((percent0_length != 0) && (sum >= 100)) {
                            $modal.find('.invalid-max-label').removeClass('hide');
                            $modal.find('.max-label, .invalid-label, .task-percent .note').addClass('hide');
                        } else if ((percent0_length === 0) && (sum < 100)) {
                            $modal.find('.task-percent .note').removeClass('hide').css('visibility', 'hidden');
                            $modal.find('.max-label, .invalid-label, .invalid-max-label').addClass('hide');
                        } else {
                            $modal.find('.invalid-label').removeClass('hide');
                            $modal.find('.max-label, .invalid-max-label, .task-percent .note').addClass('hide');
                        }
                    }, 
                    change: function(event, ui) {
                      
                    }
                });
            }); 
        }
    }); 
    if ($(this).val() != '1') {
        $(this).closest('form').find('.task-percent').removeClass('hide');
        $(this).closest('form').find('.task-list').html($task_list);
    } else {
        $(this).closest('form').find('.task-percent').addClass('hide');
        $(this).closest('form').find('.task-list').empty();
    }

    // $(this).closest('form').find('.task-percent').removeClass('hide');
    // $(this).closest('form').find('.task-list').html($task_list);

    // Slider
    $('.slider_div_class').each(function() {
        var me = $(this);
        $(this).slider({
            range: "min",
            animate: true,
            value: $(this).data('percentage'),
            min: 0,
            max: 100 ,
            step: 1,
            slide: function(event, ui) {
                //compute total percentage 
                if (ui.value != 0) {
                    $(this).removeClass('has-error');
                } else {
                    $(this).addClass('has-error');
                }

                var sum = ui.value;
                $('.x').each(function(i){
                    if(this !== me.closest('#slide_div')[0])
                        sum += parseFloat($(this).find('span').text());
                });

                if(sum > 100) { 
                    return false;
                } else {
                    me.closest('#slide_div').find('#span').html(ui.value +'%');
                    var task_id = me.closest('#slide_div').data('task_id');
                    if (task_id == undefined) {
                        $('.new_task_percentage').val(ui.value);
                    } else {
                        $('#'+task_id).val(ui.value); 
                    }  
                }

                $('#sum_percentage').val(sum);
                $(".percent-count").filter(function() { return $(this).text() === "0%"; }).closest('.x').find('.slider_div_class').addClass('has-error');

                var percent0_length = $(".percent-count").filter(function() { return $(this).text() === "0%"; }).length;
                if ((percent0_length === 0) && (sum >= 100)) {
                    $modal.find('.invalid-label, .invalid-max-label, .task-percent .note').addClass('hide');
                    $modal.find('.max-label').removeClass('hide');
                } else if ((percent0_length != 0) && (sum >= 100)) {
                    $modal.find('.invalid-max-label').removeClass('hide');
                    $modal.find('.max-label, .invalid-label, .task-percent .note').addClass('hide');
                } else if ((percent0_length === 0) && (sum < 100)) {
                    $modal.find('.task-percent .note').removeClass('hide').css('visibility', 'hidden');
                    $modal.find('.max-label, .invalid-label, .invalid-max-label').addClass('hide');
                } else {
                    $modal.find('.invalid-label').removeClass('hide');
                    $modal.find('.max-label, .invalid-max-label, .task-percent .note').addClass('hide');
                }
            }, 
            change: function(event, ui) {
              
            }
        });
    }); 
});

// Add Project Title to dropdown
$(document).on('click', '.dropdown-add .add-to-dropdown', function() {
    var $dropdown_add = $(this).closest('.dropdown-add'),
        $modal = $(this).closest('#add_task_modal'),
        $form = $(this).closest('form'),
        $hidden_title = $dropdown_add.find('[name="new_project_title"]'),
        $new_project_title = $dropdown_add.find('[name="temp_project_title"]').val(),
        $max = 0;
    if ($.trim($new_project_title) === '') {
        // if new_project_title has no value
        $dropdown_add.addClass('invalid');
        $dropdown_add.closest('.form-group').find('.invalid-input').removeClass('hide');
        $dropdown_add.closest('.form-group').find('.invalid-feedback').removeClass('show');
    } else {
        // if new_project_title has value
        $hidden_title.val($new_project_title);
        // remove invalid or has-error class
        $dropdown_add.removeClass('invalid');
        $dropdown_add.closest('.form-group').find('.invalid-feedback').removeClass('show');
        $dropdown_add.closest('.form-group').removeClass('has-error');
        // get highest value in the selection
        $dropdown_add.find('option').each(function() {
            var $value = parseInt($(this).val());
            $max = ($value > $max) ? $value : $max;
        });
        // add the input value to the selection
        var $option = '<option value="'+($max + 1)+'" selected>'+$new_project_title+'</option>'
        $dropdown_add.find('option').removeAttr('selected');
        $dropdown_add.find('select').prepend($option);
        $dropdown_add.find('.add-to-dropdown').addClass('add-option').removeClass('add-to-dropdown').html('&#9547;');
        $dropdown_add.find('[name="temp_project_title"], .cancel-add').addClass('hide');
        $dropdown_add.find('[name="temp_project_title"]').val('');
        // reset task percentage
        var $new_task = '<div class="x new-task" id="slide_div">'+
                        '<h6>New Task <span id="span" class="percent-count">0%</span></h6>'+
                        '<div data-percentage="0" data-task_name="" class="slider_div_class ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">'+
                            '<div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min" style="width: 0%;"></div>'+
                            '<span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span>'+
                        '</div>'+
                        '<input type="hidden" name="new_task_input_percentage" class="new_task_percentage">'+
                    '</div>';
        $form.find('.task-percent').removeClass('hide');
        $form.find('.task-percent .task-list').html($new_task);
        $form.find('.invalid-label, .max-label, .invalid-max-label').addClass('hide');
        $form.find('.task-percent .note').removeClass('hide').css('visibility', 'visible');
        // Slider
        $('.slider_div_class').each(function() {
            var me = $(this);
            $(this).slider({
                range: "min",
                animate: true,
                value: $(this).data('percentage'),
                min: 0,
                max: 100 ,
                step: 1,
                slide: function(event, ui) {
                    // me.closest('#slide_div').find('#span').html(ui.value +'%')
                    //compute total percentage 
                   var sum = ui.value;
                    $(  $('.x') ).each(function(i){
                        if(this !== me.closest('#slide_div')[0])
                            sum += parseFloat($(this).text());
                    });
                    if(sum > 100) { 
                        return false;
                    } else {
                        me.closest('#slide_div').find('#span').html(ui.value +'%');
                        var task_id = me.closest('#slide_div').data('task_id');
                        if (task_id == undefined) {
                            $('.new_task_percentage').val(ui.value);
                        } else {
                            $('#'+task_id).val(ui.value); 
                        }  
                    }

                    var percent0_length = $(".percent-count").filter(function() { return $(this).text() === "0%"; }).length;
                    if ((percent0_length === 0) && (sum >= 100)) {
                        $modal.find('.invalid-label, .invalid-max-label, .task-percent .note').addClass('hide');
                        $modal.find('.max-label').removeClass('hide');
                    } else if ((percent0_length != 0) && (sum >= 100)) {
                        $modal.find('.invalid-max-label').removeClass('hide');
                        $modal.find('.max-label, .invalid-label, .task-percent .note').addClass('hide');
                    } else if ((percent0_length === 0) && (sum < 100)) {
                        $modal.find('.task-percent .note').removeClass('hide').css('visibility', 'hidden');
                        $modal.find('.max-label, .invalid-label, .invalid-max-label').addClass('hide');
                    } else {
                        $modal.find('.invalid-label').removeClass('hide');
                        $modal.find('.max-label, .invalid-max-label, .task-percent .note').addClass('hide');
                    }
                }, 
                change: function(event, ui) {
                }
            });
        }); 
    }
});

// Cancel Project Title
$(document).on('click', '.dropdown-add .cancel-add', function() {
    var $dropdown_add = $(this).closest('.dropdown-add');
    $dropdown_add.find('.add-to-dropdown').addClass('add-option').removeClass('add-to-dropdown').html('&#9547;');
    $dropdown_add.find('[name="temp_project_title"], .cancel-add').addClass('hide');
    $dropdown_add.find('[name="temp_project_title"]').val('');
    $dropdown_add.removeClass('invalid');
    $(this).closest('.form-group').removeClass('has-error');
});

// Change Account Type
$(document).on('change', '.assignee-checkbox [name="account_type"],.radio_member[name="account_type"]', function() {
    $('.assignee-checkbox [type="checkbox"]').prop('checked', false);
    $('#business_panel_reminder [type="checkbox"]').prop('checked', false);
    $('#personal_panel_reminder [type="checkbox"]').prop('checked', false);
});

// Check All Members
$(document).on('click', '.checkAll', function() {
    $(this).parent().parent().find('input:checkbox').prop('checked', this.checked)
});

// Check if All Assignees are selected
$(document).on('click', '.checkAssignee', function() {
    var $assignee = $(this).parent().parent().find('.checkAssignee'),
        $count_check = 0;

    $assignee.each(function() {
        if ($(this).is(':checked')) {
            $count_check++;
        }
    });

    if($assignee.length === $count_check) {
        $(this).parent().parent().find('.checkAll').prop('checked', true);
    } else {
        $(this).parent().parent().find('.checkAll').prop('checked', false);
    }
});

// Card Add File click trigger
$(document).on('click', '.card-add-file', function() {
    $(this).parent().find('.file-custom-input').trigger('click');
});

// Get File index and info
$(document).on('click', '.file-item.js-open-viewer, .image-item.js-open-viewer', function() {
    var $attachment_list = $(this).closest('.attachment-list'),
        $file_length = $(this).closest('.attachment-list').find('.file-item').length,
        $index = $(this).is('.image-item') ? $(this).index() + 1 + $file_length : $(this).index() + 1,
        $hidden_item = $attachment_list.find('.file-info .hidden-item'),
        $item = "",
        $indicator = "";

    // console.log($index);
    $.each($hidden_item, function(index){
        // console.log($index -1 === $(this).index() ? 'active' : $(this).index());
        var file_type = $(this).find('.file-type').text(),
            file_name = $(this).find('.file-name').text(),
            file_src = $(this).find('.file-src').text();

        $item += '<div class="item frame js-close-viewer '+ ($index - 1 === $(this).index() ? 'active' : '') +'">'+
                    '<div class="frame-preview-wrapper scroll-bar">'+
                        '<div class="frame-preview">'+
                        (/\.(pdf|ppt|pptx|doc|docx|xls|xlsx|txt)$/i.test(file_name) ? '<iframe src="https://docs.google.com/gview?url='+file_src+'&embedded=true"></iframe>' : (/\.(jpg|jpeg|png|gif|bmp|web)$/i.test(file_name) ? '<img class="show-image js-stop" src="'+file_src+'">' : '<p>There is no preview available for this attachment.</p>'))+
                        '</div>'+
                    '</div>'+
                    '<div class="file-viewer-overlay flex center middle">'+
                        '<div class="file-details js-stop active" item="0" card="0">'+
                            '<p>'+
                                (/\.(jpg|jpeg|png|gif|bmp|web|pdf)$/i.test(file_name) ? '<a href="'+file_src+'" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i>Open in New Tab</a>' : '')+
                                '<a href="'+file_src+'" download="">'+
                                    '<i class="fa fa-download" aria-hidden="true"></i>Download'+
                                '</a>'+
                                (/\.(pdf|ppt|pptx|doc|docx|xls|xlsx|txt)$/i.test(file_name) ? '' : '<a href="javascript:void(0);" class="rotate-image"><i class="fa fa-repeat" aria-hidden="true"></i> Rotate</a>')+
                            '</p>'+
                        '</div>'+
                    '</div>'+
                '</div>';
        $indicator += '<li data-target="#fileViewer" data-slide-to="'+$(this).index()+'" class="'+ ($index - 1 === $(this).index() ? 'active' : '') +'"></li>';
    });

    $('#fileViewer').removeClass('hide').addClass('show');
    $('#fileViewer .carousel-inner').append($item);
    $('#fileViewer .carousel-indicators').append($indicator);
});

// Rotate Image
var degree = 0;
$(document).on('click', '.rotate-image', function(e) {
    e.preventDefault();
    degree += 90;
    $(this).closest('.item').find('.show-image').css({
        'transform': 'rotate(' + degree + 'deg)',
        '-ms-transform': 'rotate(' + degree + 'deg)',
        '-moz-transform': 'rotate(' + degree + 'deg)',
        '-webkit-transform': 'rotate(' + degree + 'deg)',
        '-o-transform': 'rotate(' + degree + 'deg)'
    });
});

// Close File Viewer
$(document).on('click', '.js-close-viewer, .js-stop', function(e) {
    if($(this).is('.js-stop')) { e.stopPropagation(); return }
    degree = 0;
    $(this).closest('.carousel.file-viewer').removeClass('show').addClass('hide');
    $(this).closest('.carousel').find('.carousel-inner, .carousel-indicators').empty();
});

// Dropdown Stop Propagation
$(document).on('click', '.profile-menu .dropdown-menu.profile, .profile-page .change-email .dropdown-menu, .profile-page .change-password .dropdown-menu, .profile-page .change-language .dropdown-menu, .profile-page .change-theme .dropdown-menu, .profile-page .change-emailnotif .dropdown-menu', function (e) {
  e.stopPropagation();
});

// Dropdown Close Button
$(document).on('click', '.dropdown .close', function() {
    var $dropdown = $(this).closest('.dropdown');
    $dropdown.removeClass('open dropup');
    $dropdown.find('.dropdown-toggle').attr('aria-expanded', 'true');
});

// Dropdown Auto Reposition
$(document).on("shown.bs.dropdown", ".dropdown", function () {
    // calculate the required sizes, spaces
    var $ul = $(this).children(".dropdown-menu");
    var $button = $(this).children(".dropdown-toggle");
    var ulOffset = $ul.offset();
    // how much space would be left on the top if the dropdown opened that direction
    var spaceUp = (ulOffset.top - $button.height() - $ul.height()) - $(window).scrollTop();
    // how much space is left at the bottom
    var spaceDown = $(window).scrollTop() + $(window).height() - (ulOffset.top + $ul.height());
    // switch to dropup only if there is no space at the bottom AND there is space at the top, or there isn't either but it would be still better fit
    if (spaceDown < 0 && (spaceUp >= 0 || spaceUp > spaceDown))
      $(this).addClass("dropup");
}).on("hidden.bs.dropdown", ".dropdown", function() {
    // always reset after close
    $(this).removeClass("dropup");
});

// Change Theme Color 
$(document).on('click', '.change-theme > a', function() {
    $(this).parent().find('.color-pick').toggleClass('hide');
});

$(document).on('click', '#sideNav .change-theme', function() {
    $(this).next('.theme-color').slideToggle();
});



// Floating Action Button
$(document).on('click', '#action_btn', function() {
    $(this).parent().toggleClass('active');
});





// REMINDER ARROW TOGGLE
$(document).on('click', '.reminder-item .toggle-btn', function() {
    var $description = $(this).closest('.reminder-item').find('.reminder-description')
    $description.slideToggle();
    $(this).toggleClass('active');
});

// Line Input
$(document).ready(function() {
    $('.line-input input').focus(function() {
        $(this).closest('.line-input').addClass('active');
    });

    $('.line-input input').focusout(function() {
        if (!$(this).val().trim()) {
            $(this).closest('.line-input').removeClass('active');
        }
    });
});

function inputDateFormater(el) {
    var $t=$(el.nextElementSibling),
               d=new Date(el.value),
        fm=$t.attr('data-date-format')||'MM/DD/YYYY';
        var m = (d.getMonth() + 1);
        $t.attr('data-date', fm.replace('MM', m).replace('DD',d.getDate()).replace('YYYY',d.getFullYear()))

        var dtToday = new Date();
        var month = dtToday.getMonth() + 1;
        var day = dtToday.getDate();
        var year = dtToday.getFullYear();
        if(month < 10)
            month = '0' + month.toString();
        if(day < 10)
            day = '0' + day.toString();
        var maxDate = year + '-' + month + '-' + day;
        $('#todo_date').attr('min', maxDate);
    }

function inputDateFormaterFocus(el) {
    el.value||$(el.nextElementSibling).attr('data-date',$(el.nextElementSibling).attr('data-date-format').toLowerCase())
}

function inputDateFormaterBlur(el) {
    el.value||$(el.nextElementSibling).attr('data-date','')
}

// Text Editor
$('.texteditor-wrap').each(function() {
    var bold = $('.pd-bold');
    var italic = $('.pd-italic');
    var underline = $('.pd-underline');
    var strikethrough = $('.pd-strikethrough');
    var undo = $('.pd-undo');
    var redo = $('.pd-redo');
    var texteditor = $('.texteditor-content');
    var textarea = $('textarea');

    bold.on('click', function(e){
        e.preventDefault();
        texteditor.focus();
        document.execCommand('bold');
    })
    italic.on('click', function(e){
        e.preventDefault();
        texteditor.focus();
        document.execCommand('italic');
    })
    underline.on('click', function(e){
        e.preventDefault();
        texteditor.focus();
        document.execCommand('underline');
    })
    strikethrough.on('click', function(e){
        e.preventDefault();
        texteditor.focus();
        document.execCommand('strikeThrough');
    })
    undo.on('click', function(e){
        e.preventDefault();
        document.execCommand('undo');
    })
    redo.on('click', function(e){
        e.preventDefault();
        document.execCommand('redo');
    })

    document.execCommand("defaultParagraphSeparator", false, "p");

    texteditor.on('input', function() {
        if(texteditor.html() === "") texteditor.html('<p><br></p>'); 
        textarea.val(texteditor.html());
    });

    $(this).on('click', '.texteditor-toolbar .right-buttons button', function() {
        $(this).toggleClass('active');
    });

    $(this).on('click', '.texteditor-content', function(e) { 
        $(this).closest('.texteditor-wrap').find('.texteditor-toolbar button').removeClass('active');
        var bubble = e.target ;
        if(this === e.target)
        {   
            if(window.getSelection)
            {
                bubble = window.getSelection().anchorNode.parentNode;
            } else if(window.selection)// IE 8 below
            {   
                bubble = window.selection().anchorNode.parentNode;
            }
        }

        while(bubble.tagName !== 'P' && this !== bubble) {
            switch (bubble.tagName) {
                case 'B':
                    bold.addClass('active');
                    break;
                case 'U':
                    underline.addClass('active');
                    break;
                case 'STRIKE':
                    strikethrough.addClass('active');
                    break;
                case 'I':
                    italic.addClass('active');
                    break;
                default:
                    break;
            }
            bubble = bubble.parentNode || bubble.parentElement
        }
    });
});

$(function() {
    // File Input
    $('.file-custom-input').on('change',function(e){
        var $this = $(this), $root = $this.closest('.file-attachment');
        var files = e.target.files;
        if($this.parent().find('[filelist]').length === 0){
            var input = '<input type="hidden" filelist >'
            $this.parent().append(input)
            $this.removeAttr('name')
        }

        var $filelist = $root.find('[filelist]'), oldlist = !!$filelist.val() ? JSON.parse($filelist.val()) : []

        $.each(files, function(i, file){ 
            if(!(file.name in oldlist)) {

                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function(e){  
                    var pieces =  file.name.split(/[\.]+/);
                    var extension = pieces[pieces.length - 1];
                    var template = '';
                    var images = '';
                    switch(file.name.match(/\.([\w]+)$/)[1]) {
                        case 'doc':
                        case 'docx':
                            // template += '<div>doc; docx</div>';
                            template += '<div class="file-item col-sm-6">'+
                                            '<div class="file-wrapper flex middle space-bet">'+
                                                '<div class="file-icon"><img src="images/icons/file-blue.png"><span>'+extension+'</span></div>'+
                                                '<p class="file-name">'+file.name+'</p>'+
                                                '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                            '</div>'+
                                        '</div>';
                        break;

                        case 'xls':
                        case 'xlsx':
                            // template += '<div>xls; xlxs</div>';
                            template += '<div class="file-item col-sm-6">'+
                                            '<div class="file-wrapper flex middle space-bet">'+
                                                '<div class="file-icon"><img src="images/icons/file-darkgreen.png"><span>'+extension+'</span></div>'+
                                                '<p class="file-name">'+file.name+'</p>'+
                                                '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                            '</div>'+
                                        '</div>';
                        break;

                        case 'ppt':
                        case 'pptx':
                            // template += '<div>ppt; pptx</div>';
                            template += '<div class="file-item col-sm-6">'+
                                            '<div class="file-wrapper flex middle space-bet">'+
                                                '<div class="file-icon"><img src="images/icons/file-orange.png"><span>'+extension+'</span></div>'+
                                                '<p class="file-name">'+file.name+'</p>'+
                                                '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                            '</div>'+
                                        '</div>';
                        break;

                        case 'pdf':
                            // template += '<div>pdf</div>';
                            template += '<div class="file-item col-sm-6">'+
                                            '<div class="file-wrapper flex middle space-bet">'+
                                                '<div class="file-icon"><img src="images/icons/file-red.png"><span>'+extension+'</span></div>'+
                                                '<p class="file-name">'+file.name+'</p>'+
                                                '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                            '</div>'+
                                        '</div>';
                        break;

                        case 'zip':
                        case 'rar':
                        case 'tar':
                        case 'gzip':
                        case 'gz':
                        case '7z':
                            // template += '<div>zip; rar; tar; gzip; gz; 7z</div>';
                            template += '<div class="file-item col-sm-6">'+
                                            '<div class="file-wrapper flex middle space-bet">'+
                                                '<div class="file-icon"><img src="images/icons/file-lightgreen.png"><span>'+extension+'</span></div>'+
                                                '<p class="file-name">'+file.name+'</p>'+
                                                '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                            '</div>'+
                                        '</div>';
                        break;

                        case 'htm':
                        case 'html':
                            // template += '<div>htm; html</div>';
                            template += '<div class="file-item col-sm-6">'+
                                            '<div class="file-wrapper flex middle space-bet">'+
                                                '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                                '<p class="file-name">'+file.name+'</p>'+
                                                '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                            '</div>'+
                                        '</div>';
                        break;

                        case 'txt':
                        case 'csv':
                        case 'ini':
                        case 'java':
                        case 'php':
                        case 'js':
                        case 'css':
                            // template += '<div>txt; csv; ini; java; php; js; css</div>';
                            template += '<div class="file-item col-sm-6">'+
                                            '<div class="file-wrapper flex middle space-bet">'+
                                                '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                                '<p class="file-name">'+file.name+'</p>'+
                                                '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                            '</div>'+
                                        '</div>';
                        break;

                        case 'avi':
                        case 'mpg':
                        case 'mkv':
                        case 'mov':
                        case 'mp4':
                        case '3gp':
                        case 'webm':
                        case 'wmv':
                        case 'm4a':
                            // template += '<div>avi; mpg; mkv; mov; mp4; 3gp; webm; wmv; m4a</div>';
                            template += '<div class="file-item col-sm-6">'+
                                            '<div class="file-wrapper flex middle space-bet">'+
                                                '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                                '<p class="file-name">'+file.name+'</p>'+
                                                '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                            '</div>'+
                                        '</div>';
                        break;
                        
                        case 'mp3':
                        case 'wav':
                            // template += '<div>mp3; wav</div>';
                            template += '<div class="file-item col-sm-6">'+
                                            '<div class="file-wrapper flex middle space-bet">'+
                                                '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                                '<p class="file-name">'+file.name+'</p>'+
                                                '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                            '</div>'+
                                        '</div>';
                        break;

                        case 'jpg':
                        case 'jpeg':
                        case 'png':
                        case 'gif':
                            // images += '<img src="">';
                            images += '<div class="image-item">'+
                                        '<img src="'+e.target.result+'">'+
                                        '<a href="#" fi-func="delete" fi-name="'+file.name+'" class="img-delete flex middle center"><span>&#9587;</span></a>'+
                                    '</div>';
                        break;

                        default:
                            // template += '<div>default file</div>';
                            template += '<div class="file-item col-sm-6">'+
                                            '<div class="file-wrapper flex middle space-bet">'+
                                                '<div class="file-icon"><img src="images/icons/file-gray.png"><span>'+extension+'</span></div>'+
                                                '<p class="file-name">'+file.name+'</p>'+
                                                '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                            '</div>'+
                                        '</div>';
                    }

                    images && $root.find('.image-list').append(images);
                    template && $root.find('.file-list').append(template);
                    var getfile = { name: file.name, value: e.target.result }
                    oldlist.push(getfile)
                    $filelist.val(JSON.stringify(oldlist))

                };
            }
        });


     }).closest('.file-attachment').on('click', '[fi-func="delete"]',function(e){
        e.preventDefault();
        // var $delete_btn = $(this), 
        //     f_input     = $(e.delegateTarget).find('input[type="file"]')[0], 
        //     d_name      = $delete_btn.attr('fi-name').trim(),
        //     $filelist   = $(e.delegateTarget).find('[filelist]')

        // var files = JSON.parse($filelist.val()).filter(function(file){
        //     return file.name !== d_name
        // });

        // $filelist.val(JSON.stringify(files))

        // $delete_btn.closest('.image-item,.file-item').remove();

    });

    // EDIT File Input
    $('.edit-file-custom-input').on('change',function(e){
        
        var $this = $(this), $root = $this.closest('.file-attachment');
        var files = e.target.files;
        if($this.parent().find('[filelist]').length === 0){
            var input = '<input type="hidden" filelist >'
            $this.parent().append(input)
            $this.removeAttr('name')
        }

        var $filelist = $root.find('[edit_filelist]'), oldlist = !!$filelist.val() ? JSON.parse($filelist.val()) : []

        $.each(files, function(i, file){ 
            if(file.name in oldlist) return;

            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){  
                var pieces =  file.name.split(/[\.]+/);
                var extension = pieces[pieces.length - 1];
                var template = '';
                var images = '';
                switch(file.name.match(/\.([\w]+)$/)[1]) {
                    case 'doc':
                    case 'docx':
                        // template += '<div>doc; docx</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-blue.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'xls':
                    case 'xlsx':
                        // template += '<div>xls; xlxs</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-darkgreen.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'ppt':
                    case 'pptx':
                        // template += '<div>ppt; pptx</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-orange.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'pdf':
                        // template += '<div>pdf</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-red.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'zip':
                    case 'rar':
                    case 'tar':
                    case 'gzip':
                    case 'gz':
                    case '7z':
                        // template += '<div>zip; rar; tar; gzip; gz; 7z</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-lightgreen.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'htm':
                    case 'html':
                        // template += '<div>htm; html</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'txt':
                    case 'csv':
                    case 'ini':
                    case 'java':
                    case 'php':
                    case 'js':
                    case 'css':
                        // template += '<div>txt; csv; ini; java; php; js; css</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'avi':
                    case 'mpg':
                    case 'mkv':
                    case 'mov':
                    case 'mp4':
                    case '3gp':
                    case 'webm':
                    case 'wmv':
                    case 'm4a':
                        // template += '<div>avi; mpg; mkv; mov; mp4; 3gp; webm; wmv; m4a</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;
                    
                    case 'mp3':
                    case 'wav':
                        // template += '<div>mp3; wav</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                    break;

                    case 'jpg':
                    case 'jpeg':
                    case 'png':
                    case 'gif':
                        // images += '<img src="">';
                        images += '<div class="image-item">'+
                                    '<img src="'+e.target.result+'">'+
                                    '<a href="#" fi-func="delete" fi-name="'+file.name+'" class="img-delete flex middle center"><span>&#9587;</span></a>'+
                                '</div>';
                    break;

                    default:
                        // template += '<div>default file</div>';
                        template += '<div class="file-item col-sm-6">'+
                                        '<div class="file-wrapper flex middle space-bet">'+
                                            '<div class="file-icon"><img src="images/icons/file-gray.png"><span>'+extension+'</span></div>'+
                                            '<p class="file-name">'+file.name+'</p>'+
                                            '<button type="button" fi-func="delete" fi-name="'+file.name+'">Delete</button>'+
                                        '</div>'+
                                    '</div>';
                }

                images && $root.find('.image-list').append(images);
                template && $root.find('.file-list').append(template);
                var getfile = { name: file.name, value: e.target.result }
                oldlist.push(getfile)
                $filelist.val(JSON.stringify(oldlist))

            };
        });


    }).closest('.file-attachment').on('click', '[fi-func="delete"]',function(e){
        e.preventDefault();
        // var $delete_btn = $(this), 
        //     f_input     = $(e.delegateTarget).find('input[type="file"]')[0], 
        //     d_name      = $delete_btn.attr('fi-name').trim(),
        //     $filelist   = $(e.delegateTarget).find('[edit_filelist]');
        // var files = JSON.parse($filelist.val()).filter(function(file){
        //     return file.name !== d_name
        // });

        // $filelist.val(JSON.stringify(files))

        // $delete_btn.closest('.image-item,.file-item').remove();

    });

});

$(document).on('click','.delete_file', function(e){
    var task_id = $(this).data('task_id');
    var task_attachment_id = $(this).data('tasks_attachment_id');
    var me = $(this);

    var data  = {
        action: 'remove_task_attachment',
        task_id : task_id,
        task_attachment_id : task_attachment_id
    };

    var c = confirm('Remove this File?');
    
    if(c) {
        $.ajax({
            url : 'task_actions',
            method : 'post',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data : data,
            dataType: 'json',
            success: function(result) {
                if(result.status){
                    me.closest('.file-item').remove();
                }
            }
        });
    }

});

// Edit Task Modal 
function edit_task(task_id) {
    var task_id = task_id;
    var data = { action:'show_edit', task_unique:task_id };

    $.ajax({
        url : "task_actions", 
        data : data, 
        dataType: 'json',
        method : "POST",
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
        success: function(r) {
            
            let user_unique = $.map(r.assignee,function(v,k){
                if(v[0].id == r.task.created_by) {
                    return v[0].user_unique;
                }
            });
        
            $('.edit_x').val(task_id);
            $('.edit_created_by').val(user_unique[0]);
            $('.edit_subject').val(r.task.title); 
            $('.edit_description').val(r.task.description);
            $('.edit_description_texteditor').html(r.task.description);
            $('.edit_deadline').val(r.task.deadline); 

            if (typeof r.attachment !== undefined){
                var files = append_files(r);
                //console.log(files);
                $('#edit_task_modal').find('.image-list').append(files[0]);
                $('#edit_task_modal').find('.file-list').append(files[1]);
            }

        },
        complete:function(c){
            var r = c.responseJSON;
            $('#edit_task_modal').modal("show",c);
        }
    })

}

// Edit Task
$(document).on('click','.edit-task',function(){
    var task_id = $(this).data('task_id');
    var data = { action:'show_edit', task_id:task_id };

    $.ajax({
        url      : "task_actions", 
        data     : data, 
        dataType : 'json',
        method   : "POST",
        headers  : { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
        success  : function(r) {
            console.log(r);
            let user_unique = $.map(r.data.get_assign,function(v,k) {
                if(v.user_id == r.data.created_by) {
                    return v.get_user[0].user_unique;
                }
            });

            $('.edit_x').val(task_id);
            $('.edit_created_by').val(user_unique[0]);
            $('.edit_subject').val(r.data.title); 
            $('.edit_description').val(r.data.description);
            $('.edit_description_texteditor').html(r.data.description);
            $('.edit_deadline').val(r.data.deadline); 

            if (r.data.attachments){
                var files = append_files(r);
                //console.log(files);
                $('#edit_task_modal').find('.image-list').append(files[0]);
                $('#edit_task_modal').find('.file-list').append(files[1]);
            }

        },
        complete:function(c){
            var r = c.responseJSON;
            $('#edit_task_modal').modal("show",r);
        }
    })

});


function append_files(task) {

    var images    = '';
    var template  = '';

    $.each(task.data.attachments,function(k,v){
        var extension = v.file_type;
        switch(extension) {
        case 'doc':
        case 'docx':
            // template += '<div>doc; docx</div>';
            template += '<div class="file-item col-sm-6">'+
                            '<div class="file-wrapper flex middle space-bet">'+
                                '<div class="file-icon"><img src="images/icons/file-blue.png"><span>'+extension+'</span></div>'+
                                '<p class="file-name">'+$('#site_name').data('site_name')+v.full_path+'</p>'+
                                '<button type="button" fi-func="delete" fi-name="'+v.image+'" data-tasks_attachment_id="'+v.tasks_attachment_id+'" data-task_id="'+task.data.task_id+'" class="delete_file" >Delete</button>'+
                            '</div>'+
                        '</div>';
        break;

        case 'xls':
        case 'xlsx':
            // template += '<div>xls; xlxs</div>';
            template += '<div class="file-item col-sm-6">'+
                            '<div class="file-wrapper flex middle space-bet">'+
                                '<div class="file-icon"><img src="images/icons/file-darkgreen.png"><span>'+extension+'</span></div>'+
                                '<p class="file-name">'+$('#site_name').data('site_name')+v.full_path+'</p>'+
                                '<button type="button" fi-func="delete" fi-name="'+v.image+'" data-tasks_attachment_id="'+v.tasks_attachment_id+'" data-task_id="'+task.data.task_id+'" class="delete_file" >Delete</button>'+
                            '</div>'+
                        '</div>';
        break;

        case 'ppt':
        case 'pptx':
            // template += '<div>ppt; pptx</div>';
            template += '<div class="file-item col-sm-6">'+
                            '<div class="file-wrapper flex middle space-bet">'+
                                '<div class="file-icon"><img src="images/icons/file-orange.png"><span>'+extension+'</span></div>'+
                                '<p class="file-name">'+$('#site_name').data('site_name')+v.full_path+'</p>'+
                                '<button type="button" fi-func="delete" fi-name="'+v.image+'" data-tasks_attachment_id="'+v.tasks_attachment_id+'" data-task_id="'+task.data.task_id+'" class="delete_file" >Delete</button>'+
                            '</div>'+
                        '</div>';
        break;

        case 'pdf':
            // template += '<div>pdf</div>';
            template += '<div class="file-item col-sm-6">'+
                            '<div class="file-wrapper flex middle space-bet">'+
                                '<div class="file-icon"><img src="images/icons/file-red.png"><span>'+extension+'</span></div>'+
                                '<p class="file-name">'+$('#site_name').data('site_name')+v.full_path+'</p>'+
                                '<button type="button" fi-func="delete" fi-name="'+v.image+'" data-tasks_attachment_id="'+v.tasks_attachment_id+'" data-task_id="'+task.data.task_id+'" class="delete_file" >Delete</button>'+
                            '</div>'+
                        '</div>';
        break;

        case 'zip':
        case 'rar':
        case 'tar':
        case 'gzip':
        case 'gz':
        case '7z':
            // template += '<div>zip; rar; tar; gzip; gz; 7z</div>';
            template += '<div class="file-item col-sm-6">'+
                            '<div class="file-wrapper flex middle space-bet">'+
                                '<div class="file-icon"><img src="images/icons/file-lightgreen.png"><span>'+extension+'</span></div>'+
                                '<p class="file-name">'+$('#site_name').data('site_name')+v.full_path+'</p>'+
                                '<button type="button" fi-func="delete" fi-name="'+v.image+'" data-tasks_attachment_id="'+v.tasks_attachment_id+'" data-task_id="'+task.data.task_id+'" class="delete_file" >Delete</button>'+
                            '</div>'+
                        '</div>';
        break;

        case 'htm':
        case 'html':
            // template += '<div>htm; html</div>';
            template += '<div class="file-item col-sm-6">'+
                            '<div class="file-wrapper flex middle space-bet">'+
                                '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                '<p class="file-name">'+$('#site_name').data('site_name')+v.full_path+'</p>'+
                                '<button type="button" fi-func="delete" fi-name="'+v.image+'" data-tasks_attachment_id="'+v.tasks_attachment_id+'" data-task_id="'+task.data.task_id+'" class="delete_file" >Delete</button>'+
                            '</div>'+
                        '</div>';
        break;

        case 'txt':
        case 'csv':
        case 'ini':
        case 'java':
        case 'php':
        case 'js':
        case 'css':
            // template += '<div>txt; csv; ini; java; php; js; css</div>';
            template += '<div class="file-item col-sm-6">'+
                            '<div class="file-wrapper flex middle space-bet">'+
                                '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                '<p class="file-name">'+$('#site_name').data('site_name')+v.full_path+'</p>'+
                                '<button type="button" fi-func="delete" fi-name="'+v.image+'" data-tasks_attachment_id="'+v.tasks_attachment_id+'" data-task_id="'+task.data.task_id+'" class="delete_file">Delete</button>'+
                            '</div>'+
                        '</div>';
        break;

        case 'avi':
        case 'mpg':
        case 'mkv':
        case 'mov':
        case 'mp4':
        case '3gp':
        case 'webm':
        case 'wmv':
        case 'm4a':
            // template += '<div>avi; mpg; mkv; mov; mp4; 3gp; webm; wmv; m4a</div>';
            template += '<div class="file-item col-sm-6">'+
                            '<div class="file-wrapper flex middle space-bet">'+
                                '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                '<p class="file-name">'+$('#site_name').data('site_name')+v.full_path+'</p>'+
                                '<button type="button" fi-func="delete" fi-name="'+v.image+'" data-tasks_attachment_id="'+v.tasks_attachment_id+'" data-task_id="'+task.data.task_id+'" class="delete_file">Delete</button>'+
                            '</div>'+
                        '</div>';
        break;
        
        case 'mp3':
        case 'wav':
            // template += '<div>mp3; wav</div>';
            template += '<div class="file-item col-sm-6">'+
                            '<div class="file-wrapper flex middle space-bet">'+
                                '<div class="file-icon"><img src="images/icons/file-white.png"><span>'+extension+'</span></div>'+
                                '<p class="file-name">'+$('#site_name').data('site_name')+v.full_path+'</p>'+
                                '<button type="button" fi-func="delete" fi-name="'+v.image+'" data-tasks_attachment_id="'+v.tasks_attachment_id+'" data-task_id="'+task.data.task_id+'" class="delete_file">Delete</button>'+
                            '</div>'+
                        '</div>';
        break;

        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'gif':
            // images += '<img src="">';
            images += '<div class="image-item">'+
                        '<img src="'+$('#site_name').data('site_name')+v.full_path+'">'+
                        '<a href="#" fi-name="'+v.image+'" data-tasks_attachment_id="'+v.tasks_attachment_id+'"  data-task_id="'+task.data.task_id+'" data-task_id="'+task.data.task_id+'" class="img-delete img_del_attachment flex middle center"><span>&#9587;</span></a>'+
                    '</div>';
        break;

        default:
            // template += '<div>default file</div>';
            template += '<div class="file-item col-sm-6">'+
                            '<div class="file-wrapper flex middle space-bet">'+
                                '<div class="file-icon"><img src="images/icons/file-gray.png"><span>'+extension+'</span></div>'+
                                '<p class="file-name">'+$('#site_name').data('site_name')+v.full_path+'</p>'+
                                '<button type="button" fi-func="delete" fi-name="'+v.image+'" data-tasks_attachment_id="'+v.tasks_attachment_id+'" data-task_id="'+task.data.task_id+'" data-task_id="'+task.data.task_id+'" class="delete_file">Delete</button>'+
                            '</div>'+
                        '</div>';
        }
    });

return [images,template];
}


$(document).on('click','#submit_edittask',function(e){
    e.preventDefault();

    var form        = $(this).closest('form');
    var task_id     = form.find('.edit_x').val();
    var title       = form.find('.edit_subject').val();
    var description = form.find('.edit_description').val();
    var deadline    = form.find('.edit_deadline').val();
    var assignee    = $.map(form.find('.assignee-item a'),function(v,k){ return $(v).attr('data-user-id'); });
    
    var $form = $(this).closest('form'),
        $count_error = 0,
        $title = $form.find('[name="title"]'),
        $description = $form.find('.edit_description_texteditor');
    
    //var assignee    = $.map(form.find('input[name="task_users[]"]:checked'),function(v,k){  return v.value });
    //var unsigned    = $.map(form.find('input[name="task_users[]"]'),function(v,k){ 
    //    if( $(v).is(":not(:checked)") ) { return v.value }
    //});
    
    // title validation
    if ($.trim($title.val()) === '') {
        $title.closest('.form-group').addClass('has-error');
    } else {
        $title.closest('.form-group').removeClass('has-error');
    }


    // description validation
    if (($.trim($description.html()) === '') || ($.trim($description.html()) === '<p><br></p>')) {
        $description.closest('.form-group').addClass('has-error');
    } else {
        $description.closest('.form-group').removeClass('has-error');
    }


    
    // count remaining errors
    $form.find('.has-error').each(function() {
        $count_error++;
    });

    if ($count_error > 0) {
        return false;
    } else {
        var form_data = new FormData();
            
        $.each(jQuery('#edit_file').prop("files"), function(i, value) {
            form_data.append('images[]', value);
        });

        form_data.append('task_id',task_id);
        form_data.append('title',title);
        form_data.append('description',$description.html());
        form_data.append('deadline',deadline);
        form_data.append('assignee', assignee);

        // form_data.append('unsigned', unsigned);

        // var data = {
        //     task_id:task_id,
        //     title:title,
        //     description:description,
        //     deadline:deadline,
        //     assignee:assignee,
        //     unsigned:unsigned,
        //     file:form_data,
        // }
       
        $.ajax({
            url : 'task/edit_task',
            data: form_data, 
            type: 'POST',
            dataType: 'JSON',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
            processData: false,
            contentType: false,
            success: function(r){
                // console.log(r);

                // var $events = r.data.get_assign.map(function(k,v){
                //     return {
                //         assignee_image: k.image
                //     };  
                // });            

                $('#edit_task_modal').modal('hide');

                var task_card = $('#div'+task_id);
                task_card.find('#title_task').text(r.data.title);

                task_card.find("#desc_task").html(r.data.description);
                var date_convert = (new Date(r.data.deadline)).toDateString();
                task_card.find("#deadline_task").text(date_convert);
                
                task_card.find('.a').remove();
                $.each(r.data.get_assign, function (k ,v) {
                    if(v.is_created == 0){
                        $assignee_img = '<img data-status="unseen" class="a" src="'+v.image+'" data-toggle="tooltip" data-placement="bottom" (Unseen)">';
                        task_card.find("#assignee-to").after($assignee_img).html(); 
                        console.log(v);         
                    }                    
                });
                
                // console.log(r.data.get_assign[0]['image']);
            },
            //error:function(e){
            //    //console.log(e);
            //}
        });
    }



});

$(document).on('click', '.img_del_attachment', function(e){
    e.stopPropagation();
    var me = $(this).closest('.image-item');
    var task_id            = $(this).attr('data-task_id'); 
    var task_attachment_id = $(this).attr('data-tasks_attachment_id'); 

    var data = { 
        action: 'remove_task_attachment',
        task_id: task_id,
        task_attachment_id: task_attachment_id 
    }

    var c = confirm('Remove this image');

    if(c){
        $.ajax({
            url: "task_actions",
            data:data,
            method: 'post',
            dataType: 'json',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function(r){
                console.log(r);
                if(r.status){
                    me.remove();
                }
            }
        });
    }
});


// View Calendar
function view_calendar () {
    var initialLocaleCode = 'en';
    var modal = $('#calendar_modal').modal("show");
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: "viewcalendar",
        data:{token:$('meta[name="csrf-token"]').attr('content') , timezone : $('#timezone').val()  },
        method: "POST", 
        success : function (response) {

                var $events = response.map(function(k,v){
                    return {
                        title: k.title,
                        start: k.start,
                        end: k.end,
                        color: '#FF5757'
                    };         
                });
                
                $('#calendar_div').fullCalendar({
                    header: {
                        left: '',
                        center: 'prev title next',
                        right: ''
                    },
                    views: {
                        list: {
                            type: 'agenda',
                            buttonText: 'day'
                        },
                    },
                    locale: initialLocaleCode,
                    defaultView: 'month', 
                    themeName:'default',  
                    displayEventTime : false,
                    events: $events,                 
                });

        }
    });


}

// Search
function kmsearch(el, lists_selector) {
    var search = el.value,
        lists = document.querySelectorAll(lists_selector),
        component = new RegExp('\('+search+'\)','i')
    for(var i = 0, l = lists.length; i < l; (function(list) { 
        var data = list.getAttribute('aria-data'),
            isMatch = component.test(data)
        list.parentElement.style.display = isMatch ? 'flex' : 'none'
        list.children[0].innerHTML = isMatch ? data.replace(component,'<span class="highlight">$1</span>') : data 
        i++
    })(lists[i], i));
}

// Search Task Title
// function kmsearchtitle(el, lists_selector) {
//     var search = el.value,
//         lists = document.querySelectorAll(lists_selector),
//         component = new RegExp('\('+search+'\)','i')
//     for(var i = 0, l = lists.length; i < l; (function(list) { 
//         var data = list.getAttribute('aria-data'),
//             isMatch = component.test(data)
//         list.parentElement.parentElement.style.display = isMatch ? 'block' : 'none'
//         i++
//     })(lists[i], i));
// }

$(document).on('click', '.search-bar li', function() {
    var $value = $(this).attr('data-value');
    $(this).closest('.search-bar').find('.search-option').val($value);
    console.log($('.search-bar .search-option').val());
});

$(function() {
    $('.search-bar .search-input').on('keyup', function kmsearchTM() {
        var el = this,
            is_all = false,
            search_def = '.list-card',
            lists_selector = $('.search-bar').find('.search-option').val() || (is_all = true, search_def),
            search = el.value,
            lists = $(lists_selector),
            component = new RegExp('\('+search+'\)', 'i');

        lists.each(function(index, val) {
            var list = val,
                data = is_all ? list.innerText : list.getAttribute('aria-data'),
                isMatch = component.test(data);
                (is_all ? list : list.closest('.list-card')).style.display = isMatch ? 'block' : 'none';
        });
    });
});


// for choose if business or personal
$(document).on('change' ,'.radio_member' , function () {
    if ($(this).val() == 'business') { // for business 
        $('#business_panel').show();
        $('#personal_panel').hide();

        $('#business_panel_reminder').show();
        $('#personal_panel_reminder').hide();
    } else { // for personal
        $('#business_panel').hide();
        $('#personal_panel').show();

        $('#business_panel_reminder').hide();
        $('#personal_panel_reminder').show();
    }
});

// function for view member
function view_member (created_by) {

    var modal = $('#member_modal').modal("show");

    $('#business_list').html('');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url : "view_member" , 
        data:{true : 1 ,  _token:$('meta[name="csrf-token"]').attr('content') },
        method : "POST" ,
        success : function ( response )  {
            // var json = jQuery.parseJSON(response);
            var tmp = "";
            $.each(response, function (k ,v) {
                if  (v.profile_image) {
                    var profile_image  = 'profile/'+v.profile_image.image;
                } else {
                    var profile_image  = "images/icons/default-user.png";
                }
                tmp +=  '<li class="flex middle" >'+
                            '<img class="user-image" src="'+profile_image+'">'+
                            '<div class="user-info flex space-bet" aria-data="'+v.firstname+' '+v.lastname+'">'+
                                '<p>'+v.firstname+' '+v.lastname+'</p>'+
                                '<div class="tools flex">'+
                                    '<a href="javascript:void(0);" class="removeMember" data-user='+v.user_unique+' data-toggle="tooltip" data-placement="bottom" title="Delete"><img src="images/icons/minus-icon.png"></a>'+
                                '</div>'+
                            '</div>'+
                        '</li>';
            });
            modal.find('#business_list').append(tmp);
        }
    });
}
// function for view member
function mobile_view_member (created_by) {

    $('#mob_business_list').html('');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url : "view_member" , 
        data:{true : 1 ,  _token:$('meta[name="csrf-token"]').attr('content') },
        method : "POST" ,
        success : function ( response )  {
            // var json = jQuery.parseJSON(response);
            var tmp = "";
            $.each(response, function (k, v) {
                if  (v.profile_image) {
                    var profile_image  = 'profile/'+v.profile_image.image;
                } else {
                    var profile_image  = "images/icons/default-user.png";
                }
                tmp +=  '<li class="flex middle" >'+
                            '<img class="user-image" src="'+profile_image+'">'+
                            '<div class="user-info flex middle space-bet" aria-data="'+v.firstname+' '+v.lastname+'">'+
                                '<p>'+v.firstname+' '+v.lastname+'</p>'+
                                '<div class="tools flex">'+
                                    '<a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="Delete"><img src="images/icons/minus-icon.png"></a>'+
                                '</div>'+
                            '</div>'+
                        '</li>';
            });
            $('#mob_business_list').append(tmp);
        }
    });
}

function my_personal_team_click () {
    var modal = $('#member_modal');
    $('#personal_list').html('');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url : "view_member_personal" , 
        data:{true : 1 ,  _token:$('meta[name="csrf-token"]').attr('content') },
        method : "POST" ,
        success : function ( response )  {
            // var json = jQuery.parseJSON(response);
            console.log(json);
            var tmp = "";
            $.each(response, function (k, v) {
                if  (v.profile_image) {
                    var profile_image  = 'profile/'+v.profile_image.image;
                } else {
                    var profile_image  = "images/icons/default-user.png";
                }
                tmp +=  '<li class="flex middle" >'+
                            '<img class="user-image" src="'+profile_image+'">'+
                            '<div class="user-info flex space-bet" aria-data="'+v.firstname+' '+v.lastname+'">'+
                                '<p>'+v.firstname+' '+v.lastname+'</p>'+
                                '<div class="tools flex">'+
                                    '<a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="Delete"><img src="images/icons/minus-icon.png"></a>'+
                                '</div>'+
                            '</div>'+
                        '</li>';
            })
            modal.find('#personal_list').append(tmp);
        }
    });
}

// Background Process
// $(document).ready(function() {
//     setInterval(function my_function() {
//      var id = $('body').data('log_id');
//      let data = "";
//      $.ajax({
//          url      : '/reminder/remind_me/'+id,
//          async    : false,
//          method   : 'get',
//          dataType : 'json',
//          headers  : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
//          success  : function (r) {
//              data = r;
//          }
//      }); 
        
//      if(data) {
//          if(data.length > 0) {
//              $.each(data,function(kk,vv){
//                  $.each(vv,function(k,v){
//                      // return if exsist
//                      if ($('div[data-reminder_member_id='+v.reminder_id+']').length > 0) { return; }
//                      notify(v);
//                  });
//              });
//          }
//      }


//     },1000); 
// });

function notify(v) {
    var dsp = "hide";
    var created_by = v.reminder.created_by;
    // SHOW REMOVE REMINDER BUTTON
    if($('body').data('log_id') == created_by){ dsp = ""; }

    $.notify(
        {   // options
            icon: 'glyphicon glyphicon-warning-sign',
            title: v.reminder.reminder_name,
            message: '',
            url: '',
            target: ''
        },
        {   // settings
            element: 'body',
            position: null,
            type: "remind",
            allow_dismiss: true,
            newest_on_top: false,
            showProgressbar: false,
            offset: 20,
            spacing: 10,
            z_index: 1031,
            delay: 0,
            timer: 0,
            url_target: '_blank',
            mouse_over: null,
            onShow: null,
            onClose: null,
            onClosed: null,
            icon_type: 'class',
            onShown: function(){$('.drop-notif').dropdown()},
            placement:{
                        from: "top",
                        align: "right"
                      },
            animate:{
                        enter: 'animated fadeInRight',
                        exit: 'animated fadeOutUp'
                    },
            template: 
                    '<div data-reminder_member_id="'+v.reminder_id+'" data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                        '<span '+
                            'data-reminder_id="'+v.reminder_id+'" '+
                            'data-reminder_member="'+v.reminder_id+'" '+
                            'type="button" '+
                            'aria-hidden="true" '+
                            'class="'+dsp+' close_remove notif_remove btn btn-xs btn-danger" '+
                            'title="Dismiss Reminder">&#9587;</span>' +
                        '<img src="images/icons/clock.png">'+
                        '<div class="flex col">'+
                            '<span data-notify="message" title="{1}">{1}</span>' +
                            '<span data-notify="date">'+v.time_remind+'</span>' +
                        '</div>'+
                        
                        '<button '+
                            'data-reminder_members="'+v.id+'" '+
                            'data-next_repeat="'+v.next_repeat+'" '+
                            'data-day_name="'+v.repeat_every+'" '+
                            'data-time="'+v.start_time+'" '+
                            'data-recurring="'+v.recurrence+'" '+
                            'data-notify="dismiss" '+
                            'class="close notif_close" '+
                            'aria-hidden="true" '+
                            'type="button" '+
                            'style="color:red; opacity: 0.5;" '+
                            'title="Dismiss Reminder">Snooze</button>' +
                    '</div>' 
        }
    );
}

//Reminder 
$(document).on('click','.add_reminder',function(e){
    e.preventDefault();
    var form = $(this).closest('form');
    var reminder_name = form.find('input[name="reminder_name"]').val();
    var start_time    = form.find('input[name="start_time"]').val();
    var repeat_every  = form.find('#select_reminder_form').val();
    var recurring     = form.find('input[name="recurring"]').val();
    var description   = form.find('input[name="description"]').val();
    var reminder_type = form.find('input[name="account_type"]:checked').val();
    //var member_true   = $.map(form.find('input[name="task_users[]"]:checked'),function(v,k){  return v.value });
    var member_true   = $.map(form.find('.assignee-item a'),function(v,k){ return $(v).attr('data-user-id'); });
    var days_of_the_week_true = $.map(form.find('input[name="days_of_the_week[]"]:checked'),function(v,k){  return v.value });
    var days_of_the_week_false = $.map(form.find('input[name="days_of_the_week[]"]'),function(v,k){ 
        if( $(v).is(":not(:checked)") ) { return v.value }
    });
   
    var data = {
        reminder_name: reminder_name,
        description:description, 
        start_time: start_time, 
        repeat_every: repeat_every, 
        recurring: recurring, 
        reminder_type:reminder_type,
        member_true:member_true, 
        days_of_the_week_true: days_of_the_week_true, 
        days_of_the_week_false: days_of_the_week_false
    }

    $.ajax({
        url:'reminder/add_reminder',
        method: "post",
        dataType: "JSON",
        data: data,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        beforeSend: function() {
            form.find('.arinfo').removeClass('hide');           
            form.find('.arinfo').removeClass('fa-1x fa-fw fa fa-check');
            form.find('.arinfo').addClass('fa fa-refresh fa-spin fa-1x fa-fw');
        },
        success :function(r) {
            //console.log(r);
            if(r.result == true){
                form.find('.arinfo').removeClass('fa fa-refresh fa-spin fa-1x fa-fw');
                form.find('.arinfo').addClass('fa-1x fa-fw fa fa-check');
                var html = 
                    '<tr class="reminder_parent_'+r.new.id+'">'+
                        '<td class="reminder_name">'+r.new.reminder_name+'</td>'+
                        '<td>'+
                            '<button class="btn btn-xs btn-warning edit_reminder" '+
                                    'data-toggle="modal" '+
                                    'data-target="#edit_reminder_modal" '+
                                    'data-reminder_id="'+r.new.id+'">Edit</button> - '+ 
                            '<span class="btn btn-xs btn-danger delete_reminder" data-reminder_id="'+r.new.id+'">Delete</span>'+
                        '</td>'+
                    '</tr>';
                
                $('#reminder_div').find('tbody').prepend(html);
                
                setTimeout(() => {
                    $('#reminder_modal').modal('hide');
                },1000);
            }
        }
    });
});

$(document).on('click','.edit_this_reminder',function(e){
    e.preventDefault();

    let form = $(this).closest('form');
    let reminder_id   = form.find('input[name="reminder_id"]').val();
    let reminder_name = form.find('input[name="reminder_name"]').val();
    let start_time    = form.find('input[name="start_time"]').val();
    let repeat_every  = form.find('#select_reminder_form').val();
    let recurring     = form.find('input[name="recurring"]').val();
    let reminder_type = form.find('input[name="account_type"]:checked').val();
    //let member_true   = $.map(form.find('input[name="task_users[]"]:checked'),function(v,k){  return v.value });
    let member_true   = $.map(form.find('.assignee-item a'),function(v,k){ return $(v).attr('data-user-id'); });
    let member_false  = $.map(form.find('input[name="task_users[]"]'),function(v,k){ 
        if( $(v).is(":not(:checked)") ) { return v.value }
    });

    let days_of_the_week_true = $.map(form.find('input[name="days_of_the_week[]"]:checked'),function(v,k){  return v.value });
    let days_of_the_week_false = $.map(form.find('input[name="days_of_the_week[]"]'),function(v,k){ 
        if( $(v).is(":not(:checked)") ) { return v.value }
    });
    
    let data = {
        reminder_id: reminder_id,
        reminder_name: reminder_name, 
        start_time: start_time, 
        repeat_every: repeat_every, 
        recurring: recurring, 
        reminder_type: reminder_type,
        member_true: member_true, 
        member_false: member_false, 
        days_of_the_week_true: days_of_the_week_true, 
        days_of_the_week_false: days_of_the_week_false
    } 

    $.ajax({
        url:'/reminder/edit_reminder',
        method: "post",
        dataType: "JSON",
        data: data,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        beforeSend: function(){
            form.find('.rinfo').removeClass('hide');            
            form.find('.rinfo').removeClass('fa-1x fa-fw fa fa-check');
            form.find('.rinfo').addClass('fa fa-refresh fa-spin fa-1x fa-fw');
        },
        success :function(r){
           
            if(r.result == true){
                form.find('.rinfo').removeClass('fa fa-refresh fa-spin fa-1x fa-fw');
                form.find('.rinfo').addClass('fa-1x fa-fw fa fa-check');

                $('.reminder_parent_'+data.reminder_id).find('.reminder_name').text(data.reminder_name);

                setTimeout(() => {
                    $('#edit_reminder_modal').modal('hide');
                },1000);
            }
        }
    });

});

$(document).on('change','.reminder_switch',function(){
    let data = {
        'reminder_id': $(this).data('reminder_id'),
        'user_id': $(this).data('user_id'),
        'notify': 0
    }

    if($(this).is(':checked')) { data.notify = 1; } 
    else { data.notify = 0; }

    
    $.ajax({
        url: '/reminder/reminder_notif',
        method: 'post',
        data: data,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        dataType: 'json',
        success: function(r) {
           
        }
    });
});

$(document).on('click','.delete_reminder,.notif_remove',function(e){
    var reminder_id = $(this).data('reminder_id');
    var data = {
        user_id: $('body').data('log_id'),
        reminder_id: reminder_id,
        status: 0
    }

    var c = confirm('Remove this reminder?');
    if(c){
        $.ajax({
            url: '/reminder/reminder_remove',
            method: 'post',
            data: data,
            dataType:  'JSON',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(r) {
                $("div[data-reminder_member_id='"+reminder_id+"']").remove();
                $('.reminder_parent_'+reminder_id).fadeOut(300);
            }
        });
    }
});

$(document).on('change','#select_reminder_form',function(e){
  // var me = $(this).val();
  // if(me == 'custom') { $('.days_of_week').show(); }
  // else{ $('.days_of_week').hide(); }
});

$(document).on('click','.notif_close',function(e){
    var data = {
        reminder_members : $(this).data('reminder_members'),
        start_time       : $(this).data('time'),
        next_repeat      : $(this).data('next_repeat'),
        repeat_every     : $(this).data('day_name'),
        recurring        : $(this).data('recurring')
    };

    $.ajax({
        url:'/reminder/snooze_reminder',
        method: "post",
        dataType: "JSON",
        data: data,
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        success :function(r){
        }
    });
});

// for button FILTER BY
function filter_btn () { }

// Show modal
$(document).on('show.bs.modal','#add_task_modal', function () {
    var me = $(this);
    var data = '';
    get_teams(me,data);
});

$(document).on('show.bs.modal','#edit_task_modal', function (e) {
    var me = $(this);
    var data = e.relatedTarget;
  
    //get_teams(me,data);
    var creator_id = $('body').data('log_id');
    me.find('.assignee-list').empty();

    if(data.data.get_assign.length > 0){
        $.each(data.data.get_assign,function(k,v){    
            //console.log(v);
            var h = '<div class="assignee-item flex middle user-list'+v.user_id+'">'+
                        '<img src="'+v.image+'" style="height:45px;width:45px;border-radius:100px;">'+
                        '<span>'+v.get_user[0].firstname+' '+v.get_user[0].lastname+'</span><a class="remove-assign" data-user-id="'+v.user_id+'">╳</a>'+
                    '</div>';
            if(creator_id != v.user_id) {
                me.find('.assignee-list').append(h);
            }
        });

        me.find('.assignee-list').removeClass('hide');
    }

    // $.ajax({
    //     url:'task/get_teams',
    //     type : 'POST',
    //     async: false,
    //     headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
    //     dataType: "json",
    //     success: function(r){
    //     }
    // });
});

$(document).on('show.bs.modal','#reminder_modal', function (e) {
    var me = $(this);
    get_teams_reminder(me,'');
});

$(document).on('show.bs.modal','#edit_reminder_modal', function (e) {
    var me = $(this);
    var reminder_id = $(e.relatedTarget).data('reminder_id');
    var result = edit_reminder_ajax(reminder_id);

    //SET DATA
    me.find('input[name="reminder_id"]').val(result.data.reminder.id);
    me.find('input[name="reminder_name"]').val(result.data.reminder.reminder_name);
    me.find('input[name="reminder_name"]').val(result.data.reminder.reminder_name);
    me.find('input[name="start_time"]').val(result.data.reminder.member_list[0].start_time);
    me.find('input[name="recurring"]').val(result.data.reminder.member_list[0].recurrence);
    me.find('#select_reminder_form').val(result.data.reminder.repeat_every).change();

    var data = {
        reminder: result.data.reminder,
        members: result.data.members
    };

    if(result.data.reminder.repeat_every == 'custom'){
        $.each(result.data.members,function(k,v){
            $.each(v,function(kk,vv){
                me.find("input[type=checkbox][value="+vv.repeat_every+"]").prop("checked",true);
            });
            return false;
        });
    }
    //console.log(result);
    get_teams_reminder(me,data);
});

function edit_reminder_ajax(reminder_id) {
    var data = "";
    $.ajax({
        url:"reminder/reminder_info/"+reminder_id,
        async:false,
        dataType: "json",
        success: function(r) {
           data = r;
           var members = r.data.members;
           
           var html = "";
           $.each(members,function(k,v){
                html += '<div class="assignee-item flex middle user-list'+v[0].user.id+'">'+
                            '<img src="images/icons/default-user.png" style="height:45px;width:45px;border-radius:100px;">'+
                            '<span>'+v[0].user.firstname+' '+v[0].user.lastname+'</span>'+
                            '<a class="remove-assign" data-user-id="'+v[0].user.id+'">╳</a>'+
                        '</div>';
           });

           var modal = $('#edit_reminder_modal').find('.assignee-list');
                       modal.append(html);
           
           if (Object.keys(members).length > 0) { modal.removeClass('hide');} 
           else { modal.addClass('hide'); }
           console.log(members);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
    return data;
}


// Hide modal
// ---------------------------------------------------
$(document).on('hide.bs.modal','#add_task_modal', function () {
    var me = $(this);
    me.find('.panel-heading').html('');
    me.find('#business_panel').html('');
    me.find('#personal_panel').html('');
    $('#add_task_modal').attr("data-create_new" , false);
    $('#create_project_task_modal').hide();
}); 

$(document).on('hide.bs.modal','#edit_task_modal', function () { 
    var me = $(this);
    me.find('.edit_x').val('');
    $(this).find('.file-list,.image-list').empty();
    $('.edit_description_texteditor').html('');
    me.find('#personal_panel').hide();
    me.find('.panel-heading').html('');
    me.find('#business_panel').html('');
    me.find('#personal_panel').html('');
    me.find('#business_panel').show();
    me.find('#personal_panel').hide();
}); 

$(document).on('hide.bs.modal','#reminder_modal', function (e) {
    var me = $(this);
    me.find('.panel-heading').html('');
    me.find('#business_panel_reminder').html('');
    me.find('#personal_panel_reminder').html('');
});

$(document).on('hide.bs.modal','#edit_reminder_modal', function(e) {
    var me = $(this);
    //SET DATA
    me.find('input[name="reminder_name"]').val('');
    me.find('input[name="start_time"]').val('');
    me.find('input[name="recurring"]').val('');
    me.find('#select_reminder_form').val('daily').change();

    me.find("input[type=checkbox]").prop("checked",false);
    me.find("#business_panel_reminder").empty();
    me.find("#personal_panel_reminder").empty();
});

// FOR ADD and EDIT TASK
function get_teams(me,data) {
    var me = me; // set instance variable;
    var data = data;

    $.ajax({
        url:'task/get_teams',
        type : 'POST',
        async: false,
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        dataType: "json",
        success: function(r){
            var assignee_title = "",
            business_team = "",
            b = false, p = false;
            if(data == '') { // Add Task
                if(r.get_my_business_team){
                    b=true;
                    assignee_title += 
                    '<div class="">'+
                            '<input type="text" id="search_member" class="form-control" placeholder="Search Member">'+
                    '</div>';
            
                    $.each(r.get_my_business_team,function(k,v){
                        business_team +=
                        '<div class="checkbox checkbox-gray checkbox-xs">'+
                        '<input id="assinee_'+v.id+'" class="checkAssignee styled" name="task_users[]" value="'+v.id+'" type="checkbox">'+
                        '<label for="assinee_'+v.id+'">'+v.firstname+' '+v.lastname+'</label>'+
                    '</div>';
                    });
                }
    
                //me.find('#business_panel').html(business_team);
                if(b == true){
                    $('.b_radio').attr('checked', true);
                }
            }

            else { //Edit Task
                if(data.task.type == 'B'){ // BUSINESS
                    assignee_title += 
                    '<div class="radio radio-gray radio-inline">'+
                        '<input type="radio" name="account_type" id="business" class=" radio_member b_radio" value="business" checked>'+
                        '<label for="radio1">'+
                            'Business Team'+
                        '</label>'+
                    '</div>';

                    //load all available business members
                    if(r.get_my_business_team.length > 0){
                        $.each(r.get_my_business_team,function(k,v){
                            let display = '';
                            if($('.edit_created_by').val() != v.id){ display = 'hide'; }

                            business_team +=
                            '<div class="checkbox checkbox-gray checkbox-xs">'+
                                '<input id="assinee_'+v.id+'" class="checkAssignee checkAll styled '+display+'" name="task_users[]" value="'+v.id+'" type="checkbox">'+
                                '<label for="assinee_'+v.id+'">'+v.firstname+' '+v.lastname+'</label>'+
                            '</div>';
                        });
                    }
                }
                else { // PERSONAL
                    assignee_title += 
                    '<div class="radio radio-gray radio-inline">'+
                        '<input type="radio" name="account_type" id="personal" class="radio_member p_radio" value="personal" checked>'+
                        '<label for="radio2">'+
                            'Personal'+
                        '</label>'+
                    '</div>';
                    //load all available personal members
                    $.each(r.get_my_personal_team,function(k,v){
                        let display = '';
                        if($('.edit_created_by').val() != v.id){ display = 'hide'; }
                        personal_team +=
                        '<div class="checkbox checkbox-gray checkbox-xs">'+
                            '<input id="assinee_'+v.id+'" class="checkAssignee checkAll styled '+display+'" name="task_users[]" value="'+v.id+'" type="checkbox">'+
                            '<label for="assinee_'+v.id+'">'+v.firstname+' '+v.lastname+'</label>'+
                        '</div>';
                    });
                }
                //same business_team variable
                if(data.task.type == 'B'){
                    me.find('#business_panel_edit').html(business_team);
                    me.find('#business_panel_edit').show();
                    me.find('#personal_panel_edit').hide();
                }
                if(data.task.type == 'P'){
                    me.find('#personal_panel_edit').html(personal_team);
                    me.find('#business_panel_edit').hide();
                    me.find('#personal_panel_edit').show();
                    //console.log('wew');
                }
                //mark assignee that are assign  
                $.each(data.assignee,function(k,v){
                    $('#assinee_'+v[0].id).prop('checked',true);
                });

                
            }
            me.find('.panel-heading').html(assignee_title);
        },
        error: function(e){
            //console.log(e.responseText);
        }
    }); 
}

// FOR ADD and EDIT Reminder
function get_teams_reminder(me,data) {
    var me = me; // set instance variable;
    var data = data;

    $.ajax({
        url:'task/get_teams',
        type : 'POST',
        async: false,
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        dataType: "json",
        success: function(r) {
            var assignee_title = "";
            var b = false, p = false;
            
            var business_team  = '<div class="checkbox checkbox-gray checkbox-xs">'+
                                    '<input id="assignee_business" class="checkAssignee checkAll styled" type="checkbox">'+
                                    '<label for="assignee_business">All Members</label>'+
                                 '</div>';
            
            var personal_team  = '<div class="checkbox checkbox-gray checkbox-xs">'+
                                    '<input id="assignee_personal" class="checkAssignee checkAll styled" type="checkbox">'+
                                    '<label for="assignee_personal">All Members</label>'+
                                 '</div>';

            if(data == '') { // Add Task
                if(r.get_my_business_team.length > 0){
                    b=true;
                    assignee_title += 
                        '<div class="radio radio-gray radio-inline">'+
                            '<input type="radio" name="account_type" id="business" class=" radio_member b_radio" value="business">'+
                            '<label for="radio1">'+
                                'Business Team'+
                            '</label>'+
                        '</div>';
                
                       $.each(r.get_my_business_team,function(k,v){
                           business_team +=
                            '<div class="checkbox checkbox-gray checkbox-xs">'+
                            '<input id="assinee_'+v.id+'" class="checkAssignee styled" name="task_users[]" value="'+v.id+'" type="checkbox">'+
                            '<label for="assinee_'+v.id+'">'+v.firstname+' '+v.lastname+'</label>'+
                        '</div>';
                       });
                }
    
                if(r.get_my_personal_team.length > 0){
                    p=true;
                    assignee_title += 
                    '<div class="radio radio-gray radio-inline">'+
                        '<input type="radio" name="account_type" id="personal" class="radio_member p_radio" value="personal">'+
                        '<label for="radio2">'+
                            'Personal'+
                        '</label>'+
                    '</div>';

                    $.each(r.get_my_personal_team,function(k,v){
                        personal_team +=
                        '<div class="checkbox checkbox-gray checkbox-xs">'+
                            '<input id="assinee_'+v.id+'" class="checkAssignee styled" name="task_users[]" value="'+v.id+'" type="checkbox">'+
                            '<label for="assinee_'+v.id+'">'+v.firstname+' '+v.lastname+'</label>'+
                        '</div>';
                    });
                }
            
                me.find('.panel-heading').html(assignee_title);
                
                me.find('#business_panel_reminder').html(business_team);
                me.find('#personal_panel_reminder').html(personal_team);
                
                if(b == true){
                    $('.b_radio').trigger('click');
                }

            }

            else { //Edit Task

                if(data.reminder.type == 'B'){ // BUSINESS
                    assignee_title += 
                    '<div class="radio radio-gray radio-inline">'+
                        '<input type="radio" name="account_type" id="business" class=" radio_member b_radio" value="business" checked>'+
                        '<label for="radio1">'+
                            'Business Team'+
                        '</label>'+
                    '</div>';

                    //load all available business members
                    if(r.get_my_business_team.length > 0){
                        $.each(r.get_my_business_team,function(k,v){
                            let display = '';
                            if($('.edit_created_by').val() != v.id){ display = 'hide'; }

                            business_team +=
                            '<div class="checkbox checkbox-gray checkbox-xs">'+
                                '<input id="assinee_'+v.id+'" class="checkAssignee styled '+display+'" name="task_users[]" value="'+v.id+'" type="checkbox">'+
                                '<label for="assinee_'+v.id+'">'+v.firstname+' '+v.lastname+'</label>'+
                            '</div>';
                        });

                        //Creator only
                        if($('body').data('log_id') == data.reminder.created_by){
                            let $members = $.map(data.members,function(k,v){ return +v; });
                            let $bt = $.map(r.get_my_business_team,function(k,v){ return k.id; });
                            $bt.push($('body').data('log_id')); //Exclude Creator
                                                
                            $.each($members,function(k,v){
                                if($.inArray(v,$bt) === -1){ // NOt Found
                                    //console.log(JSON.stringify(data.members[v][0]));
                                    var f = data.members[v][0].user.firstname;
                                    var l = data.members[v][0].user.lastname;
                                
                                    business_team +=
                                    '<div class="checkbox checkbox-gray checkbox-xs">'+
                                        '<input id="assinee_'+v+'" class="checkAssignee styled" name="task_users[]" value="'+v+'" type="checkbox">'+
                                        '<label for="assinee_'+v+'">'+f+' '+l+'</label>'+
                                    '</div>';
                                }
                            });
                        }
                    }

                    
                }
                else { // PERSONAL
                    assignee_title += 
                    '<div class="radio radio-gray radio-inline">'+
                        '<input type="radio" name="account_type" id="personal" class="radio_member p_radio" value="personal" checked>'+
                        '<label for="radio2">'+
                            'Personal'+
                        '</label>'+
                    '</div>';

                    //load all available personal members
                    $.each(r.get_my_personal_team,function(k,v){
                        let display = '';
                        if($('.edit_created_by').val() != v.id){ display = 'hide'; }
                        personal_team +=
                        '<div class="checkbox checkbox-gray checkbox-xs">'+
                            '<input id="assinee_'+v.id+'" class="checkAssignee styled '+display+'" name="task_users[]" value="'+v.id+'" type="checkbox">'+
                            '<label for="assinee_'+v.id+'">'+v.firstname+' '+v.lastname+'</label>'+
                        '</div>';
                        
                        //Creator only
                        if($('body').data('log_id') == data.reminder.created_by){
                            let $members = $.map(data.members,function(k,v){ return +v; });
                            let $bt = $.map(r.get_my_business_team,function(k,v){ return k.id; });
                            $bt.push($('body').data('log_id')); //Exclude Creator
                                                
                            $.each($members,function(k,v){
                                if($.inArray(v,$bt) === -1){ // NOt Found
                                    //console.log(JSON.stringify(data.members[v][0]));
                                    var f = data.members[v][0].user.firstname;
                                    var l = data.members[v][0].user.lastname;
                                
                                    business_team +=
                                    '<div class="checkbox checkbox-gray checkbox-xs">'+
                                        '<input id="assinee_'+v+'" class="checkAssignee styled" name="task_users[]" value="'+v+'" type="checkbox">'+
                                        '<label for="assinee_'+v+'">'+f+' '+l+'</label>'+
                                    '</div>';
                                }
                            });
                        }
                    });
                }

                me.find('.panel-heading').html(assignee_title);

                //same business_team variable
                if(data.reminder.type == 'B'){
                    me.find('#business_panel_reminder').html(business_team);
                    me.find('#business_panel_reminder').show();
                    me.find('#personal_panel_reminder').hide();
                }
                if(data.reminder.type == 'P'){
                    me.find('#personal_panel_reminder').html(personal_team);
                    me.find('#business_panel_reminder').hide();
                    me.find('#personal_panel_reminder').show();
                }

                //mark assignee that are assign  
                $.each(data.members,function(k,v){
                    //console.log(k)
                    $('#assinee_'+k).prop('checked',true);
                });
                
            }

        },
        error: function(e){
            //console.log(e.responseText);
        }
    }); 
}

function mark_all_as_read (user_id = false) {
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax({
        url:'mark_all_as_read',
        type : 'POST',
        async: false,
        data : {user_id : user_id} , 
        success : function (response) {
            $('#count_notif_div').text('0'); // set bell icon to 0
            $('#scrollbar_ul').empty(); // remove all list of notif
            $('#ul_mark_all > .mark-all').empty(); // remove message named "mark all as read"
            $('#ul_mark_all > .mark-all').append( 
                '<li class="no-comments"><a href="javascript:void(0);">'+
                '<div class="notif-item flex"><p>No available notification</p>'+
                '</div>'+
                '</a>'+
                '</li>'
            ); // change message to no "no available notif"
            $('.unread').text('0'); // all counting messages on task will change to 0 (zero)
        }
    }); 
}

$(document).on('click' , '.read_notif' , function () {
    var me = $(this);
    var task_id = $(this).data('task_id');
    highlight(task_id);
    return false;
    var task_notification_id = $(this).data('task_notification_id');
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax({
        url:'read_notif',
        type : 'POST',
        async: false,
        data : {task_id : task_id , task_notification_id : task_notification_id } , 
        success : function (response) {
            me.remove();
            var count = $('#count_notif_div').text() ; 
            if (count == 1) { // no notif left
                $('#ul_mark_all > .mark-all').empty(); // remove message named "mark all as read"
                    $('#ul_mark_all > .mark-all').append( 
                    '<li class="no-comments"><a href="javascript:void(0);">'+
                    '<div class="notif-item flex"><p>No available notification</p>'+
                    '</div>'+
                    '</a>'+
                    '</li>'
                ); // change message to no "no available notif"
            } 
            $('#count_notif_div').text(count - 1); 
            var task_card = $('#div'+task_id); // find the task card 
            var task_count = task_card.find('.unread').text();
            task_card.find('.unread').text(task_count - 1); 
        }
    }); 
});

function highlight(task_id) {
    // high-light
    $('#div'+task_id).css({'border':'2px solid #fff53b'});
    $('#div'+task_id).attr("tabindex",-1).focus();
    //remove high-light
    $('#div'+task_id).animate({borderColor: "#fff"}, 4000 );
}

// remove task
$(document).on('click' , '.remove-task' , function () {
    var task_id = $(this).data('task_id');
    // var x = confirm("Do you want to remove this task ? ");
    $('#alert_modal').find('.form-group > div').addClass('hide');
    $('#alert_modal').find('.form-group label').remove();
    var buttons = '<button type="button" class="btn no-bg-btn" data-dismiss="modal">Cancel</button><button type="button" class="btn btn-agree no-bg-btn">Yes</button>';
    alert_modal('#question_animation',question_animation,"question_icon",'Question','Are you sure you want to remove this task?',buttons,false,1.2);
    $('#alert_modal').find('.modal-footer').addClass('right');

    // if (x == true) {
    $('#alert_modal').find('.btn-agree').on('click', function() {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url:'removetask',
            type : 'POST',
            async: false,
            data : {task_id : task_id } , 
            success : function (r) {
                $('#alert_modal').modal('hide');
                setTimeout(function(){
                    $('#alert_modal').css('z-index', '1050');
                    $('.modal-backdrop.in:last-child').css({'opacity': '0.5', 'z-index':'1040'});
                }, 2000);
                $('#div'+r.task_id).remove(); // remove card
            }
        }); 
    });
});

// task complete 
$(document).on('click' , '.task_complete' , function () {
    var task_id = $(this).data('task_id');
    // var x = confirm("Are you sure you want to make this task complete ?  ");

    $('#alert_modal').find('.form-group > div').addClass('hide');
    $('#alert_modal').find('.form-group label').remove();
    var buttons = '<button type="button" class="btn no-bg-btn" data-dismiss="modal">Cancel</button><button type="button" class="btn btn-agree no-bg-btn">Yes</button>';
    alert_modal('#question_animation',question_animation,"question_icon",'Question','Are you sure you want to make this task complete?',buttons,false,1.2);
    $('#alert_modal').find('.modal-footer').addClass('right');
    
    $('#alert_modal').find('.btn-agree').on('click', function() {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url:'task_complete',
            type : 'POST',
            async: false,
            data : {task_id : task_id } , 
            success : function (response) {
                $('#alert_modal').modal('hide');
                setTimeout(function(){
                    $('#alert_modal').css('z-index', '1050');
                    $('.modal-backdrop.in:last-child').css({'opacity': '0.5', 'z-index':'1040'});
                }, 2000);

                setTimeout(function() {
                    $('.modal_active').remove();
                    $('#view_thread_modal').modal("hide");
                }, 3000);
            }
        });
    });
});

// list of to be accept
function accept_member () {
    $('#accept_member_lists').empty();
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax({
        url:'accept_member_lists',
        type : 'GET',
        async: false ,
        success : function (response) {
            // var json = jQuery.parseJSON(response);
            var tmp = "";
            if (response.length > 0) {
               $.each(response, function (k ,v) { 
                  //  tmp += '<tr><td>'+v.firstname+' '+v.lastname+'</td><td><a href="#" class="btn btn-xs btn-success btn-accept" data-auto="'+v.user_unique+'" >Accept</a></td></tr>';
                  tmp +=   '<li>'+
                                '<a href="javascript:void(0);">'+
                                    '<div class="accept-item flex">'+
                                        '<img src="images/sample/user-pic-1.png">'+
                                        '<div class="accept-info">'+
                                            '<p class="info"> <strong>'+v.created_by_full_name.firstname+' '+v.created_by_full_name.lastname+' </strong> wants  to add <b>'+v.firstname+' '+v.lastname+' </b> to your team</p>'+
                                            '<div class="button-div">'+
                                                '<button class="btn-cancel-new-member"  data-auto="'+v.user_unique+'" >No</button>'+
                                                '<button class="btn-accept" data-auto="'+v.user_unique+'" >Accept</button>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</a>'+
                            '</li>';
               });
                    $('#accept_member_lists').append(tmp);
            } else {
                    tmp += '<tr><td colspan="2" class="text-center">No data available</td></tr>';
                  //  $('#accept_member_lists').append(tmp);
            }
        }
    }); 
}

$(document).on('click' ,'.btn-accept' , function () {
    var me = $(this);
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax({
        url:'accept_member_function',
        type : 'POST',
        data:{hash : $(this).data('auto') , _token:$('meta[name="csrf-token"]').attr('content') },
        async: false ,
        success : function (response) {
            me.closest('tr').remove();
            var x = $('.accept-numbers').text() - 1
            $('.accept-numbers').text(x)
            alert("Accepted!");
        }
    });    
});

$(document).on('click' , '.btn-cancel-new-member' , function () {
    var me = $(this);
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax({
        url:'cancel_new_member',
        type : 'POST',
        data:{hash : $(this).data('auto') , _token:$('meta[name="csrf-token"]').attr('content') },
        async: false ,
        success : function (response) {
            me.closest('tr').remove();
            var x = $('.accept-numbers').text() - 1
            $('.accept-numbers').text(x)
           alert("Cancelled!")
        }
    });  

});

//Task Search Assignee-------------------------------------------------------------
$(document).on("keyup" ,'#search_member' , function () {
    var $member_div = $(this).parent().find('.member-results'),
    $btn_group = $(this).parent();

    $btn_group.addClass('open');
    $member_div.removeClass('hide').find('ul').remove();

    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax({
        url: 'get_member_search',
        data : { firstname : $(this).val() } ,
        method : "POST" , 
        async : false , 
        success : function (response) {
            // var json = jQuery.parseJSON(response);
            var tmp = '<ul>';
            $.each(response,function (k, v) {
                if(v.id != $('.body').data('log_id')){
                    tmp += '<li class="choose-name " data-generate="'+v.id+'" data-user-id="'+v.id+'"><img src="'+v.image+'"><span>'+v.firstname+' '+v.lastname+'</span></li>';                       
                }              
            });
            tmp += '</ul>';
            $member_div.append(tmp);
        }
    });
});

$(document).on("click" , '.choose-name' , function () {
    $(this).closest('.form-group').find('.assignee-list').removeClass('hide');
    var arr = [];
    var tmp = '<div class="assignee-item flex middle user-list'+$(this).data('user-id')+'">'+
                    '<img src="'+$('#site_name').data('site_name')+'/images/sample/user-pic-4.png">'+
                    '<span>'+$(this).text()+'</span>'+
                    '<input type="hidden" name="task_users[]" value='+$(this).data('user-id')+'>'+
                    '<a class="remove-assign assignee_choose" data-user-id='+$(this).data('user-id')+'>&#9587;</a>'+
                '</div>';
    
    if($('#add_task_modal .assignee-list').find('.user-list'+$(this).data('user-id')).length < 1){
        $('#add_task_modal .assignee-list').append(tmp);
    }
   //$('#add_task_modal .assignee-list').append(tmp);

    var tmp2 ='<div class="member-item flex middle b user-list'+$(this).data('user-id')+'"" data-generate='+$(this).data('generate')+'>'+
                '<img src="'+$('#site_name').data('site_name')+'/images/sample/user-pic-4.png">'+
                '<span>'+$(this).text()+'</span>'+
                '<input type="hidden" name="task_users[]" value='+$(this).data('user-id')+'>'+
                '<a class="remove-assign assignee_choose" data-user-id='+$(this).data('user-id')+'>&#9587;</a>'+
            '</div>';

    $('#filter_modal .member-list').removeClass('hide').append(tmp2);

    var h = "<input type= 'hidden' name='task_users[]' value='"+$(this).data('user-id')+"'  class='user-list"+$(this).data('user-id')+" '>";
    $('#data-lists').append(h);    
    $('#filter-lists').append(h);    
    $(this).closest('li').remove();
});



$(document).on("keyup" ,'#search_member_edit' , function () {
    var $member_div = $(this).parent().find('.member-results');
                      $(this).parent().addClass('open');
    
        $member_div.removeClass('hide').find('ul').remove();

    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax({
        url: 'get_member_search',
        data : { firstname : $(this).val() } ,
        method : "POST" , 
        async : false , 
        success : function (r) {
            //console.log(r);
            // var json = jQuery.parseJSON(response);
            var tmp = '<ul>';
            $.each(r ,function ( k , v ) {
                if(v.id != $('.body').data('log_id')){
                    tmp += '<li class="choose-name_edit" data-generate="'+v.id+'" data-user-id="'+v.id+'"><img src="'+v.image+'"><span>'+v.firstname+' '+v.lastname+'</span></li>';                       
                }
            });
            tmp += '</ul>';
            $member_div.append(tmp);
        }
    });
});

$(document).on("click" , '.choose-name_edit' , function () {
    var img = $(this).find('img').attr('src');
    $(this).closest('.form-group').find('.assignee-list').removeClass('hide');
    var arr = [];
    var tmp = '<div class="assignee-item flex middle user-list'+$(this).data('user-id')+'">'+
                    '<img src="'+img+'" style="height:45px;width:45px;border-radius:100px;">'+
                    '<span>'+$(this).text()+'</span>'+
                    '<a class="remove-assign" data-user-id='+$(this).data('user-id')+'>&#9587;</a>'+
                '</div>';
    
    if($('#edit_task_modal .assignee-list').find('.user-list'+$(this).data('user-id')).length < 1){
        $('#edit_task_modal .assignee-list').append(tmp);
    }

    var tmp2 ='<div class="member-item flex middle b user-list'+$(this).data('user-id')+'"" data-generate='+$(this).data('generate')+'>'+
                '<img src="'+img+'" style="height:45px;width:45px;border-radius:100px;">'+
                 '<span>'+$(this).text()+'</span>'+
                '<a class="remove-assign" data-user-id='+$(this).data('user-id')+'>&#9587;</a>'+
            '</div>';

    $('#filter_modal .member-list').removeClass('hide').append(tmp2);

    var h = "<input type= 'hidden' name='task_users[]' value='"+$(this).data('user-id')+"'  class='user-list"+$(this).data('user-id')+" '>";
    $('#data-lists').append(h);    
    $('#filter-lists').append(h);    
    $(this).closest('li').remove();
});
//-------------------------------------------------------------


//Reminder Search Assignee-------------------------------------------------------------
$(document).on("keyup" ,'#search_member_reminder' , function () {
    var $member_div = $(this).parent().find('.member-results');
                      $(this).parent().addClass('open');
    
        $member_div.removeClass('hide').find('ul').remove();

    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax({
        url: 'get_member_search',
        data : { firstname : $(this).val() } ,
        method : "POST" , 
        async : false , 
        success : function (r) {
            //console.log(r);
            // var json = jQuery.parseJSON(response);
            var tmp = '<ul>';
            $.each(r ,function ( k , v ) {    
                if(v.id != $('.body').data('log_id')){
                    tmp += '<li class="choose-name_reminder" data-generate="'+v.id+'" data-user-id="'+v.id+'"><img src="'+v.image+'"><span>'+v.firstname+' '+v.lastname+'</span></li>';                       
                }
            });
            tmp += '</ul>';
            $member_div.append(tmp);
        }
    });
});

$(document).on("click" , '.choose-name_reminder' , function () {
    var img = $(this).find('img').attr('src');
    $(this).closest('.form-group').find('.assignee-list').removeClass('hide');
    var arr = [];
    var tmp = '<div class="assignee-item flex middle user-list'+$(this).data('user-id')+'">'+
                    '<img src="'+img+'" style="height:45px;width:45px;border-radius:100px;">'+
                    '<span>'+$(this).text()+'</span>'+
                    '<a class="remove-assign" data-user-id='+$(this).data('user-id')+'>&#9587;</a>'+
                '</div>';

    if($('#reminder_modal .assignee-list').find('.user-list'+$(this).data('user-id')).length < 1){
        $('#reminder_modal .assignee-list').append(tmp);
    }

    var tmp2 ='<div class="member-item flex middle b user-list'+$(this).data('user-id')+'"" data-generate='+$(this).data('generate')+'>'+
                '<img src="'+img+'" style="height:45px;width:45px;border-radius:100px;">'+
                 '<span>'+$(this).text()+'</span>'+
                '<a class="remove-assign" data-user-id='+$(this).data('user-id')+'>&#9587;</a>'+
            '</div>';

    $('#filter_modal .member-list').removeClass('hide').append(tmp2);

    var h = "<input type= 'hidden' name='task_users[]' value='"+$(this).data('user-id')+"'  class='user-list"+$(this).data('user-id')+" '>";
    $('#data-lists').append(h);    
    $('#filter-lists').append(h);    
    $(this).closest('li').remove();
});

$(document).on("keyup" ,'#search_member_reminder_edit' , function () {
    var $member_div = $(this).parent().find('.member-results');
                      $(this).parent().addClass('open');
    
        $member_div.removeClass('hide').find('ul').remove();

    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax({
        url: 'get_member_search',
        data : { firstname : $(this).val() } ,
        method : "POST" , 
        async : false , 
        success : function (r) {
            //console.log(r);
            // var json = jQuery.parseJSON(response);
            var tmp = '<ul>';
            $.each(r ,function ( k , v ) {    
                if(v.id != $('.body').data('log_id')){
                    tmp += '<li class="choose-name_reminder_edit" data-generate="'+v.id+'" data-user-id="'+v.id+'"><img src="'+v.image+'"><span>'+v.firstname+' '+v.lastname+'</span></li>';                       
                }
            });
            tmp += '</ul>';
            $member_div.append(tmp);
        }
    });
});

$(document).on("click" , '.choose-name_reminder_edit' , function () {
    var img = $(this).find('img').attr('src');
    $(this).closest('.form-group').find('.assignee-list').removeClass('hide');
    var arr = [];
    var tmp = '<div class="assignee-item flex middle user-list'+$(this).data('user-id')+'">'+
                    '<img src="'+img+'" style="height:45px;width:45px;border-radius:100px;">'+
                    '<span>'+$(this).text()+'</span>'+
                    '<a class="remove-assign" data-user-id='+$(this).data('user-id')+'>&#9587;</a>'+
                '</div>';

    if($('#edit_reminder_modal .assignee-list').find('.user-list'+$(this).data('user-id')).length < 1){
        $('#edit_reminder_modal .assignee-list').append(tmp);
    }

    var tmp2 ='<div class="member-item flex middle b user-list'+$(this).data('user-id')+'"" data-generate='+$(this).data('generate')+'>'+
                '<img src="'+img+'" style="height:45px;width:45px;border-radius:100px;">'+
                 '<span>'+$(this).text()+'</span>'+
                '<a class="remove-assign" data-user-id='+$(this).data('user-id')+'>&#9587;</a>'+
            '</div>';

    $('#filter_modal .member-list').removeClass('hide').append(tmp2);

    var h = "<input type= 'hidden' name='task_users[]' value='"+$(this).data('user-id')+"'  class='user-list"+$(this).data('user-id')+" '>";
    $('#data-lists').append(h);    
    $('#filter-lists').append(h);    
    $(this).closest('li').remove();
});

$(document).on('click' , '.remove-assign' , function () {
    var $member_list = $(this).closest('.member-list, .assignee-list').find('.member-item, .assignee-item').length;
    
    if ($member_list <= 1) {
        $(this).closest('.member-list, .assignee-list').addClass('hide');
    } else {
        $(this).closest('.member-list, .assignee-list').removeClass('hide');
    }

    $('.user-list'+$(this).data('user-id')).remove();
})

// $(document).on('click' , '.filter_member' , function () {
//     var $form = $(this).closest('form');
//     var $item = $form.find('.member-item');
//     var $input = $(this).closest('form').find('input#search_member');
//     if ($.trim($input.val()) === '') {
//         $input.closest('.btn-group').addClass('has-error');
//         return false;
//     } else {
//         $('#filter_modal').modal('hide');
//         $('.list-card').addClass('hide');
//         $item.each(function() {
//             var $data_value = $(this).attr('data-generate');
//             var $data_generate = $('.card-members').find('[data-generate="'+$data_value+'"]');
//             $data_generate.closest('.list-card').removeClass('hide');
//         });
//     }
// });

// $(document).on('click', '.filter-btn', function() {
//     $('#filter_modal').find('.btn-group').removeClass('has-error');
//     $('#filter_modal').find('#search_member').val('');
// });

// $(document).on('click', '#add_new_member_modal .btn-submit', function() {
//     var $modal_body = $(this).closest('.modal-content').find('.modal-body');
//     var $count = 0;

//     $modal_body.find('.form-group').removeClass('has-error');
//     $modal_body.find('.form-group input.form-control').each(function() {
//         if ($.trim($(this).val()) === '') {
//             $(this).parent().addClass('has-error');
//             $count ++;
//         }
//     });

//     if ($count > 0) {
//         return false;
//     }

//     // Success Alert
//     $('#add_new_member_modal').modal('hide');
//     $('#alert_modal').find('.form-group > div').addClass('hide');
//     $('#alert_modal').find('.form-group label').remove();
//     var buttons = '<button type="button" class="btn no-bg-btn" data-dismiss="modal">Ok</button>';
//     alert_modal('#pink_success, #blue_success',"pink_success blue_success","success_icon",'Success','Member successfully added.',buttons,false,1.2)
//     setTimeout(function() {
//         $('#alert_modal').modal('hide');
//         setTimeout(function(){
//             $('#alert_modal').css('z-index', '1050');
//             $('.modal-backdrop.in:last-child').css({'opacity': '0.5', 'z-index':'1040'});
//         }, 2000);
//     }, 3000);

// });

$(document).on('click' ,'.add-member-btn' , function () {
    var a = [];
    var data = $(this).closest('div').find('.a');
    $.each(data , function () {
        a.push($(this).data('generate'))
    })
    var g = $(this).attr('data-task-g'); 
    var t = $(this).attr('data-t');

    a.push($(this).data('task_creator'))
    var modal = $('#add_new_member_modal_existing').modal("show");
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $('#add_new_member_table').empty();
    $.ajax({
        url: 'add_new_member_on_task',
        data : { t : $(this).data('t')  , data : a} ,
        method : "POST" , 
        async : false , 
        success : function (response) {
            var tmp = ""; 
            $.each(response , function (k , v ) {
                tmp += '<tr><td class="x">'+v.firstname+' '+v.lastname+'</td><td><a href="javascript:void(0);" data-t='+g+' data-g='+g+'  data-user_unique='+v.user_unique+' class="btn btn-xs btn-info add-member-btn-task" >add member</a></td></tr>'
            })
            $('#add_new_member_table').append(tmp);
        }
    });
})

$(document).on('click' , '.add-member-btn-task' , function () {
    $(this).closest('tr').remove();
    var tmp = '<img data-status="unseen" class="a" data-name="" src="'+$('#site_name').data('site_name')+'/images/sample/user-pic-2.png" data-toggle="tooltip" data-placement="bottom" title="(Unseen)">';
    $('.'+$(this).data('g')).find('#assignee-to').after(tmp)

    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax({
        url: 'add_new_member_on_task_function',
        data : { task_id : $(this).data('t')  , user_unique : $(this).data('user_unique') } ,
        method : "POST" , 
        async : false , 
        success : function (response) {
          console.log(response)
        }
    });
})

// Float Action Button
$(function() {
    $('.float-action2 .action-btn').click(function(){
        $(this).closest('.float-action2').toggleClass('is-opened');
        $('.sub-btn').removeClass('is-opened');
        $('body').toggleClass('is-blur');
    })

    $('.sub-btn').click(function(){
        $('.sub-btn').not(this).removeClass('is-opened');
        $(this).parent().find('.sub-btn').removeClass('is-always-opened');
        $(this).toggleClass('is-opened');
    })

    $('.sub-btn input').click(function(e) {
        e.preventDefault();
        $(this).parent().removeClass('is-opened').addClass('is-always-opened');
    });
});

$(document).on('click', '.nav-menu .img-mobile', function() {
    $('.main-container').addClass('mob-members');
    $('.mobile-members-div').find('.nav-item').removeClass('active');
    $('#pending_tab').parent().addClass('active');
    $('.mobile-members-div').find('.tab-pane').removeClass('active in');
    $('#panel_pending').addClass('active in');
    $('.nav-accept').parent().find('dropdown-backdrop').remove();
});

$(document).on('click', '.removeMember', function () {
    var me = $(this);
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    var buttons = '<button type="button" class="btn no-bg-btn" data-dismiss="modal">Cancel</button><button type="button" class="btn btn-agree no-bg-btn">Yes</button>';
    alert_modal('#question_animation',question_animation,"question_icon",'Question','Are you sure you want to remove this member?',buttons,false,1.2);
    $('#alert_modal').removeClass('center').find('label').remove();

    $('#alert_modal').find('.btn-agree').on('click', function() {
        $.ajax({
            url: 'removeMember',
            data : { user : me.data('user') } ,
            method : "POST" , 
            async : false , 
            success : function (response) {
                me.closest('li').remove();
                $('#alert_modal').modal('hide');
                setTimeout(function(){
                    $('#alert_modal').css('z-index', '1050');
                    $('.modal-backdrop.in:last-child').css({'opacity': '0.5', 'z-index':'1040'});
                }, 2000);
            }
        });
    });

    // var x = confirm("Are you sure you want to remove this member ?");
    // if (x == true) {
    //     $.ajax({
    //         url: 'removeMember',
    //         data : { user : $(this).data('user') } ,
    //         method : "POST" , 
    //         async : false , 
    //         success : function (response) {
    //           me.closest('li').remove()
    //         }
    //     });
    // } else {
    //     return false;
    // }
});

// Profile Image Upload
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            $('#upload_img').attr('src', e.target.result).removeClass('hide').prev('.upload-btn').removeClass('upload-btn').addClass('change-btn');
        }
        
        reader.readAsDataURL(input.files[0]);
    }
}

$(function() {
    $("#upload_input").change(function(){
        readURL(this);
    });
});

// Alert Modal Function
function alert_modal(animation,variable,icon,label,p,button,loop,speed){
    var $alert_modal = $('#alert_modal');
    $alert_modal.find('.form-group > div').addClass('hide');
    $alert_modal.find('.modal-footer').removeClass('right');
    $alert_modal.addClass('center').find(animation).removeClass('hide');
    $alert_modal.find('label').text(label);
    $alert_modal.find('p').text(p);
    $alert_modal.find('.modal-footer').html(button)
    $alert_modal.modal('show').css('z-index', '1000002');
    $('.modal-backdrop.in:last-child').css({'opacity': '0.4', 'z-index':'1000001'});

    if (icon === "warning_icon") {
        $('#alert_modal').find('#warning_animation').empty();
        var variable = bodymovin.loadAnimation({
            container: document.getElementById('warning_animation'),
            rederer: 'svg',
            loop: loop,
            autoplay: true,
            path: 'public/json/warning.json'
        })
        
        variable.setSpeed(speed);
    }

    if (icon === "success_icon") {
        $('#alert_modal').find('#pink_success').empty();
        var variable = bodymovin.loadAnimation({
            container: document.getElementById('pink_success'),
            rederer: 'svg',
            loop: loop,
            autoplay: true,
            path: 'public/json/pink-success.json'
        })
        
        variable.setSpeed(speed);

        $('#alert_modal').find('#blue_success').empty();
        var variable = bodymovin.loadAnimation({
            container: document.getElementById('blue_success'),
            rederer: 'svg',
            loop: loop,
            autoplay: true,
            path: 'public/json/blue-success.json'
        })
        
        variable.setSpeed(speed);
    }

    if (icon === "error_icon") {
        $('#alert_modal').find('#error_animation').empty();
        var variable = bodymovin.loadAnimation({
            container: document.getElementById('error_animation'),
            rederer: 'svg',
            loop: loop,
            autoplay: true,
            path: 'public/json/error.json'
        })
        
        variable.setSpeed(speed);
    }

    if (icon === "question_icon") {
        $('#alert_modal').find('#question_animation').empty();
        var variable = bodymovin.loadAnimation({
            container: document.getElementById('question_animation'),
            rederer: 'svg',
            loop: loop,
            autoplay: true,
            path: 'public/json/question.json'
        })
        
        variable.setSpeed(speed);
    }
}

function view_more_new_task (i) {

    $.ajax({
        url : "view_more_new_task" , 
        data : { i : i } , 
        method : "POST" , 
        context: this,
        cache:false,
        dataType : "JSON" ,
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
        beforeSend : function () {

        },
        success : function (response) {

            if (response.last_task_id == undefined) {
                $('.view_more_new_task').remove();
            } else {
                $('.view_more_new_task').attr('onclick', 'view_more_new_task(`'+response.last_task_id+'`)');
                
                $.each(response.n , function ( k , v ) {
                    console.log(v);
                });

                html += '<div class="list-card clearfix {{$value->task_id}}" id="div{{$value->task_id}}">'+
                                '<div class="list-tools flex space-bet">'+
                                    '<span class="project-name">{{$value->category_name}}</span>'+
                                    '<div class="flex">'+
                                        // '<span class="view_threads" data-toggle="tooltip" data-placement="bottom" title="{{ __("translation.show_threads") }}" data-card="working" data-task_id="{{$value->task_id}}" data-task_creator="{{$value->task_creator}}" data-task_status="N" onclick="view_threads( {{$value->task_id}} , {{$value->task_creator}} , "N" , false ,"{{ __("translation.deadline") }}" , "{{ __("translation.task_complete") }}'  , "{{ __("translation.reply") }}" );">
                                            '<img src="'+$('#site_name').data('site_name')+'/images/icons/show-thread-icon.png"></span>'+
                                        '<div class="btn-group card-menu">'+
                                            '<span class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                                                '<img src="'+$('#site_name').data('site_name')+'/images/icons/card-ellipsis.png">'+
                                            '</span>'+
                                            '<div class="dropdown-menu dropdown-menu-right">'+
                                                '<ul>'+
                                                    '<li>'+
                                                        '<a href="javascript:void(0);" class="dropdown-item edit-task" data-creator="" data-task_name="" data-task_id="'+v.task_id+'"><p>Edit</p></a>'+
                                                    '</li>'+
                                                    '<li>'+
                                                        '<a href="javascript:void(0);" class="dropdown-item remove-task" data-task_id="{{$value->task_unique}}"><p>Delete</p></a>'+
                                                    '</li>'+
                                                '</ul>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'
                                '</div>'+
                                '<div class="list-info">'+
                                    '<p class="date-created">{{ Carbon\Carbon::parse($value->task_created)->format("D, M j Y") }}</p>'+
                                    '<h3 class="title" aria-data="{{ $value->title }}">{{ $value->title }}</h3>'+
                                    '<p class="created-by">By  <span>{{ $value->firstname }} {{ $value->lastname }}</span></p>'+
                                    '<div class="deadline">'+
                                        '<img src="'+$('#site_name').data('site_name')+'/images/icons/card-date.png">'+
                                        '<span class="badge-text">Deadline:  {{ Carbon\Carbon::parse($value->deadline)->format("D, M j Y") }}</span>'+
                                    '</div>'+
                                    '<div class="description rmore">'+
                                        '{!! $value->description !!}'+
                                    '</div>'+
                                    '<br clear="all">'+
                                    '<div class="card-members">'+
                                        '<p id="assignee-to">Assigned to</p>'+
                                            /*assigness*/
                                        '<button class="add-member-btn" data-t="{{$value->task_unique}}" data-task_creator="{{$value->task_creator}}"  data-task-g="{{$value->task_id}}"  data-toggle="tooltip" data-placement="bottom" title="Add Member"><div class="add-icon">&#9587;</div></button>'+
                                    '</div>'+
                                    '<label class="attachment-title show-label"><span class="attachment-label">Show Attachment</span> <img src="'+$('#site_name').data('site_name')+'/images/icons/card-clip.png"><span class="count-items">{{$value->attachment->count()}}</span></label>'+
                                    '<div class="attachment-list" style="display: none;">'+
                                        '<div class="file-attachment">'+
                                            /*files*/
                                        '</div>'+
                                    '</div>'+
                                    '<div class="recent-comments clearfix hide">'+
                                        '<p class="comments-label">Recent Comment</p>'+
                                        '<div class="top flex middle">'+
                                            '<img src="{{ asset("images/sample/user-pic-3.png") }}">'+
                                            '<div class="info">'+
                                                '<p class="user-name"></p>'+
                                                '<p class="comment-time"></p>'+
                                            '</div>'+
                                        '</div>'+
                                        '<p class="comment rmore-comment"></p>'+
                                    '</div>'+
                                    '<div class="form-group hide">'+
                                        '<form action="" method="POST" enctype="multipart/form-data">'+
                                            '<input type="hidden" name="task_id" value="">'+
                                            '<input type="hidden" name="status" value="">'+
                                            '<input type="hidden" name="created_by" value="">'+
                                            '<div class="textbox form-group">'+
                                                '<textarea class="form-control required" style="height: 25%" name="comment" placeholder="Comment Here..." required></textarea>'+
                                                '<span class="invalid-feedback">Please input comment here.</span>'+
                                                '<div class="file-attachment">'+
                                                    '<div class="flex middle space-bet">'+
                                                        '<button type="button" class="btn btn-xs btn-info btn-submit pull-left">{{ __("translation.reply") }}</button>'+
                                                        '<label class="add-file-button card-add-file">{{ __("translation.add_files") }}</label>'+
                                                        '<input type="file" name="images[]" class="file-custom-input" multiple style="display: none;">'+
                                                    '</div>'+
                                                    '<div class="file-list clearfix"></div>'+
                                                    '<div class="image-list clearfix"></div>'+
                                                '</div>'+
                                                '<div class="show-thread-btn text-right">'+
                                                    '<a href="javascript:void(0);" onclick="" class="view_threads" data-card="working" data-task_id="" data-task_creator="" data-task_status="I"><p class="link-inset">Show Threads</p></a>'+
                                                '</div>'+
                                            '</div>'+
                                        '</form>'+
                                    '</div>'+
                                    '<div class="form-group">'+
                                        '<div class="textbox form-group">'+
                                            '<div class="show-thread-btn text-right">'+
                                                '<a href="javascript:void(0);" onclick="view_threads( {{$value->task_id}} , {{$value->task_creator}} , "N" , false ,"{{ __("translation.deadline") }}" , "{{ __("translation.task_complete") }}"  , "{{ __("translation.reply") }}" );" class="view_threads" data-card="working" data-task_id="{{$value->task_id}}" data-task_creator="{{$value->task_creator}}" data-task_status="N"><p class="link-inset">{{ __("translation.show_threads") }}</p></a>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';


            } // end else
        } // end success
    });

}

function loadmore_new_task(r) {
    $.each(r.data,function(k,v){

        var toolbar =   `<span class="view_threads show_threads_remove_notif" 
                                data-lang-deadline="${v.button.lang_deadline}" 
                                data-lang-task_complete="${v.button.lang_task_complete}" 
                                data-lang-reply="${v.button.lang_reply}" 
                                data-card="working" 
                                data-task_id="${v.task_id}" 
                                data-task_creator="${v.task_creator}" 
                                data-task_status="C" 
                                data-toggle="tooltip" 
                                data-placement="bottom" 
                                title="${v.button.lang_show_thread}">
                            <img src="`+$('#site_name').data('site_name')+`/images/icons/show-thread-icon.png">
                        </span>`;


        if ($('body').data('log_id')== v.task_creator) {
                        toolbar +=  '<div class="btn-group card-menu">'+
                            '<span class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                                '<img src="'+$('#site_name').data('site_name')+'/images/icons/card-ellipsis.png">'+
                            '</span>'+
                            '<div class="dropdown-menu dropdown-menu-right">'+
                                '<ul>'+
                                    '<li>'+
                                        '<a href="javascript:void(0);" class="dropdown-item edit-task" data-creator="'+v.task_creator+'" data-task_name="'+v.title+'" data-task_id="'+v.task_id+'"><p>Edit</p></a>'+
                                    '</li>'+
                                    '<li>'+
                                        '<a href="javascript:void(0);" class="dropdown-item remove-task" data-task_id="'+v.task_unique+'"><p>Delete</p></a>'+
                                    '</li>'+
                                '</ul>'+
                            '</div>'+
                        '</div>';
        }   

        var html = '<div class="list-card clearfix" id="div18" data-t="'+v.task_unique+'">'+
                        '<div class="list-tools flex space-bet">'+
                            '<span class="project-name">'+v.category_name+'</span>'+
                            '<div class="flex">'+
                               toolbar+
                            '</div>'+
                        '</div>'+
                        '<div class="list-info">'+
                            '<p class="date-created">'+v.created_at+'</p>'+
                            '<h3 class="title" >'+v.title+'</h3>'+
                            '<p class="created-by">By '+v.firstname+' '+v.lastname+'</p>'+
                            '<div class="deadline">'+
                                '<img src="'+$('#site_name').data('site_name')+'/images/icons/card-date.png" />'+
                                '<span class="badge-text">Deadline: '+v.deadline+'</span>'+
                            '</div>'+
                            '<div class="description rmore">'+
                                v.description+
                            '</div>'+
                        '<br clear="all" />';
            
            // Assignees
            if( v.assignee.length > 0 ){    
                $.each(v.assignee,function(kk,vv){             
                    html += '<div class="card-members">'+
                                '<p id="assignee-to">Assigned to</p>'+
                                '<img data-status="unseen" class="a 1" data-generate="1" data-name="'+vv.firstname+' '+vv.lastname+'" aria-data="'+vv.firstname+' '+vv.lastname+'" src="images/sample/user-pic-2.png" data-toggle="tooltip" data-placement="bottom" title="'+vv.firstname+' '+vv.lastname+' (Unseen)" />';
                                 if(vv.id == v.created_by) {
                                    html += '<button class="add-member-btn" data-t="'+v.task_unique+'" data-task_creator="'+v.created_by+'" data-task-g="" data-toggle="tooltip" data-placement="bottom" title="Add Member"><div class="add-icon">╳</div></button>';
                                 }
                    html += '</div>';
                });
            }     

            // Files 
            var file      = '';
            var file_hide = '';
            $.each(v.attachment,function(f,fv) { 
               switch(fv.file_type) {            
                   case 'doc':
                   case 'docx':
                       file += '<div class="file-item js-open-viewer col-sm-6">'+
                                   '<div class="file-wrapper flex middle space-bet">'+
                                       '<div class="file-icon">'+
                                           '<img src="'+$('#site_name').data('site_name')+'/images/icons/file-blue.png"/>'+
                                           '<span>'+fv.file_type+'</span>'+
                                       '</div>'+
                                       '<p class="file-name">'+fv.image+'</p>'+
                                   '</div>'+
                               '</div>';
                               
                       file_hide += '<div class="hidden-item">'+
                                       '<div class="file-type">'+fv.file_type+'</div>'+
                                       '<div class="file-name">'+fv.image+'</div>'+
                                       '<div class="file-src">'+$('#site_name').data('site_name')+fv.full_path+'/</div>'+
                                   '</div>';
                   break;

                   case 'jpg':
                   break;

                   case 'xls':
                   case 'xlsx':
                       file += '<div class="file-item js-open-viewer col-sm-6">'+
                                   '<div class="file-wrapper flex middle space-bet">'+
                                       '<div class="file-icon">'+
                                           '<img src="'+$('#site_name').data('site_name')+'/images/icons/file-darkgreen.png"/>'+
                                           '<span>'+fv.file_type+'</span>'+
                                       '</div>'+
                                       '<p class="file-name">'+fv.image+'</p>'+
                                   '</div>'+
                               '</div>';
                       
                       file_hide +=    '<div class="hidden-item">'+
                                           '<div class="file-type">'+fv.file_type+'</div>'+
                                           '<div class="file-name">'+fv.image+'</div>'+
                                           '<div class="file-src">'+$('#site_name').data('site_name')+fv.full_path+'</div>'+
                                       '</div>'; 
                   break;

                   case 'ppt':
                   case 'pptx':
                       file += '<div class="file-item js-open-viewer  col-sm-6">'+
                                   '<div class="file-wrapper flex middle space-bet">'+
                                       '<div class="file-icon">'+
                                           '<img src="images/icons/file-orange.png"/>'+
                                           '<span>'+fv.file_type+'</span>'+
                                       '</div>'+
                                       '<p class="file-name">'+fv.image+'</p>'+
                                   '</div>'+
                               '</div>';
                       
                       file_hide +=  '<div class="hidden-item">'+
                                           '<div class="file-type">'+fv.file_type+'</div>'+
                                           '<div class="file-name">'+fv.image+'</div>'+
                                           '<div class="file-src">'+$('#site_name').data('site_name')+fv.full_path+'</div>'+
                                       '</div>';
                   break;

                   case 'pdf':
                       file += '<div class="file-item js-open-viewer  col-sm-6">'+
                                   '<div class="file-wrapper flex middle space-bet">'+
                                       '<div class="file-icon">'+
                                           '<img src="images/icons/file-red.png"/>'+
                                           '<span>'+fv.file_type+'</span>'+
                                       '</div>'+
                                       '<p class="file-name">'+fv.image+'</p>'+
                                   '</div>'+
                               '</div>';
                               
                       file_hide +='<div class="hidden-item">'+
                                       '<div class="file-type">'+fv.file_type+'</div>'+
                                       '<div class="file-name">'+fv.image+'</div>'+
                                       '<div class="file-src">'+$('#site_name').data('site_name')+fv.full_path+'</div>'+
                                   '</div>';
                   break;

                   case 'zip':
                   case 'rar':
                   case 'tar':
                   case 'gzip':
                   case 'gz':
                   case '7z':
                   break;

                   case 'htm':
                   case 'html':
                   break;

                   case 'txt':
                   case 'csv':
                   case 'ini':
                   case 'csv':
                   case 'java':
                   case 'php':
                   case 'js':
                   case 'css':
                   break;

                   case 'avi':
                   case 'mpg':
                   case 'mkv':
                   case 'mov':
                   case 'mp4':
                   case '3gp':
                   case 'webm':
                   case 'wmv':
                   case 'm4a':
                       file += '<div class="file-item js-open-viewer  col-sm-6">'+
                                   '<div class="file-wrapper flex middle space-bet">'+
                                       '<div class="file-icon">'+
                                           '<img src="images/icons/file-gray.png" />'+
                                           '<span>'+fv.file_type+'</span>'+
                                       '</div>'+
                                       '<p class="file-name">'+fv.image+'</p>'+
                                   '</div>'+
                               '</div>';

                        file_hide += '<div class="hidden-item">'+
                                   '<div class="file-type">'+fv.file_type+'</div>'+
                                   '<div class="file-name">'+fv.image+'</div>'+
                                   '<div class="file-src">'+$('#site_name').data('site_name')+fv.full_path+'</div>'+
                               '</div>';
                   break;
                   
                   case 'mp3':
                   case 'wav':
                       file += '<div class="file-item js-open-viewer  col-sm-6">'+
                                   '<div class="file-wrapper flex middle space-bet">'+
                                       '<div class="file-icon">'+
                                           '<img src="images/icons/file-gray.png"/>'+
                                           '<span>'+fv.file_type+'</span>'+
                                       '</div>'+
                                       '<p class="file-name">'+fv.image+'</p>'+
                                   '</div>'+
                               '</div>';

                       file_hide +='<div class="hidden-item">'+
                                       '<div class="file-type">'+fv.file_type+'</div>'+
                                       '<div class="file-name">'+fv.image+'</div>'+
                                       '<div class="file-src">'+$('#site_name').data('site_name')+fv.full_path+'</div>'+
                                   '</div>';
                   break;

                   default:
               }
           });


            
            var l_reply = v.button.lang_reply;
            var l_deadline = v.button.lang_deadline;
            var l_task_complete = v.button.lang_task_complete;
            var l_show_threads = v.button.lang_show_threads;

            html +=    '<label class="attachment-title hide-label"><span class="attachment-label">Hide Attachment</span> <img src="'+$('#site_name').data('site_name')+'/images/icons/card-clip.png" /><span class="count-items">'+v.attachment.length+'</span></label>'+
                        '<div class="attachment-list" style="">'+
                            '<div class="file-attachment">'+
                                '<div class="file-list clearfix">'+
                                   file+
                                '</div>'+
                                '<div class="image-list clearfix">'+
                                '</div>'+
                                '<div class="file-info hide">'+
                                  file_hide+
                                '</div>'+                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                            '</div>'+
                        '</div>'+
                        '<div class="recent-comments clearfix hide">'+
                            '<p class="comments-label">Recent Comment</p>'+
                            '<div class="top flex middle">'+
                                '<img src="'+$('#site_name').data('site_name')+'/images/sample/user-pic-3.png"/>'+
                                '<div class="info">'+
                                    '<p class="user-name"></p>'+
                                    '<p class="comment-time"></p>'+
                                '</div>'+
                            '</div>'+
                            '<p class="comment rmore-comment"></p>'+
                        '</div>'+
                        '<div class="form-group hide">'+
                            '<form action="" method="POST" enctype="multipart/form-data">'+
                                '<input type="hidden" name="task_id" value="'+v.task_id+'"/>'+
                                '<input type="hidden" name="status" value="'+v.status+'"/>'+
                                '<input type="hidden" name="created_by" value="'+v.task_creator+'"/>'+
                                '<div class="textbox form-group">'+
                                    '<textarea class="form-control required" style="height: 25%" name="comment" placeholder="Comment Here..." required=""></textarea>'+
                                    '<span class="invalid-feedback">Please input comment here.</span>'+
                                    '<div class="file-attachment">'+
                                        '<div class="flex middle space-bet">'+
                                            '<button type="button" class="btn btn-xs btn-info btn-submit pull-left">reply</button>'+
                                            '<label class="add-file-button card-add-file">Add Files</label>'+
                                            '<input type="file" name="images[]" class="file-custom-input" multiple="" style="display: none;"/>'+
                                        '</div>'+
                                        '<div class="file-list clearfix"></div>'+
                                        '<div class="image-list clearfix"></div>'+
                                    '</div>'+
                                    '<div class="show-thread-btn text-right">'+
                                        `<a href="javascript:void(0);" 
                                                onclick="" 
                                                class="view_threads" 
                                                data-card="working" 
                                                data-task_id="${v.task_id}" 
                                                data-lang_reply="${l_reply}" 
                                                data-lang_deadline="${l_deadline}" 
                                                data-lang_task_complete="${l_task_complete}" 
                                                data-task_creator="${v.task_creator}" 
                                                data-task_status="N">
                                            <p class="link-inset">${l_show_threads}</p>
                                        </a>`+
                                    '</div>'+
                                '</div>'+
                            '</form>'+
                        '</div>'+
                        '<!-- DONT SHOW IF THE USER CREATED THE TASK -->'+
                        '<div class="form-group">'+
                            '<div class="textbox form-group">'+
                                '<div class="show-thread-btn text-right">'+
                                    `<a href="javascript:void(0);" 
                                            onclick="" 
                                            class="view_threads" 
                                            data-card="working" 
                                            data-task_id="${v.task_id}" 
                                            data-task_creator="${v.task_creator}" 
                                            data-lang_reply="${l_reply}" 
                                            data-lang_deadline="${l_deadline}" 
                                            data-lang_task_complete="${l_task_complete}" 
                                            data-task_status="N">
                                        <p class="link-inset">${l_show_threads}</p>
                                    </a>`+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>';

        $('#new_task').find('.list-body').append(html);
    }); // end each
}

function loadmore_waiting_for_answer(r){
    $.each(r.data,function(k,v){

        var toolbar =   `<span class="view_threads show_threads_remove_notif" 
                                data-lang-deadline="${v.button.lang_deadline}" 
                                data-lang-task_complete="${v.button.lang_task_complete}" 
                                data-lang-reply="${v.button.lang_reply}" 
                                data-card="working" 
                                data-task_id="${v.task_id}" 
                                data-task_creator="${v.task_creator}" 
                                data-task_status="C" 
                                data-toggle="tooltip" 
                                data-placement="bottom" 
                                title="${v.button.lang_show_thread}">
                            <img src="`+$('#site_name').data('site_name')+`/images/icons/show-thread-icon.png">
                        </span>`;


        if ($('body').data('log_id')== v.task_creator) {
                        toolbar +=  '<div class="btn-group card-menu">'+
                            '<span class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                                '<img src="'+$('#site_name').data('site_name')+'/images/icons/card-ellipsis.png">'+
                            '</span>'+
                            '<div class="dropdown-menu dropdown-menu-right">'+
                                '<ul>'+
                                    '<li>'+
                                        '<a href="javascript:void(0);" class="dropdown-item edit-task" data-creator="'+v.task_creator+'" data-task_name="'+v.title+'" data-task_id="'+v.task_id+'"><p>Edit</p></a>'+
                                    '</li>'+
                                    '<li>'+
                                        '<a href="javascript:void(0);" class="dropdown-item remove-task" data-task_id="'+v.task_unique+'"><p>Delete</p></a>'+
                                    '</li>'+
                                '</ul>'+
                            '</div>'+
                        '</div>';
        }   
        

        var html = '<div class="list-card clearfix" id="div18" data-t="'+v.task_unique+'">'+
                        '<div class="list-tools flex space-bet">'+
                            '<span class="project-name">'+v.category_name+'</span>'+
                            '<div class="flex">'+
                                toolbar+
                            '</div>'+
                        '</div>'+
                        '<div class="list-info">'+
                            '<p class="date-created">'+v.task_created+'</p>'+
                            '<h3 class="title">'+v.title+'</h3>'+
                            '<p class="created-by">By '+v.firstname+' '+v.lastname+'</p>'+
                            '<div class="deadline">'+
                                '<img src="'+$('#site_name').data('site_name')+'/images/icons/card-date.png" />'+
                                '<span class="badge-text">Deadline: '+v.deadline+'</span>'+
                            '</div>'+
                            '<div class="description rmore">'+
                                v.description+
                            '</div>'+
                        '<br clear="all" />';
            
            // Assignees
            if( v.assignee.length > 0 ){    
                $.each(v.assignee,function(kk,vv){             
                    html += '<div class="card-members">'+
                                '<p id="assignee-to">Assigned to</p>'+
                                '<img data-status="unseen" class="a 1" data-generate="1" data-name="'+vv.firstname+' '+vv.lastname+'" aria-data="'+vv.firstname+' '+vv.lastname+'" src="images/sample/user-pic-2.png" data-toggle="tooltip" data-placement="bottom" title="'+vv.firstname+' '+vv.lastname+' (Unseen)" />';
                                 if(vv.id == v.created_by) {
                                    html += '<button class="add-member-btn" data-t="'+v.task_unique+'" data-task_creator="'+v.created_by+'" data-task-g="18" data-toggle="tooltip" data-placement="bottom" title="Add Member"><div class="add-icon">╳</div></button>';
                                 }
                    html += '</div>';
                });
            }       

            // Files 
            var file      = '';
            var file_hide = '';
            $.each(v.attachment,function(f,fv) { 
                switch(fv.file_type) {            
                    case 'doc':
                    case 'docx':
                        file += '<div class="file-item js-open-viewer col-sm-6">'+
                                    '<div class="file-wrapper flex middle space-bet">'+
                                        '<div class="file-icon">'+
                                            '<img src="'+$('#site_name').data('site_name')+'/images/icons/file-blue.png"/>'+
                                            '<span>'+fv.file_type+'</span>'+
                                        '</div>'+
                                        '<p class="file-name">'+fv.image+'</p>'+
                                    '</div>'+
                                '</div>';
                                
                        file_hide += '<div class="hidden-item">'+
                                        '<div class="file-type">'+fv.file_type+'</div>'+
                                        '<div class="file-name">'+fv.image+'</div>'+
                                        '<div class="file-src">'+$('#site_name').data('site_name')+fv.full_path+'/</div>'+
                                    '</div>';
                    break;

                    case 'jpg':
                    break;

                    case 'xls':
                    case 'xlsx':
                        file += '<div class="file-item js-open-viewer col-sm-6">'+
                                    '<div class="file-wrapper flex middle space-bet">'+
                                        '<div class="file-icon">'+
                                            '<img src="'+$('#site_name').data('site_name')+'/images/icons/file-darkgreen.png"/>'+
                                            '<span>'+fv.file_type+'</span>'+
                                        '</div>'+
                                        '<p class="file-name">'+fv.image+'</p>'+
                                    '</div>'+
                                '</div>';
                        
                        file_hide +=    '<div class="hidden-item">'+
                                            '<div class="file-type">'+fv.file_type+'</div>'+
                                            '<div class="file-name">'+fv.image+'</div>'+
                                            '<div class="file-src">'+$('#site_name').data('site_name')+fv.full_path+'</div>'+
                                        '</div>'; 
                    break;

                    case 'ppt':
                    case 'pptx':
                        file += '<div class="file-item js-open-viewer  col-sm-6">'+
                                    '<div class="file-wrapper flex middle space-bet">'+
                                        '<div class="file-icon">'+
                                            '<img src="images/icons/file-orange.png"/>'+
                                            '<span>'+fv.file_type+'</span>'+
                                        '</div>'+
                                        '<p class="file-name">'+fv.image+'</p>'+
                                    '</div>'+
                                '</div>';
                        
                        file_hide +=  '<div class="hidden-item">'+
                                            '<div class="file-type">'+fv.file_type+'</div>'+
                                            '<div class="file-name">'+fv.image+'</div>'+
                                            '<div class="file-src">'+$('#site_name').data('site_name')+fv.full_path+'</div>'+
                                        '</div>';
                    break;

                    case 'pdf':
                        file += '<div class="file-item js-open-viewer  col-sm-6">'+
                                    '<div class="file-wrapper flex middle space-bet">'+
                                        '<div class="file-icon">'+
                                            '<img src="images/icons/file-red.png"/>'+
                                            '<span>'+fv.file_type+'</span>'+
                                        '</div>'+
                                        '<p class="file-name">'+fv.image+'</p>'+
                                    '</div>'+
                                '</div>';
                                
                        file_hide +='<div class="hidden-item">'+
                                        '<div class="file-type">'+fv.file_type+'</div>'+
                                        '<div class="file-name">'+fv.image+'</div>'+
                                        '<div class="file-src">'+$('#site_name').data('site_name')+fv.full_path+'</div>'+
                                    '</div>';
                    break;

                    case 'zip':
                    case 'rar':
                    case 'tar':
                    case 'gzip':
                    case 'gz':
                    case '7z':
                    break;

                    case 'htm':
                    case 'html':
                    break;

                    case 'txt':
                    case 'csv':
                    case 'ini':
                    case 'csv':
                    case 'java':
                    case 'php':
                    case 'js':
                    case 'css':
                    break;

                    case 'avi':
                    case 'mpg':
                    case 'mkv':
                    case 'mov':
                    case 'mp4':
                    case '3gp':
                    case 'webm':
                    case 'wmv':
                    case 'm4a':
                        file += '<div class="file-item js-open-viewer  col-sm-6">'+
                                    '<div class="file-wrapper flex middle space-bet">'+
                                        '<div class="file-icon">'+
                                            '<img src="images/icons/file-gray.png" />'+
                                            '<span>'+fv.file_type+'</span>'+
                                        '</div>'+
                                        '<p class="file-name">'+fv.image+'</p>'+
                                    '</div>'+
                                '</div>';

                        file_hide += '<div class="hidden-item">'+
                                    '<div class="file-type">'+fv.file_type+'</div>'+
                                    '<div class="file-name">'+fv.image+'</div>'+
                                    '<div class="file-src">'+$('#site_name').data('site_name')+fv.full_path+'</div>'+
                                '</div>';
                    break;
                    
                    case 'mp3':
                    case 'wav':
                        file += '<div class="file-item js-open-viewer  col-sm-6">'+
                                    '<div class="file-wrapper flex middle space-bet">'+
                                        '<div class="file-icon">'+
                                            '<img src="images/icons/file-gray.png"/>'+
                                            '<span>'+fv.file_type+'</span>'+
                                        '</div>'+
                                        '<p class="file-name">'+fv.image+'</p>'+
                                    '</div>'+
                                '</div>';

                        file_hide +='<div class="hidden-item">'+
                                        '<div class="file-type">'+fv.file_type+'</div>'+
                                        '<div class="file-name">'+fv.image+'</div>'+
                                        '<div class="file-src">'+$('#site_name').data('site_name')+fv.full_path+'</div>'+
                                    '</div>';
                    break;

                    default:
                }
            });


            var l_reply = v.button.lang_reply;
            var l_deadline = v.button.lang_deadline;
            var l_task_complete = v.button.lang_task_complete;
            var l_show_threads = v.button.lang_show_threads;

            html += '<label class="attachment-title hide-label"><span class="attachment-label">Hide Attachment</span> <img src="'+$('#site_name').data('site_name')+'/images/icons/card-clip.png" /><span class="count-items">'+v.attachment.length+'</span></label>'+
                        '<div class="attachment-list" style="">'+
                            '<div class="file-attachment">'+
                                '<div class="file-list clearfix">'+
                                   file+
                                '</div>'+
                                '<div class="image-list clearfix">'+
                                '</div>'+
                                '<div class="file-info hide">'+
                                  file_hide+
                                '</div>'+                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                            '</div>'+
                        '</div>'+
                        '<div class="recent-comments clearfix hide">'+
                            '<p class="comments-label">Recent Comment</p>'+
                            '<div class="top flex middle">'+
                                '<img src="'+$('#site_name').data('site_name')+'/images/sample/user-pic-3.png"/>'+
                                '<div class="info">'+
                                    '<p class="user-name"></p>'+
                                    '<p class="comment-time"></p>'+
                                '</div>'+
                            '</div>'+
                            '<p class="comment rmore-comment"></p>'+
                        '</div>'+
                        '<div class="form-group hide">'+
                            '<form action="" method="POST" enctype="multipart/form-data">'+
                                '<input type="hidden" name="task_id" value="'+v.task_id+'"/>'+
                                '<input type="hidden" name="status" value="'+v.status+'"/>'+
                                '<input type="hidden" name="created_by" value="'+v.task_creator+'"/>'+
                                '<div class="textbox form-group">'+
                                    '<textarea class="form-control required" style="height: 25%" name="comment" placeholder="Comment Here..." required=""></textarea>'+
                                    '<span class="invalid-feedback">Please input comment here.</span>'+
                                    '<div class="file-attachment">'+
                                        '<div class="flex middle space-bet">'+
                                            '<button type="button" class="btn btn-xs btn-info btn-submit pull-left">reply</button>'+
                                            '<label class="add-file-button card-add-file">Add Files</label>'+
                                            '<input type="file" name="images[]" class="file-custom-input" multiple="" style="display: none;"/>'+
                                        '</div>'+
                                        '<div class="file-list clearfix"></div>'+
                                        '<div class="image-list clearfix"></div>'+
                                    '</div>'+
                                    '<div class="show-thread-btn text-right">'+
                                        `<a href="javascript:void(0);" 
                                                onclick="" 
                                                class="view_threads" 
                                                data-card="working" 
                                                data-task_id="${v.task_id}" 
                                                data-task_creator="${v.task_creator}" 
                                                data-lang_reply="${l_reply}" 
                                                data-lang_deadline="${l_deadline}" 
                                                data-lang_task_complete="${l_task_complete}" 
                                                data-task_status="I">
                                                <p class="link-inset">${l_show_threads}</p>
                                        </a>`+
                                    '</div>'+
                                '</div>'+
                            '</form>'+
                        '</div>'+
                        '<!-- DONT SHOW IF THE USER CREATED THE TASK -->'+
                        '<div class="form-group">'+
                            '<div class="textbox form-group">'+
                                '<div class="show-thread-btn text-right">'+
                                    `<a href="javascript:void(0);" 
                                            class="view_threads" 
                                            data-card="working" 
                                            data-task_id="${v.task_id}" 
                                            data-task_creator="${v.task_creator}" 
                                            data-lang_reply="${l_reply}" 
                                            data-lang_deadline="${l_deadline}" 
                                            data-lang_task_complete="${l_task_complete}" 
                                            data-task_status="I">
                                        <p class="link-inset">${l_show_threads}</p>
                                    </a>`+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>';

        $('#waiting_for_answer').find('.list-body').append(html);
    }); // end each
}

function loadmore_waiting_for_me(r){   

    $.each(r.data,function(k,v){
       
        var toolbar =   `<span class="view_threads show_threads_remove_notif" 
                                data-lang-deadline="${v.button.lang_deadline}" 
                                data-lang-task_complete="${v.button.lang_task_complete}" 
                                data-lang-reply="${v.button.lang_reply}" 
                                data-card="working" 
                                data-task_id="${v.task_id}" 
                                data-task_creator="${v.task_creator}" 
                                data-task_status="C" 
                                data-toggle="tooltip" 
                                data-placement="bottom" 
                                title="${v.button.lang_show_thread}">
                            <img src="`+$('#site_name').data('site_name')+`/images/icons/show-thread-icon.png">
                        </span>`;


        if ($('body').data('log_id')== v.task_creator) {
            toolbar +=  '<div class="btn-group card-menu">'+
                            '<span class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                                '<img src="'+$('#site_name').data('site_name')+'/images/icons/card-ellipsis.png">'+
                            '</span>'+
                            '<div class="dropdown-menu dropdown-menu-right">'+
                                '<ul>'+
                                    '<li>'+
                                        '<a href="javascript:void(0);" class="dropdown-item edit-task" data-creator="'+v.task_creator+'" data-task_name="'+v.title+'" data-task_id="'+v.task_id+'"><p>Edit</p></a>'+
                                    '</li>'+
                                    '<li>'+
                                        '<a href="javascript:void(0);" class="dropdown-item remove-task" data-task_id="'+v.task_unique+'"><p>Delete</p></a>'+
                                    '</li>'+
                                '</ul>'+
                            '</div>'+
                        '</div>';
        }   
            

        
                            
        var html = '<div class="list-card clearfix" id="div'+v.task_id+'" data-t="'+v.task_unique+'">'+
                        '<div class="list-tools flex space-bet">'+
                            '<span class="project-name">'+v.category_name+'</span>'+
                            '<div class="flex">'+
                                toolbar+
                            '</div>'+
                        '</div>'+
                        '<div class="list-info">'+
                            '<p class="date-created">'+v.task_created+'</p>'+
                            '<h3 class="title" >'+v.title+'</h3>'+
                            '<p class="created-by">By '+v.firstname+' '+v.lastname+'</p>'+
                            '<div class="deadline">'+
                                '<img src="'+$('#site_name').data('site_name')+'/images/icons/card-date.png" />'+
                                '<span class="badge-text">Deadline: '+v.deadline+'</span>'+
                            '</div>'+
                            '<div class="description rmore">'+
                                v.description+
                            '</div>'+
                        '<br clear="all" />';
            
            // Assignees
            if( v.assignee.length > 0 ){    
                $.each(v.assignee,function(kk,vv){             
                    html += '<div class="card-members">'+
                                '<p id="assignee-to">Assigned to</p>'+
                                '<img data-status="unseen" class="a 1" data-generate="1" data-name="'+vv.firstname+' '+vv.lastname+'" aria-data="'+vv.firstname+' '+vv.lastname+'" src="images/sample/user-pic-2.png" data-toggle="tooltip" data-placement="bottom" title="'+vv.firstname+' '+vv.lastname+' (Unseen)" />';
                                 if(vv.id == v.created_by) {
                                    html += '<button class="add-member-btn" data-t="'+v.task_unique+'" data-task_creator="'+v.created_by+'" data-task-g="'+v.task_id+'" data-toggle="tooltip" data-placement="bottom" title="Add Member"><div class="add-icon">╳</div></button>';
                                 }
                    html += '</div>';
                });
            }       

            // Files 
            var file      = '';
            var file_hide = '';
            $.each(v.attachment,function(f,fv) { 
                console.log(fv.file_type)
                switch(fv.file_type) {            
                    case 'doc':
                    case 'docx':
                        file += '<div class="file-item js-open-viewer col-sm-6">'+
                                    '<div class="file-wrapper flex middle space-bet">'+
                                        '<div class="file-icon">'+
                                            '<img src="'+$('#site_name').data('site_name')+'/images/icons/file-blue.png"/>'+
                                            '<span>'+fv.file_type+'</span>'+
                                        '</div>'+
                                        '<p class="file-name">'+fv.image+'</p>'+
                                    '</div>'+
                                '</div>';
                                
                        file_hide += '<div class="hidden-item">'+
                                        '<div class="file-type">'+fv.file_type+'</div>'+
                                        '<div class="file-name">'+fv.image+'</div>'+
                                        '<div class="file-src">'+$('#site_name').data('site_name')+fv.full_path+'/</div>'+
                                    '</div>';
                    break;

                    case 'jpg':
                    break;

                    case 'xls':
                    case 'xlsx':
                        file += '<div class="file-item js-open-viewer col-sm-6">'+
                                    '<div class="file-wrapper flex middle space-bet">'+
                                        '<div class="file-icon">'+
                                            '<img src="'+$('#site_name').data('site_name')+'/images/icons/file-darkgreen.png"/>'+
                                            '<span>'+fv.file_type+'</span>'+
                                        '</div>'+
                                        '<p class="file-name">'+fv.image+'</p>'+
                                    '</div>'+
                                '</div>';
                        
                        file_hide +=    '<div class="hidden-item">'+
                                            '<div class="file-type">'+fv.file_type+'</div>'+
                                            '<div class="file-name">'+fv.image+'</div>'+
                                            '<div class="file-src">'+$('#site_name').data('site_name')+fv.full_path+'</div>'+
                                        '</div>'; 
                    break;

                    case 'ppt':
                    case 'pptx':
                        file += '<div class="file-item js-open-viewer  col-sm-6">'+
                                    '<div class="file-wrapper flex middle space-bet">'+
                                        '<div class="file-icon">'+
                                            '<img src="images/icons/file-orange.png"/>'+
                                            '<span>'+fv.file_type+'</span>'+
                                        '</div>'+
                                        '<p class="file-name">'+fv.image+'</p>'+
                                    '</div>'+
                                '</div>';
                        
                        file_hide +=  '<div class="hidden-item">'+
                                            '<div class="file-type">'+fv.file_type+'</div>'+
                                            '<div class="file-name">'+fv.image+'</div>'+
                                            '<div class="file-src">'+$('#site_name').data('site_name')+fv.full_path+'</div>'+
                                        '</div>';
                    break;

                    case 'pdf':
                        file += '<div class="file-item js-open-viewer  col-sm-6">'+
                                    '<div class="file-wrapper flex middle space-bet">'+
                                        '<div class="file-icon">'+
                                            '<img src="images/icons/file-red.png"/>'+
                                            '<span>'+fv.file_type+'</span>'+
                                        '</div>'+
                                        '<p class="file-name">'+fv.image+'</p>'+
                                    '</div>'+
                                '</div>';
                                
                        file_hide +='<div class="hidden-item">'+
                                        '<div class="file-type">'+fv.file_type+'</div>'+
                                        '<div class="file-name">'+fv.image+'</div>'+
                                        '<div class="file-src">'+$('#site_name').data('site_name')+fv.full_path+'</div>'+
                                    '</div>';
                    break;

                    case 'zip':
                    case 'rar':
                    case 'tar':
                    case 'gzip':
                    case 'gz':
                    case '7z':
                    break;

                    case 'htm':
                    case 'html':
                    break;

                    case 'txt':
                    case 'csv':
                    case 'ini':
                    case 'csv':
                    case 'java':
                    case 'php':
                    case 'js':
                    case 'css':
                    break;

                    case 'avi':
                    case 'mpg':
                    case 'mkv':
                    case 'mov':
                    case 'mp4':
                    case '3gp':
                    case 'webm':
                    case 'wmv':
                    case 'm4a':
                        file += '<div class="file-item js-open-viewer  col-sm-6">'+
                                    '<div class="file-wrapper flex middle space-bet">'+
                                        '<div class="file-icon">'+
                                            '<img src="images/icons/file-gray.png" />'+
                                            '<span>'+fv.file_type+'</span>'+
                                        '</div>'+
                                        '<p class="file-name">'+fv.image+'</p>'+
                                    '</div>'+
                                '</div>';

                        file_hide += '<div class="hidden-item">'+
                                    '<div class="file-type">'+fv.file_type+'</div>'+
                                    '<div class="file-name">'+fv.image+'</div>'+
                                    '<div class="file-src">'+$('#site_name').data('site_name')+fv.full_path+'</div>'+
                                '</div>';
                    break;
                    
                    case 'mp3':
                    case 'wav':
                        file += '<div class="file-item js-open-viewer  col-sm-6">'+
                                    '<div class="file-wrapper flex middle space-bet">'+
                                        '<div class="file-icon">'+
                                            '<img src="images/icons/file-gray.png"/>'+
                                            '<span>'+fv.file_type+'</span>'+
                                        '</div>'+
                                        '<p class="file-name">'+fv.image+'</p>'+
                                    '</div>'+
                                '</div>';

                        file_hide +='<div class="hidden-item">'+
                                        '<div class="file-type">'+fv.file_type+'</div>'+
                                        '<div class="file-name">'+fv.image+'</div>'+
                                        '<div class="file-src">'+$('#site_name').data('site_name')+fv.full_path+'</div>'+
                                    '</div>';
                    break;

                    default:
                }
            });


            var l_reply = v.button.lang_reply;
            var l_deadline = v.button.lang_deadline;
            var l_task_complete = v.button.lang_task_complete;
            var l_show_threads = v.button.lang_show_threads;

            html +=    '<label class="attachment-title hide-label"><span class="attachment-label">Hide Attachment</span> <img src="'+$('#site_name').data('site_name')+'/images/icons/card-clip.png" /><span class="count-items">'+v.attachment.length+'</span></label>'+
                        '<div class="attachment-list" style="">'+
                            '<div class="file-attachment">'+
                                '<div class="file-list clearfix">'+
                                   file+
                                '</div>'+
                                '<div class="image-list clearfix">'+
                                '</div>'+
                                '<div class="file-info hide">'+
                                  file_hide+
                                '</div>'+                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                            '</div>'+
                        '</div>'+
                        '<div class="recent-comments clearfix hide">'+
                            '<p class="comments-label">Recent Comment</p>'+
                            '<div class="top flex middle">'+
                                '<img src="'+$('#site_name').data('site_name')+'images/sample/user-pic-3.png"/>'+
                                '<div class="info">'+
                                    '<p class="user-name"></p>'+
                                    '<p class="comment-time"></p>'+
                                '</div>'+
                            '</div>'+
                            '<p class="comment rmore-comment"></p>'+
                        '</div>'+
                        '<div class="form-group hide">'+
                            '<form action="" method="POST" enctype="multipart/form-data">'+
                                '<input type="hidden" name="task_id" value="'+v.task_id+'"/>'+
                                '<input type="hidden" name="status" value=""'+v.status+'"/>'+
                                '<input type="hidden" name="created_by" value="'+v.task_creator+'"/>'+
                                '<div class="textbox form-group">'+
                                    '<textarea class="form-control required" style="height: 25%" name="comment" placeholder="Comment Here..." required=""></textarea>'+
                                    '<span class="invalid-feedback">Please input comment here.</span>'+
                                    '<div class="file-attachment">'+
                                        '<div class="flex middle space-bet">'+
                                            '<button type="button" class="btn btn-xs btn-info btn-submit pull-left">reply</button>'+
                                            '<label class="add-file-button card-add-file">Add Files</label>'+
                                            '<input type="file" name="images[]" class="file-custom-input" multiple="" style="display: none;"/>'+
                                        '</div>'+
                                        '<div class="file-list clearfix"></div>'+
                                        '<div class="image-list clearfix"></div>'+
                                    '</div>'+
                                    '<div class="show-thread-btn text-right">'+
                                        `<a href="javascript:void(0);" onclick="" 
                                                class="view_threads" 
                                                data-card="working" 
                                                data-task_id="${v.task_id}" 
                                                data-task_creator="${v.task_creator}" 
                                                data-lang_reply="${l_reply}" 
                                                data-lang_deadline="${l_deadline}" 
                                                data-lang_task_complete="${l_task_complete}" 
                                                data-task_status="C">
                                            <p class="link-inset">${l_show_threads}</p></a>`+
                                    '</div>'+
                                '</div>'+
                            '</form>'+
                        '</div>'+
                        '<!-- DONT SHOW IF THE USER CREATED THE TASK -->'+
                        '<div class="form-group">'+
                            '<div class="textbox form-group">'+
                                '<div class="show-thread-btn text-right">'+
                                    `<a href="javascript:void(0);" 
                                            class="view_threads" 
                                            data-card="working" 
                                            data-task_id="${v.task_id}" 
                                            data-task_creator="${v.task_creator}" 
                                            data-lang_reply="${l_reply}" 
                                            data-lang_deadline="${l_deadline}" 
                                            data-lang_task_complete="${l_task_complete}" 
                                            data-task_status="C">
                                        <p class="link-inset">${l_show_threads}</p>
                                    </a>`+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>';

        $('#waiting_for_me').find('.list-body').append(html);
    }); // end each

}


$(document).on('click','.view_more_task',function(){
    var me          = $(this);
    var action      = me.data('action');
    var task_window = me.data('task');
            
    // pagination
    var current_page = parseInt(me.attr('current_page'));
    var next_page    = (current_page + 1);  
    var last_page    = me.attr('last_page');
    
    var current_list = me.closest('.list-body').find('div.list-card');
    //var ids = $.map(current_list,function(v){return $(v).data('t');});
    
    $.ajax({
        url: action,
        type: 'POST',
        data: {page:next_page},
        dataType:'JSON',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        beforeSend: function() {
            me.text('Loading...');
        },
        success: function(r){
            //console.log(r);
            if(task_window == 'task_new'){
                loadmore_new_task(r);
            }
            if(task_window == 'task_wfa'){
                loadmore_waiting_for_answer(r);
            }
            if(task_window == 'task_wfm'){
                loadmore_waiting_for_me(r);
            }
            // update view more button current page
            me.attr('current_page',r.current_page);
            // Hide view button if all task are display
            if(last_page == r.current_page) { me.hide(); }
        },
        complete: function() {
            me.text('View more');
        }
    });
});

$(document).on('click' , '.load_more_todo' , function () {
     var me          = $(this);
    // pagination
    var current_page = parseInt(me.attr('data-current_page'));
    var next_page    = (current_page + 1);  
    var last_page    = me.attr('data-last_page');

    $.ajax({
        url : "loadmore_todo" , 
        data: {page:next_page},
        dataType:'JSON',
        type : "POST" , 
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success : function ( response ) {
            // update view more button current page
            load_more_todo(response);
            me.attr('data-current_page',response.current_page);
            if (response.current_page == last_page) { me.hide()}
        }

    });
});


function load_more_todo (response) {
    var html = "";
    $.each(response.data , function ( k , v ) {
        Date.prototype.monthNames = [
            "January", "February", "March",
            "April", "May", "June",
            "July", "August", "September",
            "October", "November", "December"
        ];
        Date.prototype.getMonthName = function() { return this.monthNames[this.getMonth()]; };
        Date.prototype.getShortMonthName = function () { return this.getMonthName().substr(0, 3);};
        var formatted_date = new Date(v.date_from);
        if (v.status > 0) {
            var checked = "checked='true'";
        }else{
            var checked = "";
        }

        html += "<div class='checkbox checkbox-gray2 task_finished checkbox-sm todo_div_"+v.todo_id+" '>"+
                    "<input id='task_"+v.todo_id+"' class='styled todo_click task_finished ' "+checked+" type='checkbox' data-todo_id='"+v.todo_id+"' value='"+v.todo_id+"' name='todo[]' >"+
                    '<label for="task_'+v.todo_id+'" ></label>'+
                    '<p class="todo_text_'+v.todo_id+'">'+v.todo_name+'</p>'+
                    '<input type="text" value="'+v.todo_name+'" class="input_'+v.todo_id+' hide" style="position: inherit;" >'+
                    '<span class="todo_date_'+v.todo_id+' " data-date="'+v.date_from+'">'+formatted_date.getShortMonthName()+" "+formatted_date.getDate()+" , "+formatted_date.getFullYear()+'</span>'+
                    '<div class="todo-tools">'+
                        '<div class="edit_todo_'+v.todo_id+'">'+
                            '<a href="javascript:void(0);">'+
                                '<img class="pink edit_todo" data-id="'+v.todo_id+'" src="images/final/my-todo-edit.png">'+
                            '</a>'+
                            '<a href="javascript:void(0);">'+
                                '<img class="pink delete_todo" data-id="'+v.todo_id+'" src="images/final/my-todo-delete.png">'+
                            '</a>'+
                        '</div>'+                        '<div class="save_todo_'+v.todo_id+' hide">'+
                            '<a href="javascript:void(0);">'+
                                '<img class="pink save_todo" data-action="save" data-id="'+v.todo_id+'" src="images/final/my-todo-save.png">'+
                            '</a>'+
                            '<a href="javascript:void(0);">'+
                                '<img class="pink cancel_todo" data-action="cancel" data-id="'+v.todo_id+'" src="images/final/my-todo-delete.png">'+
                            '</a>'+
                        '</div>'+
                    '</div>'+
                '</div>';
    });
    $('#todo_div > .checkbox:last').after(html);
}

$(document).on('click' , '.btn-cancel-new-project' , function () {
    $('.new_project_name').val('');
});