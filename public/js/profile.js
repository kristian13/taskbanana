$(function() { 


	

	// Email Validation
	$(document).on('keypress, keyup', '.dropdown.change-email input', function() {
		var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
	    if (testEmail.test(this.value))
	    	$(this).closest('.dropdown-menu').find('button').prop('disabled', false);
	    else 
	    	$(this).closest('.dropdown-menu').find('button').prop('disabled', true);
	});

	// Password Validation
	$(document).on('keypress, keyup', '.dropdown.change-password input', function() {
		// $('input').each(function() {

		// });
		var $old_pass = $(this).closest('form').find('input[name="old_password"]');
		var $new_pass = $(this).closest('form').find('input[name="new_password"]');
		var $retype_pass = $(this).closest('form').find('input[name="retype_password"]');

		if ($old_pass.val().length != 0) {
			if ($old_pass.val().length >= 8) {
				// if (($new_pass.val().length >= 1)) {
				// 	console.log('New password must have atleast 8 characters');
				// }
			} else {
				console.log('Old Password must have alteast 8 characters');
				$('.change_password').prop('disabled',true);
			}

		} else {
			console.log('Please input your old password');
			if (($old_pass.val().length === 0) && ($new_pass.val().length >= 8) && (!($retype_pass.val().length === 0))) {
				if (!($retype_pass.val().length >= 8)) {
					console.log('retype_pass password must have atleast 8 characters')
				}
			} 
			if ($new_pass.val().length === 0) {
				console.log('Please input your new password');
				if ($retype_pass.val().length >= 8) {
				} else {
					console.log('retype_pass password must have atleast 8 characters');
				}

			} else if (!($new_pass.val().length >= 8)) {
				console.log('New password must have atleast 8 characters');
			}

			if (($new_pass.val().length >= 8) && ($retype_pass.val().length >= 8)) {
				if ($new_pass.val() === $retype_pass.val()) {
					console.log('password match');
				} else {
					console.log('password mismatch');
				}
			}
		}

		if (($old_pass.val().length >= 8) && ($new_pass.val().length >= 8)){
			if (($retype_pass.val().length === 0 )) {
				console.log('Please retype your old password');
			} else if (!($retype_pass.val().length >= 8 )){
				console.log('Retype Password must have atleast 8 characters')
				$('.change_password').prop('disabled',true);
			} else {
				if (($new_pass.val().length >= 8) && ($retype_pass.val().length >= 8)) {
					if ($new_pass.val() === $retype_pass.val()) {
						console.log('password match');
						$('.change_password').prop('disabled',false);
					} else {
						console.log('password mismatch');
						$('.change_password').prop('disabled',true);
					}
				}
			}
		}

		$('.change_password').prop('disabled',false);
	});

	$(document).on('click','.change_password',function(e){
		e.preventDefault();

		var form = $(this).closest('form');
		var password = form.find('input[name="new_password"]').val();
		var old_pass = form.find('input[name="old_password"]').val();
		var retype_pass = form.find('input[name="retype_password"]').val();
		var btn = $('.cpword'); 
		$('#change_pass_error').empty();
		$.ajax({
			url : "profile/update_password" , 
			dataType:"json",
			data : {password:password, old_pass:old_pass , retype_pass : retype_pass },
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			beforeSend: function(){
				btn.removeClass('hide');
				btn.addClass('fa fa-refresh fa-spin fa-1x fa-fw'); 
				btn.closest('button').prop('disabled',true);
			},
			success: function(r){
				if (r.result == "error") {

        			$('#change_pass_error').append(
        				'<div class="alert alert-danger">'+
							'<strong>'+r.message+'!</strong>'+
						'</div>'
						);
        			btn.removeClass('fa fa-check');
        			btn.addClass('fa fa-times');
				}else{
					btn.removeClass('fa fa-times');
					btn.addClass('fa fa-check');
				}
				btn.removeClass('fa-refresh fa-spin');
				
				btn.closest('button').prop('disabled',false);
			}
		});
	});

	//Upload Image
	$(document).on('click' , '#upload_profile' , function(e) {
		e.preventDefault();

		var avatar = $('.avatar');
		var profile_image = $('.profile-image'); 
		// Create New Profile Image
		if($(".is_new").val() == 'true') {
			var form_data = new FormData();
			$.each(jQuery('#upload_input').prop("files"), function(i, value) {
				form_data.append('images[]', value);
			});
			$.ajax({ 
				url         : "profile/upload_image" , 
				data        : form_data , 
				method      : "POST" , 
				dataType    : "json",
				async       : false,
				cache       : false ,
				contentType : false ,
				processData : false ,
				headers     : { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } ,
				success : function (res) {
					avatar.attr('src',res.image);
					profile_image.attr('src',res.image);
					$('#profile_photo').modal('hide');
					
				}
			});
		}
		
		// Use Existing Image
		else{
			var data = { update: "use_image", id: $(".is_new").data('id') };
			$.ajax({
				url      : "profile/update" , 
				data     : data, 
				async    : false,
				dataType : "json",
				method   : "POST", 
				headers  : { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
				success  : function (res) {
					if ($('.upload_img').hasClass('hide')) {
						avatar.attr('src',res.image);
						profile_image.attr('src',res.image);
					}else{
						$('#profile_photo').modal('hide');
					}
				}
			});
		}
	});

	$(document).on('change','#upload_input',function(e) {
		$(".is_new").val(true);
		$(".is_new").data('id',null);
		$('.photo-item').css({"border":"0px solid #2E97ED","z-index": "1"});
		$('.photo-item').removeClass('active');
	});
	
	$(document).on('click','.photo-item',function() {

		$('.photo-item').removeClass('active');
		$(this).addClass('active');

		$(".is_new").val(false);
		$(".is_new").data('id',$(this).data('id'));
		var src = $(this).find('img').attr('src');
		$('#upload_img')
			.removeClass('hide')
			.attr('src',src);
		
		$('.upload-btn').
			addClass('change-btn').
			removeClass('upload-btn');
	});
	

	// USE IMAGE
	// $(document).on('click', ".use_image", function(e) {

	// 	$('#message_profile').empty();
	// 	var current = $(this);
	// 	var id = $(this).data('id');
	// 	var data = { update:"use_image", id: id };

	// 	$.ajax({
	// 		url : "profile/update" , 
	// 		data : data, 
	// 		dataType: "json",
	// 		method : "POST", 
	// 	    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
	// 		success : function (res) {
	// 			//console.log(response);
	// 			if(response.result == 'success') { 
	// 				// Set profile Image
	// 				$('.avatar').attr('src',res.image);
	// 				$('.use_image[data-id="'+res.old_image+'"]').text('Use');
	// 				current.text('Active');
	// 				$('#message_profile').append('<div class="alert alert-success">'+
	// 										'<strong>Save!</strong> '+
	// 								 	 '</div>');
	// 			} else {
	// 				$('#message_profile').append('<div class="alert alert-danger">'+
  	// 										'<strong>Please Try Again!</strong>'+
	// 									 '</div>');
	// 			}
	// 		},
	// 		complete: function(res) {

	// 		}
	// 	});
	// });

	// Change timezone 
	$(document).on('click','.change_timezone',function(e){
		e.preventDefault();

		var timezone = $('#profile_timezone').val();
		var btn = $('.ctz'); 

		$.ajax({
			url : "profile/update_timezone" , 
			method : "POST" , 
			data : {timezone:timezone},
	    	dataType: "json",
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			beforeSend: function(){
				btn.removeClass('hide');
				btn.addClass('fa fa-refresh fa-spin fa-1x fa-fw'); 
				btn.closest('button').prop('disabled',true);
			},
			success: function(r){
				//console.log(r);
				btn.removeClass('fa fa-refresh fa-spin');
				btn.addClass('fa fa-check');
				btn.closest('button').prop('disabled',false);
			}
		});
	});

	// REMOVE IMAGE
	$(document).on('click','.img-delete_profile',function (e) { 

		
		// ONLY CONTNUE IF TRUE;
		if (!confirm("Remove This Image?")) return false;

	

		var id   = $(this).data('id');
		var data = { id:id, update:'remove_image' };

		$.ajax({
			url      : "profile/update" , 
			data     : data, 
			dataType : "json",
			method   : "POST", 
		    headers  : { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			success  : function (res) {

				$('.upload-box')
					.next().addClass('upload-btn').
					removeClass('change-btn');

				$('#upload_img').removeAttr('src').addClass('hide');


				if(res.result == 'success') { 
					$(".photo-item[data-id="+res.image_id+"]").remove();
				} else {
					$('#message_profile').append('<div class="alert alert-danger">'+
  											'<strong>Please Try Again!</strong>'+
										 '</div>');
				}
			}
		});	
	});

	// PERSONAL INFO
	$(document).on('submit','#personal_update',function(e) {
		e.preventDefault();
		
		$('#message_profile').empty();
		var fname   = $(this).find('input[name="firstname"]').val();
		var lname   = $(this).find('input[name="lastname"]').val();
		var address = $(this).find('input[name="address"]').val();
		var city    = $(this).find('input[name="city"]').val();
		var state   = $(this).find('input[name="state"]').val();
		var zipcode = $(this).find('input[name="zipcode"]').val();
		var bname   = $(this).find('input[name="business_name"]').val();
		var bnature = $(this).find('select[name="business_nature"] option:selected').val();
		var update  = $(this).find('input[name="update"]').val();
		var btn = $('.cinfo');


		var data =  {
						firstname 		: fname,
						lastname  		: lname,
						address   		: address,
						city			: city,
						state			: state,
						zipcode			: zipcode,
						business_name   : bname,
						business_nature : bnature,
						update          : update,
					    _token          : $('meta[name="csrf-token"]').attr('content') 
				    }

		$.ajax({
			url: "profile/update",
	        data: data,
	        dataType: 'json',
	        method: "POST" , 
	        headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
	        beforeSend: function(){
				btn.removeClass('hide');
				btn.addClass('fa fa-refresh fa-spin fa-1x fa-fw'); 
				btn.closest('button').prop('disabled',true);
			},
	        success : function (response) {
	        	
	        	if(response.result == 'success') {
	        		btn.addClass('fa fa-check');
	       			$(this).find('input[name="firstname"]').val(fname);
					$(this).find('input[name="lastname"]').val(lname);
					$(this).find('input[name="address"]').val(address);
					$(this).find('input[name="city"]').val(city);
					$(this).find('input[name="state"]').val(state);
					$(this).find('input[name="zipcode"]').val(zipcode);
					$(this).find('input[name="business_name"]').val(bname);
					$(this).find('select[name="business_nature"] option:selected').val(bnature);

					$('#message_profile')
						.append('<div class="alert alert-success">'+
  									'<strong>Save!</strong> '+
								'</div>');
					//window.location.reload();
	        	} else {

	        		$.each( response.message , function(fieldname, filederrors){

	        			var list = '';

	        			$.each(filederrors, function(i,v){
	        				list += '<li>'+v+'</li>';
	        			});

	        			$('#message_profile')
	        					.append('<div class="alert alert-danger">'+
											'<strong>'+fieldname+'!</strong> <ul>'+list+'</ul>'+
										'</div>');
					});

	        	} 


        		btn.removeClass('fa-refresh fa-spin');
				btn.closest('button').prop('disabled',false);

	        }
	    });
	});

	// EMAIL
	$(document).on('submit','#email_update',function(e) {
		e.preventDefault();

		$('#message_profile').empty();
		var email  = $(this).find('input[name="email"]').val();
		var update = $(this).find('input[name="update"]').val();
		var btn = $('.cemail');

		var data =  {
						email  : email,
						update :update,
					    _token:$('meta[name="csrf-token"]').attr('content') 
				    }

		$.ajax({
			url: "profile/update",
	        data: data,
	        dataType: 'json',
	        method: "POST" , 
	        headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
	        beforeSend: function(){
				btn.removeClass('hide');
				btn.addClass('fa fa-refresh fa-spin fa-1x fa-fw'); 
				btn.closest('button').prop('disabled',true);
			},
	        success : function (response) {
	        	
	        	btn.removeClass('fa fa-refresh fa-spin');
				btn.addClass('fa fa-check');
				btn.closest('button').prop('disabled',false);
				
	        	if(response.result == 'success') {

	       			$(this).find('input[name="email"]').val(email);
					$('#message_profile').append('<div class="alert alert-success">'+
  												'<strong>Save!</strong> '+
											 '</div>');

	        	} else {

	        		$.each( response.message , function(fieldname, filederrors){
	        			var list = '';
	        			$.each(filederrors, function(i,v){
	        					list += '<li>'+v+'</li>';
	        			});

	        			
	        			$('#message_profile').append('<div class="alert alert-danger">'+
  												'<strong>'+fieldname+'!</strong> <ul>'+list+'</ul>'+
											 '</div>');
					});
	        	} 
	        }
	    });
	});

	// Theme Color
	$(".color-option:radio").on("change", function(e){
		var color = $(this).val();
		var data = { color:color, update:'change_color' };

		$.ajax({
	    	url : "profile/update", 
	    	data : data, 
	    	dataType : 'json',
	    	method : "POST", 
	       	headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
	    	success : function (response) {
	    		if(response.result == 'success'){
	    			window.location.reload();
	    		}
	    	}
	    });	
	});


	// MODAL EVENT Listiners
	$('#profile_photo').on('show.bs.modal', function (e) {
		$('.photo-list').empty();

		$.ajax({
			url     : 'profile/get_profile_info',
			method  : 'get',
			headers : { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") } ,
			success : function (res) {
				if(res.Get_images != null) {
					$.each(res.Get_images, function(k,v){
				
						var status = v.is_profile == 1 ? 'active' : '';

						var html = "<div class='photo-item "+status+"' data-id='"+v.profile_image_id+"'>"+
									"<img src='public/profile/"+v.user_id+"/"+v.image+"'>"+
										"<a href='#' fi-func='delete' fi-name='avatar.png' class='img-delete_profile flex middle center' data-id='"+v.profile_image_id+"'>"+
											"<span>&#9587;</span>"+
										"</a>"+
									"</div>";
						
						$('.photo-list').append(html);
					});
				}
			}
		});

		// Get All User Images
        // <div class="photo-item" data-id="{{$image->profile_image_id}}">
		// 	<img src="profile/{{$image->user_id}}/{{$image->image}}">
		// 	<a href="#" fi-func="delete" fi-name="avatar.png" class="img-delete flex middle center">
		// 		<span>&#9587;</span>
		// 	</a>
		// </div>

	});

});