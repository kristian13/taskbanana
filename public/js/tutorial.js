// Tutorial

// For Add Member Tutorial
var enjoyhint_add_member = new EnjoyHint({
	onEnd: function() {
		$('#add_new_member_modal').modal('hide')
		setTimeout(function() {
			create_project_tutorial();
		}, 200);
	},
	onSkip: function() {
		$('#add_new_member_modal').modal('hide')
		setTimeout(function() {
			create_project_tutorial();
		}, 200);
		
	}
});

var add_member_steps = [
	{
    	'click #add_new_member' : 'Click the "Add Member" button to add your new member.'
  	},
  	{
  		'next #at-email' : 'Input your email address.',
  		'timeout' : 300,
  		'showSkip' : false
  	},
  	{	
  		'next #at-fname' : 'Input your first name.',
  		'showSkip' : false
  	},
  	{
  		'next #at-lname' : 'Input your last name.',
  		'showSkip' : false
  	},
  	{
  		'next #at-add-member-btn' : 'Then submit the form.',
  		'showSkip' : false
  	}
];

//set script config
enjoyhint_add_member.set(add_member_steps);
//run Enjoyhint script
enjoyhint_add_member.run();


// For Add Task Tutorial
function create_project_tutorial() { 
	var enjoyhint_new_project = new EnjoyHint({
		onEnd: function() {
			$('#create_project_task_modal').modal('hide');
			setTimeout(function() {
				addtask_tutorial();
			}, 200);
		},
		onSkip: function() {
			$('#create_project_task_modal').modal('hide');
			setTimeout(function() {
				addtask_tutorial();
			}, 200);
		}
	});

	var new_project_title_steps = [
	  	{
	    	'click #create_project' : 'Click the "Create a Project" button to add new project title.'
	  	},
	  	{
	  		'next #at-new-project' : 'Input new project title.',
	  		'timeout' : 500,
	  		'showSkip' : false
	  	},
	  	{
	  		'next #at-add-project-btn' : 'Then submit to add task.',
	  		'showSkip' : false
	  	}

	];

	//set script config
	enjoyhint_new_project.set(new_project_title_steps);

	//run Enjoyhint script
	enjoyhint_new_project.run();
}


// For Add Task Tutorial
function addtask_tutorial() { 
	var enjoyhint_add_task = new EnjoyHint({
		onEnd: function() {
			$('#add_task_modal').modal('hide');
		},
		onSkip: function() {
			$('#add_task_modal').modal('hide');
		}
	});

	var add_task_steps = [
	  	{
	    	'click #add_task' : 'Click the "Add Member" button to add your new member.'
	  	},
	  	{
	  		'next #at-project-title' : 'Choose project title.',
	  		'timeout' : 700,
	  		'showSkip' : false
	  	},
	  	{	
	  		'next #at-task-name' : 'Input task name or subject here',
	  		'showSkip' : false
	  	},
	  	{
	  		'next #at-description' : 'Input description.',
	  		'showSkip' : false
	  	},
	  	{
	  		'next #at-deadline' : 'Choose date for deadline.',
	  		'showSkip' : false
	  	},
	  	{
	  		'next #at-search-assignee' : 'Search assignee name.',
	  		'showSkip' : false
	  	},
	  	{
	  		'next #at-add-file-btn' : 'Add File.',
	  		'showSkip' : false
	  	},
	  	{
	  		'next #submit_addtask' : 'Then submit to add task.',
	  		'showSkip' : false
	  	}

	];

	//set script config
	enjoyhint_add_task.set(add_task_steps);

	//run Enjoyhint script
	enjoyhint_add_task.run();
}