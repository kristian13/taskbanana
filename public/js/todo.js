
function setPercentBar(){
	var checkedCount = $('input[name="todo[]"]:checked').length;
	var total = $('input[name="todo[]"]').length;

	var percent = (checkedCount / total);
	var percent = (percent * 100);;

	$('.current-percentage').css({'width':percent+'%'});
	$('.percentage-count').text(Math.round(percent)+'%');
}

// Todo Panel
$(document).on('click', '.to-do .panel-buttons', function() {
    var $todo = $(this).closest('.to-do');
    var $target = $(this).attr('data-target');
    var $bottom = $todo.find('.bottom');
    var $bottom_div = $todo.find('.panel-body');
    $todo.find('.panel-buttons').removeClass('active');
    $(this).addClass('active');
    $bottom_div.removeClass('show').addClass('hide');
    $bottom.find($target).removeClass('hide').addClass('show');
});


// Todo > Add Todo
$(document).on('click', '.panel-body > .create-btn', function(e) {
    if ($(this).parent().is('#my_todo_panel')) {
        $(this).addClass('hide').parent().find('form').removeClass('hide');
        $('#to_do_name').focus();
    }

    if ($(this).parent().is('#project_list_panel')) {
        $(this).addClass('hide').parent().find('form').removeClass('hide');
        $('#project_name').focus();
    }

    if ($(this).parent().is('#reminders_panel')) {
        e.preventDefault();
    }
});


// Todo > Cancel Todo
$(document).on('click', '.panel-body .cancel-btn', function() {
    $(this).closest('form').addClass('hide');
    $(this).closest('.panel-body').find('.create-btn').removeClass('hide');
});


// MOBILE > Create Todo
$(document).on('click', '.bottom-nav > a', function() {
    $('.bottom-nav').find('a').removeClass('active');
    $(this).addClass('active');
    if ($(this).is('.right-section')) {
        $('.main-container').addClass('mobile').removeClass('mob-members');
        $('.float-action').addClass('hide');
        $('.to-do').find('.panel-body').removeClass('fix show').addClass('hide');
        var $target = $(this).attr('target')
        $('.to-do').find($target).addClass('fix show').removeClass('hide');
    } else {
        $('.main-container').removeClass('mobile');
         $('.float-action').removeClass('hide');
         $('.to-do').find('.panel-body').removeClass('fix active');
    }

    if ($(this).is('.view-members')) {
        $('.main-container').addClass('mob-members').removeClass('mobile');
        $('.mobile-members-div').find('.nav-item').removeClass('active');
        $('#members_tab').parent().addClass('active');
        $('.mobile-members-div').find('.tab-pane').removeClass('active in');
        $('#panel_members').addClass('active in');
    }
})

function decodeHTMLEntities(text) {
  return $("<textarea/>")
    .text(text)
    .html();
}

// ADD TASK
$(document).on('click','#to_do_add_btn',function(e) {
	e.preventDefault();

	var $this = $(this);
	var todo_name = $("#to_do_name").val();
	var todo_date = $("#todo_date").val();

	if(todo_name.trim() == ''){
		// alert("To Do Field is Required");
		$('#alert_modal').find('.form-group > div').addClass('hide');
		var buttons = '<button type="button" class="btn no-bg-btn" data-dismiss="modal">Ok</button>';
		alert_modal('#warning_animation',warning_animation,"warning_icon",'Warning','To Do field is Required.',buttons,false,1.2);
		return false;
	}

	if (todo_date.trim() == '') {
		// alert("To Do Date is Required");
		$('#alert_modal').find('.form-group > div').addClass('hide');
		var buttons2 = '<button type="submit" class="btn no-bg-btn" data-dismiss="modal">Ok</button>';
		alert_modal('#warning_animation',warning_animation,"warning_icon",'Warning','To Do date is Required.',buttons2,false,1.2);
		// alert_modal('#pink_success, #blue_success',"pink_success blue_success","success_icon",'Success','To Do date is Required.',buttons2,false,1.2)
		return false;
	}

	if($('#is_new').attr('data-status') == '') {

	var data =  { 
		action    : 'add_todo', 
		todo_name : todo_name,
		todo_date : todo_date 
	}

	// storage check 
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax({
        url:'check_storage',
        method : "POST" , 
        success : function (response) {
            if (response == '1') { // if full
                $('#storage_error_modal').modal("show");
            } else {
            	$.ajax({
					url : "todo_task",
					data : data , 
					method : "POST" , 
					dataType: "json",
					headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
							success : function (res) {
								var r = res.data;
								if(res.status == true){
									var date = new Date(r.date_from);
									var da = r.date_from.split("-");
									var month_names_short = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
									var date_arrange      = month_names_short[(da[1]-1)]+" "+da[2]+", "+da[0];
									
									//$t.attr('data-date', fm.replace('MM', m).replace('DD',d.getDate()).replace('YYYY',d.getFullYear()))
							var html = 
							'<div class="checkbox checkbox-gray2 checkbox-sm todo_div_'+r.todo_id+'">'+
									'<input id="task_'+r.todo_id+'" '+ 
									'class="styled todo_click"'+ 
									'type="checkbox"'+ 
											'data-todo_id="'+r.todo_id+'"'+ 
											'value="'+r.todo_id+'"'+ 
									'name="todo[]">'+
									'<label for="task_'+r.todo_id+'"></label>'+
									'<p class="todo_text_'+r.todo_id+'">'+decodeHTMLEntities(r.todo_name)+'</p>'+
									'<input type="text" value="'+r.todo_name+'" class="input_'+r.todo_id+' hide" style="position: inherit;">'+
									'<span class="todo_date_'+r.todo_id+'" data-date="'+r.date_from+'">'+date_arrange+'</span>'+
							'<div class="todo-tools">'+
									'	<div class="edit_todo_'+r.todo_id+'">'+
							'   	<a href="javascript:void(0);">'+
									'			<img class="pink edit_todo" data-id="'+r.todo_id+'" src="images/final/my-todo-edit.png">'+
							'   	</a>'+
							'   	<a href="javascript:void(0);">'+
									'			<img class="pink delete_todo" data-id="'+r.todo_id+'" src="images/final/my-todo-delete.png">'+
							'   	</a>'+
							'   </div>'+
									'   <div class="save_todo_'+r.todo_id+' hide">'+
							'   	<a href="javascript:void(0);">'+
									'			<img class="pink save_todo" data-action="save" data-id="'+r.todo_id+'" src="images/final/my-todo-save.png">'+
							'   	</a>'+
							'   	<a href="javascript:void(0);">'+
									'			<img class="pink cancel_todo" data-action="cancel" data-id="'+r.todo_id+'" src="images/final/my-todo-delete.png">'+
							'   	</a>'+
							'   </div>'+
							'</div>'+
							'</div>';

							$('#todo_div').prepend(html);
							// $('#my_todo_panel').find('#to_do_name, #todo_date').val('');
							setPercentBar();
				            $this.parent().find('#to_do_name, #todo_date').val('');
				            $this.parent().find('[for="todo_date"]').attr('data-date', '');
				            $this.parent().find('.active').removeClass('active');
						}
					}
				});
            }
        }
    }); 
	}

	else {
		
		var data = 
		{ 
			action : 'update',
		        id : $('#is_new').attr('data-id'), 
		    todo_name : todo_name,
		    todo_date : todo_date
		};
		var me = $(this);
		$.ajax({
			url : "todo_task",
			data : data , 
			method : "POST" , 
			dataType: "json",
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			success : function (r) {
				console.log(r);
				if(r.result == 'success'){
					const months = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
					$('.todo_text_'+data.id).text(data.todo_name);
					$('.input_'+data.id).val(data.todo_name);
					var date = data.todo_date.split("-"),
					new_date = months[parseInt(date[1])]+' '+date[2]+', '+date[0];

					$('.todo_date_'+data.id).attr('data-date',data.todo_date);
					$('.todo_date_'+data.id).text(new_date);
					me.next().click();
				}
			}
		});
	}
});	

// Mark Task as Finished and Ongoing
$(document).on('click','.todo_click',function() {
	var style_id = $(this).attr('id');
	var id       = $(this).val();

	var data = { action : 'mark_as',id : id }

	$.ajax({
		url : "todo_task",
		data : data , 
		method : "POST" , 
		dataType: "json",
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
		success : function (response) {
			if(response.result == 'success'){
				var checkedCount = $('input[name="todo[]"]:checked').length;
				$('.completed-items').text(checkedCount);
			}
			setPercentBar();
		}
	});
});


// Show Edit Button
$(document).on('click','.edit_todo',function(e) {
	e.stopPropagation();
	var id = $(this).attr('data-id');
		
	$('#to_do_add_btn').text('Update Todo');
	$('.my_todo_btn').addClass('hide');
	$('.my_todo').removeClass('hide');

	$('.line-input.left-icon').addClass('active');
	$('.line-input.right-icon').addClass('active');
	
	$('#is_new').attr('data-status',true);
	$('#is_new').attr('data-id',id);
	
	var todo_name = $('.input_'+id).val();
	var todo_date = $('.todo_date_'+id).data('date');

	var da = todo_date.split("-");
	var user_date_arrange = da[1]+"/"+da[2]+"/"+da[0];

	$('#to_do_name').val(todo_name);
	$('#todo_date').val(todo_date);
	$('#todo_date').next().attr('data-date',user_date_arrange);

	//var todo_name = $("#to_do_name").val();
	//var todo_date = $("#todo_date").val();

	// $('.input_'+id).removeClass('hide').addClass('on-edit');
	// $('.todo_text_'+id).addClass('hide');

	// // Buttons
	// $('.save_todo_'+id).removeClass('hide');
	// $('.edit_todo_'+id).addClass('hide');
});


$(document).on('click','.cancel-btn',function(){
	
	$('#is_new').attr('data-status','');
	$('#is_new').attr('data-id','');
	
	$('#to_do_add_btn').text('Add to List');
	$('.line-input.left-icon').removeClass('active');
	$('.line-input.right-icon').removeClass('active');

	$('#to_do_name').val(null);
	$('#todo_date').val(null);
	$('#todo_date').next().attr('data-date','');
});


//Save On edit Tool
$(document).on('click','.save_todo, .cancel_todo',function(e) {
	e.stopPropagation();
	var id     	  = $(this).attr('data-id');
	var action 	  = $(this).attr('data-action');
	var old_text  = $('.todo_text_'+id).text();
	var todo_name = $('.input_'+id).val();

	var data = { action : 'update', id : id, todo_name : todo_name }

	if (action == 'save' && old_text != todo_name) {
		$.ajax({
			url : "todo_task",
			data : data , 
			method : "POST" , 
			dataType: "json",
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			success : function (response) {
				if(response.result == 'success'){
					$('.todo_text_'+id).text(todo_name);
				}
			}
		});
	}

	$('.input_'+id).addClass('hide').removeClass('on-edit');
	$('.todo_text_'+id).removeClass('hide');
	// Buttons
	$('.save_todo_'+id).addClass('hide');
	$('.edit_todo_'+id).removeClass('hide');
});


// Delete Task finished or Not
$(document).on('click','.delete_todo',function(e) {
	var id = $(this).attr('data-id');
	var data = { id : id, action : 'delete' };
	
	// if (!confirm("Remove This Task?")) return false;
	$('#alert_modal').find('.form-group > div').addClass('hide');
	$('#alert_modal').find('.form-group label').remove();
	var buttons = '<button type="button" class="btn no-bg-btn" data-dismiss="modal">Cancel</button><button type="button" class="btn btn-agree no-bg-btn">Yes</button>';
    alert_modal('#question_animation',question_animation,"question_icon",'Question','Are you sure you want to remove this task?',buttons,false,1.2);
	$('#alert_modal').find('.modal-footer').addClass('right');
	// return false;

	$('#alert_modal').find('.btn-agree').on('click', function() {
		$.ajax({
			url : "todo_task",
			data : data , 
			method : "POST" , 
			dataType: "json",
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			success : function (response) {
				$('#alert_modal').modal('hide');
                setTimeout(function(){
                    $('#alert_modal').css('z-index', '1050');
                    $('.modal-backdrop.in:last-child').css({'opacity': '0.5', 'z-index':'1040'});
                }, 2000);
                
				if(response.result == 'success'){

					$('.todo_div_'+id).remove();
					setPercentBar();
				}
			}
		});
	});
});


// Hide All Completed Task
$(document).on('click','.hide-completed',function(e) {

	var data = { action : 'hide_task' };

	$.ajax({
		url : "todo_task",
		data : data , 
		method : "POST" , 
		dataType: "json",
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
		success : function (response) {
			$('.task_finished').addClass('hide');
			if(response.result == 'success'){
				$('.completed-items').text('0');
				$('.task_finished').remove();
				setPercentBar();
			}
		}
	});	
});


function addproject() {
    var project_name = $('#project_name').val();
    var i =  '<div class="project-item flex center middle">'+
                '<p class="project-title flex middle"><img class="project-icon" src="http://localhost/trackatask/images/final/project-icon.png"> '+decodeHTMLEntities(project_name)+' </p>'+
                '<p class="project-percent"></p>'+
                '<div class="todo-tools">'+
                    '<div class="edit_todo_">'+
                        '<a href="javascript:void(0);">'+
                            '<img class="pink" data-id="" src="http://localhost/trackatask/images/final/my-todo-edit.png">'+
                        '</a>'+
                        '<a href="javascript:void(0);">'+
                            '<img class="pink" data-id="" src="http://localhost/trackatask/images/final/my-todo-delete.png">'+
                        '</a>'+
                    '</div>'+
                '</div>'+
            '</div>';

    $.ajax({
        url : 'add_project' , 
        data : {project_title : project_name} ,
        method : "POST" ,
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
        success : function (response) {
          	$('.project-list').append(i);
          	$('.cancel-btn').trigger('click');
        } 
    });
   

}