// Line Input
$(document).ready(function() {
    $('.line-input input').focus(function() {
        $(this).closest('.line-input').addClass('active');
    });

    $('.line-input input').focusout(function() {
        if (!$(this).val().trim()) {
            $(this).closest('.line-input').removeClass('active');
        }
    });
});