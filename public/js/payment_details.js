$(function(){

  // "draw": 1,
  // "recordsTotal": 57,
  // "recordsFiltered": 57,
  // "data": []

  // Global Variable
  var page_length = 10;
  var search = null;
  var page = 1;
  var payment_table = 
    $('#payment_details').DataTable({
        "processing": true,
        "serverSide": true,
     
        "columns": [
            //{ "data": "ID" },
            { "data": "Description" },
            { "data": "Date" },
            { "data": "Amount" }
        ],
        "ajax": {
            "url"  : "/trackatask/payment/load_process",
            "type" : "get",
            "headers" : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            "beforeSend": function() {
            },
            "data": function(d){
                d.page_length = page_length;
                d.search      = $('#payment_details_wrapper').find('input[type="search"]').val();
                d.draw        = $('meta[name="csrf-token"]').attr('content');
                d.page        = page;
                return d;
            },
            //"success" : function(r) {
            //   console.log(r);
            //},
            "error": function(e){
                console.log(e.responseText);
            }
        }
    });

    $('#payment_details').on('page.dt', function(){
        var info = payment_table.page.info();
        console.log(info);
        page = (info.page + 1);
    });

    $('#payment_details').on( 'length.dt', function ( e, settings, len ) {
        page_length = len;
    });

    $('#payment_details').on( 'draw.dt', function () {
        page = 1;
    } );
    
});