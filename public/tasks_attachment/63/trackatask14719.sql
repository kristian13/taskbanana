-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 23, 2019 at 11:46 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trackatask`
--

-- --------------------------------------------------------

--
-- Table structure for table `business_nature_child`
--

CREATE TABLE `business_nature_child` (
  `business_nature_child_id` int(10) UNSIGNED NOT NULL,
  `business_nature_parent_id` int(11) NOT NULL,
  `business_nature_child_name` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `business_nature_child`
--

INSERT INTO `business_nature_child` (`business_nature_child_id`, `business_nature_parent_id`, `business_nature_child_name`, `created_at`, `updated_at`) VALUES
(1, 1, 'Software', NULL, NULL),
(2, 1, 'Clothing and accessories', NULL, NULL),
(3, 1, 'Beauty products', NULL, NULL),
(4, 1, 'Home goods and furniture', NULL, NULL),
(5, 1, 'Home electronics', NULL, NULL),
(6, 1, 'Auto parts and accessories', NULL, NULL),
(8, 2, 'Software as a service', NULL, NULL),
(9, 2, 'Apps', NULL, NULL),
(10, 2, 'Books', NULL, NULL),
(11, 2, 'Music or other media', NULL, NULL),
(12, 2, 'Games', NULL, NULL),
(13, 2, 'Blogs and written content', NULL, NULL),
(14, 3, 'Restaurants and nightlife', NULL, NULL),
(15, 3, 'Grocery stores', NULL, NULL),
(16, 3, 'Caterers', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `business_nature_parent`
--

CREATE TABLE `business_nature_parent` (
  `business_nature_parent_id` int(10) UNSIGNED NOT NULL,
  `business_nature_parent_name` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `business_nature_parent`
--

INSERT INTO `business_nature_parent` (`business_nature_parent_id`, `business_nature_parent_name`, `created_at`, `updated_at`) VALUES
(1, 'Retail', NULL, NULL),
(2, 'Digital Products', NULL, NULL),
(3, 'Food and drink', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `business_personal_data`
--

CREATE TABLE `business_personal_data` (
  `business_personal_data_id` int(11) UNSIGNED NOT NULL,
  `parent_id` int(11) UNSIGNED NOT NULL,
  `child_id` int(11) UNSIGNED NOT NULL,
  `related_id` int(10) UNSIGNED NOT NULL,
  `type` char(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `business_personal_data`
--

INSERT INTO `business_personal_data` (`business_personal_data_id`, `parent_id`, `child_id`, `related_id`, `type`) VALUES
(7, 5, 6, 0, 'B'),
(8, 6, 5, 0, 'B');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(24, '2014_10_12_000000_create_users_table', 1),
(25, '2014_10_12_100000_create_password_resets_table', 1),
(26, '2019_01_31_095106_business_nature_parent', 1),
(27, '2019_01_31_095143_business_nature_child', 1),
(28, '2019_02_02_113759_task_assign', 1),
(29, '2019_02_02_114404_task_users', 1),
(30, '2019_02_02_133305_tasks', 1),
(31, '2019_02_02_133324_tasks_attachment', 1),
(32, '2019_02_02_133453_tasks_notification', 1),
(33, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(34, '2016_06_01_000002_create_oauth_access_tokens_table', 3),
(35, '2016_06_01_000003_create_oauth_refresh_tokens_table', 3),
(36, '2016_06_01_000004_create_oauth_clients_table', 3),
(37, '2016_06_01_000005_create_oauth_personal_access_clients_table', 3),
(38, '2019_02_05_105323_task_reply_attachment', 3),
(82, '2014_10_12_000000_create_users_table', 1),
(83, '2014_10_12_100000_create_password_resets_table', 1),
(84, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(85, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(86, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(87, '2016_06_01_000004_create_oauth_clients_table', 1),
(88, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(89, '2019_01_31_095106_business_nature_parent', 1),
(90, '2019_01_31_095143_business_nature_child', 1),
(91, '2019_02_02_113759_task_assign', 1),
(92, '2019_02_02_114404_task_users', 1),
(93, '2019_02_02_133305_tasks', 1),
(94, '2019_02_02_133324_tasks_attachment', 1),
(95, '2019_02_02_133453_tasks_notification', 1),
(96, '2019_02_05_105323_task_reply_attachment', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) DEFAULT NULL,
  `scopes` text,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('056849bdce2d32919bf80bcc6082524c5c06242accac7cd67a6ffded5746cdd0a6710483b904c3c4', 4, 1, NULL, '[]', 0, '2019-03-11 00:41:19', '2019-03-11 00:41:19', '2020-03-11 08:41:19'),
('0db6ed8b3b3d989b514b5dfaafeb4de3f9e9d5d3b40b34fa1129a88a1abab276f77ce4baa3e5add6', 4, 1, NULL, '[]', 0, '2019-03-11 01:35:15', '2019-03-11 01:35:15', '2020-03-11 09:35:15'),
('0eabd06fc5bc624da99b6f47a386e22b4aec0ca8c3c48f4ac1e283a355ad2adaba35950ef4f9b677', 4, 1, 'MyApp', '[]', 0, '2019-03-05 19:29:17', '2019-03-05 19:29:17', '2020-03-06 03:29:17'),
('1bbe0c2269475237ad16d6681bcd6dd2d21a3fe2456aaced92aafe5e2e5164a635b8a216b29f555e', 4, 1, NULL, '[]', 0, '2019-03-10 23:36:55', '2019-03-10 23:36:55', '2020-03-11 07:36:55'),
('1db0b5b413a0c2d8a02a5467c70ebaa2927e2fc1ef314bf9ec711c2a9ac788b31c69f5ce4b76b9b3', 4, 1, 'MyApp', '[]', 0, '2019-03-11 01:26:26', '2019-03-11 01:26:26', '2020-03-11 09:26:26'),
('28d8dcd5dda51040ae976d274d9c7d568cc600a568d74c6aa5fc1871ee27f63e55a30884fe5d5347', 4, 1, 'MyApp', '[]', 0, '2019-03-11 00:27:32', '2019-03-11 00:27:32', '2020-03-11 08:27:32'),
('2ac6826709f1d1920e206a910990790fca69683b377c72540d32668cb8a97833992a54f020f4a65e', 4, 1, 'MyApp', '[]', 0, '2019-03-05 19:26:39', '2019-03-05 19:26:39', '2020-03-06 03:26:39'),
('3d7ad62c2ea8c6c961e355f2ba962bfbc75a56f04d968760198c44c69c955ff2c768e05cd66aaf01', 4, 1, 'MyApp', '[]', 0, '2019-03-11 00:12:50', '2019-03-11 00:12:50', '2020-03-11 08:12:50'),
('41a070003026e0463e042e0b2d50434aa63bad8c4ee8ff13dd7fbf7af99a2dc018efb36c252879ea', 4, 1, 'MyApp', '[]', 0, '2019-03-11 00:41:37', '2019-03-11 00:41:37', '2020-03-11 08:41:37'),
('4331b8a97670dcaab92e4e9f2892a9e7694340057d30be4068b0207ea615ddc30df98221c79ae589', 4, 1, 'MyApp', '[]', 0, '2019-03-11 00:51:33', '2019-03-11 00:51:33', '2020-03-11 08:51:33'),
('44ec26d89961af77265f6cb86a244cbb3cbc986fc737c1909c1c69619290274f265cb8c6a9ccefd4', 4, 1, 'MyApp', '[]', 0, '2019-03-11 00:47:14', '2019-03-11 00:47:14', '2020-03-11 08:47:14'),
('461b67a2ec14c5199090c4a46270ba2257bda495c047e4f91b84bf4de8a841bbd5f98fafa8a071ea', 4, 1, 'MyApp', '[]', 0, '2019-03-11 00:17:02', '2019-03-11 00:17:02', '2020-03-11 08:17:02'),
('5622f7761b7a38d97e83207b27574fcf48c2c4d4e22b1c08ff2e672fc9e94663bfb8425e143ecfa0', 4, 1, 'MyApp', '[]', 0, '2019-03-11 00:10:47', '2019-03-11 00:10:47', '2020-03-11 08:10:47'),
('5a2655ae214b4fe515e1fcbfa9902639455dce5e97257df96afb74078adbd178c6fc5dd100c021a5', 4, 1, 'MyApp', '[]', 0, '2019-03-10 23:54:31', '2019-03-10 23:54:31', '2020-03-11 07:54:31'),
('5d8fc7a6e37c4b1bf66a8a2a30133ffcb66c9ccabe2a93aefc867b40ac8d65ae80387da4fc676084', 4, 1, 'MyApp', '[]', 0, '2019-03-11 00:29:06', '2019-03-11 00:29:06', '2020-03-11 08:29:06'),
('5dabec244d64604e7b0cdba1a3f5f26714bb60bd3108908bb14153f8527bf862e18ba80b72af3940', 4, 1, NULL, '[]', 0, '2019-03-11 01:22:04', '2019-03-11 01:22:04', '2020-03-11 09:22:04'),
('5fa2bef13aff7e4497d8edd7990067ff5220674472ca9fbc128f95eb9a4240cd792162585ac5d366', 4, 1, NULL, '[]', 0, '2019-03-10 23:36:33', '2019-03-10 23:36:33', '2020-03-11 07:36:33'),
('60cc9bc2c7072995faaa4e1d90f85da5bc37052a87b0c893731842ba7d3d1ea578d9524075475d38', 4, 1, 'MyApp', '[]', 0, '2019-03-11 00:03:23', '2019-03-11 00:03:23', '2020-03-11 08:03:23'),
('61619358da5ef81ed49e8e8bac80943720603783a25dc27a5cb7bd74aba6458c8a619b3bc242742e', 4, 1, NULL, '[]', 0, '2019-03-10 23:40:58', '2019-03-10 23:40:58', '2020-03-11 07:40:58'),
('61a8291db6b8d9c8e542d501057e2d7fc6b802a0cbddf445c5401dff6a5bf679f7f414f391e30e31', 4, 1, 'MyApp', '[]', 0, '2019-03-05 19:22:13', '2019-03-05 19:22:13', '2020-03-06 03:22:13'),
('67445c07136f5d42b0c010e31ac43e38d2c09051c8754c2947cbebf22450d6a6b5e8a89a120b280c', 4, 1, 'MyApp', '[]', 0, '2019-03-11 01:59:07', '2019-03-11 01:59:07', '2020-03-11 09:59:07'),
('716879535a2a1cb59dfb6eab7c13d35c5257e20bb3286bff4e353c2ecfec7c5c09ddef9cd17db925', 4, 1, 'MyApp', '[]', 0, '2019-03-11 00:22:22', '2019-03-11 00:22:22', '2020-03-11 08:22:22'),
('76316c6883851ad8a852aac32f890577ea985ca6f523737cdc1cc26f71ed122f81ebd61aed6c3eff', 4, 1, 'MyApp', '[]', 0, '2019-03-11 01:24:41', '2019-03-11 01:24:41', '2020-03-11 09:24:41'),
('79ee0c7f2e984e0c64acffeefdcd3a60f9d13f71e4945ad43f720298bc961fd40ee74404310837e8', 4, 1, NULL, '[]', 0, '2019-03-11 01:37:12', '2019-03-11 01:37:12', '2020-03-11 09:37:12'),
('8607670acc12320bca445c7a76c07f7674724e6cb79e0d8d2310e4e21e926163af150ba253f2b8af', 4, 1, 'MyApp', '[]', 0, '2019-03-11 01:24:49', '2019-03-11 01:24:49', '2020-03-11 09:24:49'),
('90402c9455a4bbbd0cae95f2d93e4f66b8a63d4872aeb6542c27d694ff0a84e9de656a749ca5efb4', 4, 1, 'MyApp', '[]', 0, '2019-03-10 23:54:00', '2019-03-10 23:54:00', '2020-03-11 07:54:00'),
('988d226f3dfb67397d642804ae3a954708ee8e3ee34f3738bb50e0086668068f3118eda53263f714', 4, 1, NULL, '[]', 0, '2019-03-10 23:47:51', '2019-03-10 23:47:51', '2020-03-11 07:47:51'),
('a403585ddde84796b6e8244424dd83928c077b3b30d96f6d2f93536a18461bdfee77980d2f016ce2', 4, 1, 'MyApp', '[]', 0, '2019-03-10 23:58:52', '2019-03-10 23:58:52', '2020-03-11 07:58:52'),
('a6a559b18440c2565573f66cb8084debbae8915d8300175ddc83007125d7728e10f3bc8475949cf0', 4, 1, 'MyApp', '[]', 0, '2019-03-11 00:19:35', '2019-03-11 00:19:35', '2020-03-11 08:19:35'),
('b133651e9695b1593dba5490b18d62b094502bab61ae77ea26e30a272b0d8c17ffc27859a34b287c', 4, 1, NULL, '[]', 0, '2019-03-11 01:40:35', '2019-03-11 01:40:35', '2020-03-11 09:40:35'),
('b17d6bb9585bf187ef81a96c36798af3ca97690abc70b16132f9aabfe66adbb0fc8ba28fa3334d6c', 4, 1, 'MyApp', '[]', 0, '2019-03-11 00:42:43', '2019-03-11 00:42:43', '2020-03-11 08:42:43'),
('b3b21126ae4498ed3f2fdeba151646b2655f168ac9376c97b7ab2d347118bb9439b6efd745712101', 4, 1, 'MyApp', '[]', 0, '2019-03-05 20:50:00', '2019-03-05 20:50:00', '2020-03-06 04:50:00'),
('b4ee0412843d01b5bc2e173004b4c2339d701a5f1b34deadf5489742d5a357dfeebb6f8e842cd5d1', 4, 1, 'MyApp', '[]', 0, '2019-03-11 00:11:45', '2019-03-11 00:11:45', '2020-03-11 08:11:45'),
('b57c0c4a949cd675b5b00898b88b51a658e571f924a086e5a8a9eb176d30fc7b6a92da28f1f6c5ec', 4, 1, 'MyApp', '[]', 0, '2019-03-11 01:13:15', '2019-03-11 01:13:15', '2020-03-11 09:13:15'),
('ba673f8f93c96990cbd08cb5661561be82f64c9453aaaf673dee6e1385055479bbc46fb4b2ab6457', 4, 1, 'MyApp', '[]', 0, '2019-03-11 00:25:35', '2019-03-11 00:25:35', '2020-03-11 08:25:35'),
('bf325db8b141e3ddcdc43af2f8da15b26dce71edd72bdf001b3b1bf1a5da2bb6c1b4a00aa59f9349', 4, 1, 'MyApp', '[]', 0, '2019-03-05 20:54:04', '2019-03-05 20:54:04', '2020-03-06 04:54:04'),
('bff45a5b1c19c9a8ad0cd38e30159c34f63544372b902833a0a73e8de0552206bf2b7e4fe4303f6d', 4, 1, NULL, '[]', 0, '2019-03-11 01:37:36', '2019-03-11 01:37:36', '2020-03-11 09:37:36'),
('c70ed7c75f133857af987fa683ed009e46db30c981103df1981c765e7d23349111d43f8b91242294', 8, 1, 'MyApp', '[]', 0, '2019-03-05 19:26:19', '2019-03-05 19:26:19', '2020-03-06 03:26:19'),
('d436eb1503fd2a84f9a23ab80daf54e28e642080c1b0ed37939032f8baba3126a431b7273f56f178', 4, 1, 'MyApp', '[]', 0, '2019-03-11 00:49:02', '2019-03-11 00:49:02', '2020-03-11 08:49:02'),
('d49572d531d2dee1d2e69c51f20e956067250558597cd29566cd3ce42c7a6ae87e8b4676fe591351', 4, 1, 'MyApp', '[]', 0, '2019-03-10 23:40:10', '2019-03-10 23:40:10', '2020-03-11 07:40:10'),
('deb19c66caae955393816bd070a935c1f48e41d164a0460cc161a687b2fdad81d24acf10b2558634', 4, 1, NULL, '[]', 0, '2019-03-11 00:51:09', '2019-03-11 00:51:09', '2020-03-11 08:51:09'),
('ed06886ed1a0a294a38d11f876de0127c3dfa94d1d100c1171ed03d72d6b8a91edff1386e3f35c8e', 4, 1, 'MyApp', '[]', 0, '2019-03-11 00:42:13', '2019-03-11 00:42:13', '2020-03-11 08:42:13'),
('f400caaaa38cdfd1093395a6f4cda0ac0013570e31c8a23286dcbcb0b32bf79e2ecfee7cdbe7c98f', 4, 1, 'MyApp', '[]', 0, '2019-03-11 00:29:39', '2019-03-11 00:29:39', '2020-03-11 08:29:39'),
('f60410e6c3dca4db5b9bb6167fbd2c5a8beb80ed37496f89740444fc28bc8cdc082eae540771da7f', 1, 1, 'MyApp', '[]', 0, '2019-03-11 02:27:11', '2019-03-11 02:27:11', '2020-03-11 10:27:11');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) NOT NULL,
  `secret` varchar(100) NOT NULL,
  `redirect` text NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'veOvmNkcdWI1Bug3tMbQYKYOXBXaf995cvyjb4G9', 'http://localhost', 1, 0, 0, '2019-03-10 23:11:36', '2019-03-10 23:11:36'),
(2, NULL, 'Laravel Password Grant Client', 'CTNX1POvBcqzoKJvQSC0eeJf8Trl3tlj7nlL1QGL', 'http://localhost', 0, 1, 0, '2019-03-10 23:11:36', '2019-03-10 23:11:36');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-03-10 23:11:36', '2019-03-10 23:11:36');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) NOT NULL,
  `access_token_id` varchar(100) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) NOT NULL,
  `token` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `profile_image`
--

CREATE TABLE `profile_image` (
  `profile_image_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `image` text NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  `is_profile` smallint(5) UNSIGNED NOT NULL,
  `status` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile_image`
--

INSERT INTO `profile_image` (`profile_image_id`, `user_id`, `image`, `created_at`, `deleted_at`, `is_profile`, `status`) VALUES
(2, 4, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL),
(3, 4, 'member1.png', '2019-02-06 08:00:00', '0000-00-00 00:00:00', 1, NULL),
(4, 4, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reminders`
--

CREATE TABLE `reminders` (
  `id` int(11) UNSIGNED NOT NULL,
  `reminder_name` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reminder_members`
--

CREATE TABLE `reminder_members` (
  `id` int(11) UNSIGNED NOT NULL,
  `reminder_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `start_time` time NOT NULL,
  `repeat_every` varchar(200) DEFAULT NULL,
  `next_repeat` int(11) UNSIGNED NOT NULL,
  `recurrence` int(10) DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `storage`
--

CREATE TABLE `storage` (
  `storage_id` bigint(11) UNSIGNED NOT NULL,
  `storage` text NOT NULL,
  `user_id` bigint(11) UNSIGNED NOT NULL COMMENT 'id  for those who paid',
  `original_user` bigint(11) UNSIGNED NOT NULL,
  `type` char(5) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` char(5) NOT NULL DEFAULT 'A',
  `storage_type` char(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `storage`
--

INSERT INTO `storage` (`storage_id`, `storage`, `user_id`, `original_user`, `type`, `date_added`, `status`, `storage_type`) VALUES
(21, '4', 5, 5, 'TD', '2019-04-10 01:43:09', 'A', NULL),
(22, '237388', 5, 5, 'T', '2019-04-10 07:03:01', 'A', NULL),
(23, '463114', 5, 5, 'T', '2019-04-10 07:27:30', 'A', NULL),
(24, '607116', 5, 5, 'T', '2019-04-15 00:54:21', 'A', NULL),
(25, '719', 5, 5, 'T', '2019-04-15 00:57:54', 'A', NULL),
(26, '157705', 5, 5, 'T', '2019-04-15 01:01:34', 'A', NULL),
(27, '49', 5, 5, 'T', '2019-04-15 02:34:35', 'A', NULL),
(28, '187918', 5, 5, 'T', '2019-04-15 02:35:25', 'A', NULL),
(29, '245852', 5, 5, 'T', '2019-04-15 02:38:20', 'A', NULL),
(30, '44121', 5, 5, 'T', '2019-04-15 02:39:18', 'A', NULL),
(31, '30930', 5, 5, 'T', '2019-04-15 02:46:17', 'A', NULL),
(32, '44138', 5, 5, 'T', '2019-04-15 02:49:06', 'A', NULL),
(33, '157035', 5, 5, 'T', '2019-04-15 02:50:08', 'A', NULL),
(34, '44138', 5, 5, 'T', '2019-04-15 02:52:43', 'A', NULL),
(35, '6119', 5, 5, 'T', '2019-04-15 02:54:45', 'A', NULL),
(36, '449433', 5, 6, 'C', '2019-04-17 01:55:34', 'A', NULL),
(37, '6', 5, 5, 'TD', '2019-04-29 06:17:19', 'A', NULL),
(38, '6', 5, 5, 'TD', '2019-04-29 06:17:20', 'A', NULL),
(39, '6', 5, 5, 'TD', '2019-04-29 06:37:34', 'A', NULL),
(40, '6', 5, 6, 'TD', '2019-04-30 02:14:32', 'A', NULL),
(41, '632157', 5, 6, 'C', '2019-04-30 02:31:48', 'A', NULL),
(42, '23', 5, 5, 'C', '2019-04-30 02:32:17', 'A', NULL),
(43, '157030', 1, 1, 'T', '2019-04-30 03:24:08', 'A', NULL),
(44, '157018', 1, 1, 'T', '2019-05-01 04:17:59', 'A', NULL),
(45, '449902', 1, 1, 'T', '2019-05-01 04:21:03', 'A', NULL),
(46, '56030', 1, 1, 'T', '2019-05-01 04:22:18', 'A', NULL),
(47, '216343', 1, 1, 'T', '2019-05-01 04:22:50', 'A', NULL),
(48, '44808', 1, 1, 'T', '2019-05-01 04:23:25', 'A', NULL),
(49, '55330', 2, 2, 'C', '2019-05-01 04:35:38', 'A', NULL),
(50, '55322', 2, 2, 'C', '2019-05-01 04:38:45', 'A', NULL),
(51, '418549', 1, 1, 'C', '2019-05-01 04:39:39', 'A', NULL),
(52, '55324', 1, 1, 'C', '2019-05-01 04:42:48', 'A', NULL),
(53, '26350', 2, 2, 'C', '2019-05-01 04:46:46', 'A', NULL),
(54, '11', 2, 2, 'C', '2019-05-01 05:34:30', 'A', NULL),
(55, '216098', 1, 1, 'T', '2019-05-01 05:51:47', 'A', NULL),
(56, '11', 1, 1, 'TD', '2019-05-01 06:50:56', 'A', NULL),
(57, '11', 1, 1, 'TD', '2019-05-01 06:50:58', 'A', NULL),
(58, '11', 1, 1, 'TD', '2019-05-01 06:51:04', 'A', NULL),
(59, '11', 1, 1, 'TD', '2019-05-01 06:51:05', 'A', NULL),
(60, '11', 1, 1, 'TD', '2019-05-01 06:51:05', 'A', NULL),
(61, '11', 1, 1, 'TD', '2019-05-01 06:51:05', 'A', NULL),
(62, '11', 1, 1, 'TD', '2019-05-01 06:51:06', 'A', NULL),
(63, '11', 1, 1, 'TD', '2019-05-01 06:51:06', 'A', NULL),
(64, '11', 1, 1, 'TD', '2019-05-01 06:51:06', 'A', NULL),
(65, '11', 1, 1, 'TD', '2019-05-01 06:51:06', 'A', NULL),
(66, '11', 1, 1, 'TD', '2019-05-01 06:51:06', 'A', NULL),
(67, '11', 1, 1, 'TD', '2019-05-01 06:51:07', 'A', NULL),
(68, '11', 1, 1, 'TD', '2019-05-01 06:51:07', 'A', NULL),
(69, '4', 1, 1, 'TD', '2019-05-01 07:08:19', 'A', NULL),
(70, '4', 1, 1, 'TD', '2019-05-01 07:08:20', 'A', NULL),
(71, '4', 1, 1, 'TD', '2019-05-01 07:08:21', 'A', NULL),
(72, '4', 1, 1, 'TD', '2019-05-01 07:08:21', 'A', NULL),
(73, '4', 1, 1, 'TD', '2019-05-01 07:08:23', 'A', NULL),
(74, '4', 1, 1, 'TD', '2019-05-01 07:08:23', 'A', NULL),
(75, '4', 1, 1, 'TD', '2019-05-01 07:08:23', 'A', NULL),
(76, '4', 1, 1, 'TD', '2019-05-02 03:10:57', 'A', NULL),
(77, '4', 1, 1, 'TD', '2019-05-02 03:11:02', 'A', NULL),
(78, '4', 1, 1, 'TD', '2019-05-02 03:11:02', 'A', NULL),
(79, '4', 1, 1, 'TD', '2019-05-02 03:11:02', 'A', NULL),
(80, '4', 1, 1, 'TD', '2019-05-02 03:11:02', 'A', NULL),
(81, '4', 1, 1, 'TD', '2019-05-02 03:11:02', 'A', NULL),
(82, '4', 1, 1, 'TD', '2019-05-02 03:11:02', 'A', NULL),
(83, '4', 1, 1, 'TD', '2019-05-02 03:15:44', 'A', NULL),
(84, '4', 1, 1, 'TD', '2019-05-02 03:15:45', 'A', NULL),
(85, '7', 1, 1, 'TD', '2019-05-02 06:18:34', 'A', NULL),
(86, '2760', 1, 1, 'TD', '2019-05-02 07:26:38', 'A', NULL),
(87, '77883', 1, 1, 'T', '2019-05-02 08:40:01', 'A', NULL),
(88, '7', 1, 1, 'TD', '2019-05-03 00:40:06', 'A', NULL),
(89, '17', 2, 2, 'C', '2019-05-03 07:57:47', 'A', NULL),
(90, '6', 1, 1, 'TD', '2019-05-07 03:38:34', 'A', NULL),
(91, '1326956', 1, 1, 'T', '2019-05-17 02:28:43', 'A', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE `subscription` (
  `subscription_id` int(11) UNSIGNED NOT NULL,
  `subscription_name` varchar(150) NOT NULL,
  `subscription_amount` decimal(11,2) NOT NULL,
  `price` decimal(11,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subscription`
--

INSERT INTO `subscription` (`subscription_id`, `subscription_name`, `subscription_amount`, `price`) VALUES
(1, 'Free 25 MB', '25.00', '0.00'),
(2, '50 MB', '50.00', '9.99'),
(3, '100 MB', '100.00', '19.99'),
(4, '150 MB', '150.00', '29.99');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `task_id` int(10) UNSIGNED NOT NULL,
  `task_unique` text,
  `title` varchar(191) NOT NULL,
  `category_id` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `task_percentage` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `deadline` date NOT NULL,
  `date_finished` datetime DEFAULT NULL,
  `task_status` char(191) NOT NULL DEFAULT 'N',
  `status` char(191) NOT NULL DEFAULT 'A',
  `type` varchar(2) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`task_id`, `task_unique`, `title`, `category_id`, `task_percentage`, `description`, `created_by`, `deadline`, `date_finished`, `task_status`, `status`, `type`, `created_at`, `updated_at`) VALUES
(1, '$2y$10$6iaIXHOuTY1ZT/k1VMfNIOUnEfzhMBwelkt.gxHEL1se8EYx8N9wm', 'Test Task 1', 1, 100, '<p>test description</p>', 1, '2019-04-06', NULL, 'N', 'A', 'B', '2019-04-30 03:24:08', NULL),
(2, '$2y$10$3BZKPfL.DZgOv1lW4wSGSuEfR9zA.jsneG6Y11AGAABF6zkotn/Zq', 'Test Task 2', 1, 100, '<p>test</p>', 1, '2019-05-02', NULL, 'N', 'A', 'B', '2019-05-01 04:17:59', NULL),
(3, '$2y$10$41Qag6/lVA/HD1khKfGP1OqqxKJlBKQNmfC7bMG67r6wYxA3p27XC', 'Test Task 3', 1, 100, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p><br></p><p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 1, '2019-05-02', NULL, 'I', 'A', 'B', '2019-05-01 04:21:03', NULL),
(4, '$2y$10$9DaEIPE2BqjNvk8J59HuPuxAH7qeNC/OeArQID4AyD/FnvWIZ5nda', 'Test Task 4', 1, 100, '<p style=\"overflow-wrap: break-word; color: rgb(94, 94, 94); font-size: 13px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p style=\"overflow-wrap: break-word; color: rgb(94, 94, 94); font-size: 13px;\"><br></p><p style=\"overflow-wrap: break-word; color: rgb(94, 94, 94); font-size: 13px;\">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 1, '2019-05-09', NULL, 'I', 'A', 'B', '2019-05-01 04:22:18', NULL),
(5, '$2y$10$g3vxfuuw9WzmmzevCbKmEeJ5nn0csfRw0Hd.4Ajzeq7mtuEXch072', 'Test Task 5', 1, 100, '<p style=\"overflow-wrap: break-word; color: rgb(94, 94, 94); font-size: 13px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p style=\"overflow-wrap: break-word; color: rgb(94, 94, 94); font-size: 13px;\"><br></p><p style=\"overflow-wrap: break-word; color: rgb(94, 94, 94); font-size: 13px;\">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 1, '2019-05-02', NULL, 'I', 'A', 'B', '2019-05-01 04:22:50', NULL),
(6, '$2y$10$CoX6/MA/ZTC5776j6fIFquIWOOa546e5fwueyG1RCGMT5J.onamMy', 'Test Task 6', 1, 100, '<p style=\"overflow-wrap: break-word; color: rgb(94, 94, 94); font-size: 13px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p style=\"overflow-wrap: break-word; color: rgb(94, 94, 94); font-size: 13px;\"><br></p><p style=\"overflow-wrap: break-word; color: rgb(94, 94, 94); font-size: 13px;\">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 1, '2019-05-02', NULL, 'I', 'A', 'B', '2019-05-01 04:23:26', NULL),
(7, '$2y$10$aUcWBi4.o.mIl0mGFBYT5ed7zmSjrsk2.WV6FaFmMXsNemfmyZ5f6', 'Test Task 7', 1, 100, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 1, '2019-05-02', NULL, 'N', 'A', 'B', '2019-05-01 05:51:47', '2019-04-30 21:54:00'),
(8, '$2y$10$KMn0xAWJ0a9EVOqH8W41HO2WTR1pgt1Ic427//xAgwt/JwCRlUDfK', 'Test Task', 1, 100, '<p>test xls</p>', 1, '2019-05-10', NULL, 'D', 'A', 'B', '2019-05-02 08:40:01', NULL),
(9, '$2y$10$EjP5jJbZj6I7euefLtCb2O.Jm/VAjC.t0hH0U2uy13VwzZNbb.oCO', 'Test Task 8', 1, 100, '<p>Test Task Description for Test Task 8&nbsp;</p>', 1, '2019-05-16', NULL, 'N', 'A', 'P', '2019-05-22 01:36:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tasks_attachment`
--

CREATE TABLE `tasks_attachment` (
  `tasks_attachment_id` int(10) UNSIGNED NOT NULL,
  `image` text NOT NULL,
  `status` int(1) UNSIGNED NOT NULL DEFAULT '1',
  `full_path` text NOT NULL,
  `file_type` char(191) NOT NULL,
  `task_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tasks_attachment`
--

INSERT INTO `tasks_attachment` (`tasks_attachment_id`, `image`, `status`, `full_path`, `file_type`, `task_id`, `created_at`, `updated_at`) VALUES
(1, '^6F0AFEA1CE6B988BCE1F8C2113DA05DC2B489D83B44067FD85^pimgpsh_fullsize_distr64549.png', 1, 'E:\\xampp\\htdocs\\trackatask_new\\public/task_attachment/^6F0AFEA1CE6B988BCE1F8C2113DA05DC2B489D83B44067FD85^pimgpsh_fullsize_distr64549.png', 'png', 1, NULL, NULL),
(2, '^6F0AFEA1CE6B988BCE1F8C2113DA05DC2B489D83B44067FD85^pimgpsh_fullsize_distr8427.png', 1, 'E:\\xampp\\htdocs\\trackatask_new\\public/task_attachment/^6F0AFEA1CE6B988BCE1F8C2113DA05DC2B489D83B44067FD85^pimgpsh_fullsize_distr8427.png', 'png', 2, NULL, NULL),
(3, '4-049077.png', 1, 'E:\\xampp\\htdocs\\trackatask_new\\public/task_attachment/4-049077.png', 'png', 3, NULL, NULL),
(4, 'careers mobile view17463.pdf', 1, 'E:\\xampp\\htdocs\\trackatask_new\\public/task_attachment/careers mobile view17463.pdf', 'pdf', 3, NULL, NULL),
(5, 'il_570xN21201.1293932027_1gpu', 1, 'E:\\xampp\\htdocs\\trackatask_new\\public/task_attachment/il_570xN21201.1293932027_1gpu', '1293932027_1gpu', 4, NULL, NULL),
(6, 'updated-layouts-mobile30307.png', 1, 'E:\\xampp\\htdocs\\trackatask_new\\public/task_attachment/updated-layouts-mobile30307.png', 'png', 5, NULL, NULL),
(7, '1482761_826496600721969_8023681616453096906_n6029.jpg', 1, 'E:\\xampp\\htdocs\\trackatask_new\\public/task_attachment/1482761_826496600721969_8023681616453096906_n6029.jpg', 'jpg', 6, NULL, NULL),
(8, 'updated-layouts-mobile20757.png', 1, 'E:\\xampp\\htdocs\\trackatask_new\\public/task_attachment/updated-layouts-mobile20757.png', 'png', 7, NULL, NULL),
(9, '1556689990_5cc93446499ef_1482761_826496600721969_8023681616453096906_n.jpg', 1, 'E:\\xampp\\htdocs\\trackatask_new\\public/task_attachment/1556689990_5cc93446499ef_1482761_826496600721969_8023681616453096906_n.jpg', 'jpg', 7, '2019-04-30 21:53:10', '2019-04-30 21:53:10'),
(10, 'PH-NO_1-CI&PL-2019-4-16_xls_155550700153493.xls', 1, 'E:\\xampp\\htdocs\\trackatask_new\\public/task_attachment/PH-NO_1-CI&PL-2019-4-16_xls_155550700153493.xls', 'xls', 8, NULL, NULL),
(11, '4-0563239.png', 1, 'E:\\xampp\\htdocs\\trackatask_new\\public/task_attachment/4-0563239.png', 'png', 9, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tasks_reply_attachment`
--

CREATE TABLE `tasks_reply_attachment` (
  `tasks_reply_attachment_id` int(10) UNSIGNED NOT NULL,
  `task_comments_id` int(10) UNSIGNED NOT NULL,
  `image` text NOT NULL,
  `full_path` text NOT NULL,
  `file_type` char(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tasks_reply_attachment`
--

INSERT INTO `tasks_reply_attachment` (`tasks_reply_attachment_id`, `task_comments_id`, `image`, `full_path`, `file_type`, `created_at`, `updated_at`) VALUES
(1, 1, 'il_570xN26409.1293932027_1gpu', 'E:\\xampp\\htdocs\\trackatask_new\\public/task_reply_attachment/il_570xN26409.1293932027_1gpu', '1293932027_1gpu', NULL, NULL),
(2, 2, 'il_570xN33391.1293932027_1gpu', 'E:\\xampp\\htdocs\\trackatask_new\\public/task_reply_attachment/il_570xN33391.1293932027_1gpu', '1293932027_1gpu', NULL, NULL),
(3, 3, '^BCBF97E72E09A3871FC15B4CCB0B4EDF6E75D906065CC9489E^pimgpsh_fullsize_distr6799.png', 'E:\\xampp\\htdocs\\trackatask_new\\public/task_reply_attachment/^BCBF97E72E09A3871FC15B4CCB0B4EDF6E75D906065CC9489E^pimgpsh_fullsize_distr6799.png', 'png', NULL, NULL),
(4, 3, 'careers mobile view26185.pdf', 'E:\\xampp\\htdocs\\trackatask_new\\public/task_reply_attachment/careers mobile view26185.pdf', 'pdf', NULL, NULL),
(5, 4, 'il_570xN56439.1293932027_1gpu', 'E:\\xampp\\htdocs\\trackatask_new\\public/task_reply_attachment/il_570xN56439.1293932027_1gpu', '1293932027_1gpu', NULL, NULL),
(6, 5, '4-0520603.png', 'E:\\xampp\\htdocs\\trackatask_new\\public/task_reply_attachment/4-0520603.png', 'png', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `task_assign`
--

CREATE TABLE `task_assign` (
  `task_assign_id` int(10) UNSIGNED NOT NULL,
  `task_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `is_created` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `task_assign`
--

INSERT INTO `task_assign` (`task_assign_id`, `task_id`, `user_id`, `is_created`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, NULL, NULL),
(2, 1, 2, 0, NULL, NULL),
(3, 2, 1, 1, NULL, NULL),
(4, 2, 2, 0, NULL, NULL),
(5, 3, 1, 1, NULL, NULL),
(6, 3, 2, 0, NULL, NULL),
(7, 4, 1, 1, NULL, NULL),
(8, 4, 2, 0, NULL, NULL),
(9, 5, 1, 1, NULL, NULL),
(10, 5, 2, 0, NULL, NULL),
(11, 6, 1, 1, NULL, NULL),
(12, 6, 2, 0, NULL, NULL),
(13, 7, 1, 1, NULL, NULL),
(14, 7, 2, 0, NULL, NULL),
(15, 8, 1, 1, NULL, NULL),
(16, 8, 2, 0, NULL, NULL),
(17, 9, 1, 1, NULL, NULL),
(18, 9, 1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `task_categories`
--

CREATE TABLE `task_categories` (
  `category_id` int(11) UNSIGNED NOT NULL,
  `category_unique` text NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `status` smallint(3) NOT NULL DEFAULT '1' COMMENT '1 = active 0 = inactive',
  `created_by` int(11) UNSIGNED NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `task_categories`
--

INSERT INTO `task_categories` (`category_id`, `category_unique`, `category_name`, `status`, `created_by`, `date_created`) VALUES
(1, 'no_title', 'No Title', 1, 1, '2019-04-10 06:34:25'),
(4, 'task_banana', 'Task Banana', 1, 1, '2019-04-10 07:03:02'),
(5, 'trackerteer', 'Trackerteer', 1, 1, '2019-04-15 00:57:54'),
(6, '$2y$10$Igk2Qd8tQw8rcmUg3DDfFenX6iDEDm.VjvOw1YTMWfygPJ.JoPRgK', 'Rocky Superdog', 1, 1, '2019-05-16 04:07:27');

-- --------------------------------------------------------

--
-- Table structure for table `task_comments`
--

CREATE TABLE `task_comments` (
  `task_comments_id` int(11) NOT NULL,
  `comment` text,
  `task_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `seen` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `seen_date` datetime DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `action_type` char(5) DEFAULT NULL,
  `system_generated` smallint(4) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `task_comments`
--

INSERT INTO `task_comments` (`task_comments_id`, `comment`, `task_id`, `user_id`, `seen`, `seen_date`, `date_added`, `action_type`, `system_generated`) VALUES
(1, '<p>test reply 1</p>', 6, 2, 0, NULL, '2019-05-01 04:35:38', 'C', 0),
(2, '<p>okay</p>', 5, 2, 0, NULL, '2019-05-01 04:38:45', 'C', 0),
(3, '<p>test reply 2</p>', 6, 1, 0, NULL, '2019-05-01 04:39:39', 'C', 0),
(4, '<p>test 2</p>', 5, 1, 0, NULL, '2019-05-01 04:42:48', 'C', 0),
(5, '<p>test</p>', 4, 2, 0, NULL, '2019-05-01 04:46:46', 'C', 0),
(6, '<p>test</p>', 3, 2, 0, NULL, '2019-05-01 05:34:30', 'C', 0),
(7, '<p>test reply</p>', 8, 2, 0, NULL, '2019-05-03 07:57:47', 'C', 0);

-- --------------------------------------------------------

--
-- Table structure for table `task_logs`
--

CREATE TABLE `task_logs` (
  `task_logs_id` bigint(11) UNSIGNED NOT NULL,
  `task_id` bigint(11) UNSIGNED NOT NULL,
  `action` varchar(200) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `task_logs`
--

INSERT INTO `task_logs` (`task_logs_id`, `task_id`, `action`, `user_id`, `date_created`) VALUES
(1, 8, 'task_complete', 1, '2019-05-03 07:57:59');

-- --------------------------------------------------------

--
-- Table structure for table `task_notification`
--

CREATE TABLE `task_notification` (
  `task_notification_id` int(11) NOT NULL,
  `comment` text,
  `task_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `notif_by` int(10) UNSIGNED NOT NULL,
  `seen` smallint(4) UNSIGNED NOT NULL DEFAULT '0',
  `seen_date` datetime DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `action_type` char(5) DEFAULT NULL,
  `system_generated` smallint(4) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `task_notification`
--

INSERT INTO `task_notification` (`task_notification_id`, `comment`, `task_id`, `user_id`, `notif_by`, `seen`, `seen_date`, `date_added`, `action_type`, `system_generated`) VALUES
(1, NULL, 1, 2, 1, 0, NULL, '2019-04-30 03:24:08', 'T', 0),
(2, NULL, 2, 2, 1, 0, NULL, '2019-05-01 04:18:00', 'T', 0),
(3, NULL, 3, 2, 1, 1, NULL, '2019-05-01 04:21:03', 'T', 0),
(4, NULL, 4, 2, 1, 1, NULL, '2019-05-01 04:22:18', 'T', 0),
(5, NULL, 5, 2, 1, 1, NULL, '2019-05-01 04:22:50', 'T', 0),
(6, NULL, 6, 2, 1, 1, NULL, '2019-05-01 04:23:26', 'T', 0),
(7, '&lt;p&gt;test reply 1&lt;/p&gt;', 6, 1, 2, 1, NULL, '2019-05-01 04:35:38', 'C', 0),
(8, '&lt;p&gt;okay&lt;/p&gt;', 5, 1, 2, 1, NULL, '2019-05-01 04:38:45', 'C', 0),
(9, '&lt;p&gt;test reply 2&lt;/p&gt;', 6, 2, 1, 0, NULL, '2019-05-01 04:39:39', 'C', 0),
(10, '&lt;p&gt;test 2&lt;/p&gt;', 5, 2, 1, 0, NULL, '2019-05-01 04:42:49', 'C', 0),
(11, '&lt;p&gt;test&lt;/p&gt;', 4, 1, 2, 1, NULL, '2019-05-01 04:46:46', 'C', 0),
(12, '&lt;p&gt;test&lt;/p&gt;', 3, 1, 2, 1, NULL, '2019-05-01 05:34:31', 'C', 0),
(13, NULL, 7, 2, 1, 0, NULL, '2019-05-01 05:51:47', 'T', 0),
(14, NULL, 8, 2, 1, 1, NULL, '2019-05-02 08:40:01', 'T', 0),
(15, '&lt;p&gt;test reply&lt;/p&gt;', 8, 1, 2, 1, NULL, '2019-05-03 07:57:47', 'C', 0),
(16, NULL, 9, 1, 1, 0, NULL, '2019-05-22 01:36:20', 'T', 0);

-- --------------------------------------------------------

--
-- Table structure for table `task_users`
--

CREATE TABLE `task_users` (
  `task_users_id` int(10) UNSIGNED NOT NULL,
  `parent_user_id` int(10) UNSIGNED NOT NULL,
  `child_user_id` int(10) UNSIGNED NOT NULL,
  `parent_team_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `task_users`
--

INSERT INTO `task_users` (`task_users_id`, `parent_user_id`, `child_user_id`, `parent_team_user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 0, '2019-04-30 03:22:42', NULL),
(2, 1, 1, 0, '2019-04-30 03:22:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `team_data`
--

CREATE TABLE `team_data` (
  `team_data_id` bigint(11) UNSIGNED NOT NULL,
  `team_info_id` bigint(11) UNSIGNED NOT NULL,
  `user_id` bigint(11) UNSIGNED NOT NULL,
  `original_creator` bigint(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `team_info`
--

CREATE TABLE `team_info` (
  `team_info_id` bigint(11) UNSIGNED NOT NULL,
  `team_name` text NOT NULL,
  `team_leader_id` bigint(11) UNSIGNED NOT NULL,
  `type` char(5) NOT NULL DEFAULT 'B' COMMENT 'B= Business , P = Personal',
  `status` char(4) NOT NULL DEFAULT 'A' COMMENT 'Active = A , Disabled = D',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `parent_id` bigint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `theme`
--

CREATE TABLE `theme` (
  `theme_id` int(11) NOT NULL,
  `color` varchar(190) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `theme_color`
--

CREATE TABLE `theme_color` (
  `theme_color_id` int(10) UNSIGNED NOT NULL,
  `color` varchar(190) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `theme_color`
--

INSERT INTO `theme_color` (`theme_color_id`, `color`, `created`) VALUES
(1, 'pink', '2019-02-08 06:02:19'),
(2, 'blue', '2019-02-08 06:02:19');

-- --------------------------------------------------------

--
-- Table structure for table `timezone`
--

CREATE TABLE `timezone` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `timezone` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `todo_list`
--

CREATE TABLE `todo_list` (
  `todo_id` int(11) UNSIGNED NOT NULL,
  `todo_name` text CHARACTER SET utf8mb4 NOT NULL,
  `user_id` int(100) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_finished` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0 = ongoing 1 = finished',
  `todo_status` char(5) CHARACTER SET utf8mb4 NOT NULL DEFAULT 'A' COMMENT 'R = remove A = active',
  `date_from` varchar(255) NOT NULL,
  `is_active` smallint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `todo_list`
--

INSERT INTO `todo_list` (`todo_id`, `todo_name`, `user_id`, `created_at`, `updated_at`, `date_finished`, `status`, `todo_status`, `date_from`, `is_active`) VALUES
(1, 'samp[le', 1, '2019-04-02 01:07:48', '2019-04-02 19:19:41', '2019-04-03 03:19:41', 0, 'R', '04/17/2019', 1),
(2, 'Reply (Card & Modal) Validation', 1, '2019-04-02 19:20:40', '2019-05-05 17:16:41', '2019-05-06 01:16:41', 1, 'A', '04/03/2019', 0),
(3, 'To Do (Empty input box and date after add)', 1, '2019-04-02 19:25:09', '2019-04-02 19:25:26', '2019-04-03 03:25:26', 0, 'R', '04/03/2019', 1),
(4, 'To Do (Empty input box and date after add)', 1, '2019-04-02 19:25:37', '2019-05-05 17:16:41', '2019-05-06 01:16:41', 1, 'A', '04/04/2019', 0),
(5, 'To Do > Date (check after add and when page refresh)', 1, '2019-04-02 19:26:30', '2019-05-05 17:16:41', '2019-05-06 01:16:41', 1, 'A', '04/04/2019', 0),
(6, 'Project List Tab', 1, '2019-04-02 19:27:10', '2019-05-05 17:16:41', '2019-05-06 01:16:41', 1, 'A', '04/08/2019', 0),
(7, 'Task, To Do and Profile Page (Mobile Layout)', 1, '2019-04-02 19:27:53', '2019-05-05 17:16:41', '2019-05-06 01:16:41', 1, 'A', '04/09/2019', 0),
(8, 'Profile Page (Native Image Uploader)', 1, '2019-04-02 19:29:57', '2019-04-30 22:33:36', '2019-05-01 06:33:36', 0, 'R', '04/10/2019', 1),
(9, 'Profile Page (Native Image Uploader)', 1, '2019-04-02 19:29:57', '2019-05-05 17:16:41', '2019-05-06 01:16:41', 1, 'A', '04/10/2019', 0),
(10, 'Alert Modal Layout (Sucess, Warning and Delete)', 1, '2019-04-02 19:32:40', '2019-04-30 22:33:34', '2019-05-01 06:33:34', 0, 'R', '04/11/2019', 1),
(11, 'Assigned Chip (Border Colors)', 1, '2019-04-02 19:33:58', '2019-04-30 22:33:31', '2019-05-01 06:33:31', 0, 'R', '04/11/2019', 1),
(12, 'Show Threads (Move to Tools)', 1, '2019-04-02 21:57:28', '2019-04-30 22:33:25', '2019-05-01 06:33:25', 0, 'R', '04/03/2019', 1),
(13, 'test', 5, '2019-04-09 17:43:09', '2019-04-28 22:37:04', '2019-04-29 06:37:04', 1, 'A', '04/11/2019', 0),
(14, 'test 2', 5, '2019-04-28 22:17:19', '2019-04-29 17:53:36', '0000-00-00 00:00:00', 0, 'A', '04/16/2019', 1),
(15, 'test 2', 5, '2019-04-28 22:17:20', '2019-04-28 22:17:40', '2019-04-29 06:17:40', 0, 'R', '04/16/2019', 1),
(16, 'test 3', 5, '2019-04-28 22:37:34', '2019-04-29 17:53:37', '0000-00-00 00:00:00', 0, 'A', '04/30/2019', 1),
(17, 'test 1', 6, '2019-04-29 18:14:32', '2019-04-29 18:29:51', '0000-00-00 00:00:00', 0, 'A', '05/01/2019', 1),
(18, 'Sample Todo', 1, '2019-04-30 22:50:56', '2019-04-30 23:08:03', '2019-05-01 07:08:03', 0, 'R', '05/02/2019', 1),
(19, 'Sample Todo', 1, '2019-04-30 22:50:58', '2019-04-30 23:08:02', '2019-05-01 07:08:02', 0, 'R', '05/02/2019', 1),
(20, 'Sample Todo', 1, '2019-04-30 22:51:04', '2019-04-30 23:08:01', '2019-05-01 07:08:01', 0, 'R', '05/02/2019', 1),
(21, 'Sample Todo', 1, '2019-04-30 22:51:05', '2019-04-30 23:07:58', '2019-05-01 07:07:58', 0, 'R', '05/02/2019', 1),
(22, 'Sample Todo', 1, '2019-04-30 22:51:05', '2019-04-30 23:07:59', '2019-05-01 07:07:59', 0, 'R', '05/02/2019', 1),
(23, 'Sample Todo', 1, '2019-04-30 22:51:05', '2019-04-30 23:07:59', '2019-05-01 07:07:59', 0, 'R', '05/02/2019', 1),
(24, 'Sample Todo', 1, '2019-04-30 22:51:06', '2019-04-30 23:07:56', '2019-05-01 07:07:56', 0, 'R', '05/02/2019', 1),
(25, 'Sample Todo', 1, '2019-04-30 22:51:06', '2019-04-30 23:07:56', '2019-05-01 07:07:56', 0, 'R', '05/02/2019', 1),
(26, 'Sample Todo', 1, '2019-04-30 22:51:06', '2019-04-30 23:07:57', '2019-05-01 07:07:57', 0, 'R', '05/02/2019', 1),
(27, 'Sample Todo', 1, '2019-04-30 22:51:06', '2019-04-30 22:51:25', '2019-05-01 06:51:25', 0, 'R', '05/02/2019', 1),
(28, 'Sample Todo', 1, '2019-04-30 22:51:06', '2019-04-30 22:51:27', '2019-05-01 06:51:27', 0, 'R', '05/02/2019', 1),
(29, 'Sample Todo', 1, '2019-04-30 22:51:07', '2019-04-30 23:07:53', '2019-05-01 07:07:53', 0, 'R', '05/02/2019', 1),
(30, 'Sample Todo', 1, '2019-04-30 22:51:07', '2019-04-30 22:51:12', '2019-05-01 06:51:12', 0, 'R', '05/02/2019', 1),
(31, 'test', 1, '2019-04-30 23:08:19', '2019-05-01 16:39:13', '2019-05-02 00:39:13', 0, 'R', '05/03/2019', 1),
(32, 'test', 1, '2019-04-30 23:08:20', '2019-05-01 16:39:12', '2019-05-02 00:39:12', 0, 'R', '05/03/2019', 1),
(33, 'test', 1, '2019-04-30 23:08:21', '2019-05-01 16:39:10', '2019-05-02 00:39:10', 0, 'R', '05/03/2019', 1),
(34, 'test', 1, '2019-04-30 23:08:21', '2019-05-01 16:39:11', '2019-05-02 00:39:11', 0, 'R', '05/03/2019', 1),
(35, 'test', 1, '2019-04-30 23:08:23', '2019-05-01 16:39:07', '2019-05-02 00:39:07', 0, 'R', '05/03/2019', 1),
(36, 'test', 1, '2019-04-30 23:08:23', '2019-05-01 16:39:08', '2019-05-02 00:39:08', 0, 'R', '05/03/2019', 1),
(37, 'test', 1, '2019-04-30 23:08:24', '2019-05-01 16:39:05', '2019-05-02 00:39:05', 0, 'R', '05/03/2019', 1),
(38, 'test', 1, '2019-05-01 19:10:58', '2019-05-20 23:05:14', '2019-05-21 07:05:14', 1, 'A', '05/03/2019', 1),
(39, 'test', 1, '2019-05-01 19:11:02', '2019-05-20 23:05:02', '0000-00-00 00:00:00', 0, 'A', '05/03/2019', 1),
(40, 'test', 1, '2019-05-01 19:11:02', '2019-05-20 23:05:02', '0000-00-00 00:00:00', 0, 'A', '05/03/2019', 1),
(41, 'test', 1, '2019-05-01 19:11:02', '2019-05-20 23:05:04', '0000-00-00 00:00:00', 0, 'A', '05/03/2019', 1),
(42, 'test', 1, '2019-05-01 19:11:02', '2019-05-20 23:05:05', '0000-00-00 00:00:00', 0, 'A', '05/03/2019', 1),
(43, 'test', 1, '2019-05-01 19:11:02', '2019-05-20 23:05:06', '0000-00-00 00:00:00', 0, 'A', '05/03/2019', 1),
(44, 'test', 1, '2019-05-01 19:11:03', '2019-05-20 23:05:01', '0000-00-00 00:00:00', 0, 'A', '05/03/2019', 1),
(45, 'test', 1, '2019-05-01 19:15:44', '2019-05-15 18:05:00', '0000-00-00 00:00:00', 0, 'A', '05/03/2019', 1),
(46, 'test', 1, '2019-05-01 19:15:45', '2019-05-15 18:05:01', '0000-00-00 00:00:00', 0, 'A', '05/03/2019', 1),
(47, 'asdasda', 1, '2019-05-01 22:18:35', '2019-05-15 18:05:02', '0000-00-00 00:00:00', 0, 'A', '01/01/1970', 1),
(48, 'SAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASKSAMPLE TASK SAMPLE TASK', 1, '2019-05-01 23:26:39', '2019-05-02 00:57:32', '2019-05-02 08:57:32', 0, 'R', '05/17/2019', 1),
(49, 'sdgfsgf', 1, '2019-05-02 16:40:06', '2019-05-20 23:05:10', '0000-00-00 00:00:00', 0, 'A', '05/04/2019', 1),
(50, 'Sample', 1, '2019-05-06 19:38:34', '2019-05-15 18:03:50', '2019-05-16 02:03:50', 0, 'R', '05/08/2019', 1);

-- --------------------------------------------------------

--
-- Table structure for table `todo_logs`
--

CREATE TABLE `todo_logs` (
  `id` int(255) UNSIGNED NOT NULL,
  `user_id` int(255) NOT NULL,
  `todo_list_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `todo_logs`
--

INSERT INTO `todo_logs` (`id`, `user_id`, `todo_list_id`) VALUES
(1, 1, 1),
(2, 1, 3),
(3, 5, 15),
(4, 1, 12),
(5, 1, 11),
(6, 1, 10),
(7, 1, 8),
(8, 1, 30),
(9, 1, 30),
(10, 1, 30),
(11, 1, 30),
(12, 1, 30),
(13, 1, 30),
(14, 1, 30),
(15, 1, 30),
(16, 1, 27),
(17, 1, 28),
(18, 1, 29),
(19, 1, 24),
(20, 1, 25),
(21, 1, 26),
(22, 1, 21),
(23, 1, 22),
(24, 1, 23),
(25, 1, 20),
(26, 1, 19),
(27, 1, 18),
(28, 1, 37),
(29, 1, 35),
(30, 1, 36),
(31, 1, 33),
(32, 1, 34),
(33, 1, 32),
(34, 1, 31),
(35, 1, 48),
(36, 1, 50);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_unique` text,
  `firstname` varchar(191) DEFAULT NULL,
  `lastname` varchar(191) DEFAULT NULL,
  `email` varchar(191) NOT NULL,
  `address` varchar(191) DEFAULT NULL,
  `city` varchar(191) DEFAULT NULL,
  `state` varchar(191) DEFAULT NULL,
  `zipcode` varchar(191) DEFAULT NULL,
  `country_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `account_type_id` tinyint(3) UNSIGNED NOT NULL,
  `business_nature_child_id` tinyint(3) UNSIGNED NOT NULL,
  `business_name` varchar(191) NOT NULL,
  `timezone` varchar(191) DEFAULT NULL,
  `locale` varchar(50) NOT NULL DEFAULT 'en' COMMENT 'LANGUAGE',
  `theme_color_id` smallint(5) UNSIGNED NOT NULL DEFAULT '1',
  `profile_image_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `subscription_status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `subscription_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `parent_business_owner_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `created_by` int(11) UNSIGNED DEFAULT NULL COMMENT 'NULL  = means the user is the parent creator',
  `owner_accept_status` smallint(5) UNSIGNED NOT NULL DEFAULT '1',
  `password` varchar(191) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `braintree_id` int(11) UNSIGNED NOT NULL,
  `paypal_email` varchar(255) DEFAULT NULL,
  `card_brand` varchar(200) DEFAULT NULL,
  `card_last_four` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_unique`, `firstname`, `lastname`, `email`, `address`, `city`, `state`, `zipcode`, `country_id`, `account_type_id`, `business_nature_child_id`, `business_name`, `timezone`, `locale`, `theme_color_id`, `profile_image_id`, `subscription_status`, `subscription_id`, `parent_business_owner_id`, `created_by`, `owner_accept_status`, `password`, `remember_token`, `created_at`, `updated_at`, `braintree_id`, `paypal_email`, `card_brand`, `card_last_four`) VALUES
(1, '$2y$10$hH.M83nNubAMeajazMa7Iuu3aqKTtGDxPCJJIgCL7q49gtIYP35Xm', 'swsadas', 'sadasdsad', 'super@trackerteer.com', NULL, NULL, NULL, NULL, 0, 1, 6, 'asdasd', NULL, 'en', 2, 0, 1, 0, 1, NULL, 1, '$2y$10$9635xbgH0Qu8fKhbiccEuuADiRnt5cuDGHc0e78X.RSnPS4IcU2gK', 'hAqHe8YckYJ89IGmYpCtMhJf546zKAAWO8qcuCLmih7v6iKrxFNua6BNJ68r', '2019-04-29 19:21:22', '2019-05-23 01:35:02', 0, NULL, NULL, NULL),
(2, '$2y$10$6yOimvzozy5mX/I7aWrrO.JvJ47N0AeAImIE9wzaBnzDnWf62Bl/a', 'Joan', 'Magpayo', 'joanm@trackerteer.com', NULL, NULL, NULL, NULL, 0, 1, 6, 'asdasd', NULL, 'en', 1, 0, 0, 0, 1, 1, 1, '$2y$10$Tg6XemAszKmXRNaa1r0NGev92Ne6ucuk07UWAqQTEHFxltIbHlZ0y', NULL, '2019-04-30 03:22:41', NULL, 0, NULL, NULL, NULL),
(3, '$2y$10$9aPlWDKDN.P8QhglZKS8Euk/OVFkM45RX9CA09p4EwAjKeguYlTQ2', 'Conna', 'Walker', 'conna@houseofcb.com', NULL, NULL, NULL, NULL, 0, 1, 2, 'House of CB', NULL, 'en', 1, 0, 1, 0, 3, NULL, 1, '$2y$10$bpOTTyYiTiE0RJ1MzIoU3OAGn674bRwTsD2RxchliTennGESqtEoC', 'zTr2c7PIVZ5Iz0uIiTwSiqu2clajhvGgvdgsqD754wSjdYUTvv14ygf1eU8y', '2019-05-05 23:52:09', '2019-05-05 23:52:09', 0, NULL, NULL, NULL),
(4, '$2y$10$oHbWYQywumoE/VaJJoYDhuEtzChGkkN0bZhDSM3Uh.WE8ZjE/grJe', 'Kate', 'Miranda', 'katem@trackerteer.com', NULL, NULL, NULL, NULL, 0, 1, 8, 'Web Dev', NULL, 'en', 1, 0, 1, 0, 4, NULL, 1, '$2y$10$qo.y8E7BS6ZVZsAlEC5rhemBpfFiX6XtQjx/6hq8FKBzCx7qyTeBy', '7a90Wanf1twKpkh7QdQLPiyyq0gyVoHyjPmpYoZQSUjzzctEsfDDEA7EdSnw', '2019-05-05 23:53:16', '2019-05-06 16:47:22', 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_logs`
--

CREATE TABLE `users_logs` (
  `users_logs_id` bigint(11) UNSIGNED NOT NULL,
  `user_id` bigint(11) UNSIGNED NOT NULL,
  `edited_by` bigint(11) UNSIGNED NOT NULL,
  `action_type` varchar(50) NOT NULL,
  `ip_address` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_agent` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_subscription`
--

CREATE TABLE `user_subscription` (
  `user_subscription_id` bigint(11) UNSIGNED NOT NULL,
  `subscription_id` bigint(11) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `amount` decimal(11,2) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `subscription_status` char(5) NOT NULL DEFAULT 'A',
  `subscription_type` char(6) NOT NULL DEFAULT 'B'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_subscription`
--

INSERT INTO `user_subscription` (`user_subscription_id`, `subscription_id`, `user_id`, `amount`, `date_created`, `subscription_status`, `subscription_type`) VALUES
(1, 1, 1, '25.00', '2019-04-30 03:21:22', 'A', 'B'),
(2, 1, 3, '25.00', '2019-05-06 07:52:10', 'A', 'B'),
(3, 1, 4, '25.00', '2019-05-06 07:53:16', 'A', 'B');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `business_nature_child`
--
ALTER TABLE `business_nature_child`
  ADD PRIMARY KEY (`business_nature_child_id`),
  ADD KEY `business_nature_child_business_nature_parent_id_index` (`business_nature_parent_id`);

--
-- Indexes for table `business_nature_parent`
--
ALTER TABLE `business_nature_parent`
  ADD PRIMARY KEY (`business_nature_parent_id`);

--
-- Indexes for table `business_personal_data`
--
ALTER TABLE `business_personal_data`
  ADD PRIMARY KEY (`business_personal_data_id`),
  ADD KEY `parent_id` (`parent_id`,`child_id`,`type`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `profile_image`
--
ALTER TABLE `profile_image`
  ADD PRIMARY KEY (`profile_image_id`),
  ADD KEY `user_id` (`user_id`,`is_profile`);

--
-- Indexes for table `storage`
--
ALTER TABLE `storage`
  ADD PRIMARY KEY (`storage_id`);

--
-- Indexes for table `subscription`
--
ALTER TABLE `subscription`
  ADD PRIMARY KEY (`subscription_id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`task_id`);

--
-- Indexes for table `tasks_attachment`
--
ALTER TABLE `tasks_attachment`
  ADD PRIMARY KEY (`tasks_attachment_id`),
  ADD KEY `tasks_attachment_task_id_index` (`task_id`);

--
-- Indexes for table `tasks_reply_attachment`
--
ALTER TABLE `tasks_reply_attachment`
  ADD PRIMARY KEY (`tasks_reply_attachment_id`),
  ADD KEY `tasks_reply_attachment_task_id_index` (`task_comments_id`);

--
-- Indexes for table `task_assign`
--
ALTER TABLE `task_assign`
  ADD PRIMARY KEY (`task_assign_id`),
  ADD KEY `task_assign_task_id_index` (`task_id`),
  ADD KEY `task_assign_user_id_index` (`user_id`),
  ADD KEY `task_assign_is_created_index` (`is_created`),
  ADD KEY `task_id` (`task_id`,`user_id`);

--
-- Indexes for table `task_categories`
--
ALTER TABLE `task_categories`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `task_comments`
--
ALTER TABLE `task_comments`
  ADD PRIMARY KEY (`task_comments_id`),
  ADD KEY `task_id` (`task_id`,`user_id`);

--
-- Indexes for table `task_logs`
--
ALTER TABLE `task_logs`
  ADD PRIMARY KEY (`task_logs_id`);

--
-- Indexes for table `task_notification`
--
ALTER TABLE `task_notification`
  ADD PRIMARY KEY (`task_notification_id`),
  ADD KEY `task_id` (`task_id`,`user_id`);

--
-- Indexes for table `task_users`
--
ALTER TABLE `task_users`
  ADD PRIMARY KEY (`task_users_id`),
  ADD KEY `task_users_parent_user_id_index` (`parent_user_id`),
  ADD KEY `task_users_child_user_id_index` (`child_user_id`),
  ADD KEY `task_users_parent_team_user_id_index` (`parent_team_user_id`);

--
-- Indexes for table `team_data`
--
ALTER TABLE `team_data`
  ADD PRIMARY KEY (`team_data_id`);

--
-- Indexes for table `team_info`
--
ALTER TABLE `team_info`
  ADD PRIMARY KEY (`team_info_id`);

--
-- Indexes for table `theme_color`
--
ALTER TABLE `theme_color`
  ADD PRIMARY KEY (`theme_color_id`);

--
-- Indexes for table `timezone`
--
ALTER TABLE `timezone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `todo_list`
--
ALTER TABLE `todo_list`
  ADD PRIMARY KEY (`todo_id`),
  ADD KEY `created_by_2` (`user_id`,`status`),
  ADD KEY `user_id` (`user_id`,`is_active`) USING BTREE,
  ADD KEY `todo_id` (`todo_id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `todo_logs`
--
ALTER TABLE `todo_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_account_type_id_index` (`account_type_id`),
  ADD KEY `users_business_nature_child_id_index` (`business_nature_child_id`),
  ADD KEY `users_profile_image_id_index` (`profile_image_id`);
ALTER TABLE `users` ADD FULLTEXT KEY `user_unique` (`user_unique`);
ALTER TABLE `users` ADD FULLTEXT KEY `user_unique_2` (`user_unique`);

--
-- Indexes for table `users_logs`
--
ALTER TABLE `users_logs`
  ADD PRIMARY KEY (`users_logs_id`);

--
-- Indexes for table `user_subscription`
--
ALTER TABLE `user_subscription`
  ADD PRIMARY KEY (`user_subscription_id`),
  ADD KEY `user_id` (`user_id`,`subscription_status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `business_nature_child`
--
ALTER TABLE `business_nature_child`
  MODIFY `business_nature_child_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `business_nature_parent`
--
ALTER TABLE `business_nature_parent`
  MODIFY `business_nature_parent_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `business_personal_data`
--
ALTER TABLE `business_personal_data`
  MODIFY `business_personal_data_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `profile_image`
--
ALTER TABLE `profile_image`
  MODIFY `profile_image_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `storage`
--
ALTER TABLE `storage`
  MODIFY `storage_id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `subscription`
--
ALTER TABLE `subscription`
  MODIFY `subscription_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `task_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tasks_attachment`
--
ALTER TABLE `tasks_attachment`
  MODIFY `tasks_attachment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tasks_reply_attachment`
--
ALTER TABLE `tasks_reply_attachment`
  MODIFY `tasks_reply_attachment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `task_assign`
--
ALTER TABLE `task_assign`
  MODIFY `task_assign_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `task_categories`
--
ALTER TABLE `task_categories`
  MODIFY `category_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `task_comments`
--
ALTER TABLE `task_comments`
  MODIFY `task_comments_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `task_logs`
--
ALTER TABLE `task_logs`
  MODIFY `task_logs_id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `task_notification`
--
ALTER TABLE `task_notification`
  MODIFY `task_notification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `task_users`
--
ALTER TABLE `task_users`
  MODIFY `task_users_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `team_data`
--
ALTER TABLE `team_data`
  MODIFY `team_data_id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `team_info`
--
ALTER TABLE `team_info`
  MODIFY `team_info_id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `theme_color`
--
ALTER TABLE `theme_color`
  MODIFY `theme_color_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `timezone`
--
ALTER TABLE `timezone`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `todo_list`
--
ALTER TABLE `todo_list`
  MODIFY `todo_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `todo_logs`
--
ALTER TABLE `todo_logs`
  MODIFY `id` int(255) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users_logs`
--
ALTER TABLE `users_logs`
  MODIFY `users_logs_id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_subscription`
--
ALTER TABLE `user_subscription`
  MODIFY `user_subscription_id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
