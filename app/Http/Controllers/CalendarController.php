<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Calendar; # TO GET ALL TASK
use App\Http\Controllers\DateTime;
use App\Http\Controllers\DateTimeZone;
use Auth;
class CalendarController extends Controller
{

	public function __construct() {
        parent::__construct();
    }

    public function index (Request $request) {
    
	    $timezone = $request->timezone;
 		$arr = array();
 		$result = Calendar::join('task_assign as ta' , 'ta.task_id' , '=' , 'tasks.task_id')
 		->where('user_id' , Auth::user()->id)
 		->where('task_status' ,'!=','D')->where('status' ,'=','A')->get();
 		foreach ($result as $key => $value) {
 			$arr[$key]['title'] = ($value->title);
			$arr[$key]['title_raw'] =  $value->title;
			$arr[$key]['start'] = date('Y-m-d', strtotime($value->deadline)) . 'T' . date('H:i:s', strtotime("midnight"));
			$arr[$key]['end'] = date('Y-m-d', strtotime($value->deadline)) . 'T' . date('H:i:s', strtotime("midnight")) ;
 		}

 		return response()->json($arr);
    }
}
