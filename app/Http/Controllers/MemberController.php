<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Redirect;

use App\Profile;
use App\Timezone;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class MemberController extends Controller
{   
       
    public function __construct() {
        parent::__construct();
    }

    # LOGS FOR USERS
    public function user_logs ($data = false , $action_type = false ) {
        # GET THE USER ID OF UNIQUE USER
        $user_id = DB::table('users as u')
            ->select('*')
            ->where("user_unique"  , $data->hash)
            ->get()->first();

        $last_id = DB::table('users_logs')->insertGetId([
            'user_id' =>     $user_id->id,
            'edited_by' =>   Auth::user()->id , 
            'ip_address' => $data->ip() , 
            'user_agent' => $data->server('HTTP_USER_AGENT') ,
            'action_type' => $action_type
        ]);
    }

    # this was move on register controller
    // public function add_new_member(Request $request){
    // 	$validator =  Validator::make($request->all(), [
    //         'firstname' => 'required|string|max:255',
    //         'lastname' => 'required|string|max:255' ,
    //         'email' => 'required|string|email|max:255|unique:users,email',
    //     ]);
 
    //     if (!$validator->fails()) {
        
    //         # check if creator or member is adding
    //         if(Auth::user()->created_by == null){ # Parent creator member
    //             $parent_business_owner_id = Auth::user()->id;
    //             $owner_accept_status = 1;
    //         } else{ # Member 
    //             $parent_business_owner_id = Auth::user()->parent_business_owner_id;
    //             $parent = true;
    //             $owner_accept_status = 0;
    //         }

    //         #generate password
    //         $gpassword = Str::random(8);

    //         $last_id = DB::table('users')->insertGetId([
    //             'firstname'                => htmlspecialchars($request->firstname, ENT_QUOTES, 'UTF-8'),
    //             'lastname'                 => htmlspecialchars($request->lastname, ENT_QUOTES, 'UTF-8'),
    //             'account_type_id'          => 1,
    //             'business_nature_child_id' => Auth::user()->business_nature_child_id,
    //             'business_name'            => Auth::user()->business_name,
    //             'parent_business_owner_id' => $parent_business_owner_id , 
    //             'email'                    => $request->email,
    //             'subscription_status'      => 0,
    //             'owner_accept_status'      => $owner_accept_status , 
    //             'password'                 => bcrypt($gpassword),
    //             'created_by'               => Auth::user()->id ,
    //             'timezone'                 => Auth::user()->timezone
    //         ]);

    //         if ($last_id) {

    //             # UPDATE UNIQUE COLUMN FOR SECURITY
    //             DB::table('users')->where('id', $last_id)->update( ["user_unique" => bcrypt ($last_id) ] );


    //             # ADD NEW MEMBER
    //             DB::table('task_users')->insertGetId([
    //                 'parent_user_id' => $parent_business_owner_id,
    //                 'child_user_id'  => $last_id,
    //             ]);

    //             $path = public_path().'/profile/'.$last_id;
    //             if (!file_exists($path)) {
    //                 File::makeDirectory($path, $mode = 0777, true, true);
    //                 $this->create_index_html($path);
    //             }
                
    //             $user = DB::table('users')->find($last_id);
    //             $user->static_password = $gpassword;

    //             UserVerification::generate($user);
    //             UserVerification::send($user, 'TaskBanana Account Verification');

    //             # wala na ito
    //             // $timezone = new Timezone();
    //             // $timezone->user_id = $last_id;
    //             // $timezone->save();

    //             # check if user logged has data 
    //             $check = DB::table('task_users')
    //                         ->select('*')
    //                         ->where("child_user_id" , Auth::user()->id)
    //                         ->get()->first();
                
    //             if (!$check) {
    //                 # ADD NEW MEMBER
    //                 DB::table('task_users')->insertGetId([
    //                     'parent_user_id' =>     $parent_business_owner_id,
    //                     'child_user_id' =>      $parent_business_owner_id
    //                 ]);
    //             }   
    //         }

    //     } else {
    //         return Redirect::back()->withErrors("Something Went Wrong! Please Try Again");
    //     }
    // 	return redirect('/task');
    // }

    public function create_index_html($folder) {
        $content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Directory access is forbidden.</p></body></html>";
        $folder = $folder.'/index.html';
        $fp = fopen($folder , 'w');
        fwrite($fp , $content);
        fclose($fp);
    }

    # FUNCTION TO GET MY BUSINESS TEAM
    public function get_my_business_team () {

        $result = DB::table('task_users as tu')
                    ->select('*')
                    ->join('users as u' , 'u.id' , '=' , 'tu.child_user_id')
                    ->where("parent_user_id" , Auth::user()->parent_business_owner_id)
                    ->where("owner_accept_status" , 1 )
                    ->where("is_active" , 1 )
                    ->get();
      
        foreach ($result as $key => $value) {
            # GET THE PROFILE IMAGE PER USER ID
            if ($value->id == Auth::user()->id) {
              unset($result[$key]); # dont show your name on member list
            }else {
                $result[$key]->profile_image = DB::table('profile_image')
                    ->select('*')
                    ->where("user_id" , $value->id)
                    ->where("is_profile" , 1)
                    ->get()->first();    
            }    
        }
  
        return $result;
    }

    # FUNCTION TO GET MY PERSONAL TEAM
    public function get_my_personal_team () {
        $result = DB::table('team_info as ti')
            ->select("*")
            ->join('team_data as td' , 'td.team_info_id' , '=' , 'ti.team_info_id')
            ->join('users as u' , 'u.parent_business_owner_id' , '=' , 'ti.team_leader_id')
            ->where("ti.status" ,  'A')
            ->where("ti.team_leader_id" ,  Auth::user()->id)
            ->groupBy("id")
            ->get();
       
        foreach ($result as $key => $value) {
            # GET THE PROFILE IMAGE PER USER ID
            $result[$key]->profile_image = DB::table('profile_image')
                    ->select('*')
                    ->where("user_id" , $value->id)
                    ->where("is_profile" , 1)
                    ->get()->first(); 
            if (Auth::user()->created_by == NULL) {
                  unset($result[$key]);
              }  
        }
        
        return $result;
    
    }

    public function view_member_business () {
        $result = $this->get_my_business_team();
        return response()->json($result);
    }

    # NOT WORKING NOW
    public function view_member_personal () {
        $result = $this->get_my_personal_team();
        return response()->json($result);
    }

    public function accept_member_lists () {
        $result = DB::table('users as u')
            ->select('*')
            ->join('task_users as tu' , 'tu.child_user_id' , '=' , 'u.id')
            ->where("parent_user_id" , '='  ,Auth::user()->parent_business_owner_id)
            ->where("owner_accept_status" ,  0) # not accepted
            ->where("owner_accept_status" , '!=' ,   2) # cancelled
            ->groupBy("id")
            ->get();
        foreach ($result as $key => $value) {
            $result[$key]->created_by_full_name = DB::table('users')
                                ->select('*')
                                ->where('id' , $value->created_by)
                                ->get()->first();
        }
        // echo json_encode($result);
        // die();
       return response()->json($result);
    }

    public function accept_member_lists_count () {

         $result = DB::table('users as u')
            ->select('*')
            ->join('task_users as tu' , 'tu.child_user_id' , '=' , 'u.id')
            ->where("parent_user_id" , '='  ,Auth::user()->parent_business_owner_id)
            ->where("owner_accept_status" ,  0)
            ->groupBy("id")
            ->get();
        return count($result);
    }

    public function accept_member_function ( Request $data ) {     
        $return = DB::table('users')
            ->where("user_unique", $data->hash )
            ->update(['owner_accept_status' => 1 ]); 
        if ($return == true) {
            # ADD LOGS FOR USER
            $this->user_logs($data , 'accept_member_lists'  );   
        }
    }

    public function cancel_new_member ( Request $data ) {     
        $return = DB::table('users')
            ->where("user_unique", $data->hash )
            ->update(['owner_accept_status' => 2 ]); 
        if ($return == true) {
            # ADD LOGS FOR USER
            $this->user_logs($data , 'accept_member_lists'  );   
        }
    }

    public function get_member_search ( Request $data ) {
   
       $result = DB::table('users as u')
            ->select('*')
            ->where("firstname" , 'LIKE'  ,$data->firstname.'%')
            ->where("parent_business_owner_id" , '=' , Auth::user()->parent_business_owner_id)
            ->get();

        if(!$result->isEmpty()){
            foreach($result as $k => $v) {
                $result[$k]->image = url('/').'/images/icons/default-user.png';
                $r = Profile::where('user_id',$v->id)->where('is_profile',1)->where('status',null)->first();
                //overwrite default image if is_profile active
                if(!empty($r)) {
                    $result[$k]->image = url('/').'/profile/'.$v->id.'/thumbnail_profile/'.$r->image;
                }
            }
        }

        return response()->json($result);
    }

    public function removeMember (Request $request)  {
       $return = DB::table('users')
            ->where("user_unique", $request->user )
            ->update(['is_active' => 0 ]); 
        DB::table('users_logs')->insertGetId([
            'edited_by' =>   Auth::user()->id , 
            'ip_address' => $request->ip() , 
            'user_agent' => $request->server('HTTP_USER_AGENT') ,
            'action_type' => 'removeMember'
        ]);
    }

}
