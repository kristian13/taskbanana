<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use View;

use Illuminate\Http\Request;
use Laravel\Cashier\Billable;
use App\Http\Controllers\PaymentsController;

use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Facades\UserVerification;

class LandingController extends Controller
{

    public function __construct() {
        parent::__construct();
    }

    public function index () {

    	# GET ALL BUSINESS NATURES
    	$business_nature = DB::table('business_nature_parent as bnp')
		    ->select('*')
		    ->join('business_nature_child as bnc', 'bnp.business_nature_parent_id', '=', 'bnc.business_nature_parent_id')
		    ->get();
		$group = array();
		foreach ($business_nature as $key => $value) {
			$group[$value->business_nature_parent_name][] = $value;
		}

        // 	return view('landing' , ['business_nature' => $group , "timezones" => app('App\Http\Controllers\ProfileController')->generate_timezone_list()]);
        
        return view('landing', [
            "timezones" => app('App\Http\Controllers\ProfileController')->generate_timezone_list(),
            'business_nature' => $group
        ]);
    } 
    
    public function test () {
    	if ( Auth::check()  ) {
    		return '<h1>Online</h1>';
    	} else {
    		return '<h1>Offline</h1>';
    	}
    }

    public function sign_up () {

        /*logout session*/
        Auth::logout();
        # GET ALL BUSINESS NATURES
        $business_nature = DB::table('business_nature_parent as bnp')
            ->select('*')
            ->join('business_nature_child as bnc', 'bnp.business_nature_parent_id', '=', 'bnc.business_nature_parent_id')
            ->get();

        $group = array();
        foreach ($business_nature as $key => $value) {
            $group[$value->business_nature_parent_name][] = $value;
        }

        return view( 'sign_up', ['business_nature' => $group] );
    }


    public function resendVerification(Request $req) {

        if (Auth::attempt(['email' => $req->email, 'password' => $req->password])) {
            
            if( Auth::user()->verified == '0' ) {

                UserVerification::generate(Auth::user());
                UserVerification::send(Auth::user(), 'TaskBanana Verification Link');

                Auth::logout(); //logout user if not verified
                return  response()->json([
                    'status'  => false,
                    'action'  => 'resendVerification',
                    'data'    =>  '',
                    'message' => "Check your email for verification link."
                ]);

            }

        } else {
            return  response()->json([
                'status'  => false,
                'action'  => 'resendVerification',
                'data'    => '',
                'message' => "Invalid Email or Password."
            ]);
        }
    }

    public function landing  () {
        // $arr = array("a" => 1111);
        // return view( 'landing2' , ["arr" => $arr ] );
        # GET ALL BUSINESS NATURES
        $business_nature = DB::table('business_nature_parent as bnp')
            ->select('*')
            ->join('business_nature_child as bnc', 'bnp.business_nature_parent_id', '=', 'bnc.business_nature_parent_id')
            ->get();
        $group = array();
        foreach ($business_nature as $key => $value) {
            $group[$value->business_nature_parent_name][] = $value;
        }

        //return view('landing2' , ['business_nature' => $group , "timezones" => app('App\Http\Controllers\ProfileController')->generate_timezone_list()]);
    }
}
