<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User;
use App\Reminder;
use App\ReminderMembers;
use function GuzzleHttp\json_encode;

class ReminderController extends Controller
{
    
    public function __construct() {
        parent::__construct();
    }

    public function add_reminder(Request $req) {

        $reminder = User::find(Auth::user()->id)->get_reminders;
        
        # Add Reminder
        $reminder = new Reminder;
        $reminder->created_by    = Auth::user()->id;
        $reminder->description   = $req->description;
        $reminder->repeat_every  = $req->repeat_every;
        $reminder->reminder_name = $req->reminder_name;

        if ( $reminder->save() ) {

             # Include Creator as Member 
             $members   = $req->member_true;
             $members[] = Auth::user()->id;
             array_unique($members);

            # Member Daily, Weekly,  Monthly
            if($req->repeat_every == 'daily' ||$req->repeat_every == 'weekly' || $req->repeat_every == 'monthly' ) {
                foreach($members as $member) {
                    $user = User::find($member);
                    $user_time_zone = $user->timezone; # User timezone
                    date_default_timezone_set($user_time_zone); 
                    $today = strtotime("today");
    
                    # FOR PRE DEFINED DATE OF RECURRING
                    if ($req->repeat_every == "daily") {
                        $next_repeat = date("Y-m-d H:i" , strtotime("+ 1 day $req->start_time " , $today));
                        $next_repeat = strtotime($next_repeat);
                    } else if ($req->repeat_every == "weekly") {
                        $next_repeat = date("Y-m-d H:i" , strtotime("+ 7 days $req->start_time " , $today));
                        $next_repeat = strtotime($next_repeat);
                    } elseif($req->repeat_every == "monthly") {
                        $next_repeat = date("Y-m-d H:i" , strtotime("next month $req->start_time " , $today));
                        $next_repeat = strtotime($next_repeat);
                    }
    
                    # ADD Member on Reminder 
                    $reminder_member = new ReminderMembers;
                    $reminder_member->reminder_id  = $reminder->id;
                    $reminder_member->user_id      = $member;
                    $reminder_member->status       = 1;
                    $reminder_member->start_time   = $req->start_time;
                    $reminder_member->repeat_every = $req->repeat_every;
                    $reminder_member->next_repeat  = $next_repeat;
                    $reminder_member->recurrence   = 0;
                    $reminder_member->added_by     = Auth::user()->id;
                    $reminder_member->save();
                }
            }
            
            # Member Custom
            if($req->repeat_every == 'custom') {
                foreach($members as $member){
                   $user           = User::find($member);
                   $user_time_zone = $user->timezone; # User timezone 
                   date_default_timezone_set($user_time_zone);
                                                
                   foreach ($req->days_of_the_week_true as $key => $value) {
                        
                        $next_repeat =  $this->generate_dates($value, $req->start_time, $req->recurring, $user_time_zone);
    
                        # ADD Member on Reminder 
                        $reminder_member = new ReminderMembers;
                        $reminder_member->reminder_id  = $reminder->id;
                        $reminder_member->user_id      = $member;
                        $reminder_member->status       = 1;
                        $reminder_member->start_time   = $req->start_time;
                        $reminder_member->repeat_every = $value;
                        $reminder_member->next_repeat  = strtotime($next_repeat);
                        $reminder_member->recurrence   = $req->recurring;
                        $reminder_member->added_by     = Auth::user()->id;
                        $reminder_member->save();
    
                    }
                }
            }
            
            return json_encode([
                'result' => true,
                   'new' => $reminder
            ]);
        }
    }

    public function edit_reminder(Request $req) {
        
        $Reminder = Reminder::find($req->reminder_id);
        $Reminder->reminder_name = $req->reminder_name;
        $Reminder->repeat_every = $req->repeat_every;
        $Reminder->save();

        # Include Creator on member 
        $members = array_map('intval',$req->member_true);
        $members[] = Auth::user()->id;
        array_unique($members);
        
        # DAILY, Weekly, Monthly
        if($req->repeat_every != 'custom'){

            foreach($members as $member){
                $user = User::find($member);
                $user_time_zone = $user->timezone; # User timezone
                date_default_timezone_set($user_time_zone); 
                $today = strtotime("today");

                # Creator
                if($Reminder->created_by == Auth::user()->id) {
                    # Creator Member
                    $cm = $Reminder->member_list->where('added_by',Auth::user()->id)->groupBy('user_id');
                    # Other Members
                    $om = $Reminder->member_list->where('added_by','<>',Auth::user()->id)->groupBy('user_id');

                    # Creator Member
                    foreach($cm as $k => $v) {
                        # UPDATE lahat ng member na naka check na sinabmit ni creator
                        if(in_array($k,$members)) { 
                            # Delete existing
                            $Reminder->member_list()->where('user_id',$k)->delete();
                            #FOR PRE DEFINED DATE OF RECURRING
                            if ($req->repeat_every == "daily") {
                                $next_repeat = date("Y-m-d H:i" , strtotime("+ 1 day $req->start_time " , $today));
                                $next_repeat = strtotime($next_repeat.' '. $user_time_zone);
                            } else if ($req->repeat_every == "weekly") {
                                $next_repeat = date("Y-m-d H:i" , strtotime("+ 7 days $req->start_time " , $today));
                                $next_repeat = strtotime($next_repeat.' '. $user_time_zone);
                            } elseif($req->repeat_every == "monthly") {
                                $next_repeat = date("Y-m-d H:i" , strtotime("next month $req->start_time " , $today));
                                $next_repeat = strtotime($next_repeat.' '. $user_time_zone);
                            }
                           
                            $new = [
                                'user_id'      => $k,
                                'status'       => 1,
                                'reminder_id'  => $req->reminder_id,
                                'start_time'   => $req->start_time,
                                'repeat_every' => $req->repeat_every,
                                'next_repeat'  => $next_repeat,
                                'recurrence'   => 0,
                                'added_by'     => Auth::user()->id
                            ];
                            $Reminder->member_list()->insert($new);
                        }
                        # DELETE
                        else{ # Creator remove this user
                            $Reminder->member_list()->where('user_id',$k)->delete();
                        }
                    }

                    # Creator update Other Members
                    foreach($om as $k =>$v) {
                        # UPDATE
                        if(in_array($k,$req->member_true)) { 
                            # Delete existing
                            $Reminder->member_list()->where('user_id',$k)->delete();
                            #FOR PRE DEFINED DATE OF RECURRING
                            if ($req->repeat_every == "daily") {
                                $next_repeat = date("Y-m-d H:i" , strtotime("+ 1 day $req->start_time " , $today));
                                $next_repeat = strtotime($next_repeat.' '. $user_time_zone);
                            } else if ($req->repeat_every == "weekly") {
                                $next_repeat = date("Y-m-d H:i" , strtotime("+ 7 days $req->start_time " , $today));
                                $next_repeat = strtotime($next_repeat.' '. $user_time_zone);
                            } elseif($req->repeat_every == "monthly") {
                                $next_repeat = date("Y-m-d H:i" , strtotime("next month $req->start_time " , $today));
                                $next_repeat = strtotime($next_repeat.' '. $user_time_zone);
                            }

                            $new = [
                                'user_id'      => $k,
                                'status'       => 1,
                                'reminder_id'  => $req->reminder_id,
                                'start_time'   => $req->start_time,
                                'repeat_every' => $req->repeat_every,
                                'next_repeat'  => $next_repeat,
                                'recurrence'   => 0,
                                'added_by'     => $v[0]['added_by']
                            ];
                            $Reminder->member_list()->insert($new);
                          
                        }
                        # DELETE
                        else{ # Creator remove this user
                            $Reminder->member_list()->where('user_id',$k)->delete();
                        }
                    }
                
                }

                # Members
                else{
                    # member members
                    $mm = $Reminder->member_list->where('added_by',Auth::user()->id);
                }
            }
            
        }

        
        # CUSTOM
        else { 
      
            # Creator
            if($Reminder->created_by == Auth::user()->id) {

                # Creator Delete Members
                $cm = $Reminder->member_list()->where('added_by',Auth::user()->id)->delete();
                # Other Members
                $om = $Reminder->member_list->where('added_by','<>',Auth::user()->id)->groupBy('user_id');

                foreach($members as $member){

                    $user = User::find($member);
                    $user_time_zone = $user->timezone; # User timezone 
                    date_default_timezone_set($user_time_zone);
                    $today = strtotime("today");
                
                    foreach ($req->days_of_the_week_true as $key => $value) {
                        $next_repeat =  $this->generate_dates($value, $req->start_time, $req->recurring, $user_time_zone);
                        # ADD Member on Reminder 
                        $new = array(
                            'user_id'      => $member,
                            'reminder_id'  => $req->reminder_id,
                            'status'       => 1,
                            'start_time'   => $req->start_time,
                            'repeat_every' => $value,
                            'next_repeat'  => strtotime($next_repeat),
                            'recurrence'   => $req->recurring,
                            'added_by'     => Auth::user()->id
                        );
                        $Reminder->member_list()->insert($new);
                    }  

                    # Creator update Other Members
                    foreach($om as $k =>$v) {
                        # UPDATE
                        # Delete existing
                        $Reminder->member_list()->where('user_id',$k)->delete();
                        # Insert again base on submit                        
                        foreach ($req->days_of_the_week_true as $key => $value) {
                            $next_repeat =  $this->generate_dates($value, $req->start_time, $req->recurring, $user_time_zone);
                            # ADD Member on Reminder 
                            $new = [
                                'user_id'      => $k,
                                'status'       => 1,
                                'reminder_id'  => $req->reminder_id,
                                'start_time'   => $req->start_time,
                                'repeat_every' => $value,
                                'next_repeat'  => strtotime($next_repeat),
                                'recurrence'   => $req->recurring,
                                'added_by'     => $v[0]['added_by']
                            ];
                            $Reminder->member_list()->insert($new);
                        }
                    }
                
                    # DELETE other members that creator removed
                    if($req->member_false){
                        foreach($req->member_false as $k) {
                            $Reminder->member_list()->where('user_id',$k)->delete();
                        }
                    }

                }                
            
            }

            # Members
            else { 
                # member members
                $mm = $Reminder->member_list->where('added_by',Auth::user()->id);
                return  json_encode($mm);
            }
           
        }

       return json_encode(['result'=>true]);
        
    }

    public function reminder_info($id){
        $reminder = Reminder::find($id);

        $data['reminder'] = $reminder;
        $data['members']  = $reminder->member_list->groupBy('user_id');
        
        if($reminder){
            return json_encode([
                'result'=>true,
                 'data' => $data
            ]);
        }else{
            return json_encode([
                'result'=>false
            ]);
        }
    }

    public function reminder_notif(Request $req){
        $reminder = Reminder::find($req->reminder_id);
        $member_reminder = $reminder->member_list->where('user_id',$req->user_id);
        if(count($member_reminder) > 0 ){
            foreach($member_reminder as $mr){
                $mr->notify = $req->notify;
                $mr->save();
                //$mr->save();
            }
        }
        //return json_encode($member_reminder);
        //$member_reminder->status = $req->status;
        //$member_reminder->save();
    }

    public function remind_me($id) {
        
        $User     = User::find($id);
        $timezone = $User->timezone;
        date_default_timezone_set($timezone);
     
        $date  = date("Y-m-d H:i:s",time());
        $today = strtotime($date);
       
        $current_week_in_number = strtolower( date("w" , $today ) );
        $two_hours_seconds      = 7200;
       
        $result = $User->get_reminders
                        ->where('next_repeat','<', $today)
                        ->where('notify',1)
                        ->where('status',1);

        foreach ($result as $key => $value) {
            
            $time_remind = date("d/m/Y h:i A",$value->next_repeat);
            $next_repeat = date("Y-m-d h:i" , strtotime(date("Y-m-d H:i",$value->next_repeat)) );
            $next_repeat = strtotime($next_repeat);
            $diff        = ($today) - $next_repeat;
            
            $reminder = Reminder::find($value->reminder_id);

            $created_week_day_in_number = strtolower(date("w" , strtotime($reminder->created_at->format('Y-m-d H:i'))));

            # CHECK IF REMINDER IS GREATER THAN TWO HOURS THEN UPDATE ON NEXT UPDATE  
            if ($value->recurrence == 0) {

                if ( $value->repeat_every == "daily" ) {
                    $next_repeat = date("Y-m-d H:i:s A" , strtotime("tomorrow" .$this->convert_timezone($timezone, date("Y-m-d H:i",$value->next_repeat))  ));
                    $arr = array( "next_repeat" => strtotime($next_repeat) );
                   
                    # use today and add 1 day. if user does not login for a day
                    # Overwrite this -> $arr = array( "next_repeat" => strtotime($next_repeat) );
                    if($diff > $two_hours_seconds) {
                        $dateToday   = strtotime(date("Y-m-d").' '.$value->start_time);
                        $next_repeat = date("Y-m-d H:i:s A", strtotime("tomorrow". date("Y-m-d H:i:s",$dateToday)));
                        $arr = array( "next_repeat" => strtotime($next_repeat) );
                    }
                
                } elseif ($value->repeat_every == "weekly") {

                    $from      = $reminder->created_at->format('Y-m-d');
                    $to        = date("Y-m-d",$today);
                    
                    # Total week from created date until current date
                    $num_weeks = ceil(abs(strtotime($from) - strtotime($to)) / 60 / 60 / 24 / 7);
                    
                    $get_time  = date('H:i:s',$value->next_repeat); 
                    $created   = strtotime(date("Y-m-d",strtotime($from)).' '.$get_time); 

                    if ($num_weeks > 0) { # UPDATE THE REMINDER NEXT WEEK 
                        $current_week = date("Y-m-d H:i:s" , strtotime("+ $num_weeks week " . date("Y-m-d H:i:s",$created)));
                        if( strtotime($current_week) < $today) { # CHECK IF CURRENT WEEK DAY IS GREATER THAN WEEKDAY
                            $num_weeks++;
                            # OVERWRITE CURRENT WEEK VALUE
                            $current_week = date("Y-m-d H:i:s" , strtotime("+ $num_weeks week " . date("Y-m-d H:i:s",$created)));
                        }
                        $arr = array("next_repeat" => strtotime($current_week));
                    }

                    # CHECK IF CURRENT WEEK DAY IS GREATER THAN WEEKDAY THAT THE REMINDER CREATED
                    // if ($current_week_in_number >  $created_week_day_in_number ) { // UPDATE THE REMINDER NEXT WEEK 
                    //     $next_repeat = date("Y-m-d H:i:s A" , strtotime("+ 1 week" .$this->convert_timezone($timezone, date("Y-m-d H:i",$value->next_repeat))  ));
                    //     $arr = array( "next_repeat" => strtotime($next_repeat) );
                    // } else { // UPDATE THE REMINDER THIS WEEK
                    //     $next_repeat = date("Y-m-d H:i:s A" , strtotime("this week" .$this->convert_timezone($timezone, date("Y-m-d H:i",$value->next_repeat))  ));
                    //     $arr = array( "next_repeat" => strtotime($next_repeat) );
                    // }
                    
                } else {
                    $next_repeat = date("Y-m-d H:i:s A",strtotime("+1 month" ,$value->next_repeat));
                    $arr = array( "next_repeat" => strtotime($next_repeat) );
                }

                if ($diff > $two_hours_seconds) {
                    $value->update($arr);
                }
                
            }

            else { # USER CHOOSE CUSTOM
                $next_repeat = $this->generate_dates($value->repeat_every, $value->start_time, $value->recurrence,$timezone);
                $strtotime = strtotime($next_repeat);
                if ($diff > $two_hours_seconds) {
                  //$value->update(['next_repeat' => $strtotime]);
                }
            }

            if ($diff > $two_hours_seconds) { # REMOVE EXPIRED REMINDER
                unset($result[$key]);
            } else {
                $result[$key]['next_repeat'] = $next_repeat;
                $result[$key]['time_remind'] = $time_remind; 
                $result[$key]['reminder']    = $reminder;
                # dont remove this it will include members on reminder $result[$key]['reminder']
                $reminder->member_list; 
            }
        }
        
        return [$result];
    }

    public function update_reminders(){
        $reminder_members = ReminderMembers::where('status',1)->get();

        foreach($reminder_members as $member) {
            $user = User::find($member->user_id);
            $user_time_zone = $user->timezone; # Update base on User timezone
            date_default_timezone_set($user_time_zone); 
            $today = strtotime("today");
            
            $two_hours_seconds      = 7200;
            
            # Date today
            $date  = date("Y-m-d H:i:s",time());
            $today = strtotime($date);
            
            # Date on table
            $next_repeat = date("Y-m-d H:i:s" , strtotime(date("Y-m-d H:i:s",$member->next_repeat)) );
            $next_repeat = strtotime($next_repeat);
            $diff        = ($today) - $next_repeat;
            
            # FOR Check Week
            $reminder = Reminder::find($member['reminder_id']);
            $created_week_day_in_number = strtolower(date("w" , strtotime($reminder->created_at->format('Y-m-d H:i'))));
            $current_week_in_number     = strtolower( date("w" , $today ) );
            
            # Daily
            if ($member->repeat_every == "daily") {
                $next_repeat = date("Y-m-d H:i:s A" , strtotime("tomorrow" .date("Y-m-d H:i",$member->next_repeat) ));
                $arr = array( "next_repeat" => strtotime($next_repeat) );                
            }

            # Weekly
            elseif ($member->repeat_every == "weekly") {
                # CHECK IF CURRENT WEEK DAY IS GREATER THAN WEEKDAY THAT THE REMINDER CREATED
                if ($current_week_in_number >  $created_week_day_in_number ) { // UPDATE THE REMINDER NEXT WEEK 
                    $next_repeat = date("Y-m-d H:i:s A" , strtotime("+ 1 week" .date("Y-m-d H:i",$member->next_repeat) ));
                    $arr = array( "next_repeat" => strtotime($next_repeat) );
                } else { // UPDATE THE REMINDER THIS WEEK
                    $next_repeat = date("Y-m-d H:i:s A" , strtotime("this week" .date("Y-m-d H:i",$member->next_repeat) ));
                    $arr = array( "next_repeat" => strtotime($next_repeat) );
                }
            }

            # Monthly
            elseif ($member->repeat_every == "monthly") {
                $next_repeat = date("Y-m-d H:i:s A",strtotime("+1 month" ,$member->next_repeat));
                $arr = array( "next_repeat" => strtotime($next_repeat) );
            }

            # CUSTOM
            else { 
                $next_repeat = $this->generate_dates($member->repeat_every, $member->start_time, $member->recurrence,$user_time_zone);
                $arr = array( "next_repeat" => strtotime($next_repeat) );
            }

            # PROCESS OF UPDATE BASE ON CONDITION
            if($diff > $two_hours_seconds) {
                $member->update($arr);
            }
        } # foreach

        return json_encode($reminder_members);
    }

    public function snooze_reminder(Request $req) {

        $User  = User::find(Auth::user()->id);
        $timezone = $User->timezone;

        $reminder = ReminderMembers::find($req->reminder_members);
        $reminder->next_repeat = strtotime($req->next_repeat.' '.$timezone);
        if($reminder->save()) {
            return json_encode(['result'=>true]);
        }else {
            return json_encode(['result'=>false]);
        }
    }

    public function reminder_remove(Request $req) {
        
        $reminder_member = ReminderMembers::where('reminder_id',$req->reminder_id);      
        
        if( $reminder_member->update(['status' => 0]) ) {
            return json_encode(['result'=>true]);
        } else {
            return json_encode(['result'=>false]);
        }
    }

    public function convert_timezone($time_zone,$time, $epoch = false) {
    	$custom_format_date_with_hour = "M d Y H:i";
    	$timezone = $time_zone;
    	$triggerOn = date( $custom_format_date_with_hour, ($epoch == true ? $time : strtotime($time))  );
        
    	$tz = new \DateTimeZone($timezone);
    	$datetime = new \DateTime($triggerOn);
    	$datetime->setTimezone($tz);
    	return $datetime->format($custom_format_date_with_hour);
    }

    function generate_dates($day_name, $time, $recurring, $timezone) {
        $dow   =  $day_name;
        $step  = $recurring;
        $unit  = 'W';
        
        $start = new \DateTime(date('Y-m-d', strtotime('today '.$timezone)));
        $end   = clone $start;
        
        $start->modify($dow); // Move to first occurence
        $end->add(new \DateInterval('P1Y')); // Move to 1 year from start
        
        $interval = new \DateInterval("P{$step}{$unit}");
        $period   = new \DatePeriod($start, $interval, $end);
        
        $dates = [];
        foreach ($period as $date) {
            $new_date = $date->format('Y-m-d').' '.$time;
            $dates[] = date('Y-m-d H:i:s A',strtotime($new_date));
        }
        return $dates[1];
    }

}
