<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Session;
use Redirect;
use App\User;
use App\Task;
use App\Notifications;

use App\TaskComment;
use App\ReminderMembers;
use App\Http\Controllers\DateTime;
use App\Http\Controllers\DateTimeZone;
use function GuzzleHttp\json_encode;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

//For send reset email
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

use Jrean\UserVerification\Facades\UserVerification;


class TaskController extends Controller
{   

    use SendsPasswordResetEmails, ResetsPasswords {
        SendsPasswordResetEmails::broker insteadof ResetsPasswords;
    }

    public function sendResetLinkEmailForm() {
        return view("auth.passwords.email");
    }

    public function showResetForm($token,Request $request) {
        return view("auth.passwords.reset", [
            "token" => $token, 
            "email" => $request->email
        ]);
    }

    public function create_index_html($folder) {
        $content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Directory access is forbidden.</p></body></html>";
        $folder = $folder.'/index.html';
        $fp = fopen($folder , 'w');
        fwrite($fp , $content);
        fclose($fp);
    }

    function task_logs ( $action = false , $task_id = false ) {
        $task_id = DB::table('task_logs')->insertGetId([
           'task_id' => $task_id , 
           'action' => $action , 
           'user_id' => Auth::user()->id
        ]);
    }

    function resize_image($file, $w, $h, $crop=false) { // old
        list($width, $height) = getimagesize($file);
        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width-($width*abs($r-$w/$h)));
            } else {
                $height = ceil($height-($height*abs($r-$w/$h))); 
            }
            $newwidth = $w;
            $newheight = $h;
        } else { 
            if ($w/$h > $r) {
                $newwidth = $h*$r;
                $newheight = $h;  
            } else {
                $newheight = $w/$r;
                $newwidth = $w;
            }
        } 
        
        //Get file extension
        $exploding = explode(".",$file);
        $ext = end($exploding);
        switch($ext){
            case "png":
                $src = imagecreatefrompng($file);
            break;
            case "jpeg":
            case "jpg":
                $src = imagecreatefromjpeg($file);
            break;
            case "gif":
                $src = imagecreatefromgif($file);
            break;
            default:
                $src = imagecreatefrompng($file);
            break;
        }
       
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        return $dst;
    }
    
    # FUNCTION STORE TASK STORAGE
    public function storage ($title = false , $description = false , $due_date = false ,$files = array()  , $type = false , $business_or_personal = false) {
        $total_image_size = 0;
        if ($files) {
            foreach (json_decode($files[0]) as $key => $value) {
               $total_image_size += strlen(base64_decode($value->value));
            } 
        }
        $title_size = strlen($title);
        $description_size = strlen($description);
        $due_date_size = strlen($due_date) ;
        $total = $title_size + $description_size + $due_date_size + $total_image_size;
        $id =  Auth::user()->parent_business_owner_id ;

        $this->get_remaining_storage($total);
        
        
        # GET THE PARENT OWNER OF THE SUBSCRIPTION
        $result = DB::table('users as u')
            ->select('*')
            ->where('id' , $id)
            ->get()->first();

        # INSERT STORAGE
        DB::table('storage')->insertGetId([
            'storage'  => $total,
            'user_id'  => $result->id ,  
            'type'     => $type , 
            'original_user' => Auth::user()->id
        ]);
    }

    public function index (Request $data) {
        return view("task" ,
        [ "get_my_business_team" => app('App\Http\Controllers\MemberController')->get_my_business_team() ,
          "get_my_personal_team" => app('App\Http\Controllers\MemberController')->get_my_personal_team() , 
         // "task" => $this->tasks(),
          "get_new_task" => $this->get_new_task($data) , 
          "get_waiting_for_answer" => $this->get_waiting_for_answer($data) , 
          "get_waiting_for_me" => $this->get_waiting_for_me($data) , 
          "User" => User::find(Auth::user()->id),
          "Reminders" => ReminderMembers::with(['user','reminder']),
          "Profile" => User::find(Auth::user()->id),
          "categories" =>  $this->get_categories_view() , 
          "new_members" => app('App\Http\Controllers\MemberController')->accept_member_lists_count() 
        ]);
    }
    
    public function add_task (Request $request) {

        $request->assignee = json_decode($_POST['assignee']);
     
        $validator =  Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'description' => 'required|string',
        ]);

        #CHECK STORAGE
        $storage_checked = $this->check_storage(true);

        if ($storage_checked == true) {
            return Redirect::back()->withErrors("You already used all of your MB storage !");
            return false;
        }

        $type = $request->account_type == 'business' ? 'B' : 'P';
        if (!$validator->fails()) {

            //$this->storage($request->title , $request->description , $request->deadline ,  $request->images  , 'T'  , $type);
            
            #CHECK IF CATEGORY IS NEW OR EXISTING
            $check_category = DB::table('task_categories as t')
                            ->select('*')
                            ->where("t.category_unique" , $request->category_id )
                            ->get();
                          
            if ($check_category->isEmpty()) { 
                return Redirect::back()->withErrors("Something went wrong! Please try again.");
            } else {
                 # UPDATE TASK PERCENTAGE OF EXISTING TASK
                if (isset($request->existing_task_percentage)) {
                    foreach ($request->existing_task_percentage as $key => $value) {
                        DB::table('tasks')->where('task_id', $key)->update(['task_percentage' => $value ]);    
                    }
                }

                $category_id = $check_category[0]->category_id;
            }
            
            # INSERT TASK AS CREATOR
            $task_id = DB::table('tasks')->insertGetId([
                'title'           => htmlspecialchars($request->title, ENT_QUOTES, 'UTF-8'),
                'description'     => $request->description ,
                "raw_description" => htmlspecialchars($request->description, ENT_QUOTES, 'UTF-8'),
                "task_percentage" => (isset($request->new_project_title))  ? $request->new_task_input_percentage  : 100 ,  
                'created_by'      => Auth::user()->id , 
                "category_id"     => $category_id ,
                'deadline'        => ($request->deadline != null) ? $request->deadline : NULL  ,
                'type'            => $type
            ]);

            
            if ($task_id) {
                
                // Upload new version
                $this->uploadFiles($request->file('images'),'tasks_attachment', $task_id);

                #INSERT AS CREATOR
                DB::table('task_assign')->insertGetId([
                    'task_id' => $task_id ,
                    'user_id' => Auth::user()->id , 
                    'is_created' => 1
                ]);
                
                # INSER ALL ASSIGNEE
                foreach ($request->assignee as $key => $value) {
                    DB::table('task_assign')->insertGetId([
                        'task_id' => $task_id ,
                        'user_id' => $value
                    ]);

                    # ADD NOTIFICATION ON EVERY ASSIGNEE
                    $task_comments_id = DB::table('task_notification')->insertGetId([
                        'comment'     => NULL,
                        'action_type' => htmlspecialchars('T', ENT_QUOTES, 'UTF-8'), 
                        'user_id'     => $value,
                        'task_id'     => $task_id,
                        'notif_by'    => Auth::user()->id,
                        'seen_date'   => NULL
                    ]);                    
                }

                # GET THE IMAGES OF TASK 
                $attachment = DB::table('tasks_attachment as ta')
                            ->select('*')
                            ->where("ta.task_id" , $task_id)
                            ->where("ta.status" , 1)
                            ->get();       

               
                $data = Auth::user()->tasks->find($task_id);
                $data->get_assign;
                $data->get_attachment;

                foreach ($data->get_assign as $key => $value) {
                    if($value->assignee_profile == null){
                        $data->get_assign[$key]->image = url('/').'/public/images/icons/default-user.png';
                        unset($data->get_assign[$key]->assignee_profile);
                    }else if ( $data->get_assign[$key]->assignee_profile->image == null && $data->get_assign[$key]->assignee_profile != null) {
                        $data->get_assign[$key]->image = url('/').'/public/images/icons/default-user.png';
                        unset($data->get_assign[$key]->assignee_profile);
                    } else {
                        $data->get_assign[$key]->image = url('/').'/public/profile/'.$data->get_assign[$key]->assignee_profile->user_id.'/thumbnail_profile/'.$data->get_assign[$key]->assignee_profile->image;
                        unset($data->get_assign[$key]->assignee_profile);
                    }
                }
              
                return response()->json([
                    'status'  => true,
                    'data'    =>  $data,
                    'message' => "success"
                ]);
            } 
            
            # Error insert 
            else {
                return response()->json([
                    'status'  => false,
                    'data'    => null,
                    'message' => "Server time out please try again..."
                ]);
            }
               

        } 

        # Validation response
        else {
            return response()->json([
                'status'  => false,
                'data'    => null,
                'message' => $validator->messages()->getMessages()
            ]);
        }
    }

    public function edit_task(Request $req) {
        
        $task = Auth::user()->tasks->find($req->task_id);     
            
        $validator =  Validator::make($req->all(), [
            'title'       => 'required|string|max:255',
            'description' => 'required|string',
        ]);
        
        if (!$validator->fails()) {
            $task->title           = $req->title;
            $task->deadline        = $req->deadline;
            $task->description     = $req->description;
            $task->raw_description = htmlspecialchars($req->description, ENT_QUOTES, 'UTF-8');
            
            if( $task->save() ) {

                # upload new version
                $this->uploadFiles($req->file('images'),'tasks_attachment', $req->task_id);

                # ADD Assignee
                if(isset($req->assignee)) {
                    $req->assignee = explode(',',$req->assignee);
                    foreach($req->assignee as $k=>$v) {
                        $result     = $task->get_assign->where('user_id',$v);
                        $not_exists = count($result) < 1;
                        if($not_exists){ # Then Assign this user
                            $task->get_assign()->create(['user_id' => $v]);
                        }
                    }
                }
                
                # Compare prev assignee and submited assignee 
                # remove prev assignee that are not exist on new submited assignee 
                $all_assign = $task->get_assign()->where('user_id','!=',Auth::user()->id)->get()->pluck('user_id');
                $new = [];
                foreach($all_assign as $k=>$v){ $new[] = $v; }
                $assignee        = array_map('intval',(array)$req->assignee);
                $remove_assignee = array_diff($new,$assignee);
                $task->get_assign()->whereIn('user_id',$remove_assignee)->delete();

               
                $data = $task->refresh();
                $data->get_assign;
                $data->get_attachment;
                
                foreach ($data->get_assign as $key => $value) {
                    if($value->assignee_profile == null){
                        $data->get_assign[$key]->image = url('/').'/public/images/icons/default-user.png';
                        unset($data->get_assign[$key]->assignee_profile);
                    }else if ( $data->get_assign[$key]->assignee_profile->image == null && $data->get_assign[$key]->assignee_profile != null) {
                        $data->get_assign[$key]->image = url('/').'/public/images/icons/default-user.png';
                        unset($data->get_assign[$key]->assignee_profile);
                    } else {
                        $data->get_assign[$key]->image = url('/').'/public/profile/'.$data->get_assign[$key]->assignee_profile->user_id.'/thumbnail_profile/'.$data->get_assign[$key]->assignee_profile->image;
                        unset($data->get_assign[$key]->assignee_profile);
                    }
                }
               
                return response()->json([
                    'status'  => true,
                    'data'    => $data,
                    'message' => 'success'
                ]);
            } 
            
            # Not Save
            else {
                return response()->json([
                    'status'  => false,
                    'data'    => null,
                    'message' => 'Server timeout please try again...'
                ]);
            }

        }

        # Validation Error Response
        else {
            // Each failed field will have one or more messages.
            return json_encode([
                'status'  => false,
                'data'    => null,
                'message' => $validator->messages()->getMessages()
            ]);
        }

        # LOGS
        //$this->task_logs('edit_task' , $req->task_id);
    }

    /* 
     *  @var $files 
     *  @var $folder = folder name  'tasks_attachment', 'tasks_reply_attachment', 'tasks_reply_thumbnail' , 'tasks_thumbnail'
     *  @var $task_iid = task_id
     */
    public function uploadFiles($files, $folder, $task_id ,$task_reply_id = null) {


        if(!isset($files) || empty($files)) return;

        foreach ($files as $file)
        {
            // Generate a file name with extension
            $fileName = 'file_'.Str::random(10).'_'.time().'.'.$file->getClientOriginalExtension();
            
            // Ensure the directory exists
            $directory = "/public/{$folder}/{$task_id}";
            if (!Storage::exists($directory)) {
                Storage::makeDirectory($directory);
            }

            // Save the file
            $full_path = $file->storeAs("/public/{$folder}/{$task_id}", $fileName);
            
            if( $folder == 'tasks_attachment'       ){ $column_id = 'task_id'; }
            if( $folder == 'tasks_reply_attachment' ){ $column_id = 'task_comments_id'; }
            
            # if file is image
            $ext = array('jpg','jpeg','png','gif','tiff');
            if(in_array($file->getClientOriginalExtension(),$ext)){
                 Storage::makeDirectory("/public/{$folder}/{$task_id}/thumbnail/");

                # resize image for thumbnail
                $file->storeAs("/public/{$folder}/{$task_id}/thumbnail/", $fileName); # create copy of original file then resize the file after
                $thumbnailPath = public_path("storage/{$folder}/{$task_id}/thumbnail/{$fileName}");
                $image_resize = Image::make($thumbnailPath);              
                $image_resize->resize(150, 150);
                $image_resize->save($thumbnailPath);
            }
             
            $array = explode('.', $fileName);
            $file_type = end($array);

            if( $folder == 'tasks_attachment'       ){ $id = $task_id; }
            if( $folder == 'tasks_reply_attachment' ){ $id = $task_reply_id; }
            DB::table("{$folder}")->insertGetId([
                     $column_id => $id,
                    'image'     => $fileName, 
                    'file_type' => $file_type,
                    'full_path' => Storage::url($full_path)
            ]);
        }
    }

    //OLD
    // public function uploadFiles($files, $path, $id) {
    //     if(!isset($files) || empty($files)) return;
    //     
    //     $folder = public_path().$path.$id;
    //     if (!file_exists($folder)) {
    //         File::makeDirectory($folder, $mode = 0777, true, true);
    //         $this->create_index_html($folder);
    //     }
    //
    //     $files = json_decode($files[0]);
    //     foreach ($files as $file) { 
    //
    //         if(strlen($file->value) > 128) {
    //             $data     = base64_decode(explode(',', explode(';', $file->value)[1])[1]);
    //             $string   = explode('.', $file->name);
    //             $filename = $string[0].((rand()*2)-7).'.'.$string[1];
    //             $destinationPath = public_path().$path.$id.'/'.$filename;
    //             $url = url('/').$path.$id.'/'.$filename;
    //
    //             file_put_contents($destinationPath, $data);
    //             if ($string[1] == 'jpg' OR $string[1] == 'JPG' OR $string[1] == 'jpeg' OR $string[1] == 'JPEG' OR $string[1] == 'png' OR $string[1] == 'PNG' ) {
    //                 // Get the data from a png file
    //                 $imageData = $this->resize_image($destinationPath,150,150);
    //                
    //                 // Give a name to your file with the full path
    //                 $filepath = $destinationPath;
    //                 $quality = 0;
    //                 imagepng($imageData,$filepath,$quality);
    //             }
    //
    //             DB::table('tasks_attachment')->insertGetId([
    //                     'task_id'   => $id ,
    //                     'image'     => $filename , 
    //                     'file_type' => $string[1] ,
    //                     'full_path' => $url
    //             ]);
    //         }
    //     }
    // }
    
    

    /* km upload data */
    public function uploadFilesComments($files, $path, $id) {
        if(!isset($files) || empty($files)) return;

        $folder = public_path().$path.$id;
        if (!file_exists($folder)) {
            File::makeDirectory($folder, $mode = 0777, true, true);
            $this->create_index_html($folder);
        }

        $files = json_decode($files[0]);
        foreach ($files as $file) { 
            if(strlen($file->value) > 128) {
                $data            = base64_decode(explode(',', explode(';', $file->value)[1])[1]);
                $string          = explode('.', $file->name);
                $filename        = $string[0].((rand()*2)-7).'.'.$string[1];
                $destinationPath = public_path().$path.$id.'/'.$filename;
                $url             = url('/').$path.$id.'/'.$filename;
                
                file_put_contents($destinationPath, $data);
                if ($string[1] === 'jpg' OR $string[1] === 'JPG' OR $string[1] === 'jpeg' OR $string[1] === 'JPEG' OR $string[1] === 'png' OR $string[1] === 'PNG' ) {
                    // // Get the data from a png file
                     $imageData = $this->resize_image("public/task_reply_attachment/".$id."/".$filename,150,150);
                    // // Give a name to your file with the full patj
                    // $filepath = "public/task_reply_thumbnail/".$filename;
                     $quality = 0;
                    // imagepng($imageData,$filepath,$quality);
                    $filepath = "public/task_reply_thumbnail/".$id."/".$filename;
                    $quality  = 0;
                    imagepng($imageData,$filepath,$quality);
                    //$this->make_thumb($destinationPath, $filepath, 200);
                }
                 DB::table('tasks_reply_attachment')->insertGetId([
                    'task_comments_id' => $task_comment_id,
                    'file_type' => $string[1] ,
                    'image'     => $filename , 
                    'full_path' => $url
                ]);
            }
        }
    }
    /* end km upload data */

    # GET NEW TASKS
    public function get_new_task (Request $data) {
        $group    = array();
        $newArray = array();
        $array    = array();
        if (isset($data->s) && !empty($data->s) ) {
            $title = $data->s;
        } else{
            $title = "";
        }
        if (isset($data->t) && !empty($data->t) ) {
            $task_id = $data->t;
        } else{
            $task_id = "";
        }

        if (isset($data->p) && !empty($data->p) ) {
            $project = $data->p;
        } else{
            $project = "";
        }


        $result = DB::table('tasks as t')
                ->select('*' , 't.created_by as task_creator' , 't.task_id as task_id' , 't.created_at as task_created')
                ->join('task_assign as ta', 'ta.task_id', '=', 't.task_id')
                ->join('users as u' , 'u.id' , '=' , 't.created_by')
                ->join('task_categories as tcc' , 'tcc.category_id' , '=' , 't.category_id')
                ->leftJoin('task_comments as tc' , 'tc.task_id' , '=' , 't.task_id')
                ->where("ta.user_id" ,  Auth::user()->id)
                ->where("t.status" , 'A')
                ->where("t.task_status" , '=' , 'N')
                ->when($title , function($query) use ($title) {
                    return $query->where("t.title" , 'LIKE' , "%".$title."%");
                })
                ->when($task_id , function($query) use ($task_id) {
                    return $query->where("t.task_id" , '=' , $task_id);
                })
                ->when($project , function($query) use ($project) {
                    return $query->where("t.category_id" , '=' , $project);
                })

                ->orderBy('tc.date_added' ,'DESC')
                ->orderBy('t.task_id' ,'DESC')
                ->paginate(5);
           
            if (!empty($result)) {
                

                # FOR NOTIFICATION
                if (isset($data->t) && !empty($data->t) && !isset($data->c) ) {
                   if (isset($result[0]->task_id)) {
                       $taskID = $result[0]->task_id;
                       $task = Task::find($taskID);
                       $task->get_notif;
                       if ($task->get_notif) {
                           $notif = $task->get_notif->find($taskID)->where('action_type' , 'T')->where('user_id' , Auth::user()->id);
                           $notif_array = [
                                "seen" => 1 ,
                                "seen_date" =>date("Y-m-d H:i:s" , time())
                           ];

                           $notif->update($notif_array);
                       }
                   }

                } 

                if (isset($data->c) && !empty($data->c) && isset($data->t) && !empty($data->t)  )  {                 
                      if (isset($result[0]->task_id)) {
                           $taskID = $result[0]->task_id;
                           $task = Task::find($taskID);
                           $task->get_notif;
                           if ($task->get_notif) {
                            $notif = Notifications::find($taskID)
                                    ->where('task_notification_id' ,$data->c )
                                    ->where('action_type' ,'C' )
                                    ->get()->first();
                            $notif->seen  = 1;
                            $notif->seen_date  = date("Y-m-d H:i:s" , time());
                            $notif->save();                    
                           }
                      }
                }


                # GET ALL ASSIGNEE BASED ON TASK 
                foreach ($result->items() as $key => $value) {  
                        //$group['task_ids'][] = $value->task_unique; 
                        # USE NORMAL QUERY FOR GETTING THE ASSIGNEE
                        
                        $result->items()[$key]->assignee = DB::select('SELECT * FROM task_assign ta
                            INNER JOIN users u ON u.id =  ta.user_id
                            LEFT JOIN task_comments tc ON tc.user_id = u.id AND tc.task_id = '.$value->task_id.' 
                            LEFT JOIN profile_image pi ON pi.user_id = u.id AND pi.is_profile = 1 AND pi.status IS  NULL 
                            WHERE ta.task_id = '.$value->task_id.' AND ta.is_created != 1 group by ta.user_id ');
                        # GET THE IMAGES OF TASK 
                        $result->items()[$key]->attachment = DB::table('tasks_attachment as ta')
                                                        ->select('*')
                                                        ->where("ta.task_id" , $value->task_id)
                                                        ->where("ta.status" , 1)
                                                        ->get();
                        # GET THE LAST COMMENT
                        $result->items()[$key]->last_comment = DB::table('task_comments as tc')
                                                        ->select('*')
                                                        ->join("users as u" , 'u.id' ,  '=' , 'tc.user_id')
                                                        ->leftJoin("profile_image as pi" , function($q) {
                                                            $q->on('u.id' ,  '=' , 'pi.user_id')
                                                            ->where('pi.status' , null)
                                                            ->where('pi.is_profile', 1);
                                                        })
                                                        ->where("tc.task_id" , $value->task_id)
                                                        ->orderBy('tc.task_comments_id' ,'DESC')
                                                        ->get()->first();  
                        
                        # GET ALL UNREAD MESSAGE (NOTIFICATION)
                        $result->items()[$key]->unread = DB::table('task_notification as tc')
                                                        ->select('*')
                                                        ->where("tc.task_id" , $value->task_id)
                                                        ->where("tc.user_id" , Auth::user()->id )
                                                        ->where("tc.seen" , 0 )
                                                        ->where("tc.action_type" , 'C' )
                                                        ->count('*'); 
                    
                }

                //if(isset($group['task_ids'])){
                //    $group['last_task_id'] = json_encode(($group['task_ids']));
                //}
                return $result;
            }

    }

    # GET WAITING FOR ANSWER
    public function get_waiting_for_answer (Request $data) {
        if (isset($data->s) && !empty($data->s) ) {
            $title = $data->s;
        } else{
            $title = "";
        }
        
        if (isset($data->t) && !empty($data->t) ) {
            $task_id = $data->t;
        } else{
            $task_id = "";
        }

        if (isset($data->p) && !empty($data->p) ) {
            $project = $data->p;
        } else{
            $project = "";
        }

        // $group    = array();
        // $newArray = array();
        // $array    = array();

        // $result = DB::table('tasks as t')
        //        ->select('*' , 't.created_by as task_creator' , 't.task_id as task_id' , 't.created_at as task_created')
        //        ->join('task_assign as ta', 'ta.task_id', '=', 't.task_id')
        //        ->join('users as u' , 'u.id' , '=' , 't.created_by')
        //        ->join('task_categories as tcc' , 'tcc.category_id' , '=' , 't.category_id')
        //        ->leftJoin('task_comments as tc' , 'tc.task_id' , '=' , 't.task_id')
        //        ->where("ta.user_id" ,  Auth::user()->id)
        //        ->where("t.status" , 'A')
        //        ->where("t.task_status" , '!=' , 'D')
        //        ->orderBy('tc.date_added' ,'DESC')
        //        ->orderBy('t.task_id' ,'DESC')
        //        ->get();


        $result = DB::table('tasks as t')
                    ->select('t.task_id as task_id',
                             't.task_unique',
                             't.title',
                             't.category_id',
                             't.task_percentage',
                             't.description',
                             't.created_by as task_creator',
                             't.created_at as task_created',
                             't.deadline',
                             't.date_finished',
                             't.task_status',
                             't.status',
                             't.type',
                             # Task Comment
                             'tc.task_comments_id',
                             'tc.comment',
                             'tc.user_id as last_comment_user',
                             'tc.seen',
                             'tc.seen_date',
                             'tc.system_generated',
                             'tc.action_type',
                             'tc.date_added as latest_comment_date', 
                             # Task Category
                             'tcc.category_name',
                             # User
                             'u.firstname',
                             'u.lastname')
                    ->join('task_assign as ta', 'ta.task_id', '=', 't.task_id')
                    ->join('users as u' , 'u.id' , '=' , 't.created_by')
                    ->join('task_categories as tcc' , 'tcc.category_id' , '=' , 't.category_id')
                    ->leftJoin('task_comments as tc' , 'tc.task_id' , '=' , 't.task_id')
                    ->where('ta.user_id' ,  Auth::user()->id)
                    ->where('t.status' , 'A')
                    ->whereIn('t.task_status' , ['C','I'])
                    //->whereRaw("tc.date_added = ( SELECT MAX(tc2.date_added) FROM task_comments tc2 WHERE tc2.task_id = tc.task_id )")
                    ->whereRaw("tc.date_added = ( SELECT tc2.date_added FROM task_comments tc2 WHERE tc2.task_id = tc.task_id order by tc2.task_comments_id DESC LIMIT 1 )")
                    ->where('tc.user_id',Auth::user()->id)
                    ->when($title , function($query) use ($title) {
                        return $query->where("t.title" , 'LIKE' , "%".$title."%");
                    })
                    ->when($task_id , function($query) use ($task_id) {
                        return $query->where("t.task_id" , '=' , $task_id);
                    })
                    ->when($project , function($query) use ($project) {
                        return $query->where("t.category_id" , '=' , $project);
                    })
                    ->orderBy('latest_comment_date', 'DESC')
                    //->orderBy('t.task_id' ,'DESC')
                    //->distinct(['t.task_id'])
                    ->paginate(5);

          
            
            
            if (!empty($result)) {


                # FOR NOTIFICATION
                if (isset($data->t) && !empty($data->t) && !isset($data->c) ) {
                   if (isset($result[0]->task_id)) {
                       $taskID = $result[0]->task_id;
                       $task = Task::find($taskID);
                       $task->get_notif;
                       if ($task->get_notif) {
                           $notif = $task->get_notif->find($taskID)->where('action_type' , 'T')->where('user_id' , Auth::user()->id);
                           $notif_array = [
                                "seen" => 1 ,
                                "seen_date" =>date("Y-m-d H:i:s" , time())
                           ];

                           $notif->update($notif_array);
                       }
                   }

                } 

                if (isset($data->c) && !empty($data->c) && isset($data->t) && !empty($data->t)  )  {
                      if (isset($result[0]->task_id)) {
                           $taskID = $result[0]->task_id;
                           $task = Task::find($taskID);
                           $task->get_notif;
                           if ($task->get_notif) {
                            $notif = Notifications::find($taskID)
                                    ->where('task_notification_id' ,$data->c )
                                    ->where('action_type' ,'C' )
                                    ->get()->first();
                            $notif->seen  = 1;
                            $notif->seen_date  = date("Y-m-d H:i:s" , time());
                            $notif->save();                    
                           }
                      }
                }

           
            // foreach ($result as $key => $value) {
            //     if ( !in_array($value->task_id, $array) ) {
            //         $array[] = $value->task_id;
            //         $newArray[] = $value;
            //     }
            // }

            //$c = [];
            # SET STATUS BASED ON USERS
            // foreach ($newArray as $key => $row) {
            //     # check if user is the last comment
            //     $check = DB::table('task_comments as tc')
            //                     ->select('*')
            //                     ->where("tc.task_id" , $row->task_id)
            //                     ->orderBy('tc.task_comments_id' ,'DESC')
            //                     ->get()->first();

                
            //     if (!empty($check)) {
            //         if ($check->user_id == Auth::user()->id) {
            //             $c[] = $check->user_id;
            //             if ($key <=10) {
            //                 $group['I'][$key] = $row; 
            //             }
            //         }
            //     }
            // }

           // $result['total_task'] = count($result);
            if(count($result) > 0 ){
                # GET ALL ASSIGNEE BASED ON TASK 
                foreach ($result as $key => $value) {
                    # USE NORMAL QUERY FOR GETTING THE ASSIGNEE
                    $result[$key]->assignee = DB::select('SELECT * FROM task_assign ta
                        INNER JOIN users u ON u.id =  ta.user_id 
                        LEFT JOIN task_comments tc ON tc.user_id = u.id AND tc.task_id = '.$value->task_id.'  
                        LEFT JOIN profile_image pi ON pi.user_id = u.id AND pi.is_profile = 1 AND pi.status IS  NULL 
                        WHERE ta.task_id = '.$value->task_id.' AND ta.is_created != 1 group by ta.user_id');
                   
                    # GET THE IMAGES OF TASK 
                    $result[$key]->attachment = DB::table('tasks_attachment as ta')
                                                    ->select('*')
                                                    ->where('ta.task_id' , $value->task_id)
                                                    ->where('ta.status' , 1)
                                                    ->get();
                    # GET THE LAST COMMENT
                    $result[$key]->last_comment = DB::table('task_comments as tc')
                                                    ->select('*')
                                                    ->join('users as u' , 'u.id' ,  '=' , 'tc.user_id')
                                                    ->leftJoin('profile_image as pi' , function($q) {
                                                        $q->on('u.id' ,  '=' , 'pi.user_id')
                                                        ->where('pi.status' , null)
                                                        ->where('pi.is_profile', 1);
                                                    })
                                                    ->where('tc.task_id' , $value->task_id)
                                                    ->orderBy('tc.task_comments_id' ,'DESC')
                                                    ->get()->first();  
                    //GET ALL UNREAD MESSAGE (NOTIFICATION)
                    $result[$key]->unread = DB::table('task_notification as tc')
                        ->select('*')
                        ->where('tc.task_id' , $value->task_id)
                        ->where('tc.user_id' , Auth::user()->id )
                        ->where('tc.seen' , 0 )
                        ->where('tc.action_type' , 'C' )
                        ->count('*'); 
                }
            }

          
            return $result;

        } // end check if empty
    }

    # GET WAITING FOR ME
    public function get_waiting_for_me (Request $data) {

        if (isset($data->s) && !empty($data->s) ) {
            $title = $data->s;
        } else{
            $title = "";
        }

        if (isset($data->t) && !empty($data->t) ) {
            $task_id = $data->t;
        } else{
            $task_id = "";
        }

        if (isset($data->p) && !empty($data->p) ) {
            $project = $data->p;
        } else{
            $project = "";
        }
        // $group    = array();
        // $newArray = array();
        // $array    = array();
        // $result = DB::table('tasks as t')
        //         ->select('*' , 't.created_by as task_creator' , 't.task_id as task_id' , 't.created_at as task_created')
        //         ->join('task_assign as ta', 'ta.task_id', '=', 't.task_id')
        //         ->join('users as u' , 'u.id' , '=' , 't.created_by')
        //         ->join('task_categories as tcc' , 'tcc.category_id' , '=' , 't.category_id')
        //         ->leftJoin('task_comments as tc' , 'tc.task_id' , '=' , 't.task_id')
        //         ->where("ta.user_id" ,  Auth::user()->id)
        //         ->where("t.status" , 'A')
        //         ->where("t.task_status" , '!=' , 'D')
        //         ->orderBy('tc.date_added' ,'DESC')
        //         ->orderBy('t.task_id' ,'DESC')
        //         ->get();
        
        
        $result = DB::table('tasks as t')
                        ->select('t.task_id as task_id',
                                't.task_unique',
                                't.title',
                                't.category_id',
                                't.task_percentage',
                                't.description',
                                't.created_by as task_creator',
                                't.created_at as task_created',
                                't.deadline',
                                't.date_finished',
                                't.task_status',
                                't.status',
                                't.type',
                               
                                # Task Comment
                                'tc.task_comments_id',
                                'tc.comment',
                                'tc.user_id as last_comment_user',
                                'tc.seen',
                                'tc.seen_date',
                                'tc.system_generated',
                                'tc.action_type',
                                'tc.date_added as latest_comment_date', 
                                # Task Category
                                'tcc.category_name',
                                # User
                                'u.firstname',
                                'u.lastname')
                        ->join('task_assign as ta', 'ta.task_id', '=', 't.task_id')
                        ->join('users as u' , 'u.id' , '=' , 't.created_by')
                        ->join('task_categories as tcc' , 'tcc.category_id' , '=' , 't.category_id')
                        ->leftJoin('task_comments as tc' , 'tc.task_id' , '=' , 't.task_id')
                        ->where('ta.user_id' ,  Auth::user()->id)
                        ->where('t.status' , 'A')
                        ->whereIn('t.task_status' , ['C','I'])
                        //->whereRaw("tc.date_added = ( SELECT MAX(tc2.date_added) FROM task_comments tc2 WHERE tc2.task_id = tc.task_id )")
                        ->whereRaw("tc.date_added = ( SELECT tc2.date_added FROM task_comments tc2 WHERE tc2.task_id = tc.task_id order by tc2.task_comments_id DESC LIMIT 1 )")
                        ->whereNotIn('tc.user_id',[Auth::user()->id])
                        ->when($title , function($query) use ($title) {
                            return $query->where("t.title" , 'LIKE' , "%".$title."%");
                        })
                        ->when($task_id , function($query) use ($task_id) {
                            return $query->where("t.task_id" , '=' , $task_id);
                        })
                        ->when($project , function($query) use ($project) {
                            return $query->where("t.category_id" , '=' , $project);
                        })
                        ->orderBy('latest_comment_date', 'DESC')
                        //->orderBy('t.task_id' ,'DESC')
                        //->distinct(['t.task_id'])
                        ->paginate(5);

                    
        
        if (!empty($result)) {
                            
            # FOR NOTIFICATION
            if (isset($data->t) && !empty($data->t) && !isset($data->c)  ) {
               
               if (isset($result[0]->task_id)) {
                   $taskID = $result[0]->task_id;
                   $task = Task::find($taskID);
                   $task->get_notif;
                   if ($task->get_notif) {
                       $notif = $task->get_notif->find($taskID)->where('action_type' , 'T')->where('user_id' , Auth::user()->id);
                       $notif_array = [
                            "seen" => 1 ,
                            "seen_date" =>date("Y-m-d H:i:s" , time())
                       ];

                       $notif->update($notif_array);
                   }
               }

            } 

            if (isset($data->c) && !empty($data->c) && isset($data->t) && !empty($data->t)  )  {
                if (isset($result[0]->task_id)) {
                        $taskID = $result[0]->task_id;
                       $task = Task::find($taskID);
                       $task->get_notif;

                       if ($task->get_notif) {
                        $notif = Notifications::find($taskID)
                                ->where('task_notification_id' ,$data->c )
                                ->where('action_type' ,'C' )
                                ->get()->first();
                        $notif->seen  = 1;
                        $notif->seen_date  = date("Y-m-d H:i:s" , time());
                        $notif->save();                    
                       }
                }                 
            }
           
            // foreach ($result as $key => $value) {
            //     if ( !in_array($value->task_id, $array) ) { 
            //         $array[] = $value->task_id; 
            //         $newArray[] = $value; 
            //     } 
            // }

            // $c = [];
            // # SET STATUS BASED ON USERS
            // foreach ($newArray as $key => $row) {
            //     # check if user is the last comment
            //     $check = DB::table('task_comments as tc')
            //                     ->select('*')
            //                     ->where("tc.task_id" , $row->task_id)
            //                     ->where("tc.action_type" , 'C')
            //                     ->orderBy('tc.task_comments_id' ,'DESC')
            //                     ->get()->first();
        
            // if (!empty($check)) {
            //     if ($check->user_id != Auth::user()->id) {
            //         $c[] = $check->user_id;
            //         if ($key <=10) {
            //                 $group['C'][$key] = $row; 
            //             }
            //         }
                    
            //     }
            // }
            // $group['total_task'] = count($result);
        
            # GET ALL ASSIGNEE BASED ON TASK 
            if(isset($result)){
                foreach ($result as $key => $value) {
               
                    # USE NORMAL QUERY FOR GETTING THE ASSIGNEE
                    $result[$key]->assignee = DB::select('SELECT * FROM task_assign ta
                        INNER JOIN users u ON u.id =  ta.user_id
                        LEFT JOIN task_comments tc ON tc.user_id = u.id AND tc.task_id = '.$value->task_id.' 
                        LEFT JOIN profile_image pi ON pi.user_id = u.id AND pi.is_profile = 1 AND pi.status IS  NULL 
                        WHERE ta.task_id = '.$value->task_id.' AND ta.is_created != 1 group by ta.user_id ');
                    
                    # GET THE IMAGES OF TASK 
                    $result[$key]->attachment = DB::table('tasks_attachment as ta')
                                                    ->select('*')
                                                    ->where("ta.task_id" , $value->task_id)
                                                    ->where("ta.status" , 1)
                                                    ->get();
                    # GET THE LAST COMMENT
                    $result[$key]->last_comment = DB::table('task_comments as tc')
                                                    ->select('*')
                                                    ->join("users as u" , 'u.id' ,  '=' , 'tc.user_id')
                                                    ->leftJoin("profile_image as pi" , function($q) {
                                                        $q->on('u.id' ,  '=' , 'pi.user_id')
                                                        ->where('pi.status' , null)
                                                        ->where('pi.is_profile', 1);
                                                    })
                                                    ->where("tc.task_id" , $value->task_id)
                                                    ->orderBy('tc.task_comments_id' ,'DESC')
                                                    ->get()->first();  
                    //GET ALL UNREAD MESSAGE (NOTIFICATION)
                    $result[$key]->unread = DB::table('task_notification as tc')
                        ->select('*')
                        ->where("tc.task_id" , $value->task_id)
                        ->where("tc.user_id" , Auth::user()->id )
                        ->where("tc.seen" , 0 )
                        ->where("tc.action_type" , 'C' )
                        ->count('*'); 
        
                }
            }
        
            return $result;

        } // end check if empty  
    }

    # GET TASKS     
    public function tasks () {

        $group    = array();
        $newArray = array();
        $array    = array();
        
        if (Input::get('isSearch')) {
            if (Input::get('assignee') && !empty(Input::get('assignee'))) {
                $result = DB::table('tasks as t')
                    ->select('*' , 't.created_by as task_creator' , 't.task_id as task_id' , 't.created_at as task_created')
                    ->join('task_assign as ta', 'ta.task_id', '=', 't.task_id')
                    ->join('users as u' , 'u.id' , '=' , 't.created_by')
                    ->join('task_categories as tcc' , 'tcc.category_id' , '=' , 't.category_id')
                    ->leftJoin('task_comments as tc' , 'tc.task_id' , '=' , 't.task_id')
                    ->whereIn("ta.user_id" ,  Input::get('assignee'))
                    ->where("t.status" , 'A')
                    ->where("t.task_status" , '!=' , 'D')
                    ->orderBy('tc.date_added' ,'DESC')
                    ->orderBy('t.task_id' ,'DESC')
                    ->get();
            } elseif (Input::get('project-id') && !empty(Input::get('project-id'))) {
                $result = DB::table('tasks as t')
                    ->select('*' , 't.created_by as task_creator' , 't.task_id as task_id' , 't.created_at as task_created')
                    ->join('task_assign as ta', 'ta.task_id', '=', 't.task_id')
                    ->join('users as u' , 'u.id' , '=' , 't.created_by')
                    ->join('task_categories as tcc' , 'tcc.category_id' , '=' , 't.category_id')
                    ->leftJoin('task_comments as tc' , 'tc.task_id' , '=' , 't.task_id')
                    ->where("ta.user_id" ,  Auth::user()->id)
                    ->where("t.status" , 'A')
                    ->where("t.task_status" , '!=' , 'D')
                    ->where("t.category_id" , Input::get('project-id') )
                    ->orderBy('tc.date_added' ,'DESC')
                    ->orderBy('t.task_id' ,'DESC')
                    ->get();
            }
        } else {
            $result = DB::table('tasks as t')
                ->select('*' , 't.created_by as task_creator' , 't.task_id as task_id' , 't.created_at as task_created')
                ->join('task_assign as ta', 'ta.task_id', '=', 't.task_id')
                ->join('users as u' , 'u.id' , '=' , 't.created_by')
                ->join('task_categories as tcc' , 'tcc.category_id' , '=' , 't.category_id')
                ->leftJoin('task_comments as tc' , 'tc.task_id' , '=' , 't.task_id')
                ->where("ta.user_id" ,  Auth::user()->id)
                ->where("t.status" , 'A')
                ->where("t.task_status" , '!=' , 'D')
                ->orderBy('tc.date_added' ,'DESC')
                ->orderBy('t.task_id' ,'DESC')
                ->get();
        }

        foreach ($result as $key => $value) {
            if ( !in_array($value->task_id, $array) ) { 
                $array[] = $value->task_id; 
                $newArray[$key] = $value; 
            } 
        }
        
        # SET STATUS BASED ON USERS
        foreach ($newArray as $key => $row) {
            #THE LAST USERS COMMENT 
            $last_user_comment =  DB::table('task_comments')->select("user_id")
            ->where('task_id' , $row->task_id)
            ->orderBy('task_comments_id', 'DESC')
            ->get()
            ->first();

            # CHECK IF USER LOGGED IS EQUAL TO CREATOR
            if ($last_user_comment) {
                if ($row->task_creator ==  Auth::user()->id ) {
                    # CHECK IF THE LAST COMMENT USERS
                    if ($row->status == 'N') {
                        $status = 'N';
                    } elseif ($row->status == 'D') {
                        $status = 'D';
                    }elseif ($last_user_comment->user_id == Auth::user()->id ) {
                        $status = 'I';
                    } else{
                        $status = 'C';
                    }
                } else {
                   # CHECK IF THE LAST COMMENT USERS
                    if ($row->status == 'N') {
                        $status = 'N';
                    } elseif ($row->status == 'D') {
                        $status = 'D';
                    }elseif ($last_user_comment->user_id != $row->task_creator ) {
                        $status = 'I';
                    } else{
                        $status = 'C';
                    }
                }   
                $group[$status][$key] = $row;
            } else {
                $group[$row->task_status][$key] = $row;
            }  
        }

        # GET ALL ASSIGNEE BASED ON TASK 
        foreach ($group as $task_status => $value) {
            foreach ($value as $key => $row) {
                # USE NORMAL QUERY FOR GETTING THE ASSIGNEE
                $group[$task_status][$key]->assignee = DB::select('SELECT * FROM task_assign ta
                    INNER JOIN users u ON u.id =  ta.user_id
                    LEFT JOIN task_comments tc ON tc.user_id = u.id AND tc.task_id = '.$row->task_id.' 
                    LEFT JOIN profile_image pi ON pi.user_id = u.id AND pi.is_profile = 1 AND pi.status IS  NULL 
                    WHERE ta.task_id = '.$row->task_id.' AND ta.is_created != 1 group by ta.user_id ');
                # GET THE IMAGES OF TASK 
                $group[$task_status][$key]->attachment = DB::table('tasks_attachment as ta')
                                                ->select('*')
                                                ->where("ta.task_id" , $row->task_id)
                                                ->where("ta.status" , 1)
                                                ->get();
                # GET THE LAST COMMENT
                $group[$task_status][$key]->last_comment = DB::table('task_comments as tc')
                                                ->select('*')
                                                ->join("users as u" , 'u.id' ,  '=' , 'tc.user_id')
                                                 ->leftJoin("profile_image as pi" , function($q) {
                                                    $q->on('u.id' ,  '=' , 'pi.user_id')
                                                    ->where('pi.status' , null)
                                                    ->where('pi.is_profile', 1);
                                                })
                                                ->where("tc.task_id" , $row->task_id)
                                                ->orderBy('tc.task_comments_id' ,'DESC')
                                                ->get()->first();  
                //GET ALL UNREAD MESSAGE (NOTIFICATION)
                $group[$task_status][$key]->unread = DB::table('task_notification as tc')
                    ->select('*')
                    ->where("tc.task_id" , $row->task_id)
                    ->where("tc.user_id" , Auth::user()->id )
                    ->where("tc.seen" , 0 )
                    ->where("tc.action_type" , 'C' )
                    ->count('*'); 

            }
        }
    
        return $group;
    }

    # FUNCTION TO VIEW THREADS ON MODAL
    public function view_threads (Request $request) {
        $group = array();
        $result = DB::table('tasks as t')
            ->select('*' , 't.created_by as task_creator' )
            ->join('task_assign as ta', 'ta.task_id', '=', 't.task_id')
            ->join('users as u' , 'u.id' , '=' , 't.created_by')
            ->where("t.task_id" , $request->task_id)
            ->get()->first();

         # GET ALL ASSIGNE BASED ON TASK 
        $result->assignee = DB::table('task_assign as ta')
                                        ->select('*')
                                        ->join('users as u' , 'u.id' , '=' , 'ta.user_id')
                                        ->where("ta.task_id" , $request->task_id)
                                        ->where("ta.is_created" , '!=' , 1)
                                        ->get();
        # GET THE IMAGES OF TASK 
        $result->attachment = DB::table('tasks_attachment as ta')
                                        ->select('*')
                                        ->where("ta.task_id" , $request->task_id)
                                        ->where("ta.status" , 1)
                                        ->get();

        # GET ALL COMMENTS AND IMAGES
        $comment_table = DB::table('task_comments as tc')
                                        ->join("users as u" , 'u.id' , '=' , 'tc.user_id')
                                        ->select('*')
                                        ->where("tc.task_id" , $request->task_id)
                                        ->leftJoin("profile_image as pi" , function($q) {
                                            $q->on('u.id' ,  '=' , 'pi.user_id')
                                            ->where('pi.status' , null)
                                            ->where('pi.is_profile', 1);
                                        })
                                        ->orderBy("tc.task_comments_id" , 'DESC')
                                        ->paginate(5);

                             
                                    
        $results = array();
        foreach ($comment_table->items() as $key => $value) {
          
            # Default image
            if($value->image == null){
                $value->image = url('/').'/public/images/icons/default-user.png';
            } else {# Profile Image
                $value->image = url('/').'/public/profile/'.$value->id.'/thumbnail_profile/'.$value->image;
            }
            
            $results[$key]['data'] = $value;
            $results[$key]['images']  = DB::table('tasks_reply_attachment as tra')
                                        ->select('*')
                                        ->where("tra.task_comments_id" , $value->task_comments_id)
                                        ->where("tra.status" , 1)
                                        ->get();

            
        }

        $pagination['current_page']  =  $comment_table->currentPage();
        $pagination['last_page']     =  $comment_table->lastPage();
        $pagination['per_page']      =  $comment_table->perPage();
        $pagination['total']         =  $comment_table->total(); 

        # Button translation
        $result->button['lang_deadline'] = __('translation.deadline');
        $result->button['lang_task_complete'] = __('translation.task_complete');
        $result->button['lang_reply'] = __('translation.reply');
        $result->button['lang_show_threads'] = __('translation.show_threads');

        # CHANGE ALL UNSEEN TO SEEN ON TASK PER USER
        DB::table('task_notification')
            ->where('task_id', $request->task_id)
            ->where('user_id', Auth::user()->id  )
            ->update(['seen' => 1 ]);   

        //$result->storage_checked  =$this->check_storage(true);
        $result->comment = $results;
        $result->pagination = $pagination;
        return response()->json($result);
        //die(json_encode( $result ) );
    }

    # Load more Comment on task
    public function loadmore_comment(Request $request){
        
        # GET ALL COMMENTS AND IMAGES
        $comment_table = DB::table('task_comments as tc')
                                ->join("users as u" , 'u.id' , '=' , 'tc.user_id')
                                ->select('*')
                                ->where("tc.task_id" , $request->task_id)
                                ->leftJoin("profile_image as pi" , function($q) {
                                    $q->on('u.id' ,  '=' , 'pi.user_id')
                                    ->where('pi.status' , null)
                                    ->where('pi.is_profile', 1);
                                })
                                ->orderBy("tc.task_comments_id" , 'DESC')
                                ->paginate(5);
        
        if(!empty($comment_table->items())){
            foreach($comment_table->items() as $k=>$v){                        
                # Default image
                if($comment_table->items()[$k]->image == null){
                    $comment_table->items()[$k]->image = url('/').'/public/images/icons/default-user.png';
                } else {# Profile Image
                    $comment_table->items()[$k]->image = url('/').'/public/profile/'.$comment_table->items()[$k]->id.'/thumbnail_profile/'.$comment_table->items()[$k]->image;
                }
            }
        }

        return $comment_table;

    }

    // Function to get the user's timezone in the "Region/City" format
public function getUserTimezone() {
    $utcOffset = date('Z'); // Get the UTC offset in seconds

    // Loop through all possible timezones
    foreach (timezone_identifiers_list() as $timezone) {
        $timezoneObject = new \DateTimeZone($timezone);
        $currentUtcOffset = $timezoneObject->getOffset(new \DateTime('now', new \DateTimeZone('UTC')));

        // Compare the UTC offsets
        if ($currentUtcOffset == $utcOffset) {
            return $timezone;
        }
    }

    // Default to a fallback timezone (e.g., UTC) if the user's timezone is not found
    return 'UTC';
}

    # Reply_thread 
    public function reply_thread (Request $data) {
        
        
        # VALIDATION FOR COUNTINGNUMBER OF CHARACTERS
        // $str=$data->reply;
        // $str = preg_replace("/[^A-Za-z]/","",$str);
        
        // echo strlen($str);//14
        
        $tz = Auth::user()->timezone ? Auth::user()->timezone : $this->getUserTimezone();
        $timestamp = time();
        $dt = new \DateTime("now", new \DateTimeZone($tz)); //first argument "must" be a string
        $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
        $dt->format('Y-m-d H:i:s');
        $formatted =  (string) date("Y-m-d H:i:s",strtotime($dt->format('Y-m-d h:i:s')));
        
        # ADD COMMENT ON THREAD
        // $storage_checked = $this->check_storage(true);
        // if ($storage_checked == true) {
        //     return Redirect::back()->withErrors("You already used all of your MB storage !");
        //     return false;
        // }
        // $this->storage($data->reply , false , false ,  $data->image  , 'C' , false);
        $task_comments_id = DB::table('task_comments')->insertGetId([
            'comment'     => $data->reply, 
            'raw_comment' => htmlentities($data->reply, ENT_QUOTES, 'UTF-8') , 
            'action_type' => htmlentities('C', ENT_QUOTES, 'UTF-8') , 
            'user_id'     => Auth::user()->id ,
            'task_id'     => $data->task_id ,
            'seen_date'   => NULL,
            'date_added'  => $formatted
        ]);

                
        // Upload file new version
        $this->uploadFiles($data->file('files'),'tasks_reply_attachment', $data->task_id, $task_comments_id);

        //$this->uploadFilesComments($data->image,'/task_reply_attachment/', $data['task_id'], $task_comments_id );
        
        # ADD NOTIFICATION 
        ############# GET THE ASSIGNEE ON TASK ##################
        $assignees  = DB::table('task_assign as ta')
                    ->select('user_id')
                    ->where("task_id" , $data->task_id)
                    ->where("user_id" ,'!=', Auth::user()->id ) 
                    ->get();

        foreach ($assignees as $key => $value) {
            # ADD NOTIFICATION ON EVERY ASSIGNEE
            DB::table('task_notification')->insertGetId([
                'comment'     => htmlspecialchars($data->reply, ENT_QUOTES, 'UTF-8'), 
                'action_type' => htmlspecialchars('C', ENT_QUOTES, 'UTF-8') , 
                'user_id'     => $value->user_id ,
                'task_id'     => $data->task_id ,
                'notif_by'    => Auth::user()->id ,
                'seen_date'   => NULL ,
                'date_added'  => $formatted
            ]);    
        }            

        if ($data->task_status == 'N') {
            $task_status = 'I';
        } elseif ($data->task_status == 'I') {
            $task_status = 'C';
        } elseif ($data->task_status == 'C') {
            $task_status = 'I';
        }else {
            $task_status = 'C';
        }
        
        $result = DB::table('tasks')
            ->where('task_id', $data->task_id)
            ->update(['task_status' => $task_status ]);

        
        $comment = TaskComment::find($task_comments_id);
        $comment->get_attachment;

        return response()->json([
            'status'  => true,
            'data'    => $comment,
            'message' => 'Success'
        ]);
    }

    public function todo_task (Request $request) {
        $User = User::find(Auth::user()->id);
        switch ($request->action) {
            case 'add_todo':
                $validator = Validator::make($request->all(), [
                    'todo_name' => 'required|string',
                    'todo_date' => 'required|date_format:"Y-m-d"'
                 ]);
                if (!$validator->fails()) {
                    //storage 
                    $this->storage($request->todo_name , false , false ,  false  , 'TD'  ,false);
                   
                    //$date = date('yyyy-mm-dd', strtotime($request->todo_date));
                    $new = [
                        'todo_name'   => $request->todo_name,
                        'status'      => 0,
                        'todo_status' => "A",
                        'date_from'   => $request->todo_date,
                        'is_active'   => 1
                    ];
                        
                    if($inserted = $User->todo_list()->create($new)){
                      
                        return response()->json([
                            'status'  => true,
                            'data'    => $inserted,
                            'message' => 'Sucessfully Created Todo...'
                        ]);    
                    }
                    
                    else {
                        return response()->json([
                            'status'  => false,
                            'data'    => null,
                            'message' => 'Please Try Again...'
                        ]);
                    }
                    
                } else {
                    return response()->json([
                        'status'  => false,
                        'data'    => null,
                        'message' => $validator->messages()->getMessages()
                    ]);
                }         
            break;
            case 'update':
                $validator = Validator::make($request->all(), ['todo_name' => 'required|max:255']);
                //storage 
                $this->storage($request->todo_name , false , false ,  false  , 'TD'  , false);
                if (!$validator->fails()) { 
                    $todos = $User->todo_list()->find($request->id);
                    $todos->todo_name = $request->todo_name;
                    $todos->date_from = $request->todo_date;
                    $todos->save();       
                    return response()->json(['result'  => 'success','data'=>$todos]);
                } else {
                    return response()->json([
                        'result'  => 'error',
                        'message' => $validator->messages()->getMessages(),
                    ]);
                }
            break;
            case 'mark_as':
                $todos = $User->todo_list()->find($request->id);
                $todos->status = ((int)$todos->status == 0 ? 1 : 0);
                if ((int)$todos->status == 0) $todos->date_finished = "0000-00-00 00:00:00";
                $todos->save();
                return response()->json(['result'  => 'success']);                      
            break;
            case 'delete':
                //Mark Delete on todo
                $todos = $User->todo_list()->find($request->id);
               
                $todos->todo_status = 'R';
                $todos->save();
                // Add on Logs
                $delete = array('todo_list_id' => $request->id);
                $User->todo_logs()->create($delete);
                return response()->json(['result'  => 'success']);                      
            break;
            case 'hide_task':
                $todos = $User->todo_list()
                                ->Where('is_active',1)
                                ->Where('todo_status','A')
                                ->where('status',1) //task completed
                                ->update(['is_active'=>'0']);
                return response()->json(['result'  => 'success','as'=>$todos]);
            break;
            default:
                # code...
                break;
        }

    }

    public function task_actions (Request $request) {

      

        $task = Task::where('task_id',$request->task_id)->first();

        switch ($request->action) {
            case 'show_edit':

                $assignee = [];
                foreach ($task->get_assign as $value) {
                    $assignee[] = $value->get_user;
                }

                // return response()->json([
                //      'task'       => $task,
                //      'assignee'   => $assignee,
                //      'attachment' => $task->get_attachment->where('status',1)
                // ]);
                
                $task->assignee = $assignee;
                $task->attachments = $task->get_attachment->where('status',1);

                foreach ($task->get_assign as $key => $value) {
                    if($value->assignee_profile == null){
                        $task->get_assign[$key]->image = url('/').'/public/images/icons/default-user.png';
                        unset($task->get_assign[$key]->assignee_profile);
                    }else if ( $task->get_assign[$key]->assignee_profile->image == null && $task->get_assign[$key]->assignee_profile != null) {
                        $task->get_assign[$key]->image = url('/').'/public/images/icons/default-user.png';
                        unset($task->get_assign[$key]->assignee_profile);
                    } else {
                        $task->get_assign[$key]->image = url('/').'/public/profile/'.$task->get_assign[$key]->assignee_profile->user_id.'/thumbnail_profile/'.$task->get_assign[$key]->assignee_profile->image;
                        unset($task->get_assign[$key]->assignee_profile);
                    }
                }

                return response()->json([
                    'status'  => true,
                    'data'    => $task,
                    'message' => 'Show task...' 
                ]);

                break;
            
            case 'remove_task_attachment':

                if($task->created_by == Auth::user()->id) {

                    $file = $task->get_attachment->find($request->task_attachment_id);
                    $file->status = 0;

                    if($file->save()){
                        return response()->json([
                            'status'  => true,
                            'data'    => $file,
                            'message' => 'File successfully removed...' 
                        ]);
                    } 
                    
                    else {
                        return response()->json([
                            'status'  => false,
                            'data'    => null,
                            'message' => 'Server timeout please try again...' 
                        ]);
                    }
               
                }
                
                # Not Creator of the task 
                else {
                    return response()->json([
                        'status'  => false,
                        'data'    => null,
                        'message' => 'Sorry your not authorize to do that...' 
                    ]);
                }

            break;
            
            default:
                # code...
                break;
        }
    }

    # Get all categories
    public function get_categories () {
        $result = DB::table('task_categories')
            ->select('*')
            ->where("created_by" , '=' , Auth::user()->parent_business_owner_id)
            ->where("status" , 1)
            ->get();
        return response()->json($result);
    }  

    # GET BUSINESS TEAM AND PERSONAL TEAM
    public function get_teams(){
        return json_encode([
            "get_my_business_team" => app('App\Http\Controllers\MemberController')->get_my_business_team(),
            "get_my_personal_team" => app('App\Http\Controllers\MemberController')->get_my_personal_team()
        ]);  
    } 

    # For pages
    public function get_categories_view () {
        $result = DB::table('task_categories')
            ->select('*')
            ->where('created_by' ,  Auth::user()->parent_business_owner_id )
            ->get();
        return $result;
    }

    public static function get_task_under_categories (Request $request) {
        $result = DB::table('tasks as t')
            ->select('*')
            ->join('task_categories as tc' , 'tc.category_id' ,'=' ,'t.category_id')
            ->where('category_unique' , $request->category_id)
            ->get();
        return response()->json($result);
    }

    public  function mark_all_as_read (Request $data) {
        DB::table('task_notification')
            ->where('user_id', Auth::user()->id)
            ->update(['seen' => 1 ]);   
    }

    public  function read_notif (Request $data) {
        DB::table('task_notification')
            ->where('task_notification_id', $data->task_notification_id)
            ->update(['seen' => 1 ]);   
    }

    public function removetask (Request $data) {
        $id = DB::table('tasks')
            ->where('task_unique', $data->task_id)
            ->update(['status' => 'H' ]); 
        # LOGS
        $this->task_logs('removetask' , $data->task_id);

        $data = DB::table('tasks')
        ->where('task_unique', $data->task_id)
        ->first(); 
        return response()->json([
            'status' => true,
            'task_id' => $data->task_id 
        ]);
    }  

    public function task_complete (Request $data) {
        DB::table('tasks')
            ->where('task_id', $data->task_id)
            ->update(['task_status' => 'D' ]); 

        # LOGS
        $this->task_logs('task_complete' , $data->task_id);
    }

    public function check_storage ($if_backend_use = false) {
        $is_full = false;
        # GET THE STORAGE USED 
        $storage_used =  DB::table('storage as s')
                        ->select('*')
                        ->join('users as u ' , 'id' , '=' , 'user_id')
                        ->where('parent_business_owner_id' , Auth::user()->parent_business_owner_id  )
                        ->where('user_id' , Auth::user()->parent_business_owner_id  )
                        ->where('status' , 'A'  )
                        ->sum('storage');


       if ($storage_used == 0) {
           if ($if_backend_use) {
                return $is_full;
            } else {
                echo  $is_full;
                die();
            }
       }
        # GET THE SUBSCRIPTION
        $subscription = DB::table('user_subscription')
                ->select('*')
                ->where('user_id' , '=' , Auth::user()->parent_business_owner_id )
                ->where('subscription_status' , 'A' )
                ->sum('amount'); 
            
        if (((int)$subscription)  <=  ($storage_used / 1000 / 1000)) {
            $is_full = true;
            // DB::table('user_subscription')
            //     ->where('user_id', Auth::user()->parent_business_owner_id  )
            //     ->update(['subscription_status' => 'H' ]); 
            // DB::table('storage')
            //     ->where('user_id', Auth::user()->parent_business_owner_id  )
            //     ->update(['status' => 'H' ]); 
        } else{
            $is_full = false;
        }
        if ($if_backend_use) {
            return $is_full;
        } else {
            echo  $is_full;
        }
               
    }

    public function get_remaining_storage ($new_data = false ) {
        # new data  => when user is adding storage // byte 

        # GET THE ACTIVE STORAGE USED BY USER 

        $storage_used =  DB::table('storage as s')
                        ->select('*')
                        ->join('users as u ' , 'id' , '=' , 'user_id')
                        ->where('parent_business_owner_id' , Auth::user()->parent_business_owner_id  )
                        ->where('user_id' , Auth::user()->parent_business_owner_id  )
                        ->where('status' , 'A'  )
                        ->sum('storage');


         # GET THE SUBSCRIPTION
        $subscription = DB::table('user_subscription')
                ->select('*')
                ->where('user_id' , '=' , Auth::user()->parent_business_owner_id )
                ->where('subscription_status' , 'A' )
                ->sum('amount'); 

        $storage_used_mb =  ($storage_used /  1000000);
        $remaining   = (((float) $subscription - ($storage_used_mb) ) ); //mb
        $new_data_mb = number_format($new_data / 1000000 , 10 ) ;

        // CHECK IF NEW DATA IS GREATER THAN REMAINING STORAGE TO BE USED
        if ($new_data_mb  > $remaining ) {
            return Redirect::back()->withErrors("Something went wrong! Please try again. LIMIT ALREADY REACHED");
        }

    }

    public function add_project (Request $request) {
        
        $validator =  Validator::make($request->all(), [
            'project_title' => 'required|string',
        ]);

        if (!$validator->fails()) {
            $category_id = DB::table('task_categories')->insertGetId([
                'category_name' => htmlspecialchars($request->project_title, ENT_QUOTES, 'UTF-8'),  
                'created_by'    => Auth::user()->id ,
            ]);
            $bcrypt = bcrypt($category_id);
            if ($category_id) {
                DB::table('task_categories')
                ->where('category_id', $category_id)
                ->update(['category_unique' => $bcrypt ]);
            }
            return response()->json($bcrypt);
        } else {
            return Redirect::back()->withErrors("Something went wrong! Please try again.");
        }
    }

    public function add_new_member_on_task (Request $request) {
        $result = DB::table('users')
                ->select('*')
                ->whereNotIn('id' , $request->data)
                ->where('created_by' , Auth::user()->parent_business_owner_id)
                ->get();
        return response()->json($result);
    }

    public function add_new_member_on_task_function(Request $request) {
        // get the task id based on unique
        $task_id = DB::table('tasks as t')
            ->select('*')
            ->where('task_unique' , $request->t)
            ->get()->first();

         $users = DB::table('users')
                ->select('*')
                ->where('user_unique' , $request->user_unique)
                ->get()->first();

        DB::table('task_assign')->insertGetId([
            'task_id' => $task_id->task_id ,
            'user_id' => $users->id
        ]);

        # ADD NOTIFICATION ON EVERY ASSIGNEE
        $task_comments_id = DB::table('task_notification')->insertGetId([
            'comment' => NULL,
            'action_type' =>htmlspecialchars('T', ENT_QUOTES, 'UTF-8') , 
            'user_id' =>$users->id ,
            'task_id' => $task_id->task_id  ,
            'notif_by' =>     Auth::user()->id ,
            'seen_date' => NULL
        ]);       
    }

    public function view_more_new_task(Request $request ) {
        $task_unique = json_decode($request->i);
        $tmp = array();

        foreach ($task_unique as $key => $value) {
            $tmp[] =  $value;
        }    

        $whereIn = '("' . implode('","', $tmp) . '")';    
          //dd($whereIn);
        $group = array();
        $newArray = array();
        $array  = array();
        $result = DB::table('tasks as t')
                ->select('*' , 't.created_by as task_creator' , 't.task_id as task_id' , 't.created_at as task_created')
                ->join('task_assign as ta', 'ta.task_id', '=', 't.task_id')
                ->join('users as u' , 'u.id' , '=' , 't.created_by')
                ->join('task_categories as tcc' , 'tcc.category_id' , '=' , 't.category_id')
                ->leftJoin('task_comments as tc' , 'tc.task_id' , '=' , 't.task_id')
                ->where("ta.user_id" ,  Auth::user()->id)
                ->whereRaw("t.task_unique NOT IN $whereIn "  )
                ->where("t.status" , 'A')
                ->where("t.task_status" , '=' , 'N')
                ->groupBy('t.task_id')
                ->orderBy('tc.date_added' ,'DESC')
                ->orderBy('t.task_id' ,'DESC')
                ->get();


        if (!empty($result)) {
            foreach ($result as $key => $value) {
                if ( !in_array($value->task_id, $array) ) { 
                    $array[] = $value->task_id; 
                    $newArray[] = $value; 
                } 
            }
        $i = 1;
        # SET STATUS BASED ON USERS
        foreach ($newArray as $key => $row) {
            if ($key < 10) {
                $group['N'][$key] = $row; 
                $total = $i; 
                $i++;
            }     
        }

        # GET ALL ASSIGNEE BASED ON TASK 
        foreach ($group as $task_status => $value) {
            foreach ($value as $key => $row) {
                # GET TOTAL TASK 
                $group['total_task'] = $total; 
                $group['task_ids'][] = $row->task_unique; 
                # USE NORMAL QUERY FOR GETTING THE ASSIGNEE
                $group[$task_status][$key]->assignee = DB::select('SELECT * FROM task_assign ta
                    INNER JOIN users u ON u.id =  ta.user_id
                    LEFT JOIN task_comments tc ON tc.user_id = u.id AND tc.task_id = '.$row->task_id.' 
                    LEFT JOIN profile_image pi ON pi.user_id = u.id AND pi.is_profile = 1 AND pi.status IS  NULL 
                    WHERE ta.task_id = '.$row->task_id.' AND ta.is_created != 1 group by ta.user_id ');
                # GET THE IMAGES OF TASK 
                $group[$task_status][$key]->attachment = DB::table('tasks_attachment as ta')
                                                ->select('*')
                                                ->where("ta.task_id" , $row->task_id)
                                                ->where("ta.status" , 1)
                                                ->get();
                # GET THE LAST COMMENT
                $group[$task_status][$key]->last_comment = DB::table('task_comments as tc')
                                                ->select('*')
                                                ->join("users as u" , 'u.id' ,  '=' , 'tc.user_id')
                                                 ->leftJoin("profile_image as pi" , function($q) {
                                                    $q->on('u.id' ,  '=' , 'pi.user_id')
                                                    ->where('pi.status' , null)
                                                    ->where('pi.is_profile', 1);
                                                })
                                                ->where("tc.task_id" , $row->task_id)
                                                ->orderBy('tc.task_comments_id' ,'DESC')
                                                ->get()->first();  
                //GET ALL UNREAD MESSAGE (NOTIFICATION)
                $group[$task_status][$key]->unread = DB::table('task_notification as tc')
                    ->select('*')
                    ->where("tc.task_id" , $row->task_id)
                    ->where("tc.user_id" , Auth::user()->id )
                    ->where("tc.seen" , 0 )
                    ->where("tc.action_type" , 'C' )
                    ->count('*'); 
            }
        }

        if (!empty($group['task_ids'])) {
             $group['last_task_id'] = json_encode(array_merge($task_unique , $group['task_ids']));
        }
        return response()->json($group);

        } // end check if empty

    }

    #Load more
    function loadmore_new_task(Request $req){
        
        $group    = array();
        $newArray = array();
        $array    = array();
       
        // $result = DB::table('tasks as t')
        //         ->select('*' , 't.created_by as task_creator' , 't.task_id as task_id' , 't.created_at as task_created')
        //         ->join('task_assign as ta', 'ta.task_id', '=', 't.task_id')
        //         ->join('users as u' , 'u.id' , '=' , 't.created_by')
        //         ->join('task_categories as tcc' , 'tcc.category_id' , '=' , 't.category_id')
        //         ->leftJoin('task_comments as tc' , 'tc.task_id' , '=' , 't.task_id')
        //         ->where("ta.user_id" ,  Auth::user()->id)
        //         ->where("t.status" , 'A')
        //         ->where("t.task_status" , '=' , 'N')
        //         ->whereNotIn('t.task_unique',$req->ids)
        //         ->orderBy('tc.date_added' ,'DESC')
        //         ->orderBy('t.task_id' ,'DESC')
        //         ->get();


         
        $result = DB::table('tasks as t')
                ->select('*' , 't.created_by as task_creator' , 't.task_id as task_id' , 't.created_at as task_created')
                ->join('task_assign as ta', 'ta.task_id', '=', 't.task_id')
                ->join('users as u' , 'u.id' , '=' , 't.created_by')
                ->join('task_categories as tcc' , 'tcc.category_id' , '=' , 't.category_id')
                ->leftJoin('task_comments as tc' , 'tc.task_id' , '=' , 't.task_id')
                ->where("ta.user_id" ,  Auth::user()->id)
                ->where("t.status" , 'A')
                ->where("t.task_status" , '=' , 'N')
                ->orderBy('tc.date_added' ,'DESC')
                ->orderBy('t.task_id' ,'DESC')
                ->paginate(5);

       
       
        if (!empty($result)) {
            // foreach ($result as $key => $value) {
            //     if ( !in_array($value->task_id, $array) ) { 
            //         $array[] = $value->task_id; 
            //         $newArray[] = $value; 
            //     } 
            // }
        
            // $total = 0;
            // # SET STATUS BASED ON USERS
            // foreach ($newArray as $key => $row) {
            //     if ($key < 10) {
            //         $group['N'][$key] = $row; 
            //     }     
            //     $total++; 
            // }

            # GET ALL ASSIGNEE BASED ON TASK 
            foreach ($result->items() as $key => $value) {
                //foreach ($value as $key => $row) {
                    # GET TOTAL TASK  
                    //$group['task_ids'][] = $row->task_unique; 
                    # USE NORMAL QUERY FOR GETTING THE ASSIGNEE
                    
                    $result->items()[$key]->assignee = DB::select('SELECT * FROM task_assign ta
                        INNER JOIN users u ON u.id =  ta.user_id
                        LEFT JOIN task_comments tc ON tc.user_id = u.id AND tc.task_id = '.$value->task_id.' 
                        LEFT JOIN profile_image pi ON pi.user_id = u.id AND pi.is_profile = 1 AND pi.status IS  NULL 
                        WHERE ta.task_id = '.$value->task_id.' AND ta.is_created != 1 group by ta.user_id ');
                    
                    # GET THE IMAGES OF TASK 
                    $result->items()[$key]->attachment = DB::table('tasks_attachment as ta')
                                                            ->select('*')
                                                            ->where("ta.task_id" , $value->task_id)
                                                            ->where("ta.status" , 1)
                                                            ->get();
                    
                    # GET THE LAST COMMENT
                    $result->items()[$key]->last_comment = DB::table('task_comments as tc')
                                                                ->select('*')
                                                                ->join("users as u" , 'u.id' ,  '=' , 'tc.user_id')
                                                                ->leftJoin("profile_image as pi" , function($q) {
                                                                    $q->on('u.id' ,  '=' , 'pi.user_id')
                                                                    ->where('pi.status' , null)
                                                                    ->where('pi.is_profile', 1);
                                                                })
                                                                ->where("tc.task_id" , $value->task_id)
                                                                ->orderBy('tc.task_comments_id' ,'DESC')
                                                                ->get()->first(); 
                  
                    if(isset($result[$key]->last_comment)) {
                        # Default image
                        if($result[$key]->last_comment->image == null){
                            $result[$key]->last_comment->image = url('/').'/public/images/icons/default-user.png';
                        } else {# Profile Image
                            $result[$key]->last_comment->image = url('/').'/public/profile/'.$result[$key]->last_comment->id.'/thumbnail_profile/'.$result[$key]->last_comment->image;
                        }
                    }
                   

                    # Button translation
                    $result[$key]->button['lang_deadline'] = __('translation.deadline');
                    $result[$key]->button['lang_task_complete'] = __('translation.task_complete');
                    $result[$key]->button['lang_reply'] = __('translation.reply');
                    $result[$key]->button['lang_show_threads'] = __('translation.show_threads');

                    # GET ALL UNREAD MESSAGE (NOTIFICATION)
                    $result->items()[$key]->unread = DB::table('task_notification as tc')
                                                        ->select('*')
                                                        ->where("tc.task_id" , $value->task_id)
                                                        ->where("tc.user_id" , Auth::user()->id )
                                                        ->where("tc.seen" , 0 )
                                                        ->where("tc.action_type" , 'C' )
                                                        ->count('*'); 
                //}
            }
            return $result;

        } // end check if empty
        
    }

    function loadmore_waiting_for_answer(Request $req){

        //$aa = Task::where('task_id',3)->with(['get_assign'=>function($q){        
        //    $q->where('is_created',0);
        //}])->get();

        // $aa =  DB::select('SELECT * FROM task_assign ta
        // INNER JOIN users u ON u.id =  ta.user_id 
        // LEFT JOIN task_comments tc ON tc.user_id = u.id AND tc.task_id = 3  
        // LEFT JOIN profile_image pi ON pi.user_id = u.id AND pi.is_profile = 1 AND pi.status IS  NULL 
        // WHERE ta.task_id = 3 AND ta.is_created != 1 group by ta.user_id');
        
        // dd($aa);
        
        // $result = DB::table('tasks as t')
        //         ->select('*' , 't.created_by as task_creator' , 't.task_id as task_id' , 't.created_at as task_created')
        //         ->join('task_assign as ta', 'ta.task_id', '=', 't.task_id')
        //         ->join('users as u' , 'u.id' , '=' , 't.created_by')
        //         ->join('task_categories as tcc' , 'tcc.category_id' , '=' , 't.category_id')
        //         ->leftJoin('task_comments as tc' , 'tc.task_id' , '=' , 't.task_id')
        //         ->where("ta.user_id" ,  Auth::user()->id)
        //         ->where("t.status" , 'A')
        //         ->where("t.task_status" , '!=' , 'D')
        //         ->orderBy('tc.date_added' ,'DESC')
        //         ->orderBy('t.task_id' ,'DESC')
        //         ->get();

        //         return $res;
        // // foreach ($result as $key => $value) {
        // //     if ( !in_array($value->task_id, $array) ) { 
        // //         $array[] = $value->task_id; 
        // //         $newArray[] = $value; 
        // //     } 
        // // }

        // // $c = [];
        // # SET STATUS BASED ON USERS
        // foreach ($result as $key => $row) {
        //     # check if user is the last comment
        //     $check = DB::table('task_comments as tc')
        //                     ->select('*')
        //                     ->where("tc.task_id" , $row->task_id)
        //                     ->where("tc.action_type" , 'C')
        //                     ->orderBy('tc.task_comments_id' ,'DESC')
        //                     ->get()->first();
    
        //     if (!empty($check)) {
        //         if ($check->user_id == Auth::user()->id) {
        //             if ($key <=10) {
        //                     $group['I'][$key] = $row; 
        //             }
        //         }
                
        //     }
        // }
        //return $group;

        // $result = DB::table('tasks as t')
        //             ->select('*' , 't.created_by as task_creator' , 't.task_id as task_id' , 't.created_at as task_created')
        //             ->join('task_assign as ta', 'ta.task_id', '=', 't.task_id')
        //             ->join('users as u' , 'u.id' , '=' , 't.created_by')
        //             ->join('task_categories as tcc' , 'tcc.category_id' , '=' , 't.category_id')
        //             ->leftJoin('task_comments as tc' , 'tc.task_id' , '=' , 't.task_id')
        //             ->where("ta.user_id" ,  Auth::user()->id)
        //             ->where("t.status" , 'A')
        //             ->where("t.task_status" , '!=' , 'D')
                   
        //             ->orderBy('tc.date_added' ,'DESC')
        //             ->orderBy('t.task_id' ,'DESC')
        //             ->paginate(5);

        //return $result;

        $result = DB::table('tasks as t')
                    ->select('t.task_id as task_id',
                             't.task_unique',
                             't.title',
                             't.category_id',
                             't.task_percentage',
                             't.description',
                             't.created_by as task_creator',
                             't.created_at as task_created',
                             't.deadline',
                             't.date_finished',
                             't.task_status',
                             't.status',
                             't.type',
                             # Task Comment
                             'tc.task_comments_id',
                             'tc.comment',
                             'tc.user_id as last_comment_user',
                             'tc.seen',
                             'tc.seen_date',
                             'tc.system_generated',
                             'tc.action_type',
                             'tc.date_added as latest_comment_date', 
                             # Task Category
                             'tcc.category_name',
                             # User
                             'u.firstname',
                             'u.lastname')
                    ->join('task_assign as ta', 'ta.task_id', '=', 't.task_id')
                    ->join('users as u' , 'u.id' , '=' , 't.created_by')
                    ->join('task_categories as tcc' , 'tcc.category_id' , '=' , 't.category_id')
                    ->leftJoin('task_comments as tc' , 'tc.task_id' , '=' , 't.task_id')
                    ->where('ta.user_id' ,  Auth::user()->id)
                    ->where('t.status' , 'A')
                    ->whereIn('t.task_status' , ['C','I'])
                    //->whereRaw("tc.date_added = ( SELECT MAX(tc2.date_added) FROM task_comments tc2 WHERE tc2.task_id = tc.task_id )")
                    ->whereRaw("tc.date_added = ( SELECT tc2.date_added FROM task_comments tc2 WHERE tc2.task_id = tc.task_id order by tc2.task_comments_id DESC LIMIT 1 )")
                    ->where('tc.user_id',Auth::user()->id)
                    ->orderBy('latest_comment_date','DESC')
                    //->orderBy('t.task_id' ,'DESC')
                    //->distinct(['t.task_id'])
                    ->paginate(5);            

                if (!empty($result)) {

                    if(count($result) > 0 ){
                        # GET ALL ASSIGNEE BASED ON TASK 
                        foreach ($result as $key => $value) {
                            # USE NORMAL QUERY FOR GETTING THE ASSIGNEE
                            $result[$key]->assignee = DB::select('SELECT * FROM task_assign ta
                                INNER JOIN users u ON u.id =  ta.user_id 
                                LEFT JOIN task_comments tc ON tc.user_id = u.id AND tc.task_id = '.$value->task_id.'  
                                LEFT JOIN profile_image pi ON pi.user_id = u.id AND pi.is_profile = 1 AND pi.status IS  NULL 
                                WHERE ta.task_id = '.$value->task_id.' AND ta.is_created != 1 group by ta.user_id');
                        
                            # GET THE IMAGES OF TASK 
                            $result[$key]->attachment = DB::table('tasks_attachment as ta')
                                                            ->select('*')
                                                            ->where('ta.task_id' , $value->task_id)
                                                            ->where('ta.status' , 1)
                                                            ->get();
                            # GET THE LAST COMMENT
                            $result[$key]->last_comment = DB::table('task_comments as tc')
                                                            ->select('*')
                                                            ->join('users as u' , 'u.id' ,  '=' , 'tc.user_id')
                                                            ->leftJoin('profile_image as pi' , function($q) {
                                                                $q->on('u.id' ,  '=' , 'pi.user_id')
                                                                ->where('pi.status' , null)
                                                                ->where('pi.is_profile', 1);
                                                            })
                                                            ->where('tc.task_id' , $value->task_id)
                                                            ->orderBy('tc.task_comments_id' ,'DESC')
                                                            ->get()->first();  

                            # Default image
                            if($result[$key]->last_comment->image == null){
                                $result[$key]->last_comment->image = url('/').'/public/images/icons/default-user.png';
                            } else {# Profile Image
                                $result[$key]->last_comment->image = url('/').'/public/profile/'.$result[$key]->last_comment->id.'/thumbnail_profile/'.$result[$key]->last_comment->image;
                            }

                             # Button translation
                            $result[$key]->button['lang_deadline'] = __('translation.deadline');
                            $result[$key]->button['lang_task_complete'] = __('translation.task_complete');
                            $result[$key]->button['lang_reply'] = __('translation.reply');
                            $result[$key]->button['lang_show_threads'] = __('translation.show_threads');

                            # GET ALL UNREAD MESSAGE (NOTIFICATION)
                            $result[$key]->unread = DB::table('task_notification as tc')
                                ->select('*')
                                ->where('tc.task_id' , $value->task_id)
                                ->where('tc.user_id' , Auth::user()->id )
                                ->where('tc.seen' , 0 )
                                ->where('tc.action_type' , 'C' )
                                ->count('*'); 
                        }
                    }

                    return $result;

                } // end check if empty

    }
    
    function loadmore_waiting_for_me(Request $req){

        $result = DB::table('tasks as t')
                        ->select('t.task_id as task_id',
                                't.task_unique',
                                't.title',
                                't.category_id',
                                't.task_percentage',
                                't.description',
                                't.created_by as task_creator',
                                't.created_at as task_created',
                                't.deadline',
                                't.date_finished',
                                't.task_status',
                                't.status',
                                't.type',
                                # Task Comment
                                'tc.task_comments_id',
                                'tc.comment',
                                'tc.user_id as last_comment_user',
                                'tc.seen',
                                'tc.seen_date',
                                'tc.system_generated',
                                'tc.action_type',
                                'tc.date_added as latest_comment_date', 
                                # Task Category
                                'tcc.category_name',
                                # User
                                'u.firstname',
                                'u.lastname')
                        ->join('task_assign as ta', 'ta.task_id', '=', 't.task_id')
                        ->join('users as u' , 'u.id' , '=' , 't.created_by')
                        ->join('task_categories as tcc' , 'tcc.category_id' , '=' , 't.category_id')
                        ->leftJoin('task_comments as tc' , 'tc.task_id' , '=' , 't.task_id')
                        ->where('ta.user_id' ,  Auth::user()->id)
                        ->where('t.status' , 'A')
                        ->whereIn('t.task_status' , ['C','I'])
                        //->whereRaw("tc.date_added = ( SELECT MAX(tc2.date_added) FROM task_comments tc2 WHERE tc2.task_id = tc.task_id )")
                        ->whereRaw("tc.date_added = ( SELECT tc2.date_added FROM task_comments tc2 WHERE tc2.task_id = tc.task_id order by tc2.task_comments_id DESC LIMIT 1 )")
                        ->whereNotIn('tc.user_id',[Auth::user()->id])
                        ->orderBy('latest_comment_date','DESC')
                        //->orderBy('t.task_id' ,'DESC')
                        //->distinct(['t.task_id'])
                        ->paginate(5);


        if (!empty($result)) {

            if(count($result) > 0 ){
                # GET ALL ASSIGNEE BASED ON TASK 
                foreach ($result as $key => $value) {
                    # USE NORMAL QUERY FOR GETTING THE ASSIGNEE
                    $result[$key]->assignee = DB::select('SELECT * FROM task_assign ta
                        INNER JOIN users u ON u.id =  ta.user_id 
                        LEFT JOIN task_comments tc ON tc.user_id = u.id AND tc.task_id = '.$value->task_id.'  
                        LEFT JOIN profile_image pi ON pi.user_id = u.id AND pi.is_profile = 1 AND pi.status IS  NULL 
                        WHERE ta.task_id = '.$value->task_id.' AND ta.is_created != 1 group by ta.user_id');
                
                    # GET THE IMAGES OF TASK 
                    $result[$key]->attachment = DB::table('tasks_attachment as ta')
                                                    ->select('*')
                                                    ->where('ta.task_id' , $value->task_id)
                                                    ->where('ta.status' , 1)
                                                    ->get();
                    # GET THE LAST COMMENT
                    $result[$key]->last_comment = DB::table('task_comments as tc')
                                                    ->select('*')
                                                    ->join('users as u' , 'u.id' ,  '=' , 'tc.user_id')
                                                    ->leftJoin('profile_image as pi' , function($q) {
                                                        $q->on('u.id' ,  '=' , 'pi.user_id')
                                                        ->where('pi.status' , null)
                                                        ->where('pi.is_profile', 1);
                                                    })
                                                    ->where('tc.task_id' , $value->task_id)
                                                    ->orderBy('tc.task_comments_id' ,'DESC')
                                                    ->get()->first(); 

                    # Default image
                    if($result[$key]->last_comment->image == null){
                        $result[$key]->last_comment->image = url('/').'/public/images/icons/default-user.png';
                    } else {# Profile Image
                        $result[$key]->last_comment->image = url('/').'/public/profile/'.$result[$key]->last_comment->id.'/thumbnail_profile/'.$result[$key]->last_comment->image;
                    }

                    
                    # Button translation
                    $result[$key]->button['lang_deadline'] = __('translation.deadline');
                    $result[$key]->button['lang_task_complete'] = __('translation.task_complete');
                    $result[$key]->button['lang_reply'] = __('translation.reply');
                    $result[$key]->button['lang_show_threads'] = __('translation.show_threads');
                                                    
                    //GET ALL UNREAD MESSAGE (NOTIFICATION)
                    $result[$key]->unread = DB::table('task_notification as tc')
                        ->select('*')
                        ->where('tc.task_id' , $value->task_id)
                        ->where('tc.user_id' , Auth::user()->id )
                        ->where('tc.seen' , 0 )
                        ->where('tc.action_type' , 'C' )
                        ->count('*'); 
                }
            }

            return $result;

        } // end check if empty

    }

    public function loadmore_todo (Request $data) {
        $User = Auth::user();
        $todo_list = $User->todo_list()
                ->Where('is_active',1)
                ->Where('todo_status',"A")
                ->orderBy('created_at', 'desc')
                ->paginate(5);

         return response()->json($todo_list);

    }

}// end class
