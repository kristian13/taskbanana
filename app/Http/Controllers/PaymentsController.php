<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use App;
use Auth;

use Session;
use App\User;
use App\Task;
use DateTime;

use Braintree_Transaction;
use Illuminate\Http\Request;
use function GuzzleHttp\json_encode;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;
use Redirect;

class PaymentsController extends Controller
{

	public function index() {

	    return view("payment" , [
	    	"get_my_business_team" => app('App\Http\Controllers\MemberController')->get_my_business_team() ,
	        "get_my_personal_team" => app('App\Http\Controllers\MemberController')->get_my_personal_team() , 
	        "User" => User::find(Auth::user()->id),
	        "Profile" => User::find(Auth::user()->id) ,
	        "new_members" => app('App\Http\Controllers\MemberController')->accept_member_lists_count() 

	    ]); 
	}

    public function process(Request $request)
	{	
		$subscription_id = $request->subscription;
		$subscription_amount = $request->amount;

		# CHECK
		$check = DB::table('subscription')
                    ->select('*')
                    ->where("subscription_id" , $subscription_id )
                    ->where("subscription_amount" , $subscription_amount )
                   ->get();
       
        if (!$check->isEmpty()) {
			\Stripe\Stripe::setApiKey ( 'sk_test_TZStW7thNn53LUYoWmWE6GhZ00UDcgUzZD' );
		    try {
		        \Stripe\Charge::create ( array (
		                "amount" => $request->amount * 100,
		                "currency" => "usd",
		                "source" => $request->input ( 'stripeToken' ), // obtained with Stripe.js
		                "description" => "Test payment." 
		        ) );
		        Session::flash ( 'success-message', 'Payment done successfully !' );
		        return Redirect::back ();
		    } catch ( \Exception $e ) {
		    	dd($e);
		       // Session::flash ( 'fail-message', "Error! Please Try again." );
		        return Redirect::back ();
		    }
        } else {
        	dd("ulol");
        }



		// # GET THE AMOUNT OF SUBSCRIPTION ID 
		// $res = DB::table('subscription')->select('*')->where('subscription_id' ,'=' , $request->subscription_id)->get()->first();  
	 //    $payload = $request->input('payload', false);
	 //    $nonce = $payload['nonce'];

	 //    # ADD LOG FOR THE USER
	 //    $order_id = DB::table('user_subscription')->insertGetId([
  //           'subscription_id'  => $res->subscription_id ,
  //           'amount'  => $res->subscription_amount ,  
  //           'user_id'  => Auth::user()->id 
  //       ]);

	 //    $status = Braintree_Transaction::sale([
		// 	'amount' => $res->subscription_amount,
		// 	'paymentMethodNonce' => $nonce,
		// 	'orderId' => $order_id,
		// 	'customer' => [
	 //        'firstName' => Auth::user()->firstname,
	 //        'lastName' => Auth::user()->lastname,
	 //        'email' => Auth::user()->email
	 //   		 ], 
		// 	'options' => [
		// 	    'submitForSettlement' => True
		// 	]
	 //    ]);
	 //    dd($status->transaction->id);
	 //    return response()->json($status);
	}

	public function cancel_order () {
        $result = Braintree_Transaction::refund('dv2xjtm7' , 50);
        dd($result);
	}

	public function payment_history($show, $req) {
		$UserSubscriptions = User::with([
			'user_subscriptions' => function($q) use ($show,$req) {
				$q->join('subscription as s','s.subscription_id','=','user_subscription.subscription_id');
				
				if($req->search != null) {
					$q->where("amount","LIKE","%".$req->search."%");
					$q->orWhere("subscription_name","LIKE","%".$req->search."%");
				}	
				
				if($req->order[0]['column'] == "0") {
					$q->orderBy('subscription_name',$req->order[0]['dir']);
				}
				if($req->order[0]['column'] == "1") {
					$q->orderBy('date_created',$req->order[0]['dir']);
				}
				if($req->order[0]['column'] == "2") {
					$q->orderBy('amount',$req->order[0]['dir']);
				}
				
				if($show == 'page'){
					$q->paginate($req->page_length);
				}
			}
		])->find(Auth::user()->id);

		return $UserSubscriptions;
	}

	public function load_process(Request $req) {

		$UserSubscriptions = $this->payment_history('page',$req);
	
		$data["data"] = [];
		foreach ($UserSubscriptions->user_subscriptions as $subscription) {
			$row = [];
			$row['Description'] = "<a href='/trackatask/payment/invoice/".$subscription->user_subscription_id."' target='_blank'>"
									.$subscription->subscription->subscription_name.
								  "<a>";
			$row['Date']    	= date('d / M / Y',strtotime($subscription->date_created));
			$row['Amount']  	= $subscription->amount;
			$data["data"][] 	= $row;
		}

		$data["draw"]            = $req->draw;
		$data["recordsFiltered"] = $this->payment_history('all',$req)->user_subscriptions->count();
		
		$req->search = null;
		$data["recordsTotal"]    = $this->payment_history('all',$req)->user_subscriptions->count();
		
		
		
		return response()->json($data);
	}

	public function details() {
		return view("payment_details" , [
			"get_my_business_team" => app('App\Http\Controllers\MemberController')->get_my_business_team() ,
	        "get_my_personal_team" => app('App\Http\Controllers\MemberController')->get_my_personal_team() , 
	        "User" => User::find(Auth::user()->id),
	        "Profile" => User::find(Auth::user()->id)
		]);
	}

	public function invoice ($id) {
		$User = User::with(['user_subscriptions'])->find(Auth::user()->id);
		
		$pdf = PDF::loadView('pdf.invoice',[
			'user'    => $User,
			'invoice' => $User->user_subscriptions()->find($id)
		]);
		
		return $pdf->stream();
	}


	
}
