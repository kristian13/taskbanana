<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;

use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Facades\UserVerification;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */


    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
     protected $redirectTo = '/task' ;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
      
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            if( Auth::user()->verified == '0' ){
                // UserVerification::generate(Auth::user());
                // UserVerification::send(Auth::user(), 'TaskBanana Verification Link');

                Auth::logout(); //logout user if not verified
                return  response()->json([
                    'status'  => false,
                    'action'  => 'login',
                    'data'    =>  '',
                    'message' => "Please check your verification link on your email."
                ]);
            }

            if ( Auth::user()->verified == '1' ) {
                return  response()->json([
                    'status'  => true,
                    'action'  => 'login',
                    'data'    =>  Auth::user(),
                    'message' => "You have successfully Login."
                ]);
            }

        } else {
            return  response()->json([
                'status'  => false,
                'action'  => 'login',
                'data'    => '',
                'message' => "Invalid Email or Password."
            ]);
        }
    }
    
    // public function login(Request $request)
    // {
    //     if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
    //         if( Auth::user()->verified == '0' ){
    //             UserVerification::generate(Auth::user());
    //             UserVerification::send(Auth::user(), 'TaskBanana Verification Link');
    //             return redirect('email-verification/send_email');
    //         }
    //         if ( Auth::user()->verified == '1' ) {
    //             return redirect('/task');
    //         } else {
    //             return redirect('/');
    //         }
    //     }
    // }

    public function showLoginForm()
    {
        return redirect('/');
    }
}
