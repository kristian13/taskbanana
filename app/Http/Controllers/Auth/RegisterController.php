<?php

namespace App\Http\Controllers\Auth;

use DB;
use File;
use Redirect;

use App\User;
use App\Timezone;
use App\Http\Controllers\Controller;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Facades\UserVerification;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    use RegistersUsers;
    use VerifiesUsers;
    /**
     * Where to redirect users after registration.
     *
     * @var string
    */
    protected $redirectTo = '/task';

    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware('guest', ['except' => ['getVerification', 'getVerificationError','add_new_member']]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {   
        return Validator::make($data, [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255' ,
            'email' => 'required|string|email|max:255',
            'password' => 'required',
            'business_name' => 'required:string'
        ]);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */ 
     //htmlspecialchars(string, ENT_QUOTES, 'UTF-8')
    protected function create(array $data)
    {   
        $timezone = $data['timezone'];
        $data = User::create([
            'firstname' =>htmlspecialchars($data['firstname'], ENT_QUOTES, 'UTF-8'),
            'lastname' =>htmlspecialchars($data['lastname'], ENT_QUOTES, 'UTF-8'),
            'account_type_id' => 1,
            'business_nature_child_id' => $data['business_nature_child_id'],
            'business_name' => htmlspecialchars($data['business_name'], ENT_QUOTES, 'UTF-8'),
            'email' => $data['email'],
            'subscription_id' => 1,
            'owner_accept_status' => 1,
            'subscription_type' => 'B',
            'password' => bcrypt($data['password'])
           
        ]);
      
        if ($data) {
            $sub = DB::table('subscription')
                ->select('*')
                ->where('subscription_id' , '=' , 1 )->get()->first();

            $last_id = DB::table('users')
                ->select('*')
                ->where('email' , '=' , $data->email )->get()->first();  
            
            DB::table('user_subscription')->insertGetId([
                'subscription_id'  => $sub->subscription_id ,
                'amount'  => $sub->subscription_amount ,  
                'user_id'  => $last_id->id 
            ]);

            $path = public_path().'/profile/'.$last_id->id;
            
            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
                $this->create_index_html($path);
            }
            
            // $timezone = new Timezone();
            // $timezone->user_id = $last_id->id;
            // $timezone->save();
            $result = DB::table('users')
                ->where('id', $last_id->id)
                ->update([
                    'parent_business_owner_id' => $last_id->id , 
                    "user_unique"              => bcrypt($last_id->id) ,
                    "timezone" => $timezone
                ]);

            //Auth::login($data);
            return $data;
        }
        
        return $data;
    }

    public function register(Request $request) {

            $validator = Validator::make($request->all(), [
                "firstname" => "required",
                "lastname" => "required",
                "email" => "required|email|unique:users",
                "password" => "required|min:8",
                "timezone" => "required",
                "business_nature_child_id" => "required",
                "business_name" => "required"
            ]);
    

            if ($validator->fails()) {
                return response()->json([
                    'status'  => false,
                    'action'  => 'register',
                    'data'    =>  null,
                    'message' =>  $validator->messages()->getMessages(),
                ]);
            }

            $user = $this->create($request->all());

            event(new Registered($user));

            //$this->guard()->login($user);

            UserVerification::generate($user);
            UserVerification::send($user, 'TaskBanana Account Verification');


            $data = response()->json([
                    'status'  =>  true,
                    'action'  =>  'register',
                    'data'    =>  $user,
                    'message' =>  'Please Check your Email for your account verification.',
                ]);

            return $this->registered($request, $user)
                            ?: $data;
    }


    public function add_new_member(Request $request){

    	$validator =  Validator::make($request->all(), [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255' ,
            'email' => 'required|string|email|max:255|unique:users,email',
        ]);

        if (!$validator->fails()) {
        
            # check if creator or member is adding
            if(Auth::user()->created_by == null){ # Parent creator member
              
                $parent_business_owner_id = Auth::user()->id;
                $owner_accept_status = 1;
                $created_by =  Auth::user()->id;
            } else{ # Member 
              
                $parent_business_owner_id = Auth::user()->parent_business_owner_id;
                $parent = true;
                $owner_accept_status = 0;
                $created_by = Auth::user()->id;
            }
        
            #generate password
            $gpassword = Str::random(8);

            $data = [
                'firstname'                => htmlspecialchars($request->firstname, ENT_QUOTES, 'UTF-8'),
                'lastname'                 => htmlspecialchars($request->lastname, ENT_QUOTES, 'UTF-8'),
                'account_type_id'          => 1,
                'business_nature_child_id' => Auth::user()->business_nature_child_id,
                'business_name'            => Auth::user()->business_name,
                'email'                    => $request->email,
                'subscription_status'      => 0,
               
                'password'                 => bcrypt($gpassword),
              
            ];
            
            $user = User::create($data);
            
            if ($user) {
                # UPDATE UNIQUE COLUMN FOR SECURITY
                DB::table('users')->where('id', $user->id)->update( [
                    "user_unique"                   => bcrypt ($user->id) , 
                    'created_by'                    => $created_by ,
                    'parent_business_owner_id'      => $parent_business_owner_id ,  
                    'timezone'                      => Auth::user()->timezone ,
                    'owner_accept_status'           => $owner_accept_status , 

                ] );
            

                # ADD NEW MEMBER
                DB::table('task_users')->insertGetId([
                    'parent_user_id' => $parent_business_owner_id,
                    'child_user_id'  => $user->id,
                ]);

                $path = public_path().'/profile/'.$user->id;
                if (!file_exists($path)) {
                    File::makeDirectory($path, $mode = 0777, true, true);
                    $this->create_index_html($path);
                }
                                
                # check if user logged has data 
                $check = DB::table('task_users')
                            ->select('*')
                            ->where("child_user_id" , Auth::user()->id)
                            ->get()->first();

                if (!$check) {
                   
                    DB::table('task_users')->insertGetId([
                        'parent_user_id' => $parent_business_owner_id,
                        'child_user_id'  => $parent_business_owner_id
                    ]);
                }

                # Send email with generated password
                event(new Registered($user));
                UserVerification::generate($user);
                $user->static_password = $gpassword;
                UserVerification::send($user, 'TaskBanana Account Verification');

                // $this->registered($data, $user)
                //             ?: $data;
            }

        } else {
            return Redirect::back()->withErrors($validator->messages()->getMessages());
        }
    	return redirect('/task');
    }


    public function getVerificationError() {
        return view("verification_email_error");
    }

    public function send_email(Request $req) {
        return view("verification_email");
    }
        

    public function create_index_html($folder) {
        $content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Directory access is forbidden.</p></body></html>";
        $folder = $folder.'/index.html';
        $fp = fopen($folder , 'w');
        fwrite($fp , $content);
        fclose($fp);
    }
}
