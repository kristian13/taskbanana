<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Archive;
use App\TaskComment;
use DB;
class ArchiveController extends Controller
{	

    public function __construct() {
        parent::__construct();
    }
    
    public function index () {
    	# CHECK USER LOGGED IN 
    	if (!Auth::check()) {
    		# RETRN TO HOMEPAGE 
    		return redirect('/');
    	}
    	// return view("archive" ,[ 
     //        "get_my_business_team" => app('App\Http\Controllers\MemberController')->get_my_business_team() ,
     //    	"get_my_personal_team" => app('App\Http\Controllers\MemberController')->get_my_personal_team() , 
     //    	"archive" => Archive::where('status', '=', 'A')->where('task_status' , 'D')->paginate(5),
     //        "Profile" => User::find(Auth::user()->id),
     //        "User" => User::find(Auth::user()->id),
     //        "categories" =>  $this->get_categories_view() ,
     //        "new_members" => app('App\Http\Controllers\MemberController')->accept_member_lists_count() 
    	// ]);
       // dd(Archive::where('task_status' ,'=','D')->Orwhere("status" ,'=' , 'H')->with(['author'])->paginate(5));
        return view("archive" ,[ 
            "get_my_business_team" => app('App\Http\Controllers\MemberController')->get_my_business_team() ,
            "get_my_personal_team" => app('App\Http\Controllers\MemberController')->get_my_personal_team() , 
            "archive" => $this->archive(),
            "Profile" => User::find(Auth::user()->id),
            "User" => User::find(Auth::user()->id),
            "categories" =>  $this->get_categories_view() ,
            "new_members" => app('App\Http\Controllers\MemberController')->accept_member_lists_count() 
        ]);
    }


    public function archive () {
        if (isset($_GET['task_title_search']) && !empty($_GET['task_title_search'])) {
           if (isset($_GET['finished']) && !empty($_GET['finished'])) {
                $archive = Archive::where('task_status' ,'=','D')
                   ->where('title' , 'LIKE' , '%'.$_GET['task_title_search'].'%')
                   ->with('author')->paginate(5);
           } elseif (isset($_GET['dismissed']) && !empty($_GET['dismissed'])) {
                $archive = Archive::where("status" ,'=' , 'H')
                   ->where('title' , 'LIKE' , '%'.$_GET['task_title_search'].'%')
                   ->with('author')->paginate(5);
           }else{
                $archive = Archive::where('task_status' ,'=','D')->Orwhere("status" ,'=' , 'H')
                   ->where('title' , 'LIKE' , '%'.$_GET['task_title_search'].'%')
                   ->with('author')->paginate(5);
           }
        } elseif (isset($_GET['project']) && !empty($_GET['project'])) {
            if (isset($_GET['finished']) && !empty($_GET['finished'])) {
                $archive = Archive::where('task_status' ,'=','D')
                    ->where('category_id' , $_GET['project'])
                    ->with('author')->paginate(5);
           } elseif (isset($_GET['dismissed']) && !empty($_GET['dismissed'])) {
                $archive = Archive::where("status" ,'=' , 'H')
                    ->where('category_id' , $_GET['project'])
                    ->with('author')->paginate(5);
           } else{
                $archive = Archive::where('task_status' ,'=','D')->Orwhere("status" ,'=' , 'H')
                    ->where('category_id' , $_GET['project'])
                    ->with('author')->paginate(5);
           }
        }else{
            $archive = Archive::where('task_status' ,'=','D')->Orwhere("status" ,'=' , 'H')->with('author')->paginate(5);
        }
        return $archive;
    }

     # for pages
    public function get_categories_view () {
        $result = DB::table('task_categories')
            ->select('*')
            ->get();
        return $result;
    }

    public function loadmoreArchive () {
        $archive = Archive::where('task_status' ,'=','D')->Orwhere("status" ,'=' , 'H')->with('author')->paginate(5);
        return response()->json($archive);
    }   
    public function finishedArchive () {
        $archive = Archive::where('task_status' ,'=','D')->with('author')->paginate(5);
        return response()->json($archive);
    }     
    public function dismissedArchive () {
        $archive = Archive::where('status' ,'=','H')->with('author')->paginate(5);
        return response()->json($archive);
    }  

    public function loadmoreArchiveFinished () {
        $archive = Archive::where('task_status' ,'=','D')->with('author')->paginate(5);
        return response()->json($archive);
    }

    # load more archive 
    public function loadmoreArchiveDismissed () {
        $archive = Archive::where('status' ,'=','H')->with('author')->paginate(5);
        return response()->json($archive);
    }

    #un archived task
    public function unarchive (Request $data) {
        $archive = Archive::find($data->task_id);
        if ($archive) {

            $task = TaskComment::with('task')->where('task_id' , $data->task_id)->get();
            if ($task->isEmpty()) {
                # PUT THE TASK ON NEW TASK
                $archive->task_status = 'N'; # TASK COLUMN ON BOARD
                $archive->status = 'A'; # ACTIVE
                $archive->save();
                return response()->json("success");
            } else{
                $archive->task_status = 'I'; # TASK COLUMN ON BOARD ONGOING , IN PROGRESS
                $archive->status = 'A'; # ACTIVE
                $archive->save();
                return response()->json("success");
            }
        }
    }
}
