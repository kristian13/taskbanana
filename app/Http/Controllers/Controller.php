<?php

namespace App\Http\Controllers;

use Auth;
use View;
use DB;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct() {

        if(Auth::user()){
            $current_user = DB::table('users as u')
            ->join('theme_color as t', 't.theme_color_id', '=', 'u.theme_color_id')
            ->leftJoin("profile_image as pi" , function($q) {
                $q->on('u.id' ,  '=' , 'pi.user_id')
                    ->where('pi.status' , null)
                    ->where('pi.is_profile', 1);
                })
            ->where("u.id" , Auth::user()->id)->get()->first();

        } else{
            $current_user = null;
        }
        
        View::share('current_user',$current_user);
        
    }
}
