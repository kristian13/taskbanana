<?php

namespace App\Http\Controllers\API;

use DB;
use Auth;
use File;
use App\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use function GuzzleHttp\json_encode;

class MemberApiController extends Controller
{
    //
    public function add_new_member(Request $request){
    	$validator =  Validator::make($request->all(), [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255' ,
            'email' => 'required|string|email|max:255|unique:users,email',
        ]);

        
        if (!$validator->fails()) {
          
            # check if creator or member is adding
            if(Auth::user()->created_by == null){ # Parent creator member
                $parent_business_owner_id = Auth::user()->id;
                $owner_accept_status = 1;
            } else{ # Member 
                $parent_business_owner_id = Auth::user()->parent_business_owner_id;
                $parent = true;
                $owner_accept_status = 0;
            }

            $last_id = DB::table('users')->insertGetId([
                'firstname'                => htmlspecialchars($request->firstname, ENT_QUOTES, 'UTF-8'),
                'lastname'                 => htmlspecialchars($request->lastname, ENT_QUOTES, 'UTF-8'),
                'account_type_id'          => 1,
                'business_nature_child_id' => Auth::user()->business_nature_child_id,
                'business_name'            => Auth::user()->business_name,
                'parent_business_owner_id' => $parent_business_owner_id , 
                'email'                    => $request->email,
                'subscription_status'      => 0,
                'owner_accept_status'      => $owner_accept_status , 
                'password'                 => bcrypt('test1234'),
                'created_by'               => Auth::user()->id
            ]);

            if ($last_id) {

                # UPDATE UNIQUE COLUMN FOR SECURITY
                $result = DB::table('users')->where('id', $last_id)->update( ["user_unique" => bcrypt ($last_id) ] );

                # ADD NEW MEMBER
                DB::table('task_users')->insertGetId([
                    'parent_user_id' =>     $parent_business_owner_id,
                    'child_user_id' =>      $last_id,
                ]);

                $path = public_path().'/profile/'.$last_id;
                if (!file_exists($path)) {
                    File::makeDirectory($path, $mode = 0777, true, true);
                    $this->create_index_html($path);
                }

                # check if user logged has data 
                $check = DB::table('task_users')
                            ->select('*')
                            ->where("child_user_id" , Auth::user()->id)
                            ->get()->first();

                if (!$check) {
                    # ADD NEW MEMBER
                    DB::table('task_users')->insertGetId([
                        'parent_user_id' =>     $parent_business_owner_id,
                        'child_user_id' =>      $parent_business_owner_id
                    ]);
                }   
            }

            return response()->json([
                'status'  => true,
                'action'  => 'add_new_member',
                'data'    => DB::table('users')->where('id', $last_id)->get()->first(),
                'message' => $validator->messages()->getMessages()           
            ]);

        } 
        else {
            return response()->json([
                'status'  => false,
                'action'  => 'add_new_member',
                'data'    => null,
                'message' => $validator->messages()->getMessages()           
            ]);
        }
    }

    # FUNCTION TO GET MY BUSINESS TEAM
    public function get_my_business_team (Request $req) {

        
        # MEMEBER ONLY
        if(Auth::user()->created_by != null) {
            $results = User::whereNotIn('id',[Auth::user()->id])
                            ->where("owner_accept_status" , 1 )
                            ->where('parent_business_owner_id',Auth::user()->parent_business_owner_id)
                            ->where('created_by',Auth::user()->id)
                            ->get();
        }

        # CREATOR ONLY
        else {
            $results  = User::whereNotIn('id',[Auth::user()->id])
                            ->where("owner_accept_status" , 1 )
                            ->where('parent_business_owner_id',Auth::user()->parent_business_owner_id)
                            ->get();
        }
        
       

        if(isset($req->id)){
            $return = User::where('id',$req->id)
                            ->where('created_by',Auth::user()->id)
                            ->get();
            
            if(!$return->isEmpty()) {
                return response()->json([
                    'status'  => true,
                    'action'  => 'get_my_business_team',
                    'data'    => $return,
                    'message' => "Member Found..."           
                ]);
            }
            else{
                return response()->json([
                    'status'  => true,
                    'action'  => 'get_my_business_team',
                    'data'    => null,
                    'message' => "No Data Found..."           
                ]);
            }

        }

      
        if(!$results->isEmpty()){
            return response()->json([
                'status'  => true,
                'action'  => 'get_my_business_team',
                'data'    => $results,
                'message' => "Members Found..."
            ]);
        }
        else{
            return response()->json([
                'status'  => false,
                'action'  => 'get_my_business_team',
                'data'    => null,
                'message' => "No Data Found..."    
            ]);
        }
                   
    }

    public function pending_accept_member_lists () {
        $results = DB::table('users as u')
            ->select('*')
            ->join('task_users as tu' , 'tu.child_user_id' , '=' , 'u.id')
            ->where("parent_user_id" , '='  ,Auth::user()->parent_business_owner_id)
            ->where("owner_accept_status" ,  0)
            ->groupBy("id")
            ->select(['u.*'])
            ->get();
        
        if(!$results->isEmpty()){
            return response()->json([
                'status'  => true,
                'action'  => 'pending_accept_member_lists',
                'data'    => $results,
                'message' => "Pending members..."   
            ]);
        }
        else{
            return response()->json([
                'status'  => false,
                'action'  => 'pending_accept_member_lists',
                'data'    => null,
                'message' => "No Data members..."   
            ]);
        }
    }

    public function accept_member_function ( Request $req ) {     

        $results = DB::table('users as u')
                    ->join('task_users as tu' , 'tu.child_user_id' , '=' , 'u.id')
                    ->where("parent_user_id" , '='  ,Auth::user()->parent_business_owner_id)
                    ->where("owner_accept_status" ,  0)
                    ->groupBy("id")
                    ->select('u.id')
                    ->get();
       
        $ids = [];
        foreach($results as $result) {
            $ids[] = $result->id;
        }

        # check Submit ID if belong on Creator 
        if( in_array($req->id,$ids) ){

            $return = DB::table('users')
                        ->where("id", $req->id )
                        ->update(['owner_accept_status' => 1 ]); 

            if ($return) {
                # ADD LOGS FOR USER
                $hash = DB::table('users')->find($req->id);

                $this->user_logs($hash, 'accept_member_lists' ,$req);
                
                return response()->json([
                    'status'  => true,
                    'action'  => 'accept_member_function',
                    'data'    => $return,
                    'message' => "Member successfully accepted..."
                ]);
            }
            else {
                return  response()->json([
                    'status'  => false,
                    'action'  => 'accept_member_function',
                    'data'    => null,
                    'message' => "Please Try Again..."  
                ]);
            }

        } 
        
        # Not belongs ON Creator member
        else {
            return response()->json([
                'status'  => false,
                'action'  => 'accept_member_function',
                'data'    => null,
                'message' => "Sorry cant Accept Member..."  
            ]);
        }

       
    }

    # LOGS FOR USERS
    public function user_logs ($data = false , $action_type = false , $req) {
        # GET THE USER ID OF UNIQUE USER
        $user_id = DB::table('users as u')
            ->select('*')
            ->where("user_unique"  , $data->user_unique)
            ->get()->first();

        $last_id = DB::table('users_logs')->insertGetId([
            'user_id' =>     $user_id->id,
            'edited_by' =>   Auth::user()->id , 
            'ip_address' => $req->ip() , 
            'user_agent' => $req->server('HTTP_USER_AGENT') ,
            'action_type' => $action_type
        ]);
    }

    public function create_index_html($folder) {
        $content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Directory access is forbidden.</p></body></html>";
        $folder = $folder.'/index.html';
        $fp = fopen($folder , 'w');
        fwrite($fp , $content);
        fclose($fp);
    }
}
