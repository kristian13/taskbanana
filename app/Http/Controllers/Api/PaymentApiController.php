<?php

namespace App\Http\Controllers\API;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use function GuzzleHttp\json_encode;

class paymentApiController extends Controller
{
    //
    public function payment_history($req) {
      
        $UserSubscriptions = Auth::user()->load([
            'user_subscriptions' => function($q) use ($req) {
                if($req->search != null) {
                    $q->where("amount","LIKE","%".$req->search."%");
                }
                
                $q->with([
                    'subscription' => function($qq) use ($req) {
                        $qq->orWhere("subscription_name","LIKE","%".$req->search."%");
                    }
                ]);

                $q->paginate($req->page_length);
            }
        ]);
        
        // User::with([
        //     'user_subscriptions' => function($q) use ($req) {
        //         $q->join('subscription as s','s.subscription_id','=','user_subscription.subscription_id');
                
        //         if($req->search != null) {
        //             $q->where("amount","LIKE","%".$req->search."%");
        //             $q->orWhere("subscription_name","LIKE","%".$req->search."%");
        //         }
               
        //         // if($req->order[0]['column'] == "0") {
        //         //     $q->orderBy('subscription_name',$req->order[0]['dir']);
        //         // }

        //         // if($req->order[0]['column'] == "1") {
        //         //     $q->orderBy('date_created',$req->order[0]['dir']);
        //         // }

        //         // if($req->order[0]['column'] == "2") {
        //         //     $q->orderBy('amount',$req->order[0]['dir']);
        //         // }
                
        //         //if($show == 'page'){
        //         //}
        //     }
        // ])->find(Auth::user()->id);

        return $UserSubscriptions;
    }

    public function load_process(Request $req) {
        $subscriptions_list =  $this->payment_history($req)->user_subscriptions;
        return $subscriptions_list;
    }

}
