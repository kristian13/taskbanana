<?php

namespace App\Http\Controllers\Api;


use DB;
use Auth;
use File;

use App\User;
use App\PasswordReset;

use App\BusinessNatureParent;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;

use App\Notifications\PasswordResetApiRequest;
use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Facades\UserVerification;

//For send reset email
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;


class LoginApiController extends Controller
{

    use SendsPasswordResetEmails, ResetsPasswords {
        SendsPasswordResetEmails::broker insteadof ResetsPasswords;
    }

    public function index () {
    	$result = DB::table('users as u')
            ->select('*')
            ->where("email" ,  'super@trackerteer.com')
            ->where("password" , '$2y$10$FphNW8OjiCngvP/LcDiqB.k1bDqEzQ6TjABPpv0BeMVGn4LSg1Egy')
            ->get();
       return response()->json($result);
    }

    public function login(Request $request){ 
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
            
            if( Auth::user()->verified == '0' ){
                // UserVerification::generate(Auth::user());
                // UserVerification::send(Auth::user(), 'TaskBanana Verification Link');

                Auth::logout(); //logout user if not verified
                return  response()->json([
                    'status'  => false,
                    'action'  => 'login',
                    'data'    =>  '',
                    'message' => "Please check your verification link on your email."
                ]);
            }

            if ( Auth::user()->verified == '1' ) {
                
                $Auth = Auth::user(); 
                $success['user']  =  $Auth;
                $success['token'] =  $Auth->createToken('MyApp')->accessToken;

                $path = public_path().'/profile/'.$Auth->id;
                if (!file_exists($path)) {
                    File::makeDirectory($path, $mode = 0777, true, true);
                    $this->create_index_html($path);
                }

                return  response()->json([
                    'status'  => true,
                    'action'  => 'login',
                    'data'    =>  $Auth,
                    'token'   =>  $Auth->createToken('MyApp')->accessToken,
                    'message' => "You have successfully Login."
                ]);
            }
        
        } else { 

            return  response()->json([
                'status'  => false,
                'action'  => 'login',
                'data'    =>  null,
                'token'   =>  "",
                'message' => "Invalid Email or Password."
            ]);

        } 
    }
    
    public function create(Request $req)
    {   

        $validator = Validator::make($req->all(), [
            // "firstname" => "required",
            // "lastname" => "required",
            "email" => "email|unique:users",
            // "password" => "required|confirmed|min:8",
            // "timezone" => "required",
            // "business_nature_child_id" => "required",
            // "business_name" => "required"
        ]);

        if (!$validator->fails()) {

            $data = User::create([
                'email'                    => $req->email,
                'password'                 => bcrypt($req->password),
                'firstname'                => htmlspecialchars($req->firstname, ENT_QUOTES, 'UTF-8'),
                'lastname'                 => htmlspecialchars($req->lastname, ENT_QUOTES, 'UTF-8'),
                'address'                  => htmlspecialchars($req->address, ENT_QUOTES, 'UTF-8'),
                'city'                     => htmlspecialchars($req->city, ENT_QUOTES, 'UTF-8'),
                'zipcode'                  => htmlspecialchars($req->zipcode, ENT_QUOTES, 'UTF-8'),
                'timezone'                 => htmlspecialchars($req->timezone, ENT_QUOTES, 'UTF-8'),
                'business_name'            => htmlspecialchars($req->business_name, ENT_QUOTES, 'UTF-8'),
                'account_type_id'          => 1,
                'subscription_id'          => 1,
                'subscription_type'        => 'B',
                'owner_accept_status'      => 1,
                'business_nature_child_id' => $req->business_nature_child_id
            ]);

            if ($data) {
                $sub = DB::table('subscription')
                    ->select('*')
                    ->where('subscription_id' , '=' , 1 )->get()->first();

                $last_id = DB::table('users')
                    ->select('*')
                    ->where('email' , '=' , $data->email )->get()->first();  

                DB::table('user_subscription')->insertGetId([
                    'subscription_id'  => $sub->subscription_id ,
                    'amount'           => $sub->subscription_amount,  
                    'user_id'          => $last_id->id 
                ]);

                $result = DB::table('users')
                    ->where('id', $last_id->id)
                    ->update([
                        'parent_business_owner_id' => $last_id->id, 
                        "user_unique"              => bcrypt($last_id->id)
                    ]);
            }

            UserVerification::generate($data);
            UserVerification::send($data, 'TaskBanana Account Verification');
            
            $resultReturn = response()->json([
                'status'  => true,
                'action'  => 'register',
                'data'    =>  $data,
                'message' => "Please Check your email for verification."
            ]);

            return $resultReturn;


        } else {   

            return response()->json([
                'status'  => false,
                'action'  => 'register',
                'data'    =>  null,
                'message' => $validator->messages()->getMessages()
            ]);
        }      
        
        //return $last_id->id;
    }

    public function reset(Request $req) {
        
        
        $validator = Validator::make($req->all(),[
            'email' => 'required|string|email',
        ]);
        

        if (!$validator->fails()) {
            
            $user = User::where('email', $req->email)->first();

            if (!$user){
                return response()->json([
                    'status'  => false,
                    'action'  => 'forgot_password',
                    'data'    =>  null,
                    'message' => 'We cant find a user with that e-mail address.'
                ]);
            }

            $token = Str::random(60);

            $passwordReset = PasswordReset::updateOrCreate(
                ['email' => $user->email],
                [
                    'email' => $req->email,
                    'token' => bcrypt($token)
                ]
            );

            $user->remember_token = $token;
            $user->save();

            if ($user && $passwordReset){
                $user->notify(
                    new PasswordResetApiRequest($token,$req->email)
                );
            }

            return response()->json([
                'status'  =>  true,
                'action'  => 'forgot_password',
                'data'    =>  $req->email,
                'message' => 'We have e-mailed your password reset link!'
            ]);
            
        } else {
            return response()->json([
                'status'  =>  false,
                'action'  => 'forgot_password',
                'data'    =>  null,
                'message' =>  $validator->messages()->getMessages()
            ]);
        }
       
    }

    public function get_business_nature() {
        $Business = BusinessNatureParent::with(['nature_child'])->get();
       if($Business){
            return response()->json([
                'status'  => true,
                'action'  => 'get_business_nature',
                'data'    => $Business,
                'message' => "",
            ]);
       } else{
            return response()->json([
                'status'  => false,
                'action'  => 'get_business_nature',
                'data'    => $Business,
                'message' => "",
            ]);
       }
    }

    public function create_index_html($folder) {
        $content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Directory access is forbidden.</p></body></html>";
        $folder = $folder.'/index.html';
        $fp = fopen($folder , 'w');
        fwrite($fp , $content);
        fclose($fp);
    }

}
