<?php

namespace App\Http\Controllers\Api;

use DB;
use Hash;
use Auth;

use App\Profile;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProfileApiController extends Controller
{ 
    // API REQUEST 
    public function show() { 
        #GET ALL INSTALL OF USER
        $User = Auth::user();

        $data  = $User; 
        $data['theme']  = $User->theme;
        $data['images'] = $User->images;
        $data['timezone'] = $User->timezone;
        //$data['Business_nature'] = BusinessNatureParent::all();
       
        return response()->json([
            'status'  => true,
            'action'  => 'show',
            'data'    =>  $data,
            'message' => "Profile data"           
        ]);

    }

    public function profile_update(Request $req) {
      
        #GET ALL INSTALL OF USER
        $User =Auth::user();

        #PREPARE FOR VALIDATION
        $set = [
            'city'          => $req->city,
            'state'         => $req->state,
            'zipcode'       => $req->zipcode,
            'address'       => $req->address,
            'lastname'      => $req->lastname,
            'firstname'     => $req->firstname,
            'business_name' => $req->business_name
        ];

        $validator = Validator::make($set,[
            'city'          => 'max:255',
            'state'         => 'max:255',
            'zipcode'       => 'max:255',
            'address'       => 'max:255',
            'lastname'      => 'required|string|max:255',
            'firstname'     => 'required|string|max:255',
            'business_name' => 'required|string|max:255'
        ]);

        if (!$validator->fails()) {

            if ($User->update($set)) {
                return response()->json([
                    'status'  => true,
                    'action'  => 'profile_update',
                    'data'    =>  $User,
                    'message' => "Profile Successfully Updated..."        
                ]);
            }else{
                return response()->json([
                    'status'  => false,
                    'action'  => 'profile_update',
                    'data'    => null,
                    'message' => "Please Try Again..."
                ]);
            }

        } else {   

            return response()->json([
                'status'  => false,
                'action'  => 'profile_update',
                'data'    => null,
                'message' => $validator->messages()->getMessages()
            ]);
        }       
    }

    public function use_image(Request $req) {

        $result = AUTH::user();

         #SET 0 OLD PRIMARY PICTURE
         if ( $result->profile_image_id > 0) {
            $old = $result->images()->find($result->profile_image_id);
            
            if ( $old ) {
                $old->is_profile = 0;
                $old->save();
            }                                        
        }
        
        $old_image = isset($old->profile_image_id) ? $old->profile_image_id : 0;
        
        #UPDATE Profile_image_id on USER TABLE;
        $result->profile_image_id = $req->id;
        $result->save();

        #SET 1 NEW PRIMARY PICTURE
        $new = $result->images()->find($req->id);

        $new->is_profile = 1;
        $new->save();                    

        return response()->json([
            'status'  => true,
            'action'  => 'use_image',
            'data'    => url('/profile/'.Auth::user()->id.'/'.$new->image),
            'message' => 'Profile picture updated...'
        ]);

    }

    //mark image remove 
    public function remove_image(Request $req) {
        $user  = Auth::user();        

        // relationship update
        $profile = $user->images()
                            ->where('profile_image_id',$req->id)
                            ->first();

        $profile->is_profile  = 0;
        $profile->deleted_at  = date('Y-m-d H:m:s');
        $profile->status      = "D";

        if ($profile->save()) {
            
            return response()->json([
                'status'  => true,
                'action'  => 'remove_image',
                'data'    => $profile,
                'message' => "Image Successfully remove..."
            ]);

          
        } else {
            return response()->json([
                'status'  => false,
                'action'  => 'remove_image',
                'data'    => null,
                'message' => "Please Try Again..."
            ]);
        }
    }

    public function profile_email(Request $req) {
       
        #GET ALL INSTALL OF USER
        $User = Auth::user();

        #PREPARE FOR VALIDATION
        $set = [
            'email' => $req->email,
        ];

        $validator = Validator::make($set,[
            'email' => 'required|string|max:255'
        ]);

        if (!$validator->fails()) {

            if ($User->update($set)) {
                return response()->json([
                    'status'  => true,
                    'action'  => 'profile_email',
                    'data'    => $User,
                    'message' => "Email Successfully Updated..."
                ]);
            }else{
                return response()->json([
                    'status'  => false,
                    'action'  => 'profile_email',
                    'data'    => null,
                    'message' => "Please Try Again..."
                ]);
            }

        } else {   
            return response()->json([
                'status'  => false,
                'action'  => 'profile_email',
                'data'    => null,
                'message' => $validator->messages()->getMessages()
            ]);
        }       
    }

    // language
    

    public function profile_theme_color(Request $req) {

        #GET ALL INSTALL OF USER ID
        $User = Auth::user();

        #PREPARE FOR VALIDATION
        $set = [
            'theme_color_id' => $req->theme_color_id,
        ];

        $validator = Validator::make($set,[
            'theme_color_id' => 'required'
        ]);

        if (!$validator->fails()) {

            if ($User->update($set)) {
                return response()->json([
                    'status'  => true,
                    'action'  => 'profile_theme_color',
                    'data'    => $User,
                    'message' => 'Theme color Successfully Updated...'
                ]);
            }else{
                return response()->json([
                    'status'  => false,
                    'action'  => 'profile_theme_color',
                    'data'    => null,
                    'message' => 'Please Try Again...'
                ]);
            }

        } else {   
            return response()->json([
                'status'  => false,
                'action'  => 'profile_theme_color',
                'data'    => null,
                'message' => $validator->messages()->getMessages()
            ]);
        }       
    }

    public function profile_password(Request $req) {
     
        $User = Auth::user();

        if (Hash::check($req->old_password, $User->password)) {
            if(Hash::check($req->password, $User->password)) {
                return response()->json([
                    'status'  => true,
                    'action'  => 'profile_password',
                    'data'    => $User,
                    'message' => 'Password Successfully Updated...'
                ]);
            }

            $set = ['password' => $req->password];

            $validator = Validator::make($set,[
                'password' => 'required|string|min:8'
            ]);

            if (!$validator->fails()) {
                $password = Hash::make($req->password);
                $User->password = $password;
                if($User->save()){
                    return response()->json([
                        'status'  => true,
                        'action'  => 'profile_password',
                        'data'    => $User,
                        'message' => 'Password Successfully Updated...'
                    ]);
                }
            } else {   
                return response()->json([
                    'status'  => false,
                    'action'  => 'profile_password',
                    'data'    => null,
                    'message' => $validator->messages()->getMessages()
                ]);
            }      
            
        } else {
            return response()->json([
                'status'  => false,
                'action'  => 'profile_password',
                'data'    => null,
                'message' => 'Incorrect Password...'
            ]);
        }
    }

    public function upload_image (Request $request) {

        $files = $request->file('images'); // return {}
        
        $total = 0;
        $count = 0;
        foreach ($files as $value) {
            
            $total = count($_FILES['images']['name']);

            $destinationPath = public_path(). '/profile/'.Auth::user()->id.'/';
            $newName   = date('dmY')."_".uniqid()."_".$value->getClientOriginalName();
            
            if ($value->move($destinationPath,  $newName)) {
                $Profile = new Profile;
                $Profile->user_id = Auth::user()->id;
                $Profile->image   = $newName;
                if($Profile->save()){
                    $count++;
                }
            }

        }

        if($total == $count) {
            return response()->json([
                'status'  => true,
                'action'  => 'upload_image',
                'data'    =>  $_FILES['images']['name'],
                'message' => 'Files Successfully Uploaded...'
            ]);
        } else {
            return response()->json([
                'status'  => false,
                'action'  => 'upload_image',
                'data'    => $_FILES['images']['name'],
                'message' => 'Something went wrong please try again...'
            ]);
        }
        

    }

    public function update_timezone(Request $req) {
       
        $user = Auth::user();
        $user->timezone = $req->timezone;
        if($user->save()) {
            return response()->json([
                'status'  => true,
                'action'  => 'update_timezone',
                'data'    =>  $req->timezone,
                'message' => 'Timezone updated...'
            ]);
        }

        return response()->json([
            'status'  => false,
            'action'  => 'update_timezone',
            'data'    =>  null,
            'message' => 'Please Try Again...'
        ]);
     
    }

    public function timezone_list() {
       
        static $regions = array(
            \DateTimeZone::AFRICA,
            \DateTimeZone::AMERICA,
            \DateTimeZone::ANTARCTICA,
            \DateTimeZone::ASIA,
            \DateTimeZone::ATLANTIC,
            \DateTimeZone::AUSTRALIA,
            \DateTimeZone::EUROPE,
            \DateTimeZone::INDIAN,
            \DateTimeZone::PACIFIC
        );
  
        $timezones = array();
        foreach( $regions as $region )
        {
            $timezones = array_merge( $timezones, \DateTimeZone::listIdentifiers( $region ) );
        }
  
        $timezone_offsets = array();
        foreach( $timezones as $timezone )
        {
            $tz = new \DateTimeZone($timezone);
            $timezone_offsets[$timezone] = $tz->getOffset(new \DateTime);
        }
  
        // sort timezone by offset
        asort($timezone_offsets);
  
        $timezone_list = array();
        foreach( $timezone_offsets as $timezone => $offset )
        {
            $offset_prefix = $offset < 0 ? '-' : '+';
            $offset_formatted = gmdate( 'H:i', abs($offset) );
  
            $pretty_offset = "UTC${offset_prefix}${offset_formatted}";
  
            $timezone_list[$timezone] = "(${pretty_offset}) $timezone";
        }
        
        return $timezone_list;
    }

    // public function destroy(Request $request,$id) {
    //     $user = json_decode($request->user);
    //     $User = User::find($user->id);

    //     $todos = $User->todo_list()->find($id);
    //     $todos->todo_status = "R";
        
    //     if($todos->save()){
    //         return json_encode(['result'=>'success']);
    //     }
    // }
}