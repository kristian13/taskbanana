<?php

namespace App\Http\Controllers\API;

use DB;
use Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProjectApiController extends Controller
{

    public function add_project (Request $request) {

        # Only Creator can create PROJECT TITlE
        if(Auth::user()->created_by != null){
            return response()->json([
                'status'  => false,
                'action'  => 'add_project',
                'data'    => null,
                'message' => "Only Creator can create PROJECT TITlE."
            ]);
        }

        $validator =  Validator::make($request->all(), [
            'project_title' => 'required|string',
        ]);

        if (!$validator->fails()) {
            $category_id = DB::table('task_categories')->insertGetId([
                'category_name' => htmlspecialchars($request->project_title, ENT_QUOTES, 'UTF-8'),  
                'created_by'    => Auth::user()->id ,
            ]);

            $bcrypt = bcrypt($category_id);
            
            if ($category_id) {
                DB::table('task_categories')
                ->where('category_id', $category_id)
                ->update(['category_unique' => $bcrypt ]);
            }

            $category = DB::table('task_categories')->where('category_id',$category_id)->get()->first();
            
            return response()->json([
                'status'  => true,
                'action'  => 'add_project',
                'data'    =>  $category,
                'message' => "Successfully added Project title."
            ]);

        } else {
            return response()->json([
                'status'  => false,
                'action'  => 'add_project',
                'data'    =>  null,
                'message' => $validator->messages()->getMessages()
            ]);
        }
    }

    # get all categories
    public function get_categories () {
      
        $user     = Auth::user();
        $projects = Auth::user()->task_category;


        foreach($projects as $k=>$result) {
             
            $d = DB::select("
                          select count(*) as DONE from task_assign as ta 
                              left join tasks as t ON t.task_id = ta.task_id 
                              left join task_categories as tc ON tc.category_id = t.category_id 
                          where ta.user_id = $user->id AND t.task_status = 'D' AND tc.category_id = $result->category_id
                ");

            $t = DB::select("
                         select count(*) as TOTAL from task_assign as ta 
                             left join tasks as t ON t.task_id = ta.task_id 
                             left join task_categories as tc ON tc.category_id = t.category_id 
                         where ta.user_id = $user->id AND tc.category_id = $result->category_id 
                ");

            # Fixed on error Total Count Divided zero 
            if($t[0]->TOTAL == 0) {
                $t[0]->TOTAL = 1; # 1 on total if total value is 0
            }

            # GET TOTAL PERCENT
            $total_percent = ($d[0]->DONE/$t[0]->TOTAL * 100);
            
            $projects[$k]['percent'] = number_format($total_percent,2);
        }
        
        return response()->json([
            'status'  => true,
            'action'  => 'get_categories',
            'data'    =>  $projects,
            'message' => "Project List..."    
        ]);
    }

    # GET ALL LIST ON ONE CATEGORY
    public function get_all_under_category(Request $req){
        
        $user  = Auth::user();

        $results = DB::select("
                        select t.* from task_assign as ta 
                            left join tasks as t ON t.task_id = ta.task_id 
                            left join task_categories as tc ON tc.category_id = t.category_id 
                        where ta.user_id = $user->id AND tc.category_id = $req->category_id  
                    ");
        
        return response()->json([
            'status'  => true,
            'action'  => 'get_all_under_category',
            'data'    =>  $results,
            'message' => "Task List..."
        ]);
    }

    # Edit category
    public function update_category(Request $req) {
        
        $data = Auth::user()->categories()->find($req->category_id);

        # Cant edit NO PROJECT TITLE
        if($req->category_id == 1){
            return response()->json([
                'status'  => false,
                'action'  => 'update_category',
                'data'    =>  null,
                'message' => "Cant update No Project Title..."    
            ]);
        }

        if($data) {
            # Cant edit NO PROJECT TITLE
            if($data->created_by != Auth::user()->id){
                return response()->json([
                    'status'  => false,
                    'action'  => 'update_category',
                    'data'    =>  null,
                    'message' => "Sorry cant update this title..."    
                ]);
            }

            $validator =  Validator::make($req->all(), [
                'project_title' => 'required|string',
            ]);
    
            if (!$validator->fails()) {

                $data->category_name = $req->project_title;

                # Cant edit NO PROJECT TITLE
                if($data->save()){
                    return response()->json([
                        'status'  => true,
                        'action'  => 'update_category',
                        'data'    => $data,
                        'message' => "Successfully updated Category title..."    
                    ]);
                }

            } else {
                return response()->json([
                    'status'  => false,
                    'action'  => 'update_project',
                    'data'    =>  null,
                    'message' => $validator->messages()->getMessages()
                ]);
            }
        
        } else {

            return response()->json([
                'status'  => false,
                'action'  => 'update_category',
                'data'    => null,
                'message' => "Sorry your NOT authorized..."    
            ]);

        }


    }


}
