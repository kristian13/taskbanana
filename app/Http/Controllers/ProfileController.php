<?php

namespace App\Http\Controllers;

use Hash;
use Auth;
use File;

use App\User;
use App\Profile;
use App\BusinessNatureParent;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Intervention\Image\ImageManagerStatic as Image;


class ProfileController extends Controller
{

    public function index (Request $request) {

        return view("profile" , 
            [
                "Profile"                => Auth::user(),
                "Current_image"          => Auth::user()->profile_image(),
                "Get_images"             => Auth::user()->images()->Where('status','=',null)->get(),
                "Business_Nature_Parent" => BusinessNatureParent::all(),
                "get_my_business_team"   => app('App\Http\Controllers\MemberController')->get_my_business_team() ,
                "get_my_personal_team"   => array(),
                "timezones"              => $this->generate_timezone_list(),
                "User"                   => User::find(Auth::user()->id) ,
                "new_members"            => app('App\Http\Controllers\MemberController')->accept_member_lists_count() 
            ]
        );
    }

    public function create_index_html($folder) {
        $content = "<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Directory access is forbidden.</p></body></html>";
        $folder = $folder.'/index.html';
        $fp = fopen($folder , 'w');
        fwrite($fp , $content);
        fclose($fp);
    }

    function resize_image($file, $w, $h, $crop=false) {
        list($width, $height) = getimagesize($file);
        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width-($width*abs($r-$w/$h)));
            } else {
                $height = ceil($height-($height*abs($r-$w/$h)));
            }
            $newwidth = $w;
            $newheight = $h;
        } else {
            if ($w/$h > $r) {
                $newwidth = $h*$r;
                $newheight = $h;
            } else {
                $newheight = $w/$r;
                $newwidth = $w;
            }
        }
        
        //Get file extension
        $exploding = explode(".",$file);
        $ext = end($exploding);
        
        switch($ext){
            case "png":
                $src = imagecreatefrompng($file);
            break;
            case "jpeg":
            case "jpg":
                $src = imagecreatefromjpeg($file);
            break;
            case "gif":
                $src = imagecreatefromgif($file);
            break;
            default:
                $src = imagecreatefromjpeg($file);
            break;
        }
        
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        return $dst;
    }

    public function get_profile_info () {
        return response()->json([
            "Current_image"          => Auth::user()->profile_image(),
            "Get_images"             => Auth::user()->images()->Where('status','=',null)->get()
        ]);
    }

    public function update (Request $request) {

        $result = User::Where('id',AUTH::user()->id)->first();

        switch ($request->update) {
            case 'personal_update':
                        
                    $validator =  Validator::make($request->all(), [
                        'firstname'     => 'required|string|max:255',
                        'lastname'      => 'required|string|max:255',
                        'address'       => 'required|string|max:255',
                        'city'          => 'required|string|max:255',
                        'state'         => 'required|string|max:255',
                        'zipcode'       => 'required|string|max:255',
                        'business_name' => 'required|string|max:255'
                    ]);

                    if (!$validator->fails()) {

                        $result->firstname       = $request->firstname;
                        $result->lastname        = $request->lastname;
                        $result->address         = $request->address;
                        $result->city            = $request->city;
                        $result->state           = $request->state;
                        $result->zipcode         = $request->zipcode;
                        $result->business_name   = $request->business_name;
                        $result->business_nature_child_id = $request->business_nature;
                        $result->save();

                        return response()->json([
                            'result'  => 'success',
                            'request' => $request
                        ]);
                    
                     } else {
                        
                        // Each failed field will have one or more messages.
                        return response()->json([
                            'result'  => 'error',
                            'message' => $validator->messages()->getMessages(),
                            'request' => $request
                        ]);
                        
                     }

                break;

            case 'use_image':
                    
                    #SET 0 OLD PRIMARY PICTURE
                    if ( $result->profile_image_id > 0) {
                        $old = $result->images()->find($result->profile_image_id);
                        
                        if ( $old ) {
                            $old->is_profile = 0;
                            $old->save();
                        }                                        
                    }
                    
                    $old_image = isset($old->profile_image_id) ? $old->profile_image_id : 0;
                    
                    # UPDATE Profile_image_id on USER TABLE;
                    $result->profile_image_id = $request->id;
                    $result->save();

                    # SET 1 NEW PRIMARY PICTURE
                    $new = $result->images()->find($request->id);
                    $new->is_profile = 1;
                    $new->save();                    

                    return response()->json([
                        'result'      => 'success',
                        'image'       =>  url('/profile/'.Auth::user()->id.'/thumbnail_profile/'.$new->image),
                        'old_image'   =>  $old_image
                    ]);
                    
                break;

            case 'remove_image':

                    // relationship update
                    $profile = $result->images()
                                        ->where('profile_image_id',$request->id)
                                        ->first();
                    
                    $status =   $profile->is_profile == 1 ? true : false;

                    $profile->is_profile  = 0;
                    $profile->deleted_at  = date('Y-m-d H:m:s');
                    $profile->status      = "D";

                    if ($profile->save()) {
                       
                        return response()->json([
                            'result'          => 'success',
                            'image_id'    =>  $profile->profile_image_id,
                            'is_profile'      =>  $status
                        ]);

                    } else {
                        return response()->json([
                           'result' => 'error'
                        ]);
                    }

                break;

            case 'email_update':

                    $validator =  Validator::make($request->all(), [
                        'email'     => 'required|string|max:255'
                    ]);

                    if (!$validator->fails()) {

                        $result = User::Where('id',AUTH::user()->id)->first();
                        $result->email = $request->email;
                        $result->save();
                        return response()->json(['result' => 'success']);
                    
                     } else {
                        
                        // Each failed field will have one or more messages.
                        return response()->json([
                            'result' => 'error',
                            'message'=> $validator->messages()->getMessages()
                        ]);
                        
                     }
                break;

            case 'lang_update':
                break;

            case 'change_color':

                    $result->theme_color_id = $request->color;
                    if ( $result->save() ){
                        $message = "success";
                    } else {
                        $message = "error";
                    }

                    return response()->json(array('result'=>$message));
                break;
            
            default:
                # code...
                break;
        }

    }

    public function update_password(Request $request) {
        
        $user = User::Where('id',AUTH::user()->id)->first();
        if ($request->password != $request->retype_pass) {
            return response()->json([
                'result'  => 'error',
                'message' => 'Retype New Password'
            ]);
        }

        if (trim($request->password) == "" && trim($request->retype_pass) == "" && trim($request->old_pass) == ""  ) {
            return response()->json([
                    'result'  => 'error',
                    'message' => 'Password Required'
                ]);
        }

        if (Hash::check($request->old_pass, $user->password)) {
            if(Hash::check($request->password, $user->password)) {
                return response()->json([
                    'result'  => 'Success',
                    'message' => 'Password is same as old Password',
                ]);
            }

            $password = Hash::make($request->password);
            $user->password = $password;
            
            if($user->save()){
                return response()->json([
                    'result'  => 'Success',
                    'message' => 'Password Updated'
                ]);
            }
        } else {
            return response()->json([
                'result'  => 'error',
                'message' => 'Incorrect Old Password'
            ]);
        }
    }

    public function upload_image(Request $request) {
        
        $file = $request->file('images');

        foreach ($file as $key => $value) {
            $destinationPath = public_path(). '/profile/'.Auth::user()->id.'/';
            $newName         = date('dmY')."_".uniqid()."_".$value->getClientOriginalName();

            if ($value->move($destinationPath,  $newName)) {
                
                # Unset Current Profile picture
                $check = Auth::user()->images->where('is_profile', 1);
                if(!$check->isEmpty()){ 
                    Auth::user()->images()
                        ->where('is_profile', 1)
                        ->update(['is_profile' => 0]);   
                }

                # Save and Set as Current Profile picture
                $Profile             = new Profile;
                $Profile->user_id    = AUTH::user()->id;
                $Profile->image      = $newName;
                $Profile->is_profile = 1;
                $Profile->save();

                # thumbnails
                $path_thumbnail = public_path().'/profile/'.Auth::user()->id.'/thumbnail_profile/';
                if (!file_exists($path_thumbnail)) {
                    File::makeDirectory($path_thumbnail, $mode = 0777, true, true);
                    $this->create_index_html($path_thumbnail);
                }
                

                # if file is image
                $ext = array('jpg','jpeg','png','gif','tiff');
                if(in_array($value->getClientOriginalExtension(),$ext)){
                    $thumbnailPath = $destinationPath.$newName;
                    $image_resize = Image::make($thumbnailPath);              
                    # get file size
                    # $size = $image_resize->filesize();
                    $image_resize->resize(100, 100);
                    $image_resize->save($path_thumbnail.$newName);
                }
                
                //$imageData = $this->resize_image($destinationPath.$newName,100,100);
                // Give a name to your file with the full path
                $quality = 0;
                //imagepng($imageData,$path_thumbnail.$newName,$quality);
            }
        }
        
        return response()->json([
            'status' => true,
             'image' =>  'profile/'.Auth::user()->id.'/thumbnail_profile/'.$newName 
        ]);
    }

    public function update_timezone(Request $request) {
       
        $user = Auth::user();
        $user->timezone = $request->timezone;
        if($user->save()) {
            return response()->json(['result'=>'true']);
        }

        return response()->json(['result'=>'false']);
    }

    public function generate_timezone_list()
    {
       
        static $regions = array(
            \DateTimeZone::AFRICA,
            \DateTimeZone::AMERICA,
            \DateTimeZone::ANTARCTICA,
            \DateTimeZone::ASIA,
            \DateTimeZone::ATLANTIC,
            \DateTimeZone::AUSTRALIA,
            \DateTimeZone::EUROPE,
            \DateTimeZone::INDIAN,
            \DateTimeZone::PACIFIC
        );
  
        $timezones = array();
        foreach( $regions as $region )
        {
            $timezones = array_merge( $timezones, \DateTimeZone::listIdentifiers( $region ) );
        }
  
        $timezone_offsets = array();
        foreach( $timezones as $timezone )
        {
            $tz = new \DateTimeZone($timezone);
            $timezone_offsets[$timezone] = $tz->getOffset(new \DateTime);
        }
  
        // sort timezone by offset
        asort($timezone_offsets);
  
        $timezone_list = array();
        foreach( $timezone_offsets as $timezone => $offset )
        {
            $offset_prefix = $offset < 0 ? '-' : '+';
            $offset_formatted = gmdate( 'H:i', abs($offset) );
  
            $pretty_offset = "UTC${offset_prefix}${offset_formatted}";
  
            $timezone_list[$timezone] = "(${pretty_offset}) $timezone";
        }
        
        return $timezone_list;
    }
    
    
}
