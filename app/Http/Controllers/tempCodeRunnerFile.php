<?php
public function upload_image (Request $request) {
        $file = $request->file('image');
        
        foreach ($file as $key => $value) {

            //$destinationPath = "uploadedImages";
            //$newName = md5_file($request->file('file_name')->getRealPath());
            //$guessFileExtension = $request->file('file_name')->guessExtension();
            //$file = $request->file('file_name')->move($destinationPath, $newName.'.'.$guessFileExtension);

            $destinationPath = public_path(). '/profile/';
            $newName   = date('dmY')."_".uniqid()."_".$value->getClientOriginalName();
            $extension = $value->guessExtension();
            
            if ($value->move($destinationPath,  $newName)) {
                $Profile = new Profile;
                $Profile->user_id = AUTH::user()->id;
                $Profile->image   = $newName;
                $Profile->save();
            }

        }

        return json_encode(['result'=>'success']);

}