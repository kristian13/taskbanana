<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Facades\UserVerification;

use Jrean\UserVerification\Exceptions\TokenMismatchException;
use Jrean\UserVerification\Exceptions\UserNotVerifiedException;
use Jrean\UserVerification\Exceptions\UserIsVerifiedException;
use Jrean\UserVerification\Exceptions\UserNotFoundException;
use Jrean\UserVerification\Exceptions\UserHasNoEmailException;

class IsVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

    	if( !is_null($request->user()) && !$request->user()->verified){
            $data = response()->json([
                'status'  =>  true,
                'action'  =>  'login',
                'data'    =>  '',
                'message' =>  'Please Check your Email for your account verification.',
            ]);
            return false;
            // UserVerification::generate($request->user());
            // UserVerification::send($request->user(), 'TaskBanana Verification Link');
			// return redirect('email-verification/send_email');
		}
      
    	return $next($request);
    }
}

