<?php

namespace App\Http\Middleware;

use Closure;

use Auth;
use App\User;

class Invoice
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $User = User::with(['subscription'])->find(Auth::user()->id);
        # Not Found Record at Current user Login
        if(! $User->subscription()->find($request->id) ) {
            return redirect('payment');
        }
        return $next($request);
    }
}
