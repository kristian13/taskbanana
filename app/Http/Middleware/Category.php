<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Category
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        
        # Protect NO PROJECT TITLE EDIT
        if($request->category_id == 1) {
            return false;
        }



        return $next($request);
    }
}
