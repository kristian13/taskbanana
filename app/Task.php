<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';
    protected $primaryKey = 'task_id';

    public function get_creator () {
    	return $this->hasOne('App\User' , 'id' , 'created_by');
    } 

    public function get_attachment () {
        return $this->hasMany('App\TaskAttachment','task_id','task_id')
                    ->where('status',1);
    }

    public function get_assign () {
    	return $this->hasMany('App\TaskAssign','task_id','task_id')->with(['assignee_profile','get_user']);
    }

    function get_notif () {
         return $this->hasMany('App\Notifications','task_id','task_id');
    }
  
    public function get_comments () {
    	return $this->hasMany('App\TaskComment','task_id','task_id')->with(['get_attachment'])->join('users', function($q){
            $q->on('users.id','=','task_comments.user_id');
        })->Leftjoin('profile_image',function($q) {
            $q->on('profile_image.user_id','=','task_comments.user_id');
            $q->where('profile_image.is_profile',1);
        });
    }

    public function get_user(){
        return $this->hasOne('App\User','id','user_id');
    }

    public function category() {
        return $this->belongsTo('App\TaskCategory','category_id','category_id');
    }

    public function category_name() {
        return $this->hasOne('App\TaskCategory','category_id','category_id');
    }


}
