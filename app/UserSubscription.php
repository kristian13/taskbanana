<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSubscription extends Model
{

    protected $table      = 'user_subscription';
    protected $primaryKey = 'user_subscription_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_subscription_id','subscription_id', 'user_id' ,'amount', 'date_created' , 'subscription_status', 'subscription_type'
    ];

    public function subscription() {
        return $this->belongsTo('App\Subscription','subscription_id','subscription_id');
    }
}
