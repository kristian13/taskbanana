<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table      = 'subscription';
    protected $primaryKey = 'subscription_id';
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subscription_id', 'subscription_name' ,'subscription_amount','price'
    ];

}
