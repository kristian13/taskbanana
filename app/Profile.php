<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
 
 	/**
    * The table associated with the model.
    *
    * @var string
  */
  protected $table      = 'profile_image';
  protected $primaryKey = 'profile_image_id';
  
  protected $fillable = [
    'image', 'created_at', 'deleted_at', 'is_profile', 'status' 
  ];
  
  public function user() {   
    #TEST
		return $this->hasOne('App\User', 'id', 'user_id');
	}
   


}
