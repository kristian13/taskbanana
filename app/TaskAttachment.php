<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskAttachment extends Model
{
    protected $table = 'tasks_attachment';
    protected $primaryKey = 'tasks_attachment_id';

    protected $fillable = ['image','file_type','full_path'];

}
