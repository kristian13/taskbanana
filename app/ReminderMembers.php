<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReminderMembers extends Model
{
    //
    protected $table = 'reminder_members';
    protected $primaryKey = 'id';

    protected $fillable = [
        'reminder_id','user_id','status','start_time','repeat_every','next_repeat','recurrence','added_by','created_at','updated_at'
    ];

    
    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }

    public function reminder(){
        return $this->belongsTo('App\Reminder','reminder_id','id');
    }
}
