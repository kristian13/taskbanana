<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archive extends Model
{
    protected $table      = 'tasks';
    protected $primaryKey = 'task_id';

    public function author() {
        return $this->belongsTo('App\User','created_by','id')
            ->select([
                'id',
                'firstname',
                'lastname'
            ]);
    }
}
