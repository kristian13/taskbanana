<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessNatureParent extends Model
{

	protected $table = 'business_nature_parent';
	protected $primaryKey = 'business_nature_parent_id';
    
    public function nature_child() {
    	return $this->hasMany('App\BusinessNatureChild', 'business_nature_parent_id', 'business_nature_parent_id');
    }

   
}
