<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TodoList extends Model
{
    protected $table      = 'todo_list';
    protected $primaryKey = 'todo_id';
	

	protected $fillable = [ 'todo_name', 'date_from', 'is_profile','date_finished','created_at','date_from' ];

    public function user () {
    	$this->belongsTo('App\User', 'id', 'user_id');
    }
}
