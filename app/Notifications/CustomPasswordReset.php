<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use Illuminate\Auth\Notifications\ResetPassword; // add this.

class CustomPasswordReset extends ResetPassword // change extends from Notification to ResetPassword
{
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;
    public $email;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */
    public function __construct($token,$email)
    {
        $this->token = $token;
        $this->email = $email;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('TaskBanana Reset Password')
            ->line([
                'You are receiving this email because we received a password reset request for your account.',
                'Click the button below to reset your password:',
            ])
            ->line( "This password reset link will expire in ".config('auth.passwords.users.expire')." minutes" )
            ->action('Reset Password', url("account/reset/{$this->token}?email={$this->email}"))
            ->line('If you did not request a password reset, no further action is required.');
            // ->view('auth.passwords.email')
            // ->action('Reset Password', url('admin/password/reset', $this->token)); // add this. this is $actionUrl
    }
}