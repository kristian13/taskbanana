<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskReplyAttachment extends Model
{
    protected $table = 'tasks_reply_attachment';
    protected $primaryKey = 'tasks_reply_attachment_id';
}
