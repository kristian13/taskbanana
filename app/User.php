<?php

namespace App;

use DB;
use Auth;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Auth\Passwords\CanResetPassword;
use App\Notifications\CustomPasswordReset;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable,CanResetPassword;
  
    protected $primaryKey = 'id';
    protected $table      = 'users';  

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 
        'lastname',
        'address', 
        'city', 
        'state', 
        'zipcode', 
        'account_type_id',
        'business_nature_child_id' ,
        'business_name',
        'email',
        'password',
        'theme_color_id',
        'verified',
        'verification_token',
        'email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','static_password'
    ];
   
    public function sendPasswordResetNotification($token){
       $this->notify(new CustomPasswordReset($token,$this->email));
    }


    public function images () {
        return $this->hasMany('App\Profile', 'user_id', 'id'); 
    }

   
    public function profile_image() {

        $result = $this->images
                    ->where('is_profile',1)
                    ->where('status',null);

        if(!$result->isEmpty()){
            return (object)[
                        'status' => true,
                        'image'  => $result->first()->image
                    ];
        }else{
            return (object)[
                        'status' => false,
                        'image'  => 'static_image'
                    ];
        } 
    }

    public function archive() {
        return $this->hasMany('App\Archive','created_by','id')
                        ->where('task_status','D')
                        ->with(['author']); //
    }

    public function tasks() {
        return $this->hasMany('App\Task','created_by', 'id');
    }

    public function todo_list () {
        return $this->hasMany('App\TodoList', 'user_id', 'id')
        ->where('todo_status', "A");
    }
    public function todo_list_raw () {
        return $this->hasMany('App\TodoList', 'user_id', 'id');
    }

    public function todo_logs () {
        return $this->hasMany('App\TodoLogs','user_id','id');
    }

    public function get_task_assign() {
        return $this->hasMany('App\TaskAssign','user_id','id');
    }

    public function get_waiting_for_answer() {
        // return $this->hasMany('App\TaskAssign','user_id','id')
        //         ->join('tasks',function($q){
        //             $q->on('tasks.task_id','=','task_assign.task_id')
        //                 ->whereHas('task_comments',function($qq){
        //                     return true;
        //                 });
        //         });

        return $this->hasMany('App\TaskAssign','user_id','id')
        ->join('tasks','tasks.task_id','=','task_assign.task_id')
        ->with(['get_comments'])
        ->whereHas('get_comments',function($q) {
            $table = 'task_comments';
            $parentRelatedColumn = 'user_id';
            return $q->where($table . '.task_comments_id', function ($sub) use ($table, $parentRelatedColumn) {
                $sub->select('task_commentss.task_comments_id')
                ->from($table . ' AS t1')
                ->whereColumn('t1.' . $parentRelatedColumn, $table . '.' . $parentRelatedColumn)
                ->latest('date_added')
                ->take(1);
            });   
        });
                   
    }

    public function business_nature_child () {   
        return $this->hasOne('App\BusinessNatureChild', 'business_nature_child_id', 'business_nature_child_id');
    }

    public function theme () {
        return $this->hasOne('App\ThemeColor','theme_color_id', 'theme_color_id');
    }

    public function categories() {
        return $this->hasMany('App\TaskCategory','created_by','id');
    }

    public function get_reminders(){
        return $this->hasMany('App\ReminderMembers','user_id','id')->with(['reminder']);
    }

    public function get_timezone() {
        return $this->hasOne('App\Timezone','user_id','id');
    }

    public function timezone() {
        return $this->hasOne('App\Timezone','user_id','id');
    }

    public function user_subscriptions () {
        return $this->hasMany('App\UserSubscription', 'user_id','id');
    }

    public function subscription () {
        return $this->hasMany('App\UserSubscription', 'user_id','id');
    }

    public function all_members() {
        return $this->hasMany('App\TaskUser', 'parent_user_id','id')
                    ->select([
                        'u.*',

                        'pi.image',
                        'pi.is_profile',
                        'pi.status',
                        
                        't.timezone'
                    ])
                    ->join('users as u' , 'u.id' , '=' , 'task_users.child_user_id')
                    ->leftJoin('profile_image as pi' , 'pi.profile_image_id' , '=' , 'u.profile_image_id')
                    ->leftJoin('theme_color as tc','tc.theme_color_id', '=', 'u.theme_color_id')
                    ->leftJoin('timezone as t', 'u.id', '=', 't.user_id')
                    ->whereNotIn('u.id',[Auth::user()->id])
                    ->where("owner_accept_status" , 1 );
    }

    public function members() {
        return $this->hasMany('App\TaskUser', 'parent_user_id','id')
                    ->select([
                        'u.*',

                        'pi.image',
                        'pi.is_profile',
                        'pi.status',
                        
                        't.timezone'
                    ])
                    ->join('users as u' , 'u.id' , '=' , 'task_users.child_user_id')
                    ->leftJoin('profile_image as pi' , 'pi.profile_image_id' , '=' , 'u.profile_image_id')
                    ->leftJoin('theme_color as tc','tc.theme_color_id', '=', 'u.theme_color_id')
                    ->leftJoin('timezone as t', 'u.id', '=', 't.user_id')
                    ->whereNotIn('u.id',[Auth::user()->id])
                    ->where("owner_accept_status" , 1 );

        # OLD QUERY
        //    return $this->hasMany('App\TaskUser', 'parent_user_id','id')
        //                 ->select([
        //                     'u.*',

        //                     'pi.image',
        //                     'pi.is_profile',
        //                     'pi.status',
                            
        //                     't.timezone'
        //                 ])
        //                 ->join('users as u' , 'u.id' , '=' , 'task_users.child_user_id')
        //                 ->leftJoin('profile_image as pi' , 'pi.profile_image_id' , '=' , 'u.profile_image_id')
        //                 ->leftJoin('theme_color as tc','tc.theme_color_id', '=', 'u.theme_color_id')
        //                 ->leftJoin('timezone as t', 'u.id', '=', 't.user_id')
        //                 ->whereNotIn('u.id',[Auth::user()->id])
        //                 ->where("owner_accept_status" , 1 );
    }
    
    public function task_category () {
        $user = Auth::user();

        # MEMBER QUERY
        if($user->created_by != null) {
            $results  =  $this->hasMany('App\TaskAssign','user_id','id')
                        ->join('tasks as t','t.task_id','=','task_assign.task_id')
                        ->join('task_categories as tc','tc.category_id','=','t.category_id')
                        //->where('task_assign.user_id',$user->id)
                        ->groupBy('tc.category_id')
                        ->select(
                            'tc.category_id',
                            'tc.category_name'
                        );
        }

        # CREATOR QUERY
        else {
          $results = $this->hasMany('App\TaskCategory','created_by','id')->select(['category_id','category_name']);
        }
        return $results;
      
    }

    public function task_check_reply_thread () {
        $user = Auth::user();

        # MEMBER QUERY
        //if($user->created_by != null) {
            $results  =  $this->hasMany('App\TaskAssign','user_id','id')
                        ->join('tasks as t','t.task_id','=','task_assign.task_id')
                        ->join('task_categories as tc','tc.category_id','=','t.category_id')
                        //->where('task_assign.user_id',$user->id)
                        //->groupBy('tc.category_id')
                        ->select(
                            'tc.category_name',
                            't.*'
                        );
        //}

        # CREATOR QUERY
        //else {
        //  $results = $this->hasMany('App\TaskCategory','created_by','id')->select(['category_id','category_name']);
        //}
        return $results;
      
    }

   
    


}
