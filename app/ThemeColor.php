<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThemeColor extends Model
{
    
    protected $table = 'theme_color';
    
}
