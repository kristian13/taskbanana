<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{   
    
    protected $table = 'task_notification';
    protected $primaryKey = 'task_notification_id';
    const UPDATED_AT = null;
    function get_notif () {
         return $this->hasMany('App\Task','task_id','task_id');
    }
    
}
