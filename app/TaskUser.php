<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskUser extends Model
{
    //
    protected $table = 'task_users';
    protected $primaryKey = 'task_users_id';

    public function get_memebers() {
        return $this->belongsTo('App\User','id','child_user_id');
    } 
}
