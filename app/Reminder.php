<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reminder extends Model
{
    protected $table = 'reminders';
    protected $primaryKey = 'id';

    protected $fillable = [
        'reminder_name', 'created_by' ,'created_at', 'updated_at'
    ];

    
    function member_list() {
        return $this->hasMany('App\ReminderMembers');
    }

    


}
