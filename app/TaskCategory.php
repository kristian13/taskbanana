<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskCategory extends Model
{
    protected $table = 'task_categories';
    protected $primaryKey = 'category_id';

    const UPDATED_AT = null;

    protected $fillable = ['category_unique','category_name','status','created_by','date_created'];

    public function project_task() {
        return $this->hasMany('App\Task','category_id','category_id');
    }
    
}
