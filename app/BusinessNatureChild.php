<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessNatureChild extends Model
{

    protected $table = 'business_nature_child';
    protected $primaryKey = 'business_nature_child_id';

    public function nature_parent () {
    	return $this->belongsTo('App\BusinessNatureParent', 'business_nature_parent_id', 'business_nature_parent_id');
    }

}
