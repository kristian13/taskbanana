<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskAssign extends Model
{
    protected $table = 'task_assign';
    protected $primaryKey = 'task_assign_id';

    protected $fillable = ['task_id','user_id'];

    public function get_user () {
    	return $this->hasMany('App\User', 'id','user_id');
    }

    public function assignee_profile(){
        return $this->hasOne('App\Profile', 'user_id','user_id')->where('is_profile',1);
    }

    public function task () {
        return $this->belongsTo('App\Task','tasks.task_id','task_comments.task_id')->with(['category']);
    } 

    public function waiting_for_answer(){
        return $this->hasMany('App\Task','task_id','task_id');
    }

    public function get_comments(){
        return $this->hasMany('App\TaskComment','task_id','task_id');
    }

    
}
