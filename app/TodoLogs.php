<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TodoLogs extends Model
{
    protected $table      = 'todo_logs';
    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'todo_list_id'];
	
	const UPDATED_AT = null;
	const CREATED_AT = null;

	public function user () {
	    	$this->belongsTo('App\User', 'id', 'user_id');
	}
}
