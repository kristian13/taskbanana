<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskComment extends Model
{
    protected $table = 'task_comments';
    protected $primaryKey = 'task_comments_id';


    const CREATED_AT = 'date_added';


    public function task () {
    	return $this->belongsTo('App\Task' , 'task_id', 'task_id');
    }

    public function get_attachment () {
    	return $this->hasMany('App\TaskReplyAttachment', 'task_comments_id', 'task_comments_id');
    }

    public function user() {
    	return $this->belongsTo('App\User','user_id','id');
    }

}
