<?php

namespace App\Providers;

use Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema; //Import Schema

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191); //Solved by increasing StringLength

        DB::listen(function ($query) {
            Log::warning('warning : '.$query->sql);
            if (stripos($query->sql, "insert") !== false || stripos($query->sql, "update") !== false) {
                
                $user = Auth::user();
                //$this->log_event($user,$query);
                if(isset($user)){
                        //$this->log_event($user,$query); 
                }    
                    //}
            }
            // $query->sql
            // $query->bindings
            // $query->time
        });

        \Braintree_Configuration::environment(env('BRAINTREE_ENV'));
        \Braintree_Configuration::merchantId(env('BRAINTREE_MERCHANT_ID'));
        \Braintree_Configuration::publicKey(env('BRAINTREE_PUBLIC_KEY'));
        \Braintree_Configuration::privateKey(env('BRAINTREE_PRIVATE_KEY'));

    }

    public function log_event($user,$query) {

        $str = "";
        foreach ($query->bindings as $k=>$v) { 
            if(is_object($v)){
                $str .= ' '.$k.'='.$v->format('Y-m-d h:i:s').', '; 
            }else{
                $str .= ' '.$k.'='.$v.', '; 
            }
        }

        Log::info(
            'ID : '.$user->id.
            ' | USER : '.$user->firstname.' '.$user->lastname.
            ' | IP : '.$_SERVER['REMOTE_ADDR'].
            ' | QUERY :'. $query->sql.
            ' | DATA BINDINGS : '.$str);
        }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        Builder::macro('whereLatest', function ($table, $parentRelatedColumn) {
            return $this->where($table . '.id', function ($sub) use ($table, $parentRelatedColumn) {
                $sub->select('id')
                    ->from($table . ' AS t1')
                    ->whereColumn('t1.' . $parentRelatedColumn, $table . '.' . $parentRelatedColumn)
                    ->latest()
                    ->take(1);
            });
        });
    }
}
