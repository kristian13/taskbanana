<?php

use Symfony\Component\HttpKernel\Fragment\RoutableFragmentRenderer;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

##########LANGUAGE##############
Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    DB::table('users')
            ->where('id', Auth::user()->id  )
            ->update(['locale' => $locale ]);   
    return redirect('/profile');
});
################################


Route::auth();
//Route::middleware(['guest'])->group(function(){
	Route::get('/', 'LandingController@index');                                            # VIEW LANDING PAGE
	Route::get('/landing', 'LandingController@landing');                                   # VIEW LANDING PAGE 2ND 
	Route::post('/get_categories', 'TaskController@get_categories');                       # GET PROJECT TITLE
	Route::post('/get_task_under_categories', 'TaskController@get_task_under_categories'); # GET PROJECT TITLE
	Route::get('/logout','Auth\LoginController@logout');
	Route::get('/sign_up', 'LandingController@sign_up');

	Route::post('/resendVerificationLink' , 'LandingController@resendVerification');

	Route::get('login', 'LandingController@index')->name('login');
	Route::get('login', [
		'as' => 'login',
		'uses' => 'LandingController@index'
	]);
//});


# ROUTES FOR FORGOT PASSWORD 
Route::post('/account/sendPasswordResetEmail','TaskController@sendResetLinkEmail')->name('forgot.post');
Route::get('/account/sendPasswordResetEmail','TaskController@sendResetLinkEmailForm')->name('forgot.form');

Route::get('/account/reset/{token}','TaskController@showResetForm')->name('forgot.reset');
Route::post('/account/reset','TaskController@reset')->name('forgot.reset.post');
 

# Member

Route::middleware(['auth','isVerified'])->group(function(){
	Route::post('/add_new_member'          , 'Auth\RegisterController@add_new_member');  # ADD NEW MEMBER
	Route::post('/view_member'             , 'MemberController@view_member_business');   # VIEW TODO Task
	Route::post('/view_member_personal'    , 'MemberController@view_member_personal');   # VIEW TODO Task
	Route::get('/accept_member_lists'      , 'MemberController@accept_member_lists'); 
	Route::post('/accept_member_function'  , 'MemberController@accept_member_function'); 
	Route::post('/get_member_search'       , 'MemberController@get_member_search'); 
	Route::post('/removeMember'            , 'MemberController@removeMember'); 
	Route::post('/cancel_new_member'            , 'MemberController@cancel_new_member'); 
});


# TASK 
Route::middleware(['auth','isVerified'])->group(function(){
	Route::get('/task'                            , 'TaskController@index');  		# VIEW TASK
	Route::post('/add_task'                       , 'TaskController@add_task');    
	Route::post('/view_threads'                   , 'TaskController@view_threads'); 
	Route::post('/reply_thread'                   , 'TaskController@reply_thread'); 
	Route::get('/task_actions'                    , 'TaskController@task_actions');
	Route::post('/task_actions'                   , 'TaskController@task_actions');
	Route::post('/task/edit_task'                 , 'TaskController@edit_task'); 
	Route::post('/task/get_teams'                 , 'TaskController@get_teams'); 
	Route::post('/mark_all_as_read'               , 'TaskController@mark_all_as_read'); 
	Route::post('/read_notif'                     , 'TaskController@read_notif'); 
	Route::post('/removetask'                     , 'TaskController@removetask'); 
	Route::post('/task_complete'                  , 'TaskController@task_complete'); 
	Route::post('/check_storage'                  , 'TaskController@check_storage'); 
	Route::post('/add_project'                    , 'TaskController@add_project'); 
	Route::post('/add_new_member_on_task'         , 'TaskController@add_new_member_on_task'); 
	Route::post('/add_new_member_on_task_function', 'TaskController@add_new_member_on_task_function'); 
	Route::post('/view_more_new_task'             , 'TaskController@view_more_new_task'); 
	
	Route::get('/todo_task'                       , 'TaskController@todo_task'); 
	Route::post('/todo_task'                      , 'TaskController@todo_task'); 

	#load more task
	Route::post('/loadmore_new_task'              , 'TaskController@loadmore_new_task'); 
	Route::post('/loadmore_waiting_for_answer'    , 'TaskController@loadmore_waiting_for_answer'); 
	Route::post('/loadmore_waiting_for_me'        , 'TaskController@loadmore_waiting_for_me'); 

	#load more comment
	Route::get('/loadmore_comment', 'TaskController@loadmore_comment');
	#load more todo
	Route::post("/loadmore_todo" , 'TaskController@loadmore_todo' );
});

# Reminders
Route::middleware(['auth','isVerified'])->group(function(){
	Route::post('/reminder/add_reminder'      , 'ReminderController@add_reminder');
	Route::post('/reminder/edit_reminder'     , 'ReminderController@edit_reminder');
	Route::post('/reminder/reminder_notif'    , 'ReminderController@reminder_notif');
	Route::post('/reminder/snooze_reminder'   , 'ReminderController@snooze_reminder');
	Route::post('/reminder/reminder_remove'   , 'ReminderController@reminder_remove');
	Route::get('/reminder/reminder_info/{id}' , 'ReminderController@reminder_info');
	Route::get('/reminder/remind_me/{id}'     , 'ReminderController@remind_me');
	Route::get('/update_reminders'            , 'ReminderController@update_reminders'); 
});

# Payments
Route::middleware(['auth','isVerified'])->group(function(){
	Route::get('/payment'          ,'PaymentsController@index');
	Route::get('/payment/process'  ,'PaymentsController@process');
	Route::post('/payment/process' ,'PaymentsController@process');
	Route::get('/payment/refund'   ,'PaymentsController@refund');
	#################
	// BRAIN TREE
	Route::post('/payment', 'PaymentsController@process');
	#################

	Route::post('/payment/load_process','PaymentsController@load_process');
	Route::get('/payment/load_process' ,'PaymentsController@load_process');
	Route::get('/payment/details'      ,'PaymentsController@details');
	
	# INVOICE 
	Route::get('/payment/invoice/{id}','PaymentsController@invoice')->middleware('invoice');
});


# PROFILE PAGE
Route::middleware(['auth','isVerified'])->group(function () {
	Route::get('/profile'                  , 'ProfileController@index'); 
	Route::get('/profile/update'           , 'ProfileController@update');
	Route::get('/profile/update_password'  , 'ProfileController@update_password'); 
	Route::post('/profile/update_timezone' , 'ProfileController@update_timezone');
	Route::post('/profile/update'          , 'ProfileController@update');
	Route::post('/profile/upload_image'    , 'ProfileController@upload_image');
	Route::get('/profile/get_profile_info' , 'ProfileController@get_profile_info');
});

# Archive
Route::middleware(['auth','isVerified'])->group(function () {
	Route::get('/archive'                  , 'ArchiveController@index'); # VIEW ARCHIVE PAGE
	Route::post('/loadmoreArchive'         , 'ArchiveController@loadmoreArchive'); # VIEW ARCHIVE PAGE
	Route::post('/finishedArchive'         , 'ArchiveController@finishedArchive'); # VIEW ARCHIVE PAGE
	Route::post('/loadmoreArchiveFinished' , 'ArchiveController@loadmoreArchiveFinished'); # VIEW ARCHIVE PAGE
	Route::post('/dismissedArchive'        , 'ArchiveController@dismissedArchive'); # VIEW ARCHIVE PAGE
	Route::post('/loadmoreArchiveDismissed', 'ArchiveController@loadmoreArchiveDismissed'); # VIEW ARCHIVE PAGE
	Route::post('/unarchive'               , 'ArchiveController@unarchive'); # VIEW ARCHIVE PAGE
});

# Calendar
Route::middleware(['auth','isVerified'])->group(function () {
	Route::post('/viewcalendar', 'CalendarController@index'); # VIEW CALENDAR
});



//TO BE EDIT
Route::get('calendar', function () {
    return view('calendar');
});
Route::get('member', function () {
    return view('member');
});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return redirect('/');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
