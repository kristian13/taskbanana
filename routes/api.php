<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/test', 'Api\LoginApiController@index'); # VIEW LANDING PAGE
Route::get('/task', 'Api\TaskApiController@index'); # VIEW LANDING PAGE

Route::post('/login'             , 'Api\LoginApiController@login'); # Login api
Route::post('/forgot'            , 'Api\LoginApiController@reset'); # Register api
Route::post('/register'          , 'Api\LoginApiController@create'); # Register api
Route::get('/timezone_list'      ,'Api\ProfileApiController@timezone_list');
Route::get('/get_business_nature', 'Api\LoginApiController@get_business_nature'); # get_business_nature

 
// REQUEST METHODS
// GET    : SHOW
// POST   : CREATE request form-data
// PUT    : UPDATE request form-data
// DELETE : DELETE request form-data

Route::group(['middleware' => 'auth:api' ], function () {

	# TODOS
	Route::post('/todo'             		,'Api\TaskApiController@todo_store'); #Insert
	Route::get('/todo'              		,'Api\TaskApiController@todo_show');  
	Route::get('/todo/{todo_id}'    		,'Api\TaskApiController@todo_show');
	Route::post('/todo/{todo_id}'   		,'Api\TaskApiController@todo_update');
	Route::delete('/todo/{todo_id}' 		,'Api\TaskApiController@todo_destroy');
	Route::post('/todo/{todo_id}/mark_as'   ,'Api\TaskApiController@todo_mark');

	# PROFILE
    Route::get('/profile'                      ,'Api\ProfileApiController@show');
	Route::post('/profile/profile_update'      ,'Api\ProfileApiController@profile_update');
	Route::post('/profile/profile_email'       ,'Api\ProfileApiController@profile_email');
	Route::post('/profile/profile_theme_color' ,'Api\ProfileApiController@profile_theme_color');
	Route::post('/profile/profile_password'    ,'Api\ProfileApiController@profile_password');
	Route::post('/profile/upload_image'        ,'Api\ProfileApiController@upload_image');
	Route::post('/profile/use_image/{id}'      ,'Api\ProfileApiController@use_image');
	Route::post('/profile/remove_image/{id}'   ,'Api\ProfileApiController@remove_image');
	Route::post('/profile/update_timezone'     ,'Api\ProfileApiController@update_timezone');
	Route::get('/profile/timezone_list'        ,'Api\ProfileApiController@timezone_list');

	# Projects
	Route::get ('/projects'                 ,'Api\ProjectApiController@get_categories');
	Route::post('/project'                  ,'Api\ProjectApiController@add_project');
	Route::post('/project/{category_id}'    ,'Api\ProjectApiController@update_category');
	Route::get ('/project/{category_id}'    ,'Api\ProjectApiController@get_all_under_category');

	# TASKS
	Route::get ('/tasks' 		   		   , 'Api\TaskApiController@get_tasks');
	Route::post('/task'  		   		   , 'Api\TaskApiController@add_task');
	Route::get ('/task/{task_id}'  		   , 'Api\TaskApiController@get_single_task');
	Route::post('/task/{task_id}' 		   , 'Api\TaskApiController@edit_task');
	Route::post('/task/{task_id}/image'    , 'Api\TaskApiController@remove_file');
	Route::post('/task/{task_id}/status'   , 'Api\TaskApiController@remove_task');
	Route::post('/task/{task_id}/reply'    , 'Api\TaskApiController@reply_thread');

	# TASK COMMENTS
	ROUTE::get('/task/{task_id}/comment'   , 'Api\TaskApiController@loadmore_comment');
	

	# new revision query
	Route::get('/tasks/get_initial_task'       , 'Api\TaskApiController@get_initial_task');
	Route::get('/tasks/get_new_task'           , 'Api\TaskApiController@get_new_task');
	Route::get('/tasks/get_waiting_for_answer' , 'Api\TaskApiController@get_waiting_for_answer');
	Route::get('/tasks/get_waiting_for_me'     , 'Api\TaskApiController@get_waiting_for_me');

	
	# PAYMENTS
	Route::get('/payment'       ,'Api\PaymentApiController@load_process');
	Route::get('/payment/{id}'  ,'Api\PaymentApiController@invoice');
	
	# MEMBERS
	Route::post('/member'                , 'Api\MemberApiController@add_new_member');
	Route::get('/member'                 , 'Api\MemberApiController@get_my_business_team');
	Route::post('/member/{id}'           , 'Api\MemberApiController@get_my_business_team');
	Route::get('/member_pendings'        , 'Api\MemberApiController@pending_accept_member_lists');
	Route::post('/member_pendings/{id}'  , 'Api\MemberApiController@accept_member_function');


	# REMINDERS
	Route::get('/reminders'               , 'Api\ReminderApiController@get_reminders');
	Route::get('/reminder/{reminder_id}'  , 'Api\ReminderApiController@get_reminders');
	Route::post('/reminder'               , 'Api\ReminderApiController@create_reminder');


	# Archive
	Route::get('/archives'           ,'Api\ArchiveApiController@get_archive');
	Route::get('/archive/{task_id}'  ,'Api\ArchiveApiController@get_single');
	Route::post('/archive/{task_id}' ,'Api\ArchiveApiController@restore_archive');


});


