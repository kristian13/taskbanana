@extends('theme.verification')
@section('content')
<div class="text-center">
   <h2>Please verify link on your Email...</h2> 
   Go back to <a href="{{URL::to('/logout')}}">login</a> page
</div>
@endsection
