
<!DOCTYPE html>
<html>
<head>
  <title><?php echo config('define.title_name'); ?></title>
  <meta name="csrf-token" content="{{ csrf_token() }}" id="site_name" data-site_name="{{ url('/') }}" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/font.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/landing.css?version=2') }}">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<div class="topnav" id="myTopnav">
  <!-- <div class="nav_overlay"> -->
    <div class="nav_container">
      <div class="login-container">
       <a href="#home" class="active">
        <img class="site_logo" src="{{ asset('images/icons/landing-navlogo.png') }}">
<!--           <img class="logo" src="{{ asset('images/icons/logo.png') }}"> -->
       </a>

       <!-- LOGGED IN -->
      <!--      

        <img src="https://media.licdn.com/dms/image/C5603AQF_1LXsl8RQXQ/profile-displayphoto-shrink_200_200/0?e=1551916800&v=beta&t=ZfSC8AL91CoK-AQimu_rcnpz3BU7buB3n4r8GVCZ_OM" style="width: 30px; height: 30px; border-radius: 50px; position: relative; left: -3px; top: 1px; float: right;">
              <a href="task/?act=task" class="btn-signup signup-form-btn" >DASHBOARD</a>  -->

       <!-- CLOSED -->
        @if (!$current_user)
          <a href="javascript:void(0);" class="btn-signup signup-form-btn free-sign-up" data-toggle="modal" data-target="#sign_up_modal" >FREE SIGN UP</a> 
          <a href="javascript:void(0);" class="btn-login login-form-btn" data-toggle="modal" data-target="#login_modal">LOGIN</a>
        <!-- <a href="#" class="btn-signup signup-form-btn btn-pricing" >PRICING</a>  -->
        @else
          <a href="task" class="btn-login login-form-btn">Go to Dashboard</a>
        @endif
     </div>
                      
      <a href="javascript:void(0);" class="icon" onclick="myFunction()">
        <i class="fa fa-bars"></i>
      </a>

      <div class="welcome_div">
        <p class="welcome_title">Welcome to</p> 
        <img src="{{ asset('images/icons/landing-logo.png') }}">
        <p class="welcome_desc">
        Are your tasks making you go bananas? Time to manage your tasks with TaskBanana. Get your work done with our easy to use user interface. Collaborate with your team, organise and categorise your work.
        </p>  
        <button class="btn-try-it-now" data-toggle="modal" data-target="#sign_up_modal">Try it now</button><br>
        <a class="btn_already_urtasked" href="javascript:void(0);" data-toggle="modal" data-target="#login_modal">Already used TaskBanana ?  Login.</a>
      </div>
        <br clear="all">
    </div>
  <!-- </div> -->
</div>
<body>

{{-- Laravel Comment

<!-- Modal FOR LOGIN -->
<!-- <div id="myModal" class="modal fade" role="dialog" style="z-index: 1000000">
  <div class="modal-dialog modal-md">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
         
      </div>
      <div class="text-center">
        <img src="{{ asset('images/icons/landing-navlogo.png') }}" alt="Avatar" class="avatar">
      </div>
      <div class="modal-body">
           <form class="form-horizontal" method="POST" action="{{ route('login') }}">
          {{ csrf_field() }}
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-12 control-label">E-Mail Address</label>
                        <div class="col-md-12">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
            </div>
            <div class="col-lg-12">
              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-12 control-label">Password</label>
                        <div class="col-md-12">
                            <input id="password" type="password" class="form-control" name="password" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                   <div class="col-lg-12">
                    <div class="pull-right">
                       <button type="submit" class="btn btn-primary">
                          Login
                      </button>
                    </div>
                   </div>
            </div>
          </div>
          
        </div>
        </form>
    </div>
  </div>
</div> -->
<!-- Modal for Signup -->
<!-- <div id="signup_modal" class="modal fade" role="dialog" style="z-index: 1000000">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
         
      </div>
      <div class="text-center">
        <img src="{{ asset('images/icons/landing-navlogo.png') }}" alt="Avatar" class="avatar">
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="POST" action="{{ route('register') }}">
          {{ csrf_field() }} 
          <div class="row">
            <span class="first_step col-lg-12 col-md-12 col-xs-12">
              <div class="col-lg-12 col-md-12 col-xs-12">
              <div class="form-group">
                <label class="control-label">First name</label>
                <input type="text" name="firstname" class="form-control" placeholder="Enter your Firstname" required>
              </div>
            </div>
            <div class="col-lg-12 col-md-12 col-xs-12">
              <div class="form-group">
                <label class="control-label">Last name</label>
                <input type="text" name="lastname" class="form-control" placeholder="Enter your Lastname" required>
              </div>
            </div>
            <div class="col-lg-12 col-md-12 col-xs-12">
              <div class="form-group">
                <label class="control-label">Email</label>
                <input type="email" name="email" class="form-control" placeholder="Enter your Email" required>
              </div>
            </div>
            <div class="col-lg-12 col-md-12 col-xs-12">
              <div class="form-group">
                <label class="control-label">Password</label>
                <input type="password" name="password" class="form-control" placeholder="Enter your  Password" required>
              </div>
            </div>
            <div class="col-lg-12 col-md-12 col-xs-12">
              <div class="form-group">
                <label class="control-label">Choose Default TimeZone</label>
                <select class="form-control" name="timezone">
                  @foreach($timezones as $timezone=>$value)
                  <option value="{{$timezone}}">{{$value}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            </span>

            <span class="second_step col-lg-12 col-md-12 col-xs-12">
            <div class="col-lg-12 col-md-12 col-xs-12" >
                  <div class="form-group">
                    <label class="control-label">Business Nature</label>
                    <select name="business_nature_child_id" class="form-control " style="color: #5e5e5e;" required>
                      <option value=""  selected>Select Business Nature</option>
                      @foreach($business_nature as $business_nature => $value )
                        <optgroup  label="{{ $business_nature }}">
                          @foreach($value as $row )
                            <option value="{{ $row->business_nature_child_id }}">{{ $row->business_nature_child_name }}</option>
                          @endforeach
                        </optgroup>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label class="control-label">Business Name</label>
                    <input type="text" name="business_name" class="form-control" placeholder="Enter Business Name" required> 
                  </div>
            </div>
        </span>
        </div>
            <div class="col-lg-12 col-md-12 col-xs-12">
               <button class="login-modal-btn btn btn-submit" type="submit">Submit</button>  
            </div>
      </div>
    </div>
  </div>
</div> -->

--}}

<!-- Modal Login-->
<div class="modal fade taskbanana" id="login_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" id="login_form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&#9587;</span>
                    </button>
                    <h5 class="modal-title" id=""><img class="site_logo" src="{{ asset('images/icons/landing-navlogo.png') }}"></h5>
                </div>
                <div class="modal-body">
                <div id="validation-errors"></div> 
                    <div class="form-group">
                        <img class="email_icon" src="{{ asset('images/icons/login/user.png') }}">
                        <input id="email" type="email" name="email" class="form-control clear-register" placeholder="Email Address"  required style="padding: 0.375rem 1.30rem;">
                        <span class="invalid-feedback">Please input valid email.</span>     
                    </div>
  
                    <div class="form-group">
                        <img class="password_icon" src="{{ asset('images/icons/login/locked.png') }}">
                        <input id="password" type="password" name="password" class="form-control clear-register" placeholder="Password" required  style="padding: 0.375rem 1.30rem;">
                        <img class="eye_open_icon hidden" onclick="showPassword()" src="{{ asset('images/icons/login/eye-open.png') }}">
                        <img class="eye_closed_icon" onclick="showPassword()" src="{{ asset('images/icons/login/eye-closed.png') }}">
                        <span class="invalid-feedback">Please input correct password.</span>       
                    </div>
      
                    <div class="form-group forgot_pass_remember">
                      <a href="account/sendPasswordResetEmail">
                        <label class="forgot_pass">Forgot password?</label>
                      </a>

                    <div class="pull-right">          
                      <input type="checkbox" class="check_box input_class_checkbox" value="lsRememberMe" id="rememberMe">                 
                      <label class="remember_me">Remember me</label>
					</div>

						<div class="hide resendLink">
							<br/>
							<br/>
							No email yet ? <a href="#" class="resendVerification">Check here </a>
							<span class="hide">loading...</span>
						</div>
                    </div>
                </div>

                <div class="modal-footer row"> 
                    <button type="submit" onclick="lsRememberMe()" class="col-md-8 btn btn-save login-btn">Login <i class="fa fa-refresh fa-spin fa-1x fa-fw hide arinfo"></i></button>
                    <p class="col-md-12 donthaveaccnt">Dont have an account? <a class="donthaveaccnt_signup" href="javascript:void(0);">Sign Up Now</a></p>  
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal Login -->


<!-- Modal Sign Up-->
<div class="modal fade taskbanana" id="sign_up_modal" tabindex="-1" role="dialog" data-controls-modal="your_div_id" data-backdrop="static" data-keyboard="false" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}

               
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&#9587;</span>
                    </button>
                    <h5 class="modal-title"><img class="site_logo" width="240px" src="{{ asset('images/icons/login/signup_title_logo.png') }}"></h5>
                </div>
                <div class="modal-body">
                    <div id="validation-errors-signup"></div>
                    <div class="form-group">
                        <!-- <label>First Name*</label> -->
                        <input type="text" id="firstName" name="firstname" class="form-control" placeholder="Enter your First Name" autocomplete="false" required>
                        <span class="invalid-feedback">Please input your first name.</span>    
                    </div>
                    <div class="form-group">
                        <!-- <label>Last Name*</label> -->
                        <input type="text" id="lastName" name="lastname" class="form-control" placeholder="Enter your Last Name" autocomplete="false" required>
                        <span class="invalid-feedback">Please input your last  name.</span>    
                    </div>
                    <div class="form-group">
                        <!-- <label>Email*</label> -->
                        <input type="email" id="email_signup" name="email" class="form-control" placeholder="Enter your Email" autocomplete="false" required>
                        <span class="invalid-feedback">Please input valid email.</span>  
                    </div>
                    <div class="form-group">
                        <!-- <label>Password*</label> -->
                        <input type="password" id="password_signup" name="password" class="form-control" placeholder="Enter your Password" autocomplete="false" required>
                        <span class="invalid-feedback">Please input password.</span>  
                        <img class="eye_open_icon hidden" onclick="showPassword()" src="{{ asset('images/icons/login/eye-open.png') }}">
                        <img class="eye_closed_icon" onclick="showPassword()" src="{{ asset('images/icons/login/eye-closed.png') }}">
                    </div>
                    {{--
                    <div class="form-group">
                        <!-- <label>Confirm Password</label> -->
                        <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" placeholder="Enter Confirm Password" autocomplete="false" required>
                        <span class="invalid-feedback">Please input confirm password.</span>       
                    </div>
                    --}}

                    <div class="form-group">
                        <!-- <label>Choose Default Timezone*</label> -->
                        <select class="form-control" name="timezone" id="timezone" required>
                            <option value="" selected disabled>Select Timezone</option>
                            @foreach($timezones as $timezone=>$value)
                                <option value="{{$timezone}}">{{$value}}</option>
                            @endforeach
                        </select>
                        <span class="invalid-feedback">Please select timezone.</span>       
                    </div>
                    <div class="form-group">
                        <!-- <label>Business Nature*</label> -->
                        <select class="form-control" id="business_nature" name="business_nature_child_id" required>
                            <option value="" selected disabled>Select Business Nature</option>
                            @if(!empty($business_nature))
                            @foreach($business_nature as $business_natures => $value)
                                <optgroup label="{{$business_natures}}">
                                    @foreach($value as $row)
                                        <option value="{{$row->business_nature_child_id}}">{{$row->business_nature_child_name}}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                           @endif
                        </select>
                    <span class="invalid-feedback">Please select business nature.</span>       
                    </div>
                    <div class="form-group">
                        <!-- <label>Business Name*</label> -->
                        <input type="text" id="business_name" name="business_name" class="form-control" placeholder="Enter Business Name" autocomplete="false" required>
                        <span class="invalid-feedback">Please select business name.</span>       
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-cancel btn-border-radius-50" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-save register-btn btn-border-radius-50">Register <i class="fa fa-refresh fa-spin fa-1x fa-fw hide arinfo"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal Sign Up -->

</form>

<div class="full_div">
  <div class="container">
    <div class="row div_container">
      <div class="col-md-6">
        <img src="{{ asset('images/icons/landing_img2.png') }}">
      </div>
      <div class="col-md-6 center_div">
        <div class="center_vertically">
          <h1 class="center_row_title">Simple Design</h1>
          <h1 class="center_row_sub_title">TaskBanana Card List</h1>
          <p class="center_row_desc">TaskBanana is very easy to use with its simple design visualizing everything in a single view helps you to understand the entire workload and make decisions to improve efficiency.</p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="full_div bgThird">
  <div class="container">
  <div class="row div_container">
    <div class="col-md-6 center_div">
      <div class="center_vertically txt-left">
        <h1 class="center_row_title">It's Super Easy to</h1>
        <h1 class="center_row_sub_title">Create Checklists</h1>
        <p class="center_row_desc">Creating a checklist without overwhelming your team. It is very simple to do that can make you better at your job and save you time.</p>
      </div>
    </div>
    <div class="col-md-6">
        <img src="{{ asset('images/icons/landing_img3.png') }}">
    </div>
  </div>
  </div>
</div>


<div class="container">
  <div class="row div_container">
    <div class="col-md-6">
        <img width="100%" src="{{ asset('images/icons/landing_img4.png') }}">
    </div>
    <div class="col-md-6 center_div">
      <div class="center_vertically">
        <h1 class="center_row_title">Elegant Design</h1>
        <h1 class="center_row_sub_title">User Interface</h1>
        <p class="center_row_desc">Visualizing everything in an elegant way to manage your project, team, and task at one place. All wrapped up in a beautiful design.</p>
      </div>
    </div>
  </div>
  <br clear="all">
  <br clear="all">
</div>
<!-- 
<div class="full_div gray_bg">
  <div class="container">
    <div class="row div_container">
      <div class="col-md-6 center_div">
        <div class="center_vertically txt-left">
          <h1 class="center_row_title">Sign Up Now!</h1>
          <h1 class="center_row_sub_title">Free 50 MB</h1>
          <p class="center_row_desc">Our free plan includes:
            <ul class="txt-left" style="color: #5e5e5e; padding: 0px; margin-left: 20px;">
              <li>50 mb Free Storage</li>
              <li>Unlimited Users</li>
              <li>Free All Features</li>
              <li>Free Support</li>
            </ul>
          </p>
          <button class="signup-form-btn signup-try-it" data-toggle="modal" data-target="#signup_modal">Give it a try. Sign up now!</button>
        </div>
      </div>
      <div class="col-md-6">
          <img width="100%" src="{{ asset('images/icons/landing_img4.png') }}">
      </div>
    </div>
  </div>
  <br clear="all">
  <br clear="all">
</div> -->

<!-- PRICES -->
<div class="price-section bgFourth">
  <?php 
  $prices = DB::table('subscription')->select('*')->where('subscription_id' ,'!=' , 1)->get();

  ?>
  <h1 class="center_row_title text-center">Pricing</h1>
  <div class="container box pricing_sub_conntainer">
      @foreach($prices as $data)
        <div class="item clearfix">
            <div class="">
                <h5 class="amount"><span>$</span> {{ number_format($data->price, 2) }}</h5>
                <h5 class="mb">{{ $data->subscription_name }}  </h5>
                @if( Auth::check() )
                <button class="subscription_landing_btn" data-amount="{{number_format($data->price, 2)}}" data-subscription_id="{{ $data->subscription_id }}">Buy</button>   
                @else
                <button  data-toggle="modal" data-target="#myModal">Buy</button>
                @endif
            </div>
        </div>
      @endforeach
  </div>
  <div class="container pricing_container col-md-6" style="display: none;">
    <div class="row">
      <div class="container">
         <div class="row">
            <div class="col-md-8 col-md-offset-2" style="margin: 0 auto;">
              <div id="dropin-container"></div>
              <div style="text-align: center;">
                <input type="hidden" value="" id="subscription_id_hidden">
                <button id="submit-button" >Request Payment Method</button>
              </div>
            </div>
         </div>
      </div>
    </div>
    <form accept-charset="UTF-8" action="{{ url('/payment/process') }}" class="require-validation"
              data-cc-on-file="false"
              data-stripe-publishable-key="pk_test_q0ONViGb9E4uScjp5UJ3nZyg009lhUlnPE"
              id="payment-form" method="post">
              <input type="hidden" name="subscription" id="subscription">
              <input type="hidden" name="amount" id="amount-">
              {{ csrf_field() }}
              <div class='form-row'>
                  <div class='col-xs-12 form-group required'>
                      <label class='control-label'>Name on Card</label> <input
                          class='form-control' size='4' type='text'>
                  </div>
              </div>
              <div class='form-row'>
                  <div class='col-xs-12 form-group card required'>
                      <label class='control-label'>Card Number</label> <input
                          autocomplete='off' class='form-control card-number' size='20'
                          type='text'>
                  </div>
              </div>
              <div class='form-row'>
                  <div class='col-xs-4 form-group cvc required'>
                      <label class='control-label'>CVC</label> <input autocomplete='off'
                          class='form-control card-cvc' placeholder='ex. 311' size='4'
                          type='text'>
                  </div>
                  <div class='col-xs-4 form-group expiration required'>
                      <label class='control-label'>Expiration</label> <input
                          class='form-control card-expiry-month' placeholder='MM' size='2'
                          type='text'>
                  </div>
                  <div class='col-xs-4 form-group expiration required'>
                      <label class='control-label'> </label> <input
                          class='form-control card-expiry-year' placeholder='YYYY' size='4'
                          type='text'>
                  </div>
              </div>
              <div class='form-row'>
                  <div class='col-md-12'>
                      <div class='form-control total btn btn-info'>
                          Total: <span class='amount amount-'>$300</span>
                      </div>
                  </div>
              </div>
              <div class='form-row'>
                  <div class='col-md-12 form-group'>
                      <button class='form-control btn btn-primary submit-button'
                          type='submit' style="margin-top: 10px;">Pay »</button>
                  </div>
              </div>
              <div class='form-row'>
                  <div class='col-md-12 error form-group hide'>
                      <div class='alert-danger alert'>Please correct the errors and try
                          again.</div>
                  </div>
              </div>
          </form>
  </div> 
  <h1 class="center_or">or</h1>
  <div class="item clearfix" style="margin: 0 auto; width: 400px;">
          <h1 class="center_row_title">Sign Up Now!</h1>
          <h1 class="center_row_sub_title">Free 50 MB</h1>
          <p class="center_row_desc">Our free plan includes:
            <ul class="txt-left" style="color: #5e5e5e; padding: 0px; margin-left: 33%;">
              <li>50 mb Free Storage</li>
              <li>Unlimited Members</li>
              <li>Free All Features</li>
              <li>Free Support</li>
              </ul>
            </p>
            <button class="signup-form-btn signup-try-it signup-form-btn" data-toggle="modal" data-target="#sign_up_modal">Give it a try. Sign up now!</button>
    </div>

</div>


<div class="full_div bgFifth">
  <div class="container">
    <div class="row div_container">
      <div class="col-md-12 center_div">
        <h1 class="center_row_title txt-center feature-list-title">Features</h1>
        <br>
        <div class="row txt-left">
          <div class="col-md-6">
              <div class="row feature-container">
                <span class="col-md-2">
                    <span><img src="{{ asset('images/icons/icon-calendar.png') }}" style="width: 45px;"></span>   
                </span>
                <span class="col-md-10">
                <p class="feature-title">Calendar</p>
                <p class="feature-desc">
                Control your day, week and month! <br> Manage your schedule by viewing your planned tasks for a month. 
                </p>
              </span>
              </div>
          </div>
          <div class="col-md-6">
              <div class="row feature-container">
                <span class="col-md-2">
                    <span><img src="{{ asset('images/icons/icon-unlimited-users.png') }}" style="width: 45px;"></span>   
                </span>
                <span class="col-md-10">
                <p class="feature-title">Unlimited Members</p>
                <p class="feature-desc">
                Add as many members you have in your team! <br>
                Your member can also add their team members.
                </p>
              </span>
              </div>
          </div>
          <div class="col-md-6">
              <div class="row feature-container">
              <span class="col-md-2">
                    <span><img src="{{ asset('images/icons/icon-todo-list.png') }}" style="width: 45px;"></span>   
                </span>
                <span class="col-md-10">
                <p class="feature-title">To do list</p>
                <p class="feature-desc">
                Create, edit and get your to do’s done! <br> 
                List your to-do lists easily and get the most out of your day!
                </p>
              </span>
              </div>
          </div>
          <div class="col-md-6">
                <div class="row feature-container">
                <span class="col-md-2">
                    <span><img src="{{ asset('images/icons/icon-card-notif.png') }}" style="width: 45px;"></span>   
                </span>
                <span class="col-md-10">
                <p class="feature-title">Notifications</p>
                <p class="feature-desc">
                Be notified when your members added their team member. <br>
                All your notifications are curated in one place!
                </p>
              </span>
              </div>
          </div>

          <!-- <div class="col-md-6">
                <div class="row feature-container">
                 <span class="col-md-2">
                    <span><img src="{{ asset('images/icons/icon-organization.png') }}" style="width: 45px;"></span>   
                </span>
                <span class="col-md-10">
                <p class="feature-title">Business / Organization</p>
                <p class="feature-desc">
                  Organize and categorize your work
                </p>
              </span>
              </div>
          </div> -->
        </div>

      </div>
    </div>
  </div>
  <br><br>
</div>
<!-- Footer -->
<div class="full_div gray_bg">
    <div class="col-md-12 txt-center copyright-container">
        <p class="copyright-txt">Copyright 2019, Trackerteer Web Development Corporation. All rights reserved. </p>
  </div>
</div>  
</body>
</html>
<script type="text/javascript">
  // Method Scroll for "div"
  $(".btn-pricing").click(function() {
    $('html,body').animate({
        scrollTop: $(".card").offset().top},
        'slow');
  });
  // Method Show Business Type
  $(document).ready(function(){
    if($('input[name="optradio"]').is(':checked')){
      $('#if_business').show();
    } else {
      $('#if_business').hide();
    }
    $(document).on('click', '.signup-next', function(){
      $(this).hide();
      $('.signup-prev').show();
      $('.first_step').hide();
      $('.second_step').fadeIn();
      $('.btn-submit').show();
    });

    $(document).on('click', '.signup-prev', function(){
      $(this).hide();
      $('.signup-next').show();
      $('.second_step').hide();
      $('.first_step').fadeIn();

      // Hide Button Submit
      $('.btn-submit').hide();
    });
  });

  $(document).on('change' , '.type' , function () {
    var type = $(this).val();
    if (type == 1) {
      $('#if_business').show();
    } else {
      $('#if_business').hide();
    }
  });
</script>
{{--
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://js.braintreegateway.com/web/dropin/1.8.1/js/dropin.min.js"></script>
<script>
    //2223000048400011
  $(document).on('click' , '.subscription_landing_btn' , function () {
    $('#subscription_id_hidden').val($(this).data('subscription_id'));
    $('.pricing_container').css('opacity', '0');
    setTimeout(function() {
      $('.pricing_container').show().css('opacity', '1');
    }, 200);
  });
  var button = document.querySelector('#submit-button');
  braintree.dropin.create({
    authorization: "{{ Braintree_ClientToken::generate() }}",
    container: '#dropin-container'
  }, function (createErr, instance) {
      button.addEventListener('click', function (e) {
        instance.requestPaymentMethod(function (err, payload) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
              url : "payment" ,
              method :"POST" , 
              data : {payload : payload , token: $('meta[name="csrf-token"]').attr('content') , subscription_id  : $('#subscription_id_hidden').val() } ,
              success : function (response) {
                console.log(response)
              }
            });
        });
      });
      4242424242424242
    });
</script> -->
--}}
<script src='https://js.stripe.com/v2/' type='text/javascript'></script>
<script type="text/javascript" src="{{ asset('js/landing_validation.js') }}"></script>
