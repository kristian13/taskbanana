@extends('theme.main')
@section('content')
    </div>
    <!-- End Main Header -->
    <div class="main-container payment-page">
        <div class="profile-head">
            <div class="max-container flex">
                <div class="user-info flex middle">
                   <!--  <div class="user-image">
                        <a href="javascript:void(0);">
                            <img src="{{ asset('images/sample/avatar.png') }}" class="avatar" alt="Avatar">
                            <span>Update</span>
                        </a>
                    </div> -->
                    <div class="user-details flex col center">
                        <p class="full-name">{{ $User->firstname }} {{ $User->lastname }}</p>
                        <p class="email-add">{{ $User->email }}</p>
                    </div>
                </div>
                <div class="usage-info flex col">
                    <p class="usage-label text-center" >Usages</p>
                    <div class="progress-bar">
                        <div class="percentage"></div>
                    </div>
                    <p class="usage-mb"><span></span> MB of <span></span> MB used</p>
                    <!-- <a href="javascript:void(0);" class="upgrade">Upgrade Space</a> -->
                </div>
            </div>
        </div>
        <div class="filter-section profile-page">
            <div class="filter-body flex space-bet">
                <div class="filter-buttons max-container">
                    <a href="javascript:void(0);" class="active"><p>Pricing</p></a>
                    <a href="{{url('/task')}}"><p>Back to Home</p></a>
                </div>
            </div>
        </div>
        <?php 
        $prices = DB::table('subscription')->select('*')->where('subscription_id' ,'!=' , 1)->get();
        ?>

        <div class="max-container payment-page text-center">
            <img class="logo" src="{{ asset('images/icons/landing-logo.png') }}">
            <h3 class="text-label">TRUSTED BY TEAMS AROUND THE WORLD</h3>
            <br clear="all">

            <div class="pricing-section flex middle center">
                @foreach($prices as $data)
                    <div class="price-item">
                        <h3 class="price flex top center"><span>$</span>{{ number_format($data->price, 2) }}</h3>
                        <p class="byte">{{ $data->subscription_name }} </p>
                         @if( Auth::check() )
                            <button class="subscription_landing_btn" data-subscription_id="{{ $data->subscription_id }}">Buy</button>   
                        @else
                            <button  data-toggle="modal" data-target="#myModal">Buy</button>
                        @endif
                    </div>
                @endforeach
            </div>
            <div class="container pricing_container" style="display: none">
                <div class="row">
                    <div class="container">
                        <div class="col-md-8 col-md-offset-2" >
                            <div id="dropin-container"></div>                              
                            <div style="text-align: center;">
                                <input type="hidden" value="" id="subscription_id_hidden">
                                <button id="submit-button"  class="btn btn-info">Pay</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://js.braintreegateway.com/web/dropin/1.8.1/js/dropin.min.js"></script>
<script>
    //2223000048400011
  $(document).on('click' , '.subscription_landing_btn' , function () {
    $('#subscription_id_hidden').val($(this).data('subscription_id'));
    $('.pricing_container').css('opacity', '0');
    setTimeout(function() {
      $('.pricing_container').show().css('opacity', '1');
    }, 200);
  });
  var button = document.querySelector('#submit-button');
  braintree.dropin.create({
    authorization: "{{ Braintree_ClientToken::generate() }}",
    container: '#dropin-container'
  }, function (createErr, instance) {
      button.addEventListener('click', function (e) {
        instance.requestPaymentMethod(function (err, payload) {
            if (!err) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                  url : "payment" ,
                  method :"POST" , 
                  data : {payload : payload , token: $('meta[name="csrf-token"]').attr('content') , subscription_id  : $('#subscription_id_hidden').val() } ,
                  success : function (response) {
                  }
                });
            }

        });
      });
    });
</script>
@endsection
