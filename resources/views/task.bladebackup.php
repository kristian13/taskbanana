@extends('theme.main')
@section('content')
	<div class="filter-section">
		<div class="filter-header flex space-bet">
			<div class="left">
				<button class="filter-btn blue-green">
					<img src="{{ asset('images/icons/filter.png') }}">
					<span>Filter by</span><i class="fa fa-caret-down" aria-hidden="true"></i>
				</button>
			</div>
			<div class="right flex">
				<a class="blue btn-inset" id="add_new_member" href="javascript:void(0)" onclick="add_new_member( {{Auth::user()->parent_business_owner_id}} , {{Auth::user()->task_creator}} )">
					<img src="{{ asset('images/icons/view-member.png') }}">
					<span>Add Member</span>
				</a>
				<a class="dark-grey btn-inset" id="view_member" href="{{ url('member') }}">
					<img src="{{ asset('images/icons/view-member.png') }}">
					<span>View Member</span>
				</a>
				<a class="green btn-inset" id="view_calendar" href="{{ url('calendar') }}">
					<img src="{{ asset('images/icons/Calendar.png') }}">
					<span>View Calendar</span>
				</a>
				<!-- <button class="green btn-inset" id="view_calendar" type="button" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
					<img src="{{ asset('images/icons/Calendar.png') }}">
					<span>View Calendar</span>
				</button> -->
				<button class="blue btn-inset" id="add_task" data-toggle="modal" data-target="#add_task_modal">
					<img src="{{ asset('images/icons/add-task.png') }}">
					<span>Add Task</span>
				</button>
			</div>
		</div>
		<div class="filter-body">
			<form>
				<div class="filter-buttons">
					<!-- <button class="active">Member</button> -->
					<a href="javascript:void(0);" class="active"><p>Way Past Dealine</p></a>
					<a href="javascript:void(0);"><p>My Assigned Tasks</p></a>
					<a href="javascript:void(0);"><p>Task Assigned to Me</p></a>
					<a href="javascript:void(0);"><p>Reset / Clear</p></a>
				</div>
				<div class="filter-by">
					<div class="filter-member">
						<div class="member-list">
							<div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="1" class="styled" type="checkbox" checked>
			                    <label for="1">All Members</label>
			                </div>
			                <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="2" class="styled" type="checkbox">
			                    <label for="2"> Nadia</label>
			                </div>
			                <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="3" class="styled" type="checkbox">
			                    <label for="3"> Marj Trackerteer</label>
			                </div>
			                <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="4" class="styled" type="checkbox">
			                    <label for="4"> Ian</label>
			                </div>
			                <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="5" class="styled" type="checkbox">
			                    <label for="5"> Jo Krissie</label>
			                </div>
			                <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="6" class="styled" type="checkbox">
			                    <label for="6"> Tolani</label>
			                </div>
			                <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="7" class="styled" type="checkbox">
			                    <label for="7"> Leanne</label>
			                </div>
			                <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="8" class="styled" type="checkbox">
			                    <label for="8"> Brian</label>
			                </div>
						</div>
						<button type="submit" class="search-assignee blue-green">Filter</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	</div>
	<!-- End Main Header -->
	<div class="main-container flex space-bet">
		<!-- Task Section -->
		<div class="task-container scrollbar">
			<div class="list-wrapper" id="list-cards">
				<div class="list-item pending-box" id="new_task">
					<div class="list-container pink">
						<div class="list-header"><h2>New Task</h2></div>
						<div class="list-body scrollbar">
							@if (@isset($task['N']))
								@foreach($task['N'] as $key => $value)
									<div class="list-card" id="div{{$value->task_id}}">
											<div class="list-tools">
												<span class="remove-task" data-task_name="sfasf" data-task_id="2" title="Delete Task" title="Delete Task"><img src="{{ asset('images/icons/card-delete.png') }}"></span>	
												<span class="edit-task" onclick="edit_task({{$value->task_id}});" title="Edit Task"><img src="{{ asset('images/icons/card-edit.png') }}"></span>
											</div>
											<div class="list-card-photo">
												@foreach($value->attachment as $file)
													<img src="{{ URL::to('/') }}/task_attachment/{{$file->image}}" height="80px;">
												@endforeach
											</div>
											<div class="list-info">
												<h3 class="title">{{ $value->title }}
													<p>(Created by: {{ $value->firstname }} {{ $value->lastname }} )</p>
												</h3>
												<div class="deadline">
													<img src="{{ asset('images/icons/card-date.png') }}">
													<span class="badge-text">Deadline: {{ $value->deadline }}</span>
												</div>
												<div class="description rmore">
													{{ $value->description }}
												</div>
												<br clear="all">

												<div class="card-members">
													@foreach($value->assignee as $key => $r)
														<span class="chip unseen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Unread">{{ $r->firstname }} {{ $r->lastname }}</span>
													@endforeach
												</div>
												<!-- DONT SHOW IF THE USER CREATED THE TASK -->
												@if(Auth::user()->id != $value->task_creator )
												<div class="form-group">
													<div class="textbox form-group">
														<div class="show-thread-btn text-right">
															<a href="javascript:void(0);" onclick="view_threads( {{$value->task_id}} , {{$value->task_creator}} , 'N' );" class="view_threads" data-card="working" data-task_id="{{$value->task_id}}" data-task_creator="{{$value->task_creator}}" data-task_status="N"><p class="link-inset">Show Threads</p></a>
														</div>
													</div>
												</div>
												@endif
											</div>
										</div>
								@endforeach
							@endif
						</div>
					</div>
				</div>
				<div class="list-item progress-box" id="waiting_for_answer">  
					<div class="list-container pink">
						<div class="list-header"><h2>Waiting for Answer</h2></div>
						<div class="list-body scrollbar waiting_div" >
							@if (@isset($task['I']))
								@foreach($task['I'] as $key => $value)
									<div class="list-card" id="div{{$value->task_id}}">
										<div class="list-tools">
											<span class="remove-task" data-task_name="sfasf" data-task_id="2" title="Delete Task" title="Delete Task"><img src="{{ asset('images/icons/card-delete.png') }}"></span>	
											<span class="edit-task" onclick="edit_task({{$value->task_id}});" title="Edit Task"><img src="{{ asset('images/icons/card-edit.png') }}"></span>
										</div>
										<div class="list-card-photo">
											@foreach($value->attachment as $file)
												<img src="{{ URL::to('/') }}/task_attachment/{{$file->image}}" height="80px;">
											@endforeach
										</div>
										<div class="list-info">
											<h3 class="title">{{ $value->title }}
												<p>(Created by: {{ $value->firstname }} {{ $value->lastname }} )</p>
											</h3>
											<div class="deadline">
												<img src="{{ asset('images/icons/card-date.png') }}">
												<span class="badge-text">Deadline: {{ $value->deadline }}</span>
											</div>
											<div class="description rmore">
												{{ $value->description }}
											</div>
											<br clear="all">
											<div class="latest-comment">
	                                            <p class="label-comment">Recent Comment:</p>
	                                            <p class="last-comment-name"> (  {{$value->last_comment->firstname}} {{$value->last_comment->lastname}} )</p>
	                                            <p class="last-comment-date"> <?php echo date("d-m-Y H:i:s" , strtotime($value->last_comment->date_added)); ?></p>
	                                            <p class="last-comment-comment">{{$value->last_comment->comment}}</p>
	                                        </div>
											<br clear="all">
											<div class="card-members">
												@foreach($value->assignee as $key => $r)
													@if($r->comment != null)
													<span class="chip seen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Read">{{ $r->firstname }} {{ $r->lastname }}</span>
													@else
													<span class="chip unseen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Unread">{{ $r->firstname }} {{ $r->lastname }}</span>
													@endif
												@endforeach
											</div>
											<div class="form-group">
												<div class="textbox form-group">
													<div class="show-thread-btn text-right">
														<a href="javascript:void(0);" onclick="view_threads( {{$value->task_id}} , {{$value->task_creator}} , 'I' );" class="view_threads" data-card="working" data-task_id="{{$value->task_id}}" data-task_creator="{{$value->task_creator}}" data-task_status="I"><p class="link-inset">Show Threads</p></a>
													</div>
												</div>
											</div>
										</div>
									</div>
								@endforeach
							@endif
						</div>
					</div>
				</div>
				<div class="list-item check-box" id="waiting_for_me">
					<div class="list-container pink">
						<div class="list-header"><h2>Waiting for Me</h2></div>
						<div class="list-body scrollbar waiting_for_me_div">
							@if (@isset($task['C']))
								@foreach($task['C'] as $key => $value)
									<div class="list-card" id="div{{$value->task_id}}" >
											<div class="list-tools">
												<span class="remove-task" data-task_name="" data-task_id="2" title="Delete Task" title="Delete Task"><img src="{{ asset('images/icons/card-delete.png') }}"></span>	
												<span class="edit-task" onclick="edit_task({{$value->task_id}});" title="Edit Task"><img src="{{ asset('images/icons/card-edit.png') }}"></span>
											</div>
											<div class="list-card-photo">
												@foreach($value->attachment as $file)
													<img src="{{ URL::to('/') }}/task_attachment/{{$file->image}}" height="80px;">
												@endforeach
											</div>
											<div class="list-info">
												<h3 class="title">{{ $value->title }}
													<p>(Created by: {{ $value->firstname }} {{ $value->lastname }} )</p>
												</h3>
												<div class="deadline">
													<img src="{{ asset('images/icons/card-date.png') }}">
													<span class="badge-text">Deadline: {{ $value->deadline }}</span>
												</div>
												<div class="description rmore">
													{{ $value->description }}
												</div>
												<br clear="all">
												<div class="card-members">
													@foreach($value->assignee as $key => $r)
														@if($r->comment != null )
														<span class="chip seen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Read">{{ $r->firstname }} {{ $r->lastname }}</span>
														@else
														<span class="chip unseen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Unread">{{ $r->firstname }} {{ $r->lastname }}</span>
													@endif
												@endforeach
												</div>
												<div class="form-group">
													<div class="textbox form-group">
														<div class="show-thread-btn text-right">
															<a href="javascript:void(0);" onclick="view_threads( {{$value->task_id}} , {{$value->task_creator}} , 'C' );" class="view_threads" data-card="working" data-task_id="{{$value->task_id}}" data-task_creator="{{$value->task_creator}}" data-task_status="C"><p class="link-inset">Show Threads</p></a>
														</div>
													</div>
												</div>
											</div>
										</div>
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Task Section -->

		<!-- To Do List Section -->
		<div class="to-do">
			<div class="container-name">
				<h3 class="flex center middle"><img class="blue task-icon" src="images/icons/blue-todo-icon.png"><img class="pink task-icon" src="images/icons/pink-todo-icon.png"> My List Task</h3>
			</div>
			<div class="checklist">
				<div class="checklist-header" style="">
					<div class="add-item">
						<div class="top flex">
							<input type="text" class="form-control" id="to_do_name" placeholder="Add to do">
							<input type="text" class="form-control datepicker hasDatepicker" id="todo_date" placeholder="Date">
							<button type="submit" class="blue btn-inset" id="to_do_add_btn">Add</button>
						</div>
						<div class="bottom flex middle">
							<div class="reminder flex">
								<button type="button" id="new_reminder" class="flex middle center add-reminder btn-inset" data-toggle="modal" data-target="#reminder_modal"><img src="{{ asset('images/icons/clock-white.png') }}"><p>Set Reminder</p></button>
								<!-- <span class="flex middle center"><img src="{{ asset('images/icons/clock-white.png') }}"><p>Set Reminder</p></span>
								<button class="delete-reminder">&#9587;</button>
								<button class="test-notif" style="margin-left: 10px;">Test Notif</button> -->
							</div>
							<!-- <div class="checkbox checkbox-gray checkbox-xs">
			                    <input class="styled" type="checkbox" name="reminder" id="reminder">
			                    <label for="reminder">Set Reminder</label>
			                </div>
							<select name="remind_me_every" class="form-control" id="remind_me_every">
								<option value="1">Daily</option>
								<option value="2">Weekly</option>
								<option value="3">Monthly</option>
							</select> -->
						</div>
					</div>
					<div class="checklist-progress">
						<span class="percentage-count">50%</span>
						<div class="checklist-progress-bar">
							<div class="current-percentage" style="width: 50%;"></div>
						</div>
						<a href="javascript:void(0);" class="hide-completed pull-right current"><img src="{{ asset('images/icons/todo-hide.png') }}">Hide completed items</a>
						<a href="javascript:void(0);" class="show-completed hide pull-right"><img src="{{ asset('images/icons/todo-show.png') }}">Show checked items <span class="checked-items">(1)</span></a>
						<br clear="all">
						<p class="checklist-completed-text hide quiet js-completed-message">Everything in this checklist is complete!</p>
					</div>
				</div>
				<div class="checklist-body">
					<div class="checklist-list" id="todo_div">
						<div class="checkbox checkbox-gray2 checkbox-sm">
		                    <input id="10" class="styled task_finished" type="checkbox" data-todo_id="10" value="10" name="user_id[]" checked="">
		                    <label for="10"></label>
		                    <p>1st Task Sample</p>
		                    <span>12/28/2018</span>
		                    <div class="todo-tools">
			                    <a href="javascript:void(0);"><img class="blue edit" src="images/icons/blue-todo-edit.png"></a>
			                    <a href="javascript:void(0);"><img class="blue delete" src="images/icons/blue-todo-delete.png"></a>
			                    <a href="javascript:void(0);"><img class="pink edit" src="images/icons/pink-todo-edit.png"></a>
			                    <a href="javascript:void(0);"><img class="pink delete" src="images/icons/pink-todo-delete.png"></a>
		                    </div>
		                </div>
		                <div class="checkbox checkbox-gray2 checkbox-sm">
		                    <input id="11" class="styled task_finished" type="checkbox" data-todo_id="11" value="11" name="user_id[]" checked="">
		                    <label for="11"></label>
		                    <p>2nd Task Sample</p>
		                    <span>12/28/2018</span>
		                    <div class="todo-tools">
			                    <a href="javascript:void(0);"><img class="blue edit" src="images/icons/blue-todo-edit.png"></a>
			                    <a href="javascript:void(0);"><img class="blue delete" src="images/icons/blue-todo-delete.png"></a>
			                    <a href="javascript:void(0);"><img class="pink edit" src="images/icons/pink-todo-edit.png"></a>
			                    <a href="javascript:void(0);"><img class="pink delete" src="images/icons/pink-todo-delete.png"></a>
		                    </div>
		                </div>
		                <div class="checkbox checkbox-gray2 checkbox-sm">
		                    <input id="12" class="styled task_finished" type="checkbox" data-todo_id="12" value="12" name="user_id[]" checked="">
		                    <label for="12"></label>
		                    <p>3rd Task Sample</p>
		                    <span>12/28/2018</span>
		                    <div class="todo-tools">
			                    <a href="javascript:void(0);"><img class="blue edit" src="images/icons/blue-todo-edit.png"></a>
			                    <a href="javascript:void(0);"><img class="blue delete" src="images/icons/blue-todo-delete.png"></a>
			                    <a href="javascript:void(0);"><img class="pink edit" src="images/icons/pink-todo-edit.png"></a>
			                    <a href="javascript:void(0);"><img class="pink delete" src="images/icons/pink-todo-delete.png"></a>
		                    </div>
		                </div>
		                <div class="checkbox checkbox-gray2 checkbox-sm">
		                    <input id="13" class="styled task_finished" type="checkbox" data-todo_id="13" value="13" name="user_id[]" checked="">
		                    <label for="13"></label>
		                    <p>4th Task Sample</p>
		                    <span>12/28/2018</span>
		                    <div class="todo-tools">
			                    <a href="javascript:void(0);"><img class="blue edit" src="images/icons/blue-todo-edit.png"></a>
			                    <a href="javascript:void(0);"><img class="blue delete" src="images/icons/blue-todo-delete.png"></a>
			                    <a href="javascript:void(0);"><img class="pink edit" src="images/icons/pink-todo-edit.png"></a>
			                    <a href="javascript:void(0);"><img class="pink delete" src="images/icons/pink-todo-delete.png"></a>
		                    </div>
		                </div>
		                <div class="checkbox checkbox-gray2 checkbox-sm">
		                    <input id="14" class="styled task_finished" type="checkbox" data-todo_id="14" value="14" name="user_id[]" checked="">
		                    <label for="14"></label>
		                    <p>5th Task Sample</p>
		                    <span>12/28/2018</span>
		                    <div class="todo-tools">
			                    <a href="javascript:void(0);"><img class="blue edit" src="images/icons/blue-todo-edit.png"></a>
			                    <a href="javascript:void(0);"><img class="blue delete" src="images/icons/blue-todo-delete.png"></a>
			                    <a href="javascript:void(0);"><img class="pink edit" src="images/icons/pink-todo-edit.png"></a>
			                    <a href="javascript:void(0);"><img class="pink delete" src="images/icons/pink-todo-delete.png"></a>
		                    </div>
		                </div>
		                <div class="checkbox checkbox-gray2 checkbox-sm">
		                    <input id="15" class="styled task_finished" type="checkbox" data-todo_id="15" value="15" name="user_id[]" checked="">
		                    <label for="15"></label>
		                    <p>6th Task Sample</p>
		                    <span>12/28/2018</span>
		                    <div class="todo-tools">
			                    <a href="javascript:void(0);"><img class="blue edit" src="images/icons/blue-todo-edit.png"></a>
			                    <a href="javascript:void(0);"><img class="blue delete" src="images/icons/blue-todo-delete.png"></a>
			                    <a href="javascript:void(0);"><img class="pink edit" src="images/icons/pink-todo-edit.png"></a>
			                    <a href="javascript:void(0);"><img class="pink delete" src="images/icons/pink-todo-delete.png"></a>
		                    </div>
		                </div>
		                <div class="checkbox checkbox-gray2 checkbox-sm">
		                    <input id="16" class="styled task_finished" type="checkbox" data-todo_id="16" value="16" name="user_id[]" checked="">
		                    <label for="16"></label>
		                    <p>7th Task Sample</p>
		                    <span>12/28/2018</span>
		                    <div class="todo-tools">
			                    <a href="javascript:void(0);"><img class="blue edit" src="images/icons/blue-todo-edit.png"></a>
			                    <a href="javascript:void(0);"><img class="blue delete" src="images/icons/blue-todo-delete.png"></a>
			                    <a href="javascript:void(0);"><img class="pink edit" src="images/icons/pink-todo-edit.png"></a>
			                    <a href="javascript:void(0);"><img class="pink delete" src="images/icons/pink-todo-delete.png"></a>
		                    </div>
		                </div>
					</div>
				</div>
			</div>
		</div>
		<!-- End To Do List Section -->
	</div>
@endsection