<!DOCTYPE html>
<html>
<head>
    <title><?php echo config('define.title_name'); ?></title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font.css') }}">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/sign_up.css?version=2') }}">
</head>
<body class="pink-background">
    <div class="border-box">
        <img class="logo" src="{{ asset('images/final/sign-up-logo.png') }}">
        <form class="form-horizontal" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }} 
            <div class="first-step">
                <div class="line-input left-icon">
                    <label>First Name</label>
                    <input type="text" name="firstname" required>
                </div>
                <div class="line-input left-icon">
                    <label>Last name</label>
                    <input type="text" name="lastname" required>
                </div>
                <div class="line-input left-icon">
                    <label>Email</label>
                    <input type="email" name="email" required>
                </div>
                <div class="line-input left-icon">
                    <label>Password</label>
                    <input type="password" name="password" required>
                </div>
            </div>
            <div class="second-step">
                <div class="form-group">
                    <label class="control-label">Business Nature</label>
                    <select name="business_nature_child_id" class="form-control " style="color: #5e5e5e;" required>
                        <option value=""  selected>Select Business Nature</option>
                        @foreach($business_nature as $business_nature => $value )
                        <optgroup  label="{{ $business_nature }}">
                            @foreach($value as $row )
                            <option value="{{ $row->business_nature_child_id }}">{{ $row->business_nature_child_name }}</option>
                            @endforeach
                        </optgroup>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label">Business Name</label>
                    <input type="text" name="business_name" class="form-control" placeholder="Enter Business Name" required> 
                </div>
            </div>

            <button class="login-modal-btn btn btn-submit" type="submit">Sign Up</button>  
        </form>
        <div class="footer">
            Powered by Trackerteer
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('js/jquery-2.2.4.min.js?version=2') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/sign_up.js') }}"></script>
</body>

