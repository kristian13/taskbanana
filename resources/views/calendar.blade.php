@extends('theme.main')
@section('content')
	<div class="filter-section">
		<div class="filter-header flex space-bet">
			<div class="left">
				<button class="filter-btn blue-green">
					<img src="{{ asset('images/icons/filter.png') }}">
					<span>Filter by</span><i class="fa fa-caret-down" aria-hidden="true"></i>
				</button>
			</div>
			<div class="right flex">
				<a class="dark-grey btn-inset" id="view_member" href="{{ url('member') }}">
					<img src="{{ asset('images/icons/view-member.png') }}">
					<span>View Member</span>
				</a>
				<a class="green btn-inset" id="view_calendar" href="{{ url('task') }}">
					<img src="{{ asset('images/icons/Calendar.png') }}">
					<span>View Task</span>
				</a>
				<!-- <button class="green btn-inset" id="view_calendar" type="button" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
					<img src="{{ asset('images/icons/Calendar.png') }}">
					<span>View Calendar</span>
				</button> -->
				<button class="blue btn-inset" id="add_task" data-toggle="modal" data-target="#add_task_modal">
					<img src="{{ asset('images/icons/add-task.png') }}">
					<span>Add Task</span>
				</button>
			</div>
		</div>
		<div class="filter-body">
			<form>
				<div class="filter-buttons">
					<!-- <button class="active">Member</button> -->
					<a href="javascript:void(0);" class="active"><p>Way Past Dealine</p></a>
					<a href="javascript:void(0);"><p>My Assigned Tasks</p></a>
					<a href="javascript:void(0);"><p>Task Assigned to Me</p></a>
					<a href="javascript:void(0);"><p>Reset / Clear</p></a>
				</div>
				<div class="filter-by">
					<div class="filter-member">
						<div class="member-list">
							<div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="1" class="styled" type="checkbox" checked>
			                    <label for="1">All Members</label>
			                </div>
			                <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="2" class="styled" type="checkbox">
			                    <label for="2"> Nadia</label>
			                </div>
			                <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="3" class="styled" type="checkbox">
			                    <label for="3"> Marj Trackerteer</label>
			                </div>
			                <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="4" class="styled" type="checkbox">
			                    <label for="4"> Ian</label>
			                </div>
			                <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="5" class="styled" type="checkbox">
			                    <label for="5"> Jo Krissie</label>
			                </div>
			                <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="6" class="styled" type="checkbox">
			                    <label for="6"> Tolani</label>
			                </div>
			                <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="7" class="styled" type="checkbox">
			                    <label for="7"> Leanne</label>
			                </div>
			                <div class="checkbox checkbox-gray checkbox-xs">
			                    <input id="8" class="styled" type="checkbox">
			                    <label for="8"> Brian</label>
			                </div>
						</div>
						<button type="submit" class="search-assignee blue-green">Filter</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<p class="text-center">Calendar Page</p>
@endsection