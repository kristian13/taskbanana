<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<style>
    .page-break {
        page-break-after: always;
    }
</style>
<body>

    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-4">
                <img src="images/icons/landing-logo.png" height="70" width="200"><br>
                [Street Address]<br>
                [City, ST ZIP]<br>
                [Phone]
            </div>
        </div>
    </div>
    <br>
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-3 pull-right">
                <table class="table" style="width:300px; padding:5px;">
                    <thead class="thead-inverse">
                        <tr style="background-color:#B8B8B8;">
                            <th>INVOICE #</th>
                            <th>DATE</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="text-align:center;">{{$invoice->user_subscription_id}}</td>
                            <td>{{date("d / m / Y")}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        
        
        <div class="col-lg-3">
            <table class="table" style="width:300px; padding:5px;">
                <thead class="thead-inverse">
                    <tr style="background-color:#B8B8B8;">
                        <th>BILL TO</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            Name : {{strtoupper($user->firstname)}} {{strtoupper($user->lastname)}} <br>
                            Address : {{$user->address}} <br>
                            City,ST ZIP : {{$user->city}}, {{$user->state}} {{$user->zipcode}} <br>
                            Phone : {{$user->phone}} <br>
                            Email : {{$user->email}} 
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        </div>
    </div>
    <br>
    <div class="col-lg-12">
        <table class="table" style=" padding:5px;">
            <thead class="thead-inverse">
                <tr style="background-color:#B8B8B8;">
                    <th>DESCRIPTION</th>
                    <th>DATE</th>
                    <th>AMOUNT</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$invoice->subscription->subscription_name}} </td>
                    <td>{{date("m / d / Y",strtotime($invoice->date_created))}}</td>
                    <td>${{$invoice->amount}}</td>
                </tr>
                <tr>
                    <td><i><h2></h2></i></td>
                    <td><h3></h3></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>

</body>
</html>