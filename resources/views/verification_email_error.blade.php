@extends('theme.verification')
@section('content')
<div class="text-center">
   <h2>Verification Link Expired...</h2>
   <br>
   <span class="alert alert-info">login again to send verification on your email</span>
   <br>
   <br>
   Go back to <a href="{{URL::to('/logout')}}">login</a> page
</div>
@endsection
