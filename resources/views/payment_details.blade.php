@extends('theme.main')
@section('content')
    </div>
    <!-- End Main Header -->
    <div class="main-container payment-page">
        <div class="profile-head">
            <div class="max-container flex">
                <div class="user-info flex middle">
                   <!--  <div class="user-image">
                        <a href="javascript:void(0);">
                            <img src="{{ asset('images/sample/avatar.png') }}" class="avatar" alt="Avatar">
                            <span>Update</span>
                        </a>
                    </div> -->
                    <div class="user-details flex col center">
                        <p class="full-name">{{ $User->firstname }} {{ $User->lastname }}</p>
                        <p class="email-add">{{ $User->email }}</p>
                    </div>
                </div>
                <div class="usage-info flex col">
                    <p class="usage-label text-center" >Usages</p>
                    <div class="progress-bar">
                        <div class="percentage"></div>
                    </div>
                    <p class="usage-mb"><span></span> MB of <span></span> MB used</p>
                    <!-- <a href="javascript:void(0);" class="upgrade">Upgrade Space</a> -->
                </div>
            </div>
        </div>
        <div class="filter-section profile-page">
            <div class="filter-body flex space-bet">
                <div class="filter-buttons max-container">
                    <a href="javascript:void(0);" class="active"><p>Pricing</p></a>
                    <a href="{{url('/task')}}"><p>Back to Home</p></a>
                </div>
            </div>
        </div>
      

        <div class="max-container payment-page">
            <h3 class="text-label">PAYMENT HISTORY</h3>
            <br clear="all">

            <table id="payment_details" class="table">
                <thead>
                    <tr>
                        <!-- <th>ID</th> -->
                        <th>Description</th>
                        <th>Date</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <!-- <th>ID</th> -->
                        <th>Description</th>
                        <th>Date</th>
                        <th>Amount</th>
                    </tr>
                </tfoot>
             </table>
        </div>
    </div>
</div>
@endsection


