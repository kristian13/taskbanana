{{-- Click here to verify your account: <a href="{{ $link = route('email-verification.check', $user->verification_token) . '?email=' . urlencode($user->email) }}">{{ $link }}</a> --}}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <body style="min-width: 450px; max-width: 750px; margin: 0 auto;">
    <div class="bodyClass" style="background:#fff;">
      <div class="header" style="background:#FFE9DE;height:80px;text-align:center;">
        <img src="https://trackerteer.com/taskbanana/images/icons/landing-logo.png" style="width:160px; margin-top: 20px;" alt="logo rockysuperdog">
      </div>
      <div class="mid" style="background:#FFFFFF;position:relative;padding-left:5%;padding-right:5%;padding-top:5%;padding-bottom:2%;">
      <p style="font-family:Arial;color:#696969;top:15px;position:relative;margin-bottom:35px;text-align:center;">Hey {{$user->firstname}},
        </p>

        {{-- Click here to verify your account: <a href="{{ $link = route('email-verification.check', $user->verification_token) . '?email=' . urlencode($user->email) }}">{{ $link }}</a> --}}
        
        <div class="content-container">
          <p style="font-family:Arial;color:#696969;top:15px;position:relative;margin-bottom:35px;font-size:14px;text-align:center;">
            HOoray! Thanks for registering for an account with TaskBanana!
          </p>
          
          <p style="font-family:Arial;color:#696969;top:15px;position:relative;margin-bottom:35px;font-size:14px;text-align:center;">
            Please click on the link below to verify your email address and complete your registration.
          </p>
          
          <div style="text-align:center;padding-top:30px;">
            <a href="{{ $link = route('email-verification.check', $user->verification_token) . '?email=' . urlencode($user->email) }}" style="background:#55C2F8;color:#fff !important;border-radius:50px;text-decoration:none;font-family:Arial;padding:10px 50px !important;">
                {{-- {{ $link }} --}}
                Verify your email
            </a>
            
			@if($user->static_password)
				<p style="font-family:Arial;color:#696969;top:15px;position:relative;font-size:14px;text-align:center;">
					Your default username : <b>{{$user->email}}</b>
				</p>
				<br>
            	<p style="font-family:Arial;color:#696969;top:15px;position:relative;margin-bottom:35px;font-size:14px;text-align:center;">
				   Your default password : <b>{{$user->static_password}}</b>
            	</p>
            @endif
            
            <p style="font-family:Arial;color:#696969;top:15px;position:relative;margin-bottom:35px;font-size:14px;text-align:center;">
              We're here to help you if you need it. Contact us and let us know your concerns. <br>
              <br>
              Team team at TaskBanana
            </p>
            
          </div>
          
        </div>
        
        
      </div>
      <div style="background:#dedede;">
        <p style="width:100%;line-height:1.4;font-family:'ebrima bold', arial;font-size:13px;text-align:center;position:relative;margin-bottom:35px;">
          <br>By joining <b>TaskBanana,</b> you've agreed to our <b> Terms of Use</b> and <b>Privacy Policy</b> <br>
          <br>This email has been sent to you as part of your TaskBanana subscription. <br>
          Please do not reply to this emnail, as we are unable to respond from this email address. <br>
          If you need help or would like to contact us <a href="#" target="_blank"><b>click here</b></a>. <br>
          <br>
          This message was mailed to <a href="#" target="_blank"><b>subscriber@gmail.com</b></a> by TaskBanana. <br>
          The use of the TaskBanana service and website is subject to our <a href="#" target="_blank"><b>Terms of Use and Privacy Policy.</b></a>
        </p>
        <div style="text-align:center;">
          <img src="https://trackerteer.com/taskbanana/images/icons/landing-logo.png" style="width:160px; margin-top: 20px; margin-bottom: 10px;" alt="logo rockysuperdog">
          <p style="font-family:Arial;color:#696969;position:relative;padding-bottom:25px;font-size:14px;text-align:center;"> © 2020 Trackerteer Web Developer Corp. </p>
        </div>
      </div>
    </div>
  </body>
</html>

