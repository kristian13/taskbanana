<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" id="site_name" data-site_name="{{ url('/') }}" />
        <title><?php echo config('define.title_name'); ?></title>
        <!-- Fonts -->
		<link rel="stylesheet" type="text/css" href="{{ asset('css/font.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">
		<!-- Jquery UI -->
		<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-ui.css') }}">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/datatable.min.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/new_layout.css?version=2') }}">
		<!--[if !IE]><!-->
		<link rel="stylesheet" type="text/css" href="{{ asset('css/unsupported_new.css?version=1') }}">
		<!--<![endif]-->
	    <!--[if IE]>
		<link rel="stylesheet" type="text/css" href="{{ asset('css/unsupported_old.css?version=1') }}">
		<![endif]-->
		<link rel="icon" href="" type="image/x-icon">
		<!-- full calendar css -->
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.css">
    </head>
    <!-- GET THE COLOR THEME OF THE USER -->

	    @yield('content')
		
		<footer>
			<p class="footer">&copy; <?php echo date("Y"); ?> Copyright. Powered by Trackerteer | All Rights Reserved.</p>
		</footer>		  	
        <!-- Latest compiled and minified -->
		<script type="text/javascript" src="{{ asset('js/jquery-2.2.4.min.js?version=2') }}"></script>
		<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/datatable.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/jquery-ui.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/lottie.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/task.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/todo.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/payment_details.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/profile.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/js-notify.min.js') }}"></script>
		<!-- full calendar js -->
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone-with-data.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.4/jstz.min.js"></script>
		<script type="text/javascript">
			var tz = jstz.determine(); // Determines the time zone of the browser client
    		var timezone = tz.name(); //
    		$('#timezone').val(timezone);
		</script>
    </body>
</html>
